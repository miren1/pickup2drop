@extends('layouts.app')

@section('content')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
  <!--begin::Subheader-->
  <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
    <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
      <!--begin::Info-->
      <div class="d-flex align-items-center flex-wrap mr-2">
        <!--begin::Page Title-->
        <i class="fa fa-city text-danger" aria-hidden="true"></i>
        <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Add City</h5>
        <!--end::Page Title-->
        <!--begin::Actions-->
        <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

        <!--end::Actions-->
      </div>
      <!--end::Info-->
    </div>
  </div>
  <!--end::Subheader-->
  <!--begin::Entry-->
  <div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container">
      <!--end::Notice-->
      <!--begin::Card-->
      <div class="card card-custom">
        {{-- <div class="card-header">
          <div class="card-title justify-content-between flex-right align-items-right">
            <span class="card-icon">
              <i class="fa fa-hashtag text-danger" aria-hidden="true"></i>
            </span>
            <h3 class="card-label">Areacode</h3>

          </div>
        </div> --}}
        <div class="card-body" style="overflow-x: auto;">

          <div class="container">
            <form id="editForm" action="{{route('city.store')}}" method="POST" class="form-horizontal">
             @csrf

                           @if(isset($city))
                           <input type="hidden" name="id" value="{{ $city->id }}">
                           @endif
                           <div class="row">


                            <div class="col-md-12">

                              <div class="form-group">
                                <label>City Name</label>
                                <input type="text" id="city_name"  name="city_name" class="form-control" placeholder="Enter City Name" required
                                data-bv-notempty="true" onblur="trimval('city')"
                                data-bv-notempty-message="City name is required"
                                @if(isset($city)) value="{{ $city->city_name }}" @endif>

                              </div>

                              <div class="form-group">
                                <label>State</label>
                                <input type="text" id="state_name"  name="state" class="form-control" placeholder="Enter Your State" required
                                data-bv-notempty="true" onblur="trimval('state')"
                                data-bv-notempty-message="State is required"
                                @if(isset($city)) value="{{ $city->state }}" @endif>

                              </div>







                            </div>

                            <div class="col-md-12">
                              <button type="submit" id="btn_save" class="btn btn-danger btn-lg">Save</button>
                              {{-- <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" href="{{ route('city.index') }}">Back</a>    --}}
                              <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" onclick="window.history.go(-1); return false;">Back</a>   
                            </div>
                            
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--end::Card-->
                </div>
                <!--end::Container-->
              </div>
              <!--end::Entry-->
            </div>
            <!-- Content Wrapper. Contains page content -->
            @endsection

            @push('js')
            <script>
        //create and store category
        $(function() {
          $('#editForm').bootstrapValidator();
        });

        function trimval(type){
          if(type == 'city')
               $("#city_name").val($.trim($("#city_name").val()))
          else
               $("#state_name").val($.trim($("#state_name").val()))
        }

      </script>
      @endpush
