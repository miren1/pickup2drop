@extends('layouts.app')

@section('content')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <i class="fa fa-city text-danger" aria-hidden="true"></i>
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;City List</h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

                <a href="{{ route('city.create') }}" class="btn btn-light-danger font-weight-bolder btn-sm"
                data-keyboard="false" data-backdrop="static">Add New City</a>
                <!--end::Actions-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--end::Notice-->
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-body" style="overflow-x: auto;">
                    <!--begin: Datatable-->
                    <table class="table table-hover" id="myTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>City Name</th>
                                <th>State</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!-- Content Wrapper. Contains page content -->
@endsection

@push('js')
<script>

   $(document).ready(function() {
        $table = $("#myTable").DataTable({
            "ajax": "{{ url('all-city') }}",
            "processing": true,
            "serverSide": true,
            "ordering": false,
            'stateSave' : true,
            "columns": [
            {
                data: "id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1
                }
            },
            { "data": "city_name" },
            { "data": "state" },
            { "data": "id",
            render:function(data,type,row,meta){
                var id =row.id;
                $html = '<div class="dropdown mo-mb-2">'+
                                  '<a href="#" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                                      'Action'+
                                  '</a>'+

                                  '<div class="dropdown-menu" x-placement="bottom-start" style="cursor: pointer;">'+
                                      
                                      '<a class="dropdown-item " href="city/'+id+'/edit"><i class="fa fa-edit text-primary"></i>&nbsp; Edit</a>'+ ' ';
                                      
                                     $html+= '<a class="dropdown-item btn_delete" data-url="city/delete" data-id="'+row.id+'"><i class="fa fa-trash text-danger"></i>&nbsp; Delete</a>' ;
                                    

                                  '</div>'+
                              '</div>';
                      return $html;
            }
        },
        ]
    });
    } );



   $(document).on("click", '.btn_delete', function(event) { 
        var id =$(this).attr('data-id');
        swal({
            title: "Are you sure?",
            text: "You Want To Delete This City,You Will Not Be Able To Recover This City.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {

                $.ajax({
                    url: 'city/'+id,
                    type: 'DELETE',
                    dataType: 'json',
                    "data":{"_token":"{{csrf_token()}}"},
                success:function($data){
                    if($data == "unauthorized_access"){
                        swal("Unauthorized access.", {
                            icon: "error",
                        });
                    }else if($data == true)
                    {
                        swal("City has been Deleted.", {
                            icon: "success",
                        });
                        $('#myTable').DataTable().ajax.reload();
                    }else{
                        swal("Error while deleting city.", {
                            icon: "error",
                        });
                    }
                }
            });


            } else {
                swal("Your City is Safe.");
            }
        });
    });

</script>
@endpush
