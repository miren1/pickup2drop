@extends('layouts.app')

@section('content')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <i class="fa fa-percentage text-danger" aria-hidden="true"></i>
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Order Commission</h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <!--end::Actions-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--end::Notice-->
            <!--begin::Card-->
            <div class="card card-custom">

                {{-- <div class="card-header">
                    <div class="card-title">
                        <a class="btn btn-light-danger font-weight-bolder btn-sm" data-keyboard="false" data-backdrop="static">Add Commission</a> 
                    </div>
                </div>  --}}


                <div class="card-body" style="overflow-x: auto;">
                    <div class="container">
                        <form id="editForm" action="{{route('commission.store')}}" method="POST" class="form-horizontal">
                            @csrf 
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Percentage of Commission</label>
                                        <input type="number"  min="1"  pattern="[0-9.]+"  id="commission"  name="commission" class="form-control @error('commission') is-invalid @enderror" placeholder="Enter Percentage of Commission" required
                                        data-bv-notempty="true"
                                        data-bv-notempty-message="Percentage of Commission is required" 
                                        @if(App\Models\Commission::first()  && App\Models\Commission::first()->commission) value="{{App\Models\Commission::first()->commission}}" @endif
                                        >
                                        @error('commission')
                                        <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div> 
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" id="btn_save" class="btn btn-danger btn-lg">Save & Update</button>
                                {{-- <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" href="{{ url()->previous() }}">Back</a>     --}}
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script>
    $(function() {
        $('#editForm').bootstrapValidator();
    });
</script>
@endpush
