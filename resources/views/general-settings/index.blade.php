@extends('layouts.app')
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center flex-wrap mr-2">
                    <i class="fa fa-subway text-danger" aria-hidden="true"></i>
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;General settings</h5>
                    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                </div>
            </div>
        </div>
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="card card-custom">
                    <div class="card-body" style="overflow-x: auto;">
                        <div class="row">
                            @can('GST-Setting')
                                <div class="col-6 form-group">
                                    <h3>GST Settings</h3>
                                    <label>SGST</label>
                                    <input type="text" class="form-control" name="sgst" id="gs_sgst" onkeypress="return isNumberKey(this,event)">
                                    <label>CGST</label>
                                    <input type="text" class="form-control" name="cgst" id="gs_cgst">
                                    <br/>
                                    <a href="javascript:void(0)" class="btn btn-success" id="btnsavegst">Update GST</a>
                                </div>
                            @endcan
                            @can('Radius-Setting')
                                <div class="col-6 form-group">
                                    <h3>Area Settings</h3>
                                    <label>Radius Area (In KM)</label>
                                    <input type="text" class="form-control"  onkeypress="return isNumberKey(this,event)" name="covered_area" id="gs_covered_area">
                                    <br/>
                                    <a href="javascript:void(0)" class="btn btn-success" id="btnsavecoveredarea">Update Radius</a>
                                </div>
                            @endcan
                        </div>
                    </div>
                </div>
                <div class="card card-custom mt-5">
                      <div class="card-body" style="overflow-x: auto;">
                          <div class="col-6 form-group mt-5">
                                <h3>Application Settings</h3>
                                    @csrf
                                    <div class="col-md-12 row mt-5">
                                        <div class="col-md-7">
                                            <label><strong>Maintainance Mode for Customer : </strong> </label>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="button r my-auto" id="button-1">
                                                <input id="customer_maintance_mode"  type="checkbox" class="checkbox" />
                                                <div class="knobs"></div>
                                                <div class="layer"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 row mt-5">
                                        <div class="col-md-7">
                                            <label><strong>Maintainance Mode for Delivery Agent : </strong> </label>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="button r my-auto" id="button-1">
                                                <input id="da_maintance_mode"  type="checkbox" class="checkbox" />
                                                <div class="knobs"></div>
                                                <div class="layer"></div>
                                            </div>
                                        </div>
                                    </div>
                                <br/>
                                <a type="submit" class="btn btn-success" id="btnsaveappsetting">Update App</a>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(document).ready(function (){
            $.ajax({
                url : 'general-settings/getdata',
                type : 'get',
                success:function(response){
                    $.each(response.data,function(k,v){
                        if(v.name == "sgst")
                            $("#gs_sgst").val(v.value);
                        if(v.name == "cgst")
                            $("#gs_cgst").val(v.value);
                        if(v.name == "Covered Area")
                            $("#gs_covered_area").val(v.value);
                        if(v.name == "Covered Area")
                            $("#gs_covered_area").val(v.value);
                        if(v.name == "customer_maintance_mode"){
                            let value = v.value == "true" ? true : false;
                            $("#customer_maintance_mode").prop('checked', value);
                        }
                        if(v.name == "da_maintance_mode"){
                            let valuea = v.value == "true" ? true : false;
                            $("#da_maintance_mode").prop('checked', valuea);
                        }
                    })
                },
                error:function (error){
                    console.log(error.responseText);
                }
            });
        });

        $(document).on('click','#btnsavegst',function(e){
            e.preventDefault();
            var sgst = $("#gs_sgst").val();
            var cgst = $("#gs_cgst").val();
            token = `{{ csrf_token() }}`;
            $.ajax({
                url: 'general-settings/storedata',
                type : 'post',
                data : {"sgst":sgst,"cgst":cgst,'_token':token},
                success: function(response){
                     if(response.status == 'success')
                        swal("Success",response.message,'success');
                    else if(response.status == 'error')
                        swal("Error",response.message,'error');
                },
                error:function (error){
                    swal("Error",error.responseText);
                }
            });
        });
        $(document).on('click','#btnsaveappsetting',function(e){
            e.preventDefault();

            let customer_maintance_mode = $('#customer_maintance_mode').prop("checked");
            let da_maintance_mode = $('#da_maintance_mode').prop("checked");

            token = `{{ csrf_token() }}`;
            $.ajax({
                url: 'general-settings/appsetting',
                type : 'post',
                data : {"da_maintance_mode":da_maintance_mode,"customer_maintance_mode":customer_maintance_mode,'_token':token},
                success: function(response){
                     if(response.status == 'success')
                        swal("Success",response.message,'success');
                    else if(response.status == 'error')
                        swal("Error",response.message,'error');
                },
                error:function (error){
                    swal("Error",error.responseText);
                }
            });
        });
        $(document).on('click','#btnsavecoveredarea',function(e){
            e.preventDefault();
            var covered_area = $("#gs_covered_area").val();
            token = `{{ csrf_token() }}`;
            $.ajax({
                url: 'general-settings/storearearadius',
                type : 'post',
                data : {"covered_area":covered_area,'_token':token},
                success: function(response){
                    if(response.status == 'success')
                        swal("Success",response.message,'success');
                    else if(response.status == 'error')
                        swal("Error",response.message,'error');
                },
                error:function (error){
                    swal("Error",error.responseText);
                }
            });
        });

        function isNumberKey(txt, evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode == 46) {
                if (txt.value.indexOf('.') === -1) {
                    return true;
                } else {
                    return false;
                }
            } else {
                if (charCode > 31 &&
                    (charCode < 48 || charCode > 57))
                    return false;
            }
            if(txt.value.length > 1){
                if(txt.value.indexOf('.') > -1){
                    if(txt.value.split('.')[1].length >1){
                        return false;
                    }
                }else{
                    return false;
                }
            }
            return true;
        }
    </script>
@endpush
<style>


.toggle-button-cover {
  display: table-cell;
  position: relative;
  /* width: 100px; */
  top:12%;
  box-sizing: border-box;
}



.button-cover:before {
  counter-increment: button-counter;
  content: counter(button-counter);
  position: absolute;
  right: 0;
  bottom: 0;
  color: #d7e3e3;
  font-size: 12px;
  line-height: 1;
  padding: 5px;
}

.button-cover,
.knobs,
.layer {
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  height:13px;
}

.button {
  position: relative;
  /* top: 50%; */
  width: 80px;
  height: 25px;
  margin: -20px auto 0 auto;
  overflow: hidden;
}

.button.r,
.button.r .layer {
  border-radius: 100px;
}

.button.b2 {
  border-radius: 2px;
}

.checkbox {
  position: relative;
  width: 100%;
  height: 100%;
  padding: 0;
  margin: 0;
  opacity: 0;
  cursor: pointer;
  z-index: 3;
}

.knobs {
  z-index: 2;
}

.layer {
  width: 100%;
  background-color: #ebf7fc;
  transition: 0.9s ease all;
  z-index: 1;
}

/* Button 1 */
#button-1 .knobs:before {
  content: "  Deactive  ";
}

#button-1 .knobs:before {
  position: absolute;
  top: -3px;
  left: 0px;
  width: 80px;
  height: 40px;
  color: #fff;
  font-size: 11px;
  font-weight: 600;
  text-align: center;
  line-height: 1;
  padding: 9px 4px;
  /* background-color: #1BC5BD; */
  background-color: #F64E60;
  border-radius: 1%;
  transition: 0.9s cubic-bezier(0.18, 0.89, 0.35, 1.15) all;
}

#button-1 .checkbox:checked + .knobs:before {
  content: "Active ";
  left: 00px;
  /* background-color: #F64E60; */
  background-color: #1BC5BD;
}
#button-1 .checkbox:checked + .knobs1:before {
  content: " On ";
  /* content: "Off "; */
  left: 00px;
  /* background-color: #F64E60; */
  background-color: #1BC5BD;
}

#button-1 .checkbox:checked ~ .layer {
  background-color: #fcebeb;
}

#button-1 .knobs,
#button-1 .knobs:before,
#button-1 .layer {
  transition: 0.9s ease all;
}
</style>