@extends('layouts.app')

@section('content')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
  <!--begin::Subheader-->
  <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
    <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
      <!--begin::Info-->
      <div class="d-flex align-items-center flex-wrap mr-2">
        <!--begin::Page Title-->
        <i class="fa fa-hashtag text-danger" aria-hidden="true"></i>
        <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Delivery Agent assign</h5>
        <!--end::Page Title-->
        <!--begin::Actions-->
        <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

        <!--end::Actions-->
      </div>
      <!--end::Info-->
    </div>
  </div>
  <!--end::Subheader-->
  <!--begin::Entry-->
  <div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container">
      <!--end::Notice-->
      <!--begin::Card-->
      <div class="card card-custom">
       {{--  <div class="card-header">
          <div class="card-title justify-content-between flex-right align-items-right">
            <span class="card-icon">
              <i class="fa fa-hashtag text-danger" aria-hidden="true"></i>
            </span>
            <h3 class="card-label">Deliveryboy assign List</h3>

          </div>
        </div> --}}
        <div class="card-body" style="overflow-x: auto;">

          <div class="container">
            <form id="editForm" action="{{url('deliveryboy_assign/store')}}" method="POST" class="form-horizontal">
             @csrf

                           @if(isset($deliveryboy_assign))
                           <input type="hidden" name="id" value="{{ $deliveryboy_assign->id }}">
                           @endif
                           <div class="row">



                            <div class="col-md-12">

                              <div class="form-group">
                                <label>Area Assign</label>
                                <select class="form-control js-example-basic-single"  {{-- multiple="multiple" --}} name="area_id"data-bv-notempty="true"
                                data-bv-notempty-message="The Area is required">
                                <option value="">Select a Area</option>

                                @foreach($areacode as $n)

                                <option @if(isset($deliveryboy_assign) && $deliveryboy_assign->area_id == $n->id) selected="" @endif  value="{{$n->id}}">{{$n->name}} || {{ $n->pincode }}</option>

                                @endforeach
                              </select>


                             </div>


                             <div class="form-group">
                               <label>Delivery Agent Assign</label>

                               <select class="form-control js-example-basic-single" name="deliveryboy_id"data-bv-notempty="true"
                                data-bv-notempty-message="The Area is required">
                                <option value="">Select a Delivery Agent</option>

                                @foreach($deliveryboy as $n)

                                <option @if(isset($deliveryboy_assign) && $deliveryboy_assign->deliveryboy_id == $n->id) selected="" @endif  value="{{$n->id}}">{{$n->name}}</option>

                                @endforeach
                              </select>
                             </div>






                            </div>

                            <button type="submit" id="btn_save" class="btn btn-danger btn-lg">Save</button>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--end::Card-->
                </div>
                <!--end::Container-->
              </div>
              <!--end::Entry-->
            </div>
            <!-- Content Wrapper. Contains page content -->
            @endsection

            @push('js')
            <script>
        //create and store category
        $(function() {
          $('#editForm').bootstrapValidator();
           $(".js-example-basic-single").select2();
        });



      </script>
      @endpush
