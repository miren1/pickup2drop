@extends('layouts.app')

@section('content')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <i class="fa fa-hashtag text-danger" aria-hidden="true"></i>
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Delivery Agent assign List</h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

                <a href="{{ url('deliveryboy_assign/add') }}" class="btn btn-light-danger font-weight-bolder btn-sm"
                data-keyboard="false" data-backdrop="static">Add New delivery Agent assign</a>
                <!--end::Actions-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--end::Notice-->
            <!--begin::Card-->
            <div class="card card-custom">
                {{-- <div class="card-header">
                    <div class="card-title">
                        <span class="card-icon">
                            <i class="fa fa-hashtag text-danger" aria-hidden="true"></i>
                        </span>
                        <h3 class="card-label">Deliveryboy assign List</h3>

                         <a href="{{ url('deliveryboy_assign/add') }}" class="btn btn-light-danger font-weight-bolder btn-sm" data-keyboard="false"
                        data-backdrop="static">Add New deliveryboy_assign</a>
                    </div>
                </div> --}}
                <div class="card-body" style="overflow-x: auto;">
                    <!--begin: Datatable-->
                    <table class="table table-hover" id="myTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Delivery Agent Name</th>
                                <th>Area Code</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!-- Content Wrapper. Contains page content -->
@endsection

@push('js')
<script>

   $(document).ready(function() {
        $table = $("#myTable").DataTable({
            "ajax": "{{ url('deliveryboy_assign/get_all') }}",
            "processing": false,
            "serverSide": false,
            "ordering": false,
            "columns": [
              { "data": "id",
                render:function(data,type,row,meta){
                  console.log(meta);
                    return meta.row + 1;
                }
            },
            //{ "data": "boy.name" },
             { "data": "id",
               render:function(data,row,alldata){
               var name ="";
               if(alldata.boy){
               if(alldata.boy.name  || alldata.boy.name != ''){
                 // alert(alldata.boy.name);
                 return name+=alldata.boy.name;
                 //alert(name);
               }else{
                name+='';
               }
               }

               return '<span  data-toggle="tooltip">'+name+'</span>';
              }
            },
            //{ "data": "areacode.pincode"},
            { "data": "id",
               render:function(data,row,alldata){
               var name ="";
               if(alldata.areacode){
               if(alldata.areacode.pincode  || alldata.areacode.pincode != ''){

                 return name+=alldata.areacode.pincode;

               }else{
                name+='';
               }
               }

               return '<span  data-toggle="tooltip" >'+name+'</span>';
              }
            },
            { "data": "id",
            render:function(data,type,row,meta){
                console.log(meta);
                $id =btoa(row.id);
                $html = '<div class="dropdown mo-mb-2">'+
                                  '<a href="#" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                                      'Action'+
                                  '</a>'+

                                  '<div class="dropdown-menu" x-placement="bottom-start" style="cursor: pointer;">'+

                                      '<a class="dropdown-item " href="{{ url('deliveryboy_assign/edit') }}/'+$id+'"><i class="fa fa-edit text-primary"></i>&nbsp; Edit</a>'+ ' ';

                                     $html+= '<a class="dropdown-item btn_delete" data-url="areacode/delete" data-id="'+row.id+'"><i class="fa fa-trash text-danger"></i>&nbsp; Delete</a>' ;


                                  '</div>'+
                              '</div>';
                      return $html;
            }
        },
        ]


    });

        $( document ).ajaxComplete(function() {
            // Required for Bootstrap tooltips in DataTables
            $('body').tooltip({
                 selector: '[data-toggle="tooltip"]',
                 container: 'body',
                 "delay": {"show": 100, "hide": 0},
            });
        });

    } );



   $(document).on("click", '.btn_delete', function(event) {
        var id =$(this).attr('data-id');
        swal({
            title: "Are you sure?",
            text: "You Want To Delete This Deliveryboy Assign,You Will Not Be Able To Recover This Deliveryboy Assign.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {

                $.ajax({
                    url: '{{ url('deliveryboy_assign/destroy') }}/'+id,
                    type: 'get',
                    dataType: 'json',
                //data: {id: 'id'},
                success:function($data){
                    //console.log(data);
                    if($data == true)
                    {
                        swal("Deliveryboy Assign  has been Delete.", {
                            icon: "success",
                        });
                        $('#myTable').DataTable().ajax.reload();
                    }else{
                        swal("Deliveryboy Assign  has been not Delete.", {
                            icon: "error",
                        });
                    }
                    //$('#city').DataTable().ajax.reload();
                }
            });


            } else {
                swal("Your Deliveryboy Assign is Safe.");
            }
        });
    });

</script>
@endpush
