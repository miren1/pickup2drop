@extends('layouts.app')

@section('content')

@push('css')
<style type="text/css">
  .gj-datepicker-bootstrap [role=right-icon] button .gj-icon, .gj-datepicker-bootstrap [role=right-icon] button .material-icons {
    position: absolute;
    font-size: 21px;
    top: 17px;
    left: 9px;
}
</style>
@endpush
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <i class="fa fa-subway text-danger" aria-hidden="true"></i>
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Subscription</h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

                <!--end::Actions-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--end::Notice-->
            <!--begin::Card-->
            <div class="card card-custom">
                {{-- <div class="card-header">
                    <div class="card-title justify-content-between flex-right align-items-right">
                        <span class="card-icon">
                            <i class="fa fa-hashtag text-danger" aria-hidden="true"></i>
                        </span>
                        <h3 class="card-label">Deliveryboy</h3>

                    </div>
                </div> --}}
                <div class="card-body" style="overflow-x: auto;">

                    <div class="container">
                        <form id="editForm" action="{{url('subscription/store')}}" method="POST" class="form-horizontal">
                           @csrf

                           @if(isset($tiffin))
                           <input type="hidden" name="id" value="{{ $tiffin->id }}">
                           @endif
                           <div class="row">
                             <div class="col-md-6">
                                <div class="form-group">
                                  <label>Name</label>
                                  <input type="text"  name="item_name" class="form-control" placeholder="Enter Your item Name" required
                                  data-bv-notempty="true"
                                  data-bv-notempty-message="item Name is required"
                                  @if(isset($tiffin)) value="{{ $tiffin->item_name }}" @endif>

                              </div>

                              <div class="form-group">
                                  <label>Description</label>
                                  <textarea class="form-control" name="description" placeholder="Enter Your Description...." rows="3" cols="5" required="" data-bv-notempty="true"
                                  data-bv-notempty-message="Description is required">@if(isset($tiffin)) {{ $tiffin->description }} @endif</textarea>


                              </div>

                              <div class="form-group">
                                  <label>Price</label>
                                  <input type="number"  name="amount" class="form-control" placeholder="Enter Your amount" required
                                  data-bv-notempty="true"
                                  data-bv-notempty-message="amount is required"
                                  @if(isset($tiffin)) value="{{ $tiffin->amount }}" @endif>

                              </div>

                              <div class="form-group">
                               <label>Delivery Agent Assign</label>

                               <select class="form-control js-example-basic-single" name="deliveryboy_id"data-bv-notempty="true"
                                data-bv-notempty-message="The Area is required">
                                <option value="">Select a Delivery Agent</option>

                                @foreach($deliveryboy as $n)

                                <option @if(isset($tiffin) && $tiffin->deliveryboy_id == $n->id) selected="" @endif  value="{{$n->id}}">{{$n->name}}</option>

                                @endforeach
                              </select>
                             </div>

                          </div>


                          <div class="col-md-6">

                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control @error('is_status') is-invalid @enderror" id="is_status" name="is_status" required
                                data-bv-notempty="true"
                                data-bv-notempty-message="Status is required"
                                >
                                <option value="">Select a Status</ option>

                                  <option  @if(isset($tiffin) && $tiffin->is_status =='active') selected="" @endif value="active">Active</option>
                                  <option @if(isset($tiffin) && $tiffin->is_status =='deactive') selected="" @endif value="deactive">Deactive</option>
                                </select>


                              </div>

                             <div class="form-group">
                                <label>Start Date</label>
                                <input readonly='true' type="date"  name="start_date" class="form-control" placeholder="Enter Your Start Date" required id="start_date"
                                data-bv-notempty="true" autocomplete="off"
                                data-bv-notempty-message="Start Date  is required"

                                @if(isset($tiffin)) value="{{ date('Y-m-d', strtotime($tiffin->start_date)) }}" @endif>

                            </div>


                             <div class="form-group">
                                <label>End Date</label>
                                <input readonly='true' type="date"  name="end_date" class="form-control" placeholder="Enter Your End Date" required id="end_date" autocomplete="off"
                                data-bv-notempty="true"
                                data-bv-notempty-message="End Date  is required"
                                @if(isset($tiffin)) value="{{ date('Y-m-d', strtotime($tiffin->end_date)) }}" @endif>

                            </div>



                        </div>
                        <div class="col-md-12">
                            <button type="submit" id="btn_save" class="btn btn-danger btn-lg">Save</button>
                            <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" onclick="window.history.go(-1); return false;">Back</a>
                            {{-- <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" href="{{ route('subscription.index') }}">Back</a> --}}
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--end::Card-->
</div>
<!--end::Container-->
</div>
<!--end::Entry-->
</div>
<!-- Content Wrapper. Contains page content -->
@endsection

@push('js')
{{-- <script>
        //create and store category
        $(function() {
            $('#editForm').bootstrapValidator();
        });



</script> --}}

<script type="text/javascript">
   $(document).ready(function() {
       $('#editForm').bootstrapValidator();

      var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());

      $('#start_date').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'yyyy-mm-dd',
            required: true,
            minDate: today,
            orientation: "bottom",
            maxDate: function () {
                return $('#end_date').val();
            }

        });
     $('#end_date').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'yyyy-mm-dd',
            required: true,
            orientation: "bottom",
            minDate: function () {
                return $('#start_date').val();
            }

        });

});
</script>
@endpush
