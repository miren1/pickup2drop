@extends('layouts.app')

@section('content')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <i class="fa fa-subway text-danger" aria-hidden="true"></i>
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Subscription</h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

               {{--  <a href="{{ url('subscription/add') }}" class="btn btn-light-danger font-weight-bolder btn-sm"
                data-keyboard="false" data-backdrop="static">Add New</a> --}}
                <!--end::Actions-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--end::Notice-->
            <!--begin::Card-->
            <div class="card card-custom">
                {{-- <div class="card-header">
                    <div class="card-title">
                        <span class="card-icon">
                            <i class="icon-building text-danger" aria-hidden="true"></i>
                        </span>
                        <h3 class="card-label">Agencies</h3>

                         <a href="{{ url('admin/customers/add') }}" class="btn btn-light-danger font-weight-bolder btn-sm" data-keyboard="false"
                        data-backdrop="static">Add New Customer</a>
                    </div>
                </div> --}}
                <div class="card-body" style="overflow-x: auto;">
                    <!--begin: Datatable-->
                    <table class="table table-hover" id="dataTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Order Id</th>
                                <th>Customer Name</th>
                                <th>Customer Number</th>
                                <th>Delivery Agent Name</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Amount</th>
                                <th>Status</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Start Time</th>
                                <th>End Time</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!-- Content Wrapper. Contains page content -->
@endsection

@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<script>

    //load data in dataTable

    $(function() {
        var uri = location.href;
        var filter = 'all';
        
        if(uri.indexOf("?") > 0)
            var filter = uri.substring(uri.indexOf("?")+1);
        
            $("#dataTable").DataTable({
            "ajax": `{{ url('subscription/show/${filter}') }}`,
            "processing": true,
            "serverSide": true,
            "ordering": false,
            "searchable": true,
            "columns": [
            {
                    data: "id",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1
                    }
                },
                { "data": "id"},
                { "data": "id",
                    render:function(data,row,alldata){
                    var name ="";
                    if(alldata.customers){
                    if(alldata.customers.name  || alldata.customers.name != ''){
                     // alert(alldata.customers.name);
                    name+=alldata.customers.name;
                     //alert(name);
                    }else{
                     name+='';
                    }
               }

               return '<span  data-toggle="tooltip" title="'+name+'">'+name.substring(0,20) + '...'+'</span>';
              }
            },

                { data: "customers.mobile_number",defaultContent :'-'},

               { "data": "id",
                    render:function(data,row,alldata){
                    var name ="";
                    if(alldata.boy){
                    if(alldata.boy.name  || alldata.boy.name != ''){
                     // alert(alldata.boy.name);
                    name+=alldata.boy.name;
                     //alert(name);
                    }else{
                     name+='';
                    }
               }

               return '<span  data-toggle="tooltip" title="'+name+'">'+name.substring(0,20) + '...'+'</span>';
              }
            },

                { data: "item_name"},


               /* { "data": "description",
                    render:function(data,type,row,meta){
                       if (data.length > 20)
                        return '<span  data-toggle="tooltip" title="'+data+'">'+data.substring(0,20) + '...'+'</span>';
                    else
                        return data;
                 }
                },  */
                { "data": "id",
                render:function(data,row,alldata){
                     var msg ='';
                     if(alldata.description && alldata.description !='' && alldata.description !=null){
                        if(alldata.description.length > 15){
                           msg='<span  data-toggle="tooltip" title="'+alldata.description+'">'+alldata.description.substring(0,15) + '...'+'</span>';
                        }else{
                         msg =alldata.description;
                      }
                     }

                     return msg;
                }
             },
                {data: "amount",defaultContent :'0.00'},

                //{ data: "is_status"},
                 {
                    data: "is_status",
                    render: function(data, row, alldata){
                        if (alldata.is_status=="active") {
                            return '<span class="label label-lg font-weight-bold label-light-success label-inline">Active</span>';
                        } else {
                            return '<span class="label label-lg font-weight-bold label-light-danger label-inline">Deactive</span>';
                        }
                    }
                },

                //{data: "start_date"},
                { "data": "start_date",
                    render : function (data,full) {
                    return moment(data).format('DD-MM-YYYY');
                    }
                },

                //{data: "end_date"},
                { "data": "end_date",
                    render : function (data,full) {
                    return moment(data).format('DD-MM-YYYY');
                    }
                },
                { "data": "start_time",
                    // render : function (data,full) {
                    // return moment(data).format('DD-MM-YYYY');
                    // }
                },
                { "data": "end_time",
                    // render : function (data,full) {
                    // return moment(data).format('DD-MM-YYYY');
                    // }
                },


                { "data": "id",
                render:function(data,type,row,meta){
                    $id =btoa(row.id);
                    $html = '<div class="dropdown mo-mb-2">'+
                    '<a href="#" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                    'Action'+
                    '</a>'+

                    '<div class="dropdown-menu" x-placement="bottom-start" style="cursor: pointer;">'+

                    '<a class="dropdown-item " href="{{ url('subscription/edit') }}/'+$id+'"><i class="fa fa-edit text-primary"></i>&nbsp; Edit</a>'+ ' ';

                    $html+= '<a class="dropdown-item btn_delete" data-url="subscription/delete" data-id="'+row.id+'"><i class="fa fa-trash text-danger"></i>&nbsp; Delete</a>' ;

                    $html+= '<a class="dropdown-item" href="{{ url('subscription/view') }}/'+$id+'"><i class="fa fa-eye text-primary mr-1"></i>&nbsp;View</a>'+ ' ';


                    '</div>'+
                    '</div>';
                    return $html;
                }
            },

            ]
        });

        $( document ).ajaxComplete(function() {
            // Required for Bootstrap tooltips in DataTables
            $('body').tooltip({
                 selector: '[data-toggle="tooltip"]',
                 container: 'body',
                 "delay": {"show": 100, "hide": 0},
            });
        });
    });






    $(document).on("click", '.btn_delete', function(event) {
        var id =$(this).attr('data-id');
        swal({
            title: "Are you sure?",
            text: "You Want To Delete This Subscription,You Will Not Be Able To Recover This Subscription.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {

                $.ajax({
                    url: '{{ url('subscription/destroy') }}/'+id,
                    type: 'get',
                    dataType: 'json',
                //data: {id: 'id'},
                success:function($data){
                    if($data == true)
                    {
                        swal("Subscription  has been Delete.", {
                            icon: "success",
                        });
                        $('#dataTable').DataTable().ajax.reload();
                    }else{
                        swal("Subscription  has been not Delete.", {
                            icon: "error",
                        });
                    }
                    //$('#city').DataTable().ajax.reload();
                }
            });


            } else {
                swal("Your Subscription is Safe.");
            }
        });
    });

</script>
@endpush
