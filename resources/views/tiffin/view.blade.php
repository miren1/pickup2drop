@extends('layouts.app')

@section('content')

@push('css')
<style type="text/css">
  h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
    margin-bottom: 2.5rem;
    font-weight: 500;
    line-height: 1.2;
}
</style>
@endpush
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
  <!--begin::Subheader-->
  <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
    <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
      <!--begin::Info-->
      <div class="d-flex align-items-center flex-wrap mr-2">
        <!--begin::Page Title-->
        <i class="fa fa-subway text-danger"  aria-hidden="true"></i>
        <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp; Subscription Information view</h5>


        <!--end::Page Title-->
        <!--begin::Actions-->
        <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

        <!--end::Actions-->
      </div>
      <!--end::Info-->
    </div>
  </div>
  <!--end::Subheader-->
  <!--begin::Entry-->
  <div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container">
      <!--end::Notice-->
      <!--begin::Card-->
      <div class="card card-custom">
        {{-- <div class="card-header">
          <div class="card-title justify-content-between flex-right align-items-right">
            <span class="card-icon">
              <i class="fa fa-hashtag text-danger" aria-hidden="true"></i>
            </span>
            <h3 class="card-label">Areacode</h3>

          </div>
        </div> --}}
        <div class="card-body" style="overflow-x: auto;">

          <div class="container">

            <div class="row">
              <div class="col-md-12">
                <h4 style="margin-bottom: 0.5rem;">Type:-{{ $pickup->item_name }}</h4>
                      <hr>

              </div>

              <div class="col-xl-3 col-md-6">
                <label>Customer Name</label>
                {{-- @php dd($pickup); @endphp --}}
                <h5 class="m-b-30 f-w-700"> {{ $pickup->customer && $pickup->customer->name ? $pickup->customer->name : " - "  }}
                  <span class="text-c-green m-l-10"></span></h5>

              </div>


              <div class="col-xl-3 col-md-6">
                <label>Customer Number</label>
                {{-- @php dd($pickup); @endphp --}}
                <h5 class="m-b-30 f-w-700"> {{ $pickup->customer && $pickup->customer->mobile_number ?$pickup->customer->mobile_number : ' - ' }}
                  <span class="text-c-green m-l-10"></span></h5>

              </div>


              <div class="col-xl-3 col-md-6">
                <label>Delivery Agent Name</label>
                {{-- @php dd($pickup); @endphp --}}
                 @if($pickup['boy'] != null)
                <h5 class="m-b-30 f-w-700"> {{ $pickup['boy']->name }}
                  <span class="text-c-green m-l-10"></span></h5>
                 @endif
              </div>


              <div class="col-xl-3 col-md-6">
                <label>Name</label>
                {{-- @php dd($pickup); @endphp --}}
                <h5 class="m-b-30 f-w-700"> {{ $pickup->item_name }}
                  <span class="text-c-green m-l-10"></span></h5>

              </div>

               <div class="col-xl-3 col-md-6">
                <label>Description</label>
                {{-- @php dd($pickup); @endphp --}}
                <h5 class="m-b-30 f-w-700"> {{ $pickup->description }}
                  <span class="text-c-green m-l-10"></span></h5>

              </div>


              <div class="col-xl-3 col-md-6">
                <label>Amount</label>
                {{-- @php dd($pickup); @endphp --}}
                <h5 class="m-b-30 f-w-700"> {{ $pickup->amount ? $pickup->amount : '0.00' }}
                  <span class="text-c-green m-l-10"></span></h5>
              </div>

                <div class="col-xl-3 col-md-6">
                  <label>Promocode</label>
                  <h5 class="m-b-30 f-w-700"> {{ $pickup->promocode ? $pickup->promocode : ' - ' }}
                    <span class="text-c-green m-l-10"></span></h5>
                  </div>

              <div class="col-xl-3 col-md-6">
                  </label>Payment Status</label>

                    @if($pickup->is_status =="active")
                      <h5 class="m-b-30 f-w-700">
                        <span class="badge badge-success">{{ $pickup->is_status }}</span>
                      </h5>
                    @elseif(!is_null($pickup->is_status))
                      <h5 class="m-b-30 f-w-700">
                        <span class="badge badge-danger">{{ $pickup->is_status }}</span>
                      </h5>
                    @else
                    <h5 class="m-b-30 f-w-700">
                        <span > - </span>
                    </h5>
                      
                    @endif
                </div>

                <div class="col-xl-3 col-md-6">
                  <label>Areacode</label>
                  <h5 class="m-b-30 f-w-700"> {{ $pickup->pincode ? $pickup->pincode : ' - ' }}
                    <span class="text-c-green m-l-10"></span></h5>
                  </div>
              
                  
                  <div class="col-xl-3 col-md-6">
                    <label>Pick Address</label>
                    <h5 class="m-b-30 f-w-700"> {{ $pickup->Pickupaddress && $pickup->Pickupaddress->address ? $pickup->Pickupaddress->address : ' - ' }}
                    <span class="text-c-green m-l-10"></span></h5>
                  </div>
                  <div class="col-xl-3 col-md-6">
                    <label>Drop Address</label>
                    <h5 class="m-b-30 f-w-700"> {{ $pickup->Dropaddress && $pickup->Dropaddress->address ? $pickup->Dropaddress->address : " - " }}
                    <span class="text-c-green m-l-10"></span></h5>
                  </div>

                <div class="col-xl-3 col-md-6">
                  <label>Start Date</label>
                  {{-- @php dd($pickup); @endphp --}}
                  <h5 class="m-b-30 f-w-700"> {{ $pickup->start_date ? $pickup->start_date : ' - ' }}
                    <span class="text-c-green m-l-10"></span></h5>

                </div>

                <div class="col-xl-3 col-md-6">
                  <label>End Date</label>
                  {{-- @php dd($pickup); @endphp --}}
                  <h5 class="m-b-30 f-w-700"> {{ $pickup->end_date ? $pickup->end_date : ' - ' }}
                    <span class="text-c-green m-l-10"></span></h5>

                </div>
                {{-- @if($pickup->start_time) --}}
                <div class="col-xl-3 col-md-6">
                  <label>Start Time</label>
                  <h5 class="m-b-30 f-w-700"> {{ $pickup->start_time ? $pickup->start_time : ' - '  }}
                    <span class="text-c-green m-l-10"></span></h5>
                </div>
                {{-- @endif --}}
                {{-- @if($pickup->end_time) --}}
                <div class="col-xl-3 col-md-6">
                  <label>End Time</label>
                  <h5 class="m-b-30 f-w-700"> {{ $pickup->end_time ? $pickup->end_time : ' - ' }}
                    <span class="text-c-green m-l-10"></span></h5>
                </div>
                {{-- @endif --}}

            </div>
            <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" onclick="window.history.go(-1); return false;">Back</a>
            {{-- <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" href="{{ route('subscription.index') }}">Back</a> --}}
          </div>
        </div>
      </div>
      <!--end::Card-->
    </div>
    <!--end::Container-->
  </div>
  <!--end::Entry-->
</div>
<!-- Content Wrapper. Contains page content -->
@endsection

@push('js')
<script>
        //create and store category
        $(function() {
          $('#editForm').bootstrapValidator();
        });



      </script>
      @endpush
