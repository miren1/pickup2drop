@extends('layouts.app')

@section('content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <!-- <i class="fas fa-sliders-h"></i> -->
                <i class="fas fa-sliders-h text-danger" aria-hidden="true"></i>
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Slider</h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

                <!-- <a href="{{ url('subscription/add') }}" class="btn btn-light-danger font-weight-bolder btn-sm"
                data-keyboard="false" data-backdrop="static">Add New</a>   -->
                <!-- <a href="" class="btn btn-light-danger font-weight-bolder btn-sm"
                data-keyboard="false" data-backdrop="static">Add New Slider</a> -->
                 <button class="btn btn-light-danger font-weight-bolder btn-sm"
                data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#myModal">Add New Slider</button>
            </div>
            <!--end::Info-->
        </div>
    </div>
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--end::Notice-->
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-body" style="overflow-x: auto;">
                    <!--begin: Datatable-->
                    <table class="table table-hover" id="myTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Slider Image</th>
                                <!-- <th>Code Time</th> -->
                                <!-- <th>Price/ %</th> -->
                                <!-- <th>Max Amount</th> -->
                                <!-- <th style="width: 80px;">End Date</th> -->
                                <!-- <th>One Day</th> -->
                                <th style="width: 75px;">Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                    <!--end: Datatable-->
                    <div class="container">
                        <div class="modal fade" id="myModal">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h4 class="modal-title">Slider</h4>
                                        <button type="button" class="close" id="closeModal"
                                            data-dismiss="modal">&times;</button>
                                    </div>
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <h4 id="error-edit" style="display: none;" class='alert alert-danger'></h4>
                                        <form id="editForm" method="POST" class="form-horizontal">
                                            @csrf
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <input type="hidden" name="id" id="id">
                                                    <label>Title</label>
                                                    <input type="text" id="title" name="title" class="form-control" placeholder="Title" required
                                                    data-bv-notempty="true"
                                                    data-bv-notempty-message="Title is required">
                                                    <span style="display: none;" class="text-danger title"><small>Title Already Exist</small></span>
                                                </div>      
                                                <div class="form-group col-md-6">
                                                    <label>URL</label>
                                                    <!-- <input type="text" id="image_url" name="image_url" class="form-control" placeholder="Enter URL" required
                                                    data-bv-notempty="true"
                                                    data-bv-notempty-message="URL is required"> -->
                                                    <input type="text" id="image_url" name="image_url" class="form-control" placeholder="Enter URL" >
                                                    <span style="display: none;" class="text-danger title"><small>URL Already Exist</small></span>
                                                </div> 

                                                <div class="form-group col-md-6">
                                                    <label>Description</label>
                                                    <textarea class="form-control" rows="3"  cols="50" id="description" required name="description" placeholder="Description"  data-bv-notempty-message="Please Enter a Description"></textarea>
                                                </div>
                                                 <div class="form-group col-md-6">
                                                    <!-- <input type="hidden" name="id" id="id"> -->
                                                    <label>Image Upload</label>
                                                    <input type="file" id="file" accept=".jpg,.jpeg,.png" name="file" class="form-control" placeholder="File" required
                                                    data-bv-notempty="true"
                                                    data-bv-notempty-message="Image is required">
                                                    <div class="mt-2">
                                                        <span class="text-danger title"><small>Please upload image with the aspect ratio of 16:9 for better result.</small></span>
                                                    </div>


                                                </div>
                                                 <div class="form-group col-md-6">
                                                    <label>Slider Type</label>
                                                    <select class="form-control" id="slider_type" name="slider_type" required data-bv-notempty="true" data-bv-notempty-message="Slider Type is required">
                                                        <option value=""> -- Select Slider Type -- </option>
                                                        <option value="dashboard slider">Dashboard Slider</option>
                                                        <option value="pickup slider">Pickup Slider</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <button type="submit" id="btn_save" class="btn btn-danger btn-lg">Save</button>
                                            <!-- /.card-body -->
                                          </form>
                                    </div>
                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" id="closeModal"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
</div>

@endsection

@push('js')
<script>
    $(function() {

    	$('#editForm').bootstrapValidator();
        $(document).on('click', '#closeModal', function(e) {
            e.preventDefault();
            /* Act on the event */
            $('.perc_price').html('Percentage/Price');
            $('#description').val("");
            $('#editForm').bootstrapValidator('resetForm', true);
            $('.upto_amount').css('display','none');
            $('.promo').css('display','none');
            $('#error').css('display','none');
            $("#btn_save").html("Save");
            $('#id').val("");
        });


        $('#editForm').bootstrapValidator()
            .on('success.form.bv', function(e) {
                e.preventDefault();
                var val = $('#id').val();
                $.ajax({
                    type: "POST",
                    url: (val=="")?"{{ route('slider.store') }}":"{{ route('slider.update') }}",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(result) {
                        if(result.status == 'aunthorized_access'){
                            swal(result.title, result.message, result.icon); 
                            $('#myModal').modal('hide');   
                        }else{
                            if(result.status==="exist")
                        {
                            $('.promo').css('display','block');
                        }
                        if(result.status==="more")
                        {
                            $('.pmore').css('display','block');
                        }
                        swal(result.title, result.message, result.icon);
                        if (result.icon === "success") {
                            $('#editForm').bootstrapValidator('resetForm', true);
                            $('.upto_amount').css('display','none');
                            $('.promo').css('display','none');
                            $('#myModal').modal('hide');
                            $("#btn_save").html("Save");
                            $('#description').val("");
                            $('#id').val("");
                            $('#myTable').DataTable().ajax.reload(null,false);
                        }
                        else
                        {
                            $('.promo').css('display','none');
                            $("#error-edit").html("Something Wrong Try Again...");
                            $("#error-edit").css("display", "block");
                        }
                        }
                        
                    }
                });
        });


        $(document).ready(function() {
            $table = $("#myTable").DataTable({
                'serverMethod': 'get',
                "ajax": {
                    'url':"{{ route('slider.show_list') }}",
                    "data":{"_token":"{{csrf_token()}}"},
                },
                "processing": true,
                "serverSide": true,
                "ordering": false,
                'stateSave' : true,
                "columns": [
                  /*{ "data": "id",
                    render:function(data,type,row,meta){
                        return meta.row + 1;
                    }
                },*/
                {
                    data: "id",
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1
                        }
                },
                { "data": "title" },
                { "data": "description",
                   render: function(data,type,row,meta){
                     var data = row.description;
                     if(data.length >= 10)
                     {
                        return data.substr(0,5) + '....';
                     }

                     return  data;
                     // console.log('df',row.description);
                   } 
                },
                { "data": "file",
                    render: function(data,type,row,meta){
                     var data = row.file;
                     if(data.length >= 20)
                     {
                        return data.substr(0,20) + '....';
                     }

                     return  data;
                     // console.log('df',row.description);
                   } 
                },
                { "data": "created_at" },
                { "data": "id",
                render:function(data,type,row,meta){
                    // console.log('dfgdffd',data);
                    $id =btoa(row.id);
                    $html = '<div class="dropdown mo-mb-2">'+
                                  '<a href="#" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                                      'Action'+
                                  '</a>'+

                                  '<div class="dropdown-menu" x-placement="bottom-start" style="cursor: pointer;">'+
                                      '<a class="dropdown-item edit_plan" data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#myModal" data-id="'+row.id+'"data-title="'+row.title+'"data-description="'+row.description+'"data-slider_type="'+row.slider_type+'"data-image_url="'+row.image_url+'"data-file="'+row.file+'"><i class="fa fa-edit text-primary"></i> &nbsp;Edit</a>'+' ';
                                      
                                     $html+= '<a class="dropdown-item btn_delete" data-id="'+row.id+'"><i class="fa fa-trash text-danger"></i>&nbsp; Delete</a>' ;
                                  '</div>'+
                              '</div>';
                      return $html;
                    }
                },
                ]
            });
        });

        $(document).on('click', '.edit_plan', function(e) {
            // document.querySelector('#file').required = false;
            // // $('#file').prop('required',false);
            // $('#file').removeAttr('data-bv-notempty');
            // $('#file').removeAttr('bv-notempty-message');
            // $('#file').removeAttr('data-bv-field');
            $('#editForm').bootstrapValidator('enableFieldValidators', 'file', false);

            e.preventDefault();
            /* Act on the event */
            var id = $(this).data('id');
            var title = $(this).data('title');
            var description = $(this).data('description');
            var slider_type = $(this).data('slider_type');
            var image_url = $(this).data('image_url');
            var file = $(this).data('file');
          
            $("#id").val(id);
            $("#title").val(title);
            $("#description").val(description);
            $("#slider_type").val(slider_type).trigger('change');
            $("#image_url").val(image_url);
            $("#file").val(file);
            $("#btn_save").html("Edit");
            
        });




        $(document).on("click", '.btn_delete', function(event) { 
            var id =$(this).attr('data-id');
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this Slider.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: '{{ route('slider.destroy') }}',
                        type: 'post',
                        data: {
                            slider_id: id,_token:"{{csrf_token()}}"
                        },
                        dataType: 'json',
                    success: function(response) {
                        if(response.status == 'aunthorized_access'){
                            swal("Error","Unauthorized access.",{icon:'error'});
                        }else if (response.status == "success") {
                            swal(response.message, {
                                icon: 'success',
                            }).then((result) => {
                                $('#myTable').DataTable().ajax.reload(null,false);
                            });
                        } else {
                            swal("Error","Something Wrong try agian..",{icon:'error'});
                        }
                    }
                });


                } else {
                    swal("Your Slider is Safe.");
                }
            });
        });



    });
</script>
@endpush
