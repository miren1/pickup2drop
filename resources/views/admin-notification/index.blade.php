@extends('layouts.app')

@section('content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <i class="fas fa-bell text-danger"></i>
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Admin Notification</h5>
                

                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

                <!-- <a href="{{ url('subscription/add') }}" class="btn btn-light-danger font-weight-bolder btn-sm"
                data-keyboard="false" data-backdrop="static">Add New</a>   -->
                <!-- <a href="" class="btn btn-light-danger font-weight-bolder btn-sm"
                data-keyboard="false" data-backdrop="static">Add New Slider</a> -->
                 <!-- <button class="btn btn-light-danger font-weight-bolder btn-sm"
                data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#myModal">Add New Slider</button> -->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--end::Notice-->
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-body" style="overflow-x: auto;">
                    <!--begin: Datatable-->
                    <table class="table table-hover" id="myTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Notification</th>
                                <th>Notification type</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
</div>

@endsection

@push('js')
<script>


       


        $(document).ready(function() {
            $table = $("#myTable").DataTable({
                'serverMethod': 'get',
                "ajax": {
                    'url':"{{ route('notification.get') }}",
                    "data":{"_token":"{{csrf_token()}}"},
                },
                "processing": true,
                "serverSide": true,
                "ordering": false,
                "searching": false,
                'stateSave' : true,
                "columns": [
                  /*{ "data": "id",
                    render:function(data,type,row,meta){
                        return meta.row + 1;
                    }
                },*/
                {
                    data: "id",
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1
                        },
                       
                },
                 { "data": "data",
                            render: function(data, type, row, meta) {
                                 return row.data.message;
                                },
                },
                 { "data": "type",class:'text-center',
                            render: function(data, type, row, meta) {
                                $type = null;
                                if(row.type == 'App\\Notifications\\DisputeNotification')  
                                    $type =  "Dispute";
                                else
                                    $type = ' - ';

                                    return $type
                                },
                },
                 { "data": "read_at",class:'text-center',
                            render: function(data, type, row, meta) {
                                $read = row.read_at ? 
                                '<i class="fa fa-envelope-open text-danger" aria-hidden="true"></i>' 
                                : 
                                '<i class="fa fa-envelope text-danger" aria-hidden="true"></i>';
                                return $read;
                            }
                },
                { "data": "id",class:'text-center',
                render:function(data,type,row,meta){
                    $id =btoa(row.id);
                    $html = '<a href="admin-notification/show/'+$id+'" class="p-6" data-id="'+row.order_id+'"><i class="fas fa-eye  text-primary mr-2"></i></a>' ;
                      return $html;
                    }
                },
             
                ]
            });
        });

        



</script>
@endpush
