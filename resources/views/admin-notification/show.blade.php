@extends('layouts.app')

@section('content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <!-- <i class="fas fa-sliders-h"></i> -->
                <!-- <i class="fas fa-shipping-fast"></i>  -->
                <i class="fas fa-bell text-danger"></i>
                <!-- <i class="fas fa-sliders-h text-danger" aria-hidden="true"></i> -->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Admin Notification</h5>
                

                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
            </div>
            <!--end::Info-->
        </div>
    </div>
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--end::Notice-->
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-body row" style="overflow-x: auto;">
                    <!--begin: Datatable-->
                        <div class="col-xl-3 col-md-6">
                      <h5>Pickup ID</h5>
                        <p class="m-b-30 f-w-100">{{$data['dispute_data'] && $data['pickup_data']->id ? $data['pickup_data']->id : ' - '}}<span class="text-c-green m-l-10"></span></p>
                    </div>
                    <div class="col-xl-3 col-md-6">
                      <h5>Item Name</h5>
                        <p class="m-b-30 f-w-100">{{$data['pickup_data'] && $data['pickup_data']->item_name ? $data['pickup_data']->item_name : ' - '}}<span class="text-c-green m-l-10"></span></p>
                    </div>
                    <div class="col-xl-3 col-md-6">
                      <h5>Item description</h5>
                        <p class="m-b-30 f-w-100 ">{{$data['pickup_data'] && $data['pickup_data']->description ? $data['pickup_data']->description : ' - '}}<span class="text-c-green m-l-10"></span></p>
                    </div>
                    <div class="col-xl-3 col-md-6">
                      <h5>Delivery Agent</h5>
                        <p class="m-b-30 f-w-100">{{$data['pickup_data'] && $data['pickup_data']->boy && $data['pickup_data']->boy->name ? $data['pickup_data']->boy->name : ' - '}}<span class="text-c-green m-l-10"></span></p>
                    </div>
                    <div class="col-xl-12 col-md-12 ">
                      <h5 class="f-w-100">Dispute Description</h5>
                        <p class="m-b-30 text-justify">{{$data['dispute_data'] && $data['dispute_data']->dispute ? $data['dispute_data']->dispute : ' - '}}<span class="text-c-green m-l-10"></span></p>
                    </div>
                    <div class="col-xl-3 col-md-6">
                      <h5>Customer Name</h5>
                        <p class="m-b-30 f-w-100">{{$data['dispute_data'] && $data['dispute_data']->user && $data['dispute_data']->user['name'] ? $data['dispute_data']->user['name'] : ' - ' }}<span class="text-c-green m-l-10"></span></p>
                    </div>
                    <div class="col-xl-3 col-md-6">
                      <h5>Customer Mobile</h5>
                        <p class="m-b-30 f-w-100">{{$data['dispute_data'] && $data['dispute_data']->user && $data['dispute_data']->user['mobile_number'] ? $data['dispute_data']->user['mobile_number'] : ' - '}}<span class="text-c-green m-l-10"></span></p>
                    </div>
                    
                    
                    <div class="col-xl-3 col-md-6">
                      <h5>Delivery Status</h5>
                        <p class="m-b-30 f-w-100">{{$data['pickup_data'] && $data['pickup_data']->status ? $data['pickup_data']->status : ' - '}}<span class="text-c-green m-l-10"></span></p>
                    </div>
                    <div class="col-xl-3 col-md-6">
                      <h5>Pick Address</h5>
                        <p class="m-b-30 f-w-100">{{$data['pickup_data'] && $data['pickup_data']->pickupaddress && $data['pickup_data']->pickupaddress->address ? $data['pickup_data']->pickupaddress->address : ' - '}}<span class="text-c-green m-l-10"></span></p>
                    </div>
                    <div class="col-xl-3 col-md-6">
                      <h5>Drop Address</h5>
                        <p class="m-b-30 f-w-100">{{$data['pickup_data'] && $data['pickup_data']->dropaddress && $data['pickup_data']->dropaddress->address ? $data['pickup_data']->dropaddress->address : ' - '}}<span class="text-c-green m-l-10"></span></p>
                    </div>
                    
                    <div class="col-md-12">
                      {{-- <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" href="{{ route('notification.index') }}">Back</a>    --}}
                      <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" onclick="window.history.go(-1); return false;">Back</a>   
                    </div>
                    <!--end: Datatable-->
                  </div>
                  
                </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
</div>

@endsection

