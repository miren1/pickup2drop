@extends('layouts.app', ['title' => 'End User'])
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Toolbar-->
        <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center flex-wrap mr-2">
                    <i class="fa fa-user text-danger" style="transform: rotateY(160deg);" aria-hidden="true"></i>
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;&nbsp;User Management | User Detail</h5>
                    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                </div>
            </div>
        </div>
        <!--end::Toolbar-->
        <!--begin::Post-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div id="kt_content_container" class="container-lg">
                <div class="card">
                    <!--begin::Card header-->
                    <div class="card p-5">
                        <!--begin::Card header-->
                        <div class="card-toolbar p-5 mx-5">
                            <div class="mb-3 row">
                                <span class="font-weight-bold col-2">Name :</span>
                                <span class="col-10">{{$user && $user->name ? $user->name : '-' }}</span>
                            </div>


                            <div class="mb-3 row">
                                <span class="font-weight-bold col-2">Image :</span>
                                @if($user->profile_photo_path)
                                <span class="col-10"><img src="/{{$user->profile_photo_path}}" width="100px" alt="User Logo" srcset=""></span>
                                @else
                                <span class="col-10">-</span>
                                @endif
                            </div>

                            <div class="mb-3 row">
                                <span class="font-weight-bold col-2">Email :</span>
                                <span class="col-10">{{$user && $user->email ? $user->email : '-' }}</span>
                            </div>

                            <div class="mb-3 row">
                                <span class="font-weight-bold col-2">Mobile No :</span>
                                <span class="col-10">{{$user && $user->mobile_number ? $user->mobile_number : '-' }}</span>
                            </div>
                            @if($user->Roles && isset($user->Roles[0]))
                            <div class="mb-3 row">
                                <span class="font-weight-bold col-2">Assigned Role :</span>
                                <span class="col-10">{{$user->Roles[0]->name }}</span>
                            </div>
                            @endif 
                            <div class="col-md-12">
                                <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" href="{{ route('user.index') }}">Back</a>
                            </div>
                        </div>


                        


                    </div>
                </div>
            </div>
        </div>
    </div>
     <!--*********************************************** Delete comapany modal start from here ***********************************************-->

                <div class="modal fade" id="DeleteUserModal" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h6 class="modal-title">Delete User</h6>
                                <button type="button" class="close btn" data-bs-dismiss="modal">&times;</button>
                            </div>
                            <!-- Modal body -->
                            <div class="modal-body">
                                <p>Are you sure want to delete this User ?</p>
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" onclick="deleteUser()">Yes</button>
                                <button type="button" class="btn btn-default" data-bs-dismiss="modal">No</button>
                            </div>
                        </div>
                    </div>
                </div>

      
@endsection
@push('js')
<script type="text/javascript">
    var deleteId;

    function deletecall(id) {
        deleteId = btoa(id);
    }

    function deleteUser() {
        $.ajax({
            url: '/admin/user/' + deleteId,
            type: 'DELETE',
            dataType: 'JSON',
            data: {
                'id': deleteId,
                '_token': '{{ csrf_token() }}',
            },
            success: function(data) {
                $('#DeleteUserModal').modal('hide');

                toastr.success("User deleted successfully");
                setTimeout(() => {
                    window.location.replace('/admin/user');
                }, 3000);

            },
            error: function(xhr) {
                console.log(xhr.responseText);
            }
        })
    }
</script>
@endpush
<style>
    .font-weight-bold {
        font-weight: 600;
    }
</style>