@extends('layouts.app',['title'=>'Edit User'])
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Toolbar-->
       <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center flex-wrap mr-2">
                    <i class="fa fa-user text-danger" style="transform: rotateY(160deg);" aria-hidden="true"></i>
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;&nbsp;User Management | User Edit</h5>
                    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                </div>
            </div>
        </div>

        <!--end::Toolbar-->
        <!--begin::Post-->
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <!--begin::Container-->

            <div id="kt_content_container" class="container-lg">

                    <!--begin::Card header-->
                    <div class="card p-5">

                        <!--begin::Card body-->
                        <div class="card-body p-5">
                            <form action="{{route('user.update',$user->id)}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="mb-3">
                                    <label for="name" class="form-label">Name</label>
                                    <input type="text" name="name" class="form-control" id="name" value="{{ $user->name, old('name') }}" required>
                                </div>
                                <div class="mb-3">
                                    <label for="email" class="form-label">Email</label>
                                    <input type="email" name="email" class="form-control"  id="email" value="{{ $user->email, old('email') }}" required>
                                    <span id="errEmail" style="display: none; color:red">Enter Valid Email</span>
                                </div>
                                <div class="mb-3">
                                    <label for="mobile_no" class="form-label">Mobile Number</label>
                                    <input type="text" name="mobile_number" class="form-control" onkeypress="return event.charCode>=48 && event.charCode<=57" id="mobile_no" value="{{ $user->mobile_number, old('mobile_number') }}" required>
                                </div>
                                <div class="mb-5 row">
                                    <label for="role" class="form-label col-md-12">Assign Role</label>
                                    <div class="role-select col-md-12" >
                                    <select required name="role" class="form-control" id="sel_role" style="height: 40px" value="{{ 1, old('role') }}">
                                        <option disabled value="">----Select Role----</option>
                                        @foreach($roles as $role)
                                            <option value="{{$role->id}}" 
                                                @if($user->Roles && isset($user->Roles[0]))
                                                {{$role->id == $user->Roles[0]->id ? 'selected' : ''}}
                                                @endif
                                            >{{$role->name}}</option>
                                        @endforeach
                                    </select>
                                    </div>      
                                </div>
                                <br>
                                <div class="col-md-12 mt-5">
                                    <button type="submit" id="btn_save" class="btn btn-danger btn-lg">Update</button>
                                    <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" href="{{route('user.index')}}">Back</a>
                                    {{-- <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" href="{{ route('deliveryboy.index') }}">Back</a> --}}
                                </div>
                            </form>
                        </div>
                        <!--end::Card body-->
                    </div>

            </div>
            <!--end::Container-->
        </div>
        <!--end::Post-->
    </div>

@endsection
@push('js')
<script>
    $( "#email" ).on( "keyup", function() {
        var email = $('#email').val();
        var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
        if (!pattern.test(email)) {
            $('#errEmail').show();
            $('#btn_save').attr('disabled','disabled');
        }else {
            $("#btn_save").removeAttr("disabled");
            $('#errEmail').hide();
        }
    });
</script>
@endpush