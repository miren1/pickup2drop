@extends('layouts.app', ['title' => 'User'])
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center flex-wrap mr-2">
                    <i class="fa fa-user text-danger" style="transform: rotateY(160deg);" aria-hidden="true"></i>
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;&nbsp;User Management</h5>
                    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

                    <a href="{{route('user.create')}}" class="btn btn-light-danger font-weight-bolder btn-sm float-right"
                    data-keyboard="false" data-backdrop="static">Add New User</a>
                </div>
            </div>
        </div>
       
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-lg">
                <div class="card">
                    <div class="card">
                        <div class="card-body py-4">
                            <div id="kt_table_users_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                <div class="table-responsive">
                                    <table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer" id="kt_table_users">
                                        <thead>
                                            <tr class="text-start text-muted fw-bolder fs-7 text-uppercase gs-0">

                                                <th tabindex="0" aria-controls="kt_table_users">No
                                                </th>
                                                <th class="min-w-125px" tabindex="0" aria-controls="kt_table_users" rowspan="1" colspan="1" aria-label="User: activate to sort column ascending" style="width: 314.375px;">Name
                                                </th>
                                                <th class="min-w-125px" tabindex="0" aria-controls="kt_table_users" rowspan="1" colspan="1" style="width: 155.266px;">Role
                                                </th>
                                                <th class="min-w-125px" tabindex="0" aria-controls="kt_table_users" rowspan="1" colspan="1" style="width: 155.266px;">Mobile
                                                </th>                                              
                                                <th class="min-w-125px" tabindex="0" aria-controls="kt_table_users" rowspan="1" colspan="1" style="width: 155.266px;">Email
                                                </th>
                                                <th class="text-end min-w-100px sorting_disabled" rowspan="1" colspan="1" aria-label="Actions" style="width: 124.719px;">Actions
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <!--*********************************************** Delete comapany modal start from here ***********************************************-->

                <div class="modal fade" id="DeleteUserModal" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h6 class="modal-title">Delete User</h6>
                                <button type="button" class="close btn" data-bs-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <p>Are you sure want to delete this User ?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" onclick="deleteUser()">Yes</button>
                                <button type="button" class="btn btn-default" data-bs-dismiss="modal">No</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!--*********************************************** Delete company modal end ***********************************************-->

                


               
            </div>
        </div>
    </div>
@endsection
@push('js')
<script>
    $(document).ready(function(){
        $table = $("#kt_table_users").DataTable({
        'processing':true,
        'serverSide':true,
        "ordering": false,
        'stateSave' : true,
        'ajax': "{{route('user.index')}}",
        'columns':[
            { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
            { data : 'name' , defaultContent : 'N/A',class:'text-center'},
            { data : 'role',defaultContent : 'N/A',class:'text-center' },
            { data : 'mobile_number',defaultContent : 'N/A',class:'text-center' },
            { data : 'email',defaultContent : 'N/A',class:'text-center' },
            { "data": "id",
            render:function(data,type,row,meta){
                $id =btoa(row.id);
                $html = '<div class="dropdown mo-mb-2">'+
                                  '<a href="#" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                                      'Action'+
                                  '</a>'+

                                  '<div class="dropdown-menu" x-placement="bottom-start" style="cursor: pointer;">'+

                                      '<a class="dropdown-item d-block" href="{{ url('user/edit') }}/'+$id+'"><i class="fa fa-edit text-primary"></i>&nbsp; Edit</a>'+ ' ';

                                     $html+= '<a class="dropdown-item btn_delete d-block" data-id="'+row.id+'"><i class="fa fa-trash text-danger"></i>&nbsp; Delete</a>'+ '' ;

                                    $html+= '<a class="dropdown-item btn_show d-block"  data-id="'+row.id+'"><i class="fa fa-file text-info"></i>&nbsp; View</a>'+ '' ;
                                  '</div>'+
                              '</div>';
                      return $html;
            }
        },
        ],
        });
    });

    $(document).on("click", '.btn_show', function(event) {
            var id =$(this).attr('data-id');
            window.location.replace('user/show/'+btoa(id));
    });

    $(document).on("click", '.btn_delete', function(event) {
        var id =$(this).attr('data-id');
        swal({
            title: "Are you sure?",
            text: "You Want To Delete This User,You Will Not Be Able To Recover This User.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {

                $.ajax({
                    url: '{{ url('user') }}/'+id,
                    type: 'delete',
                    dataType: 'json',
                    "data":{"_token":"{{csrf_token()}}"},
                //data: {id: 'id'},
                success:function($data){
                    if($data == true)
                    {
                        swal("Success","User has been Deleted.", {
                            icon: "success",
                        });
                        $('#kt_table_users').DataTable().ajax.reload(null, false);
                    }else{
                        swal("Error","User has been not Deleted.", {
                            icon: "error",
                        });
                    }
                    //$('#city').DataTable().ajax.reload();
                }
            });


            } else {
                swal("Your User is Safe.");
            }
        });
    });
</script>

@endpush
