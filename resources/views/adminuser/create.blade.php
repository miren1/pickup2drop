@extends('layouts.app',['title'=>'Add User'])
@section('content')

    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Toolbar-->
         <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center flex-wrap mr-2">
                    <i class="fa fa-user text-danger" style="transform: rotateY(160deg);" aria-hidden="true"></i>
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;&nbsp;User Management | User Create</h5>
                    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                </div>
            </div>
        </div>

        <!--end::Toolbar-->
        <!--begin::Post-->
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <!--begin::Container-->
            <div id="kt_content_container" class="container-lg">
                <div class="card p-5">
                        <div class="card-body p-5">
                            <form action="{{route('user.store')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="mb-3">
                                    <label for="name" class="form-label">Name</label><span class="text-danger">*</span>
                                    <input type="text" name="name" class="form-control" id="name" value="{{ old('name') }}" required>
                                </div>
                                <div class="mb-3">
                                    <label for="email" class="form-label">Email</label><span class="text-danger">*</span>
                                    <input type="email" name="email" class="form-control" id="email" value="{{ old('email') }}" required>
                                    <span id="errEmail" style="display: none; color:red">Enter Valid Email</span>
                                </div>
                                <div class="mb-3">
                                    <label for="mobile_no" class="form-label">Mobile Number</label><span class="text-danger">*</span>
                                    <input type="text" min="-10" max="10" onkeyup="mobileCheck()"  onkeypress="return event.charCode>=48 && event.charCode<=57" name="mobile_number" class="form-control" id="mobile_no" value="{{ old('mobile_number') }}" required>
                                    <span class="mt-3" id="numberErr" style="color: red; display:none">Enter Valid Mobile Number</span>
                                </div>
                                <div class="mb-3" style="width: 100%;display:flex;">
                                    <div style="width: 93%;margin-right:2%" >
                                        <label for="password" class="form-label">Password</label><span class="text-danger">*</span>
                                        <input type="password" name="password"  data-bv-notempty="true"
                                            data-bv-stringlength="true"
                                            data-bv-stringlength-min="6"
                                            data-bv-notempty-message="Password  is required" class="form-control" id="password" value="{{ old('password') }}" required>
                                    </div>
                                    <div style="width: 5%;">
                                        {{-- <button >Show Password</button>\ --}}
                                        {{-- <i class="bi bi-eye-slash" id="togglePassword"></i> --}}
                                        {{-- <button onclick="showPassword()"  class="btn btn-danger btn-lg" ></button> --}}
                                        <div class="btn btn-danger btn-lg" id="togglePassword" style="padding: 8px;width: 100%;margin-top: 25px;" ><i  class="fa fa-eye" id="toogleeye" aria-hidden="true" style="padding-right:0"></i></div>
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="role" class="form-label col-md-12">Assign Role</label>
                                    <div class="role-select col-md-12">
                                        <select required name="role" class="form-control" id="sel_role" value="{{ old('role') }}" style="height: 40px">
                                            <option disabled selected value="">----Select Role----</option>
                                            @foreach($roles as $role)
                                                <option value="{{$role->id}}" @if($role->id == old('role')) selected @endif>{{$role->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <div class="col-md-12 mt-5">
                                    <button type="submit" id="btn_save" class="btn btn-danger btn-lg">Save</button>
                                    <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" href="{{route('user.index')}}">Back</a>
                                </div>
                            </form>
                        </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>

            function mobileCheck(key)

            {
            var input = $('#mobile_no').val();
            // alert(input)
            if(input.length > 10)
            {   
                // alert('hi')
                $('#numberErr').show();
                $('#btn_save').attr('disabled','disabled');
            }else if(input.length < 10)
            {
                $('#numberErr').show();
                $('#btn_save').attr('disabled','disabled');
            }
            else if(input.length == 10){
                $('#btn_save').removeAttr('disabled');
                $('#numberErr').hide();
                $('#mobileErr').hide();
            }
        }

        const togglePassword = document.querySelector("#togglePassword");
        const password = document.querySelector("#password");

        togglePassword.addEventListener("click", function () {
            const type = password.getAttribute("type") === "password" ? "text" : "password";
            password.setAttribute("type", type);
            $("#toogleeye").toggleClass("fa-eye-slash");

        });

         $( "#email" ).on( "keyup", function() {
        
        var email = $('#email').val();
        var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i

        if (!pattern.test(email)) {
            
            $('#errEmail').show();
            $('#btn_save').attr('disabled','disabled');
            
        }else {
            $("#btn_save").removeAttr("disabled");
            $('#errEmail').hide();

        }

    });
    </script>
@endpush