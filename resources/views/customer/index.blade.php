@extends('layouts.app')

@section('content')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <i class="fa fa-users text-danger" aria-hidden="true"></i>
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Customers</h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

                {{-- <a href="{{ url('admin/customers/add') }}" class="btn btn-light-warning font-weight-bolder btn-sm"
                data-keyboard="false" data-backdrop="static">Add New Customer</a> --}}
                <!--end::Actions-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--end::Notice-->
            <!--begin::Card-->
            <div class="card card-custom">
                {{-- <div class="card-header">
                    <div class="card-title">
                        <span class="card-icon">
                            <i class="icon-building text-danger" aria-hidden="true"></i>
                        </span>
                        <h3 class="card-label">Agencies</h3>

                         <a href="{{ url('admin/customers/add') }}" class="btn btn-light-danger font-weight-bolder btn-sm" data-keyboard="false"
                        data-backdrop="static">Add New Customer</a>
                    </div>
                </div> --}}
                <div class="card-body" style="overflow-x: auto;">
                    <!--begin: Datatable-->
                    <table class="table table-hover" id="dataTable">
                        <thead>
                            <tr>
                                <th class="tempclass">No</th>
                                <th>Name</th>
                                <th>Gender</th>
                                <th>E-mail</th>
                                <th>Wallet</th>
                                <th>Mobile</th>
                                {{-- <th>Email</th>
                                <th>Created_At</th> --}}
                            </tr>
                        </thead>
                    </table>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!-- Content Wrapper. Contains page content -->
@endsection

@push('js')
<script>

    $(document).ready(function() {

        $table = $("#dataTable").DataTable({
            "ajax": "{{ url('customer/show') }}",
            "processing": true,
            "serverSide": true,
            "ordering": true,
            'stateSave' : true,
            "pageLength": 100,
            "initComplete":function(){
                $(".tempclass").removeClass("sorting_asc")
            },
            "columns": [

               {
                data: "id",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1
                    }
                },
                {
                    data: "name"
                },
                {
                    data: "gender"
                },
                {
                    data: "email"
                },
                {
                    data: "wallet"
                },
                {
                    data: "mobile_number",
                    // "data": "id",
                        render:function(data,type,row,meta){
                            $id =btoa(row.id);
                            $html = '<div class="mo-mb-2"><a title="View" class="text-center" href="{{ url('pickup?id-') }}'+$id+'">'+row.mobile_number+'</a></div>';
                            return $html;
                        }

                },
        ],
            'columnDefs': [{orderable: false, targets: [0,1,2,3,5]}],
    });
    } );

</script>
@endpush
