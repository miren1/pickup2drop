@extends('layouts.app', ['title' => 'Roles and Permissions'])
@section('content')
    <div class="container-fluid">
        <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
             <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
                <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                    <div class="d-flex align-items-center flex-wrap mr-2">
                        <i class="fa fa-unlock-alt text-danger"></i>
                        <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;&nbsp;Assign Permissions</h5>
                        <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                    </div>
                </div>
            </div>
            
            <div class="post d-flex flex-column-fluid p-2" id="kt_post">
                <div id="kt_content_container" class="container-lg mt-5">
                    <div class="card">
                        <div class="card-header border-0 pt-6 col-md-12 mt-3">
                            <div class="role-select" style="width: 50%;display: grid;grid-template-columns: 0.2fr 0.5fr;">
                                <label style="line-height: 43px;font-size: 1.35rem;font-weight: 500;">Select Role</label>
                                <select class="form-control" id="sel_role" style="height: 40px">
                                    <option value="">----Select Role----</option>
                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}">{{$role->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div id="rolenote" class="col-12"><span class="text-danger"> * Please select any role for enable permissions</span></div>
                        </div>
                        <div class="card-body py-4">
                            <div id="kt_table_companies_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                <h3>Permissions</h3>
                                <div class="row">
                                    <div class="col-md-2 py-3 mt-5 mb-5">
                                        <label class="checkbox checkbox-success">
                                            <input class="form-check-input" id="chk_all_permission" name="chk_all_permission" type="checkbox">
                                            <span></span>
                                            <div class="mx-3">    
                                                <span class="fw-bold ps-2 fs-6">Select All</span>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <div class="">
                                    @foreach($permissions as $key=>$permissionlist)
                                    <div class="mt-5">
                                        <label class="form-check form-check-inline form-check-solid me-5 is-invalid">
                                            <div class="font-weight-bold col-md-12 list-disc"><li class="list-type-square">{{$key}}</li></div>
                                        </label>
                                        <div class="row p-3 mx-5">
                                            @foreach ($permissionlist as $permission)
                                                <div class="col-md-3 p-2 checkbox-list">
                                                    <label class="checkbox checkbox-success">
                                                        <input class="form-check-input checkone border mr-2 chk_permission_{{$permission->id}} {{$permission->name}}" id="{{$permission->name}}" name="permissions" type="checkbox" value="{{$permission->id}}">
                                                    <span></span>
                                                    <div><span class="fw-bold ps-2 fs-6 ml-5">{{ucfirst($permission->name)}}</span></div>
                                                </label>
                                                </div>
                                            @endforeach
                                        </div>                                        
                                    </div>
                                    @endforeach 
                                </div>
                                <div class="row my-5">
                                    <div class="col-md-12 d-flex justify-content-end">
                                        <button class="btn btn-danger" name="update_permission" id="update_permission">Save Permission</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">

    $(document).ready(function() {
        if ($(".checkone").length == $(".checkone:checked").length) {
            $("#chk_all_permission").prop("checked", true);
        }
        $("#chk_all_permission").click(function() {
            $('.checkone').prop('checked', this.checked);
        });

        $('.checkone').change(function() {
            if ($(".checkone").length == $(".checkone:checked").length) {
            $("#chk_all_permission").prop("checked", true);
            } else {
            $("#chk_all_permission").prop("checked", false);
            }
            // console.log("else",$(this).attr('id').lastIndexOf('-'));
            console.log($(this).attr('id').slice(0,$(this).attr('id').lastIndexOf('-')));
            
            if($(this).is(":checked") == false){
                if($(this).attr('id').split('-')[$(this).attr('id').split('-').length - 1] && $(this).attr('id').split('-')[$(this).attr('id').split('-').length - 1] == 'List'){
                    $.each(document.querySelectorAll(`[class*=${$(this).attr('id').split('-')[0]}]`),function(k,v){ 
                        $(v).prop("checked", false);
                    });
                }
            }else{
                
                if($(this).attr('id').split('-')[$(this).attr('id').split('-').length - 1] && $(this).attr('id').split('-')[$(this).attr('id').split('-').length - 1] != 'List'){
                    $.each(document.querySelectorAll(`[class*=${$(this).attr('id').slice(0,$(this).attr('id').lastIndexOf('-'))}-List]`),function(k,v){ 
                        $(v).prop("checked", true);
                    });
                }
            }
            

        });
    });


    $(document).ready(function(){
        $("input[type=checkbox]").attr('disabled',true);
    });
    $(document).on('change','#chk_all_permission',function(e){
        $('input:checkbox').not(this).prop('checked', this.checked);
    });
    $(document).on('change','#sel_role',function(e){
        let role_id = $(this).val();
        if(role_id){
            $('#rolenote').html("");
        }else{
            $('#rolenote').html("<span class='text-danger'> * Please select any role for enable permissions<span>");
        }
        $("input[type=checkbox]").prop('checked','');
        // $("input[type=checkbox]").removeAttr('checked');
        if(role_id == ''){
            $("input[type=checkbox]").attr('disabled',true);
            return false;
        }
        $("input[type=checkbox]").attr('disabled',false);
        $.ajax({
            url: 'role/getPermission/'+role_id,
            method: 'get',
            type: 'json',
            success:function(res){
                if(res.code == 302){
                    swal(res.message, {
                            icon: "success",
                        });
                }else{                    
                    $.each(res.data.permissions,function(k,v){
                        $('.chk_permission_'+v.id).prop('checked',true)
                    });
                    if ($(".checkone").length == res.data.permissions.length) {
                        $("#chk_all_permission").prop("checked", true);
                    }
                }
            }
        });
        
    });

    $(document).on('click','#update_permission',function(e){
        let permissions = [];
        let role = $("#sel_role").val();
        let role_name = $("#sel_role").find('option:selected').text();
        let allowed = true;
        let token = $("input:hidden[name='_token']").val();

        $("input:checkbox[name=permissions]:checked").each(function(){
            permissions.push($(this).val())
        });

        if(role == '' || role < 1){
            swal("Error","Please select any role.", {
                            icon: "error",
                        });
            return false;
        }

        // if(role_name == 'Admin')
        //     allowed = confirm("You are about to change admin's permission.\nAre you sure you want to update?");

        e.preventDefault();
        if(allowed){
            $.ajax({
                url:'/roles-permission/role/storepermission',
                method: 'POST',
                type: 'json',
                data: {'_token':token,'role_id':role,'permissions':permissions},
                success:function(response){
                    swal("Success",response.message, {
                            icon: "success",
                        });
                },
                error:function(status,error){
                    swal("Error",status.responseJSON.message, {
                            icon: "error",
                    });
                }
            });
        }
    });
    </script>
@endpush
