@extends('layouts.app', ['title' => 'Roles and Permissions'])
@section('content')
        <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
            <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
                <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                    <div class="d-flex align-items-center flex-wrap mr-2">
                        <i class="fa fa-universal-access text-danger" style="transform: rotateY(160deg);" aria-hidden="true"></i>
                        <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;&nbsp;Roles and Permissions</h5>
                        <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

                        <a class="btn btn-light-danger font-weight-bolder btn-sm float-right" data-toggle="modal" data-target="#kt_modal_add_role"
                        data-keyboard="false" data-backdrop="static">Add Role</a>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="kt_modal_add_role" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered mw-650px">
                    <div class="modal-content">
                        <div class="modal-header" id="kt_modal_add_role_header">
                            <h2 class="fw-bolder">Add Role</h2>
                            <div class="btn btn-icon btn-sm btn-active-icon-primary"
                                    data-dismiss="modal"
                                    data-kt-users-modal-action="close">
                                <span class="svg-icon svg-icon-1">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                    height="24" viewBox="0 0 24 24" fill="none">
                                                <rect opacity="0.5" x="6" y="17.3137" width="16"
                                                        height="2" rx="1"
                                                        transform="rotate(-45 6 17.3137)"
                                                        fill="black"></rect>
                                                <rect x="7.41422" y="6" width="16" height="2"
                                                        rx="1" transform="rotate(45 7.41422 6)"
                                                        fill="black"></rect>
                                            </svg>
                                        </span>
                            </div>
                        </div>
                        <div class="modal-body scroll-y mx-5 mx-xl-15 my-7">
                            <form id="kt_modal_add_role_form"
                                    class="form fv-plugins-bootstrap5 fv-plugins-framework"
                                    method="post"
                                    action="{{ route('role.store') }}">
                            @csrf
                                <div class="d-flex flex-column scroll-y me-n7 pe-7"
                                        id="kt_modal_add_role_scroll" data-kt-scroll="true"
                                        data-kt-scroll-activate="{default: false, lg: true}"
                                        data-kt-scroll-max-height="auto"
                                        data-kt-scroll-dependencies="#kt_modal_add_role_header"
                                        data-kt-scroll-wrappers="#kt_modal_add_role_scroll"
                                        data-kt-scroll-offset="300px" style="max-height: 373px;">
                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                        <label class="required fw-bold fs-6 mb-2">Role
                                            Name</label>
                                        <input required type="text" id="role_name" name="role_name"
                                                class="form-control"
                                                placeholder="Role Name" autocomplete="off">
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="text-center pt-15">
                                    <button type="submit" class="btn btn-danger mx-2"
                                            data-kt-users-modal-action="submit">
                                        <span class="indicator-label">Submit</span>
                                    </button>
                                    <button type="button" class="btn btn-light mx-2"
                                            data-dismiss="modal"
                                            data-kt-users-modal-action="cancel">Discard
                                    </button>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="post d-flex flex-column-fluid" id="kt_post">
                <div id="kt_content_container" class="container-lg">
                    <div class="card">
                        <div class="card-header border-0 pt-6 col-md-12">
                            <div class="card-body py-4">
                            <div id="kt_table_role_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                <div class="table-responsive">
                                    <table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer" id="kt_table_role">
                                        <thead>
                                            <tr class="text-start text-muted fw-bolder fs-7 gs-0">
                                                <th class="min-w-125px" tabindex="1" aria-controls="kt_table_role" style="width: 155.266px;">No.</th>
                                                <th class="min-w-125px" tabindex="1" aria-controls="kt_table_role" style="width: 155.266px;">Role Name</th>
                                                <th class="text-center" rowspan="1" colspan="1" aria-label="Actions" style="width: 124.719px;">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@push('js')
<script>

$(function() {
        $('#role_name').on('focusout', function(e) {
            $(this).val($.trim($(this).val()));
        });
});

$(document).ready(function(){
$table = $("#kt_table_role").DataTable({
'processing':true,
'serverSide':true,
"ordering": false,
'stateSave' : true,
'ajax': "{{ route('role_permission.index') }}",
'columns':[
{ data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
{ data : 'name' },
{data: "action" , class:"text-center"}
],
'columnDefs': [{ orderable: true, targets: 0 }],
});
$("#kt_table_departments_filter input").addClass('p-2');
});
    var deleteId;
     function deletecall(id) {
         deleteId = id;

        $.ajax({
            type: "GET",
            dataType: "json",
            url: `roles-permission/assign/${deleteId}`,
            success: function(data) {
                if(data.assign == true){
                    toastr.error('Role can not delete it is assign to user ');
                }else{
                    $('#DeleteRoleModal').modal('show');
                }
            }
        });
    }

    function deleteRole(deleteId) {
        swal({
            title: "Are you sure?",
            text: "You Want To Delete This Role,You Will Not Be Able To Recover This Role.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {

                $.ajax({
                    url: '{{ url('roles-permission/destroy/') }}/'+deleteId,
                    type: 'POST',
                    dataType: 'json',
                    "data":{"_token":"{{csrf_token()}}"},
                success:function($data){
                    if($data['assign'] == true){
                        swal("Error","Role has been already assign to the user ,Please remove role from the user to delete this role.", {
                            icon: "error",
                        });
                    }else{
                        if($data['status']  == true){
                            swal("Role has been Deleted.", {
                                icon: "success",
                            });
                            $('.dataTable').DataTable().ajax.reload(null, false);
                        }else{
                            swal("Role has been not Deleted.", {
                                icon: "error",
                            });
                        }
                    }

                }
            });


            } else {
                swal("Your Role is Safe.");
            }
        });
    };
    </script>
@endpush
