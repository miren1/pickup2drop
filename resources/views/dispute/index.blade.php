@extends('layouts.app')

@section('content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <!-- <i class="fas fa-sliders-h"></i> -->
                <!-- <i class="fas fa-shipping-fast"></i>  -->
                <i class="fas fa-shipping-fast text-danger"></i>
                <!-- <i class="fas fa-sliders-h text-danger" aria-hidden="true"></i> -->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Dispute Order</h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

                <!-- <a href="{{ url('subscription/add') }}" class="btn btn-light-danger font-weight-bolder btn-sm"
                data-keyboard="false" data-backdrop="static">Add New</a>   -->
                <!-- <a href="" class="btn btn-light-danger font-weight-bolder btn-sm"
                data-keyboard="false" data-backdrop="static">Add New Slider</a> -->
                 <!-- <button class="btn btn-light-danger font-weight-bolder btn-sm"
                data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#myModal">Add New Slider</button> -->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--end::Notice-->
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-body" style="overflow-x: auto;">
                    <!--begin: Datatable-->
                    <table class="table table-hover" id="myTable">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Mobile Number</th>
                                <th>Customer Name</th>
                                <th>Order Id</th>
                                {{-- <th>Dispute</th> --}}
                                <!-- <th>Price/ %</th> -->
                                <!-- <th>Max Amount</th> -->
                                <!-- <th style="width: 80px;">End Date</th> -->
                                <th>Status</th>
                                <th style="width: 240px;">Action</th>
                            </tr>
                        </thead>
                    </table>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
</div>

@endsection

@push('js')
<script>
    $(function() {

    	$('#editForm').bootstrapValidator();
        $(document).on('click', '#closeModal', function(e) {
            e.preventDefault();
            /* Act on the event */
            $('.perc_price').html('Percentage/Price');
            $('#description').val("");
            $('#editForm').bootstrapValidator('resetForm', true);
            $('.upto_amount').css('display','none');
            $('.promo').css('display','none');
            $('#error').css('display','none');
            $("#btn_save").html("Save");
            $('#id').val("");
        });





        $(document).ready(function() {
            $table = $("#myTable").DataTable({
                'serverMethod': 'get',
                "ajax": {
                    'url':"{{ route('dispute.show_list') }}",
                    "data":{"_token":"{{csrf_token()}}"},
                },
                "processing": true,
                "serverSide": true,
                "ordering": false,
                'stateSave' : true,
                "columns": [
                  /*{ "data": "id",
                    render:function(data,type,row,meta){
                        return meta.row + 1;
                    }
                },*/
                { "data": "created_at" },
                { "data": "mobile_number" ,name:'user.mobile_number'},
                { "data": "customer_name" ,render:function(data,type,row,meta){
                    return row.user.name;
                }},
                { "data": "order_id" },
                // { "data": "dispute",
                //    render: function(data,type,row,meta){
                //        console.log(row);
                //      var data = row.dispute;
                //      if(data.length >= 20)
                //      {
                //         return data.substr(0,15) + '....';
                //      }

                //      return  data;
                //    }
                // },

                { "data": "status" ,
                render:function(data,type,row,meta){
                    console.log(row.status);
                    if(row.status == 'Valid'){
                        return '<span class="badge badge-success">&nbsp;&nbsp;&nbsp;&nbsp; Valid &nbsp;&nbsp;&nbsp;&nbsp;</span>';

                    }
                    else if(row.status == 'Invalid'){
                        return '<span class="badge badge-warning text-white">&nbsp;&nbsp;&nbsp; Invalid &nbsp;&nbsp;</span>';

                    }
                    else if(row.status == 'Pending'){
                        return '<span class="badge badge-danger">&nbsp; Pending &nbsp;</span>';

                    }
                }
            },

                { "data": "id",
                render:function(data,type,row,meta){
                    $id =btoa(row.id);
                    $html = '<div class="mo-mb-2"><div  x-placement="bottom-start" style="cursor: pointer;">';
                        $html+= '<a href="dispute-order/show/'+$id+'" class="px-2"><i class="fas fa-eye  text-primary mr-2"></i><span class="text-dark"> View</span></a>' ;
                                    // if(row.status == 'Pending'){
                                    //     $html+='<a class="btn_valid px-2" data-id="'+row.id+'"><i class="fas fa-check-square text-primary mr-2"></i><span class="text-dark">   Valid</span></a> '+' ';
                                    //     $html+= '<a class="btn_invalid px-2" data-id="'+row.id+'"><i class="fas fa-times-circle  text-danger mr-2"></i> <span class="text-dark">Invalid</span></a>' ;
                                    // }
                                  '</div>'+
                              '</div>';
                      return $html;
                    }
                },
                ]
            });
        });

        $(document).on('click', '.edit_plan', function(e) {
            // document.querySelector('#file').required = false;
            // // $('#file').prop('required',false);
            // $('#file').removeAttr('data-bv-notempty');
            // $('#file').removeAttr('bv-notempty-message');
            // $('#file').removeAttr('data-bv-field');
            $('#editForm').bootstrapValidator('enableFieldValidators', 'file', false);

            e.preventDefault();
            /* Act on the event */
            var id = $(this).data('id');
            var title = $(this).data('title');
            var description = $(this).data('description');
            var slider_type = $(this).data('slider_type');
            var image_url = $(this).data('image_url');
            var file = $(this).data('file');

            $("#id").val(id);
            $("#title").val(title);
            $("#description").val(description);
            $("#slider_type").val(slider_type).trigger('change');
            $("#image_url").val(image_url);
            $("#file").val(file);
            $("#btn_save").html("Edit");

        });




        $(document).on("click", '.btn_valid', function(event) {
            var id =$(this).attr('data-id');
            swal({
                title: "Are you sure?",
                text: "Dispute is Valid!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: '{{ route('dispute.check') }}',
                        type: 'post',
                        data: {
                            dispute_id: id,_token:"{{csrf_token()}}",
                            type: 'Valid'
                        },
                        dataType: 'json',
                    success: function(response) {
                        console.log('response',response);

                        if (response.status == "success") {
                            swal(response.message, {
                                icon: 'success',
                            }).then((result) => {
                                $('#myTable').DataTable().ajax.reload(null,false);
                            });
                        } else {
                            swal("Error","Something Wrong try agian..",{icon:'error'});
                        }
                    }
                });


                } else {
                    swal("Your Dispute is Safe.");
                }
            });
        });


        $(document).on("click", '.btn_invalid', function(event) {
            var id =$(this).attr('data-id');
            swal({
                title: "Are you sure?",
                text: "Dispute is Invalid!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: '{{ route('dispute.check') }}',
                        type: 'post',
                        data: {
                            dispute_id: id,_token:"{{csrf_token()}}",
                            type: 'Invalid'
                        },
                        dataType: 'json',
                    success: function(response) {
                        if (response.status == "success") {
                            swal(response.message, {
                                icon: 'success',
                            }).then((result) => {
                                $('#myTable').DataTable().ajax.reload(null,false);
                            });
                        } else {
                            swal("Error","Something Wrong try agian..",{icon:'error'});
                        }
                    }
                });


                } else {
                    swal("Your Dispute is Safe.");
                }
            });
        });



    });
</script>
@endpush
