@extends('layouts.app')

@section('content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <!-- <i class="fas fa-sliders-h"></i> -->
                <!-- <i class="fas fa-shipping-fast"></i>  -->
                <i class="fas fa-shipping-fast text-danger"></i>
                <!-- <i class="fas fa-sliders-h text-danger" aria-hidden="true"></i> -->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Dispute Detail</h5>


                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
            </div>
            <!--end::Info-->
        </div>
    </div>
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--end::Notice-->
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-body row" style="overflow-x: auto;">


                    <!--begin: Datatable-->
                        <div class="col-xl-3 col-md-6">
                      <h5>Pickup ID</h5>
                        <p class="m-b-30 f-w-100">{{$data['dispute_data'] && $data['pickup_data']->id ? $data['pickup_data']->id : ' - '}}<span class="text-c-green m-l-10"></span></p>
                    </div>
                    <div class="col-xl-3 col-md-6">
                      <h5>Item Name</h5>
                        <p class="m-b-30 f-w-100">{{$data['pickup_data'] && $data['pickup_data']->item_name ? $data['pickup_data']->item_name : ' - '}}<span class="text-c-green m-l-10"></span></p>
                    </div>
                    <div class="col-xl-3 col-md-6">
                      <h5>Item description</h5>
                        <p class="m-b-30 f-w-100 ">{{$data['pickup_data'] && $data['pickup_data']->description ? $data['pickup_data']->description : ' - '}}<span class="text-c-green m-l-10"></span></p>
                    </div>
                    <div class="col-xl-3 col-md-6">
                      <h5>Delivery Agent</h5>
                        <p class="m-b-30 f-w-100">{{$data['pickup_data'] && $data['pickup_data']->boy && $data['pickup_data']->boy->name ? $data['pickup_data']->boy->name : ' - '}}<span class="text-c-green m-l-10"></span></p>
                    </div>
                    <div class="col-xl-12 col-md-12 ">
                      <h5 class="f-w-100">Dispute Description</h5>
                        <p class="m-b-30 text-justify">{{$data['dispute_data'] && $data['dispute_data']->dispute ? $data['dispute_data']->dispute : ' - '}}<span class="text-c-green m-l-10"></span></p>
                    </div>
                    <div class="col-xl-3 col-md-6">
                      <h5>Customer Name</h5>
                        <p class="m-b-30 f-w-100">{{$data['dispute_data'] && $data['dispute_data']->user && $data['dispute_data']->user['name'] ? $data['dispute_data']->user['name'] : ' - ' }}<span class="text-c-green m-l-10"></span></p>
                    </div>
                    <div class="col-xl-3 col-md-6">
                      <h5>Customer Mobile</h5>
                        <p class="m-b-30 f-w-100">{{$data['dispute_data'] && $data['dispute_data']->user && $data['dispute_data']->user['mobile_number'] ? $data['dispute_data']->user['mobile_number'] : ' - '}}<span class="text-c-green m-l-10"></span></p>
                    </div>


                    <div class="col-xl-3 col-md-6">
                      <h5>Delivery Status</h5>
                        <p class="m-b-30 f-w-100">{{$data['pickup_data'] && $data['pickup_data']->status ? $data['pickup_data']->status : ' - '}}<span class="text-c-green m-l-10"></span></p>
                    </div>
                    <div class="col-xl-3 col-md-6">
                      <h5>Order Amount</h5>
                      <input type="hidden" value="{{$data['pickup_data'] && $data['pickup_data']->amount ? $data['pickup_data']->amount : 0}}">
                        <p class="m-b-30 f-w-100" >{{$data['pickup_data'] && $data['pickup_data']->amount ? $data['pickup_data']->amount : 0}}<span class="text-c-green m-l-10"></span></p>
                    </div>
                    <div class="col-xl-3 col-md-6">
                      <h5>Original Amount</h5>
                      <input type="hidden"  value="{{$data['pickup_data'] && $data['pickup_data']->original_amount ? $data['pickup_data']->original_amount : 0}}">
                        <p class="m-b-30 f-w-100" >{{$data['pickup_data'] && $data['pickup_data']->original_amount ? $data['pickup_data']->original_amount : 0}}<span class="text-c-green m-l-10"></span></p>
                    </div>
					
					<div class="col-xl-3 col-md-6">
                      <h5>Paid Amount</h5>
                      <input type="hidden" id="amount" value="{{$data['pickup_data'] && $data['pickup_data']->paid_amount ? $data['pickup_data']->paid_amount : 0}}">
                        <p class="m-b-30 f-w-100" >{{$data['pickup_data'] && $data['pickup_data']->paid_amount ? $data['pickup_data']->paid_amount : 0}}<span class="text-c-green m-l-10"></span></p>
                    </div>
					
                    <div class="col-xl-3 col-md-6">
                      <h5>Pick Address</h5>
                        <p class="m-b-30 f-w-100">{{$data['pickup_data'] && $data['pickup_data']->pickupaddress && $data['pickup_data']->pickupaddress->address ? $data['pickup_data']->pickupaddress->address : ' - '}}<span class="text-c-green m-l-10"></span></p>
                    </div>
                    <div class="col-xl-3 col-md-6">
                      <h5>Drop Address</h5>
                        <p class="m-b-30 f-w-100">{{$data['pickup_data'] && $data['pickup_data']->dropaddress && $data['pickup_data']->dropaddress->address ? $data['pickup_data']->dropaddress->address : ' - '}}<span class="text-c-green m-l-10"></span></p>
                    </div>
                    <div class="col-xl-3 col-md-6">
                      <h5>Dispute Status</h5>
                        @if($data['dispute_data']->status == 'Valid')
                            <span class="badge badge-success">&nbsp;&nbsp;&nbsp;&nbsp; Valid &nbsp;&nbsp;&nbsp;&nbsp;</span>
                        @elseif($data['dispute_data']->status == 'Invalid')
                            <span class="badge badge-warning text-white">&nbsp;&nbsp;&nbsp; Invalid &nbsp;&nbsp;</span>
                        @elseif($data['dispute_data']->status == 'Pending')
                            <span class="badge badge-danger">&nbsp; Pending &nbsp;</span>
                        @else
                            <p> - </p>
                        @endif
                    </div>


                    <div class="col-md-12 text-right">
                      @if($data['dispute_data'] && $data['dispute_data']->status && $data['dispute_data']->status == 'Pending')
                      <a class="btn btn-outline-success p-3 btn_valid px-2 cursor-pointer ml-3" data-id="{{$data['dispute_data']->id}}"><i class="fas fa-check-square  mr-2"></i><span class="pr-3">   Valid</span></a>
                      <a class="btn btn-outline-warning  p-3 btn_invalid px-2 cursor-pointer ml-3" data-id="{{$data['dispute_data']->id}}"><i class="fas fa-times-circle  mr-2"></i> <span class="pr-3">Invalid</span></a>
                      @endif
                      <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 " onclick="window.history.go(-1); return false;">Back</a>
                      {{-- <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 " href="{{ route('dispute.index') }}">Back</a> --}}
                    </div>

                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
</div>

@endsection
@push('js')
<script>
  $(document).on("click", '.btn_valid', function(event) {
            var id =$(this).attr('data-id');
            amount = parseFloat($('#amount').val()).toFixed(2);
            swal({
                title: "Are you sure?",
                content: "input",
                text: `Dispute is Valid, Please enter refund amount upTo ${amount} !`,
                type:"input",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: '{{ route('dispute.check') }}',
                        type: 'post',
                        data: {
                            dispute_id: id,_token:"{{csrf_token()}}",
                            type: 'Valid',amount:willDelete
                        },
                        dataType: 'json',
                    success: function(response) {
                        console.log('response',response);

                        if (response.status == "success") {
                            swal(response.message, {
                                icon: 'success',
                            }).then((result) => {
                                window.location.reload();
                            });
                        } else if(response.status == "error"){
                            console.log(response.message);
                              swal(response.message, {
                                icon: 'error',
                            }).then((result) => {
                                window.location.reload();
                            });
                        }else {
                            swal("Error","Something Wrong try agian..",{icon:'error'});
                        }
                    }
                });


                } else {
                    swal("Your Dispute is Safe.");
                }
            });
        });


        $(document).on("click", '.btn_invalid', function(event) {
            var id =$(this).attr('data-id');
            swal({
                title: "Are you sure?",
                text: "Dispute is Invalid!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: '{{ route('dispute.check') }}',
                        type: 'post',
                        data: {
                            dispute_id: id,_token:"{{csrf_token()}}",
                            type: 'Invalid'
                        },
                        dataType: 'json',
                    success: function(response) {
                        // console.log(response);
                        if (response.status == "success") {
                        //   console.log(response.message);
                            swal(response.message, {
                                icon: 'success',
                            }).then((result) => {
                                window.location.reload();
                            });
                        }else if(response.status == "error"){
                            console.log(response.message);
                              swal(response.message, {
                                icon: 'error',
                            }).then((result) => {
                                window.location.reload();
                            });
                        } else {
                            // swal("Error","Something Wrong try agian..",{icon:'error'});
                        }
                    }
                });


                } else {
                    swal("Your Dispute is Safe.");
                }
            });
        });
</script>
@endpush
