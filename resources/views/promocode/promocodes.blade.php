@extends('layouts.app')

@section('content')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <i class="fa fa-gift text-danger" aria-hidden="true"></i>

                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Promocodes</h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

                <button class="btn btn-light-danger font-weight-bolder btn-sm"
                data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#myModal">Add New</button>
                <!--end::Actions-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--end::Notice-->
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-body" style="overflow-x: auto;">
                    <!--begin: Datatable-->
                    <table class="table table-hover" id="myTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Promocode</th>
                                <th>Order Type</th>
                                <th>Code Type</th>
                                <th>Code Time</th>
                                <th>Price/ %</th>
                                <th>Minimum Order Amount</th>
                                <th>Max Amount</th>
                                <th>One Day</th>
                                <th style="width: 80px;">Created Date</th>
                                <th style="width: 80px;">End Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                    <!--end: Datatable-->
                    <div class="container">
                        <div class="modal fade" id="myModal">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h4 class="modal-title">Promocode</h4>
                                        <button type="button" class="close" id="closeModal"
                                            data-dismiss="modal">&times;</button>
                                    </div>
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <h4 id="error-edit" style="display: none;" class='alert alert-danger'></h4>
                                        <form id="editForm" method="POST" class="form-horizontal">
                                            @csrf
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <input type="hidden" name="id" id="id">
                                                    <label>Promocode</label>
                                                    <input type="text" oninput="this.value = this.value.toUpperCase()" id="promocode" name="promocode" class="form-control" placeholder="Promocode" required
                                                    data-bv-notempty="true"
                                                    data-bv-notempty-message="Promocode is required">
                                                    <span style="display: none;" class="text-danger promo"><small>Promocode Already Exist</small></span>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label>Order Type</label>
                                                    <select class="form-control" id="order_type" name="order_type"
                                                    required
                                                    data-bv-notempty="true"
                                                    data-bv-notempty-message="Order Type is required">
                                                        <option value=""> -- Select Order Type -- </option>
                                                        <option value="all">All</option>    
                                                        <option value="pickup to drop">Pickup to Drop</option>
                                                        {{-- <option value="pickup urgent">Pickup Urgent</option> --}}
                                                        <option value="medicine">Medicine</option>
                                                        <option value="food">Food</option>
                                                        <option value="subscription">Subscription</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label>Promocode Type</label>
                                                    <select class="form-control" id="code_type" name="code_type"
                                                    required
                                                    data-bv-notempty="true"
                                                    data-bv-notempty-message="Code Type is required">
                                                        <option value=""> -- Select Code Type -- </option>
                                                        <option value="percentage">Percentage</option>
                                                        <option value="price">Price</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label class="perc_price">Percentage/Price</label>
                                                    <input type="number" min="0" id="perc_price" name="perc_price" max=""  class="form-control" placeholder="Percentage/Price" required
                                                    data-bv-notempty="true"
                                                    data-bv-notempty-message="Percentage/Price is required">
                                                    <small style="display: none;" class="text-danger pmore">Please Insert Percentage value under 100</small>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label>Promocode Time</label>
                                                    <input type="number" min="0" id="code_time" name="code_time" class="form-control" placeholder="Promocode Time" required
                                                    data-bv-notempty="true"
                                                    data-bv-notempty-message="Time is required">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label>Apply Only Once Per Day</label>
                                                    <select class="form-control" id="one_day" name="one_day" required data-bv-notempty="true" data-bv-notempty-message="Field is required">
                                                        <option value=""> -- Select Yes or No -- </option>
                                                        <option value="Yes">Yes</option>
                                                        <option value="No">No</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4 upto_amount" style="display: none;">
                                                    <label>Max Amount</label>
                                                    <input type="number" min="0" id="upto_amount" name="upto_amount" class="form-control" placeholder="Discount Up To" required
                                                    data-bv-notempty="true"
                                                    data-bv-notempty-message="Max Amount is required">
                                                </div>
                                                 <div class="form-group col-md-4 min_amount" style="display: none;">
                                                    <label>Minimum Order Amount</label>
                                                    <input type="number" min="0" id="min_amount" name="min_order_amount" class="form-control" placeholder="Minimum Order Amount" required
                                                    data-bv-notempty="true"
                                                    data-bv-notempty-message="Minimum Order Amount is required">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label>End Date</label>
                                                    <input type="date" onkeydown="return false"  min="{{date('Y-m-d')}}" id="enddate" name="enddate" class="form-control" placeholder="Promocode End Date" required
                                                    data-bv-notempty="true"
                                                    data-bv-notempty-message="End Date is required">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label>Description</label>
                                                    <textarea class="form-control" rows="2" id="description" name="description" placeholder="Promocode Description"></textarea>
                                                </div>
                                            </div>
                                           
                                            <!-- /.card-body -->
                                          
                                    </div>
                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                         <button type="submit" id="btn_save" class="btn btn-danger btn-lg float-left mx-2">Save</button>
                                         {{-- <button type="submit" id="btn_save" class="btn btn-danger btn-lg float-left mx-2">Save</button> --}}
                                        <button type="button" class="btn btn-secondary btn-lg mx-2" id="closeModal"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!-- Content Wrapper. Contains page content -->
@endsection

@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<script>

    //create and store promocode
    $(function() {
        // $('.select2').select2();
        $(document).on('change', '#code_type', function() {
            var per_price = $('#code_type').val();
            //console.log(per_price);
            if (per_price==="percentage")
            {
                $('.perc_price').html('Percentage');
                $('#perc_price').attr('max','100');
                $('.upto_amount').css('display','block');
                $('.min_amount').css('display','none');
                $('#min_amount').val("");
            }
            else if(per_price==="price")
            {
                $('.perc_price').html('Price');
                // $("#perc_price").attr('max',false);
                 $('#perc_price').removeAttr('max');
                $('.upto_amount').css('display','none');
                $('#upto_amount').val("");
                $('.min_amount').css('display','block');
            }
            else
            {
                $('.perc_price').html('Percentage/Price');
                $('.upto_amount').css('display','none');
                $('#upto_amount').val("");
                $('#perc_price').removeAttr('max');
            }
        });


        $('#editForm').bootstrapValidator()
            .on('success.form.bv', function(e) {
                e.preventDefault();
                var val = $('#id').val();
                $.ajax({
                    type: "POST",
                    url: (val=="")?"{{ route('promocode.store') }}":"{{ route('promocode.update') }}",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(result) {
                        if(result.status==="unauthorized_access"){
                            swal("Error","Unauthorized access.",{icon:'error'});
                            $('#myModal').modal('hide');
                        }else{
                            if(result.status==="exist")
                        {
                            $('.promo').css('display','block');
                        }
                        if(result.status==="more")
                        {
                            $('.pmore').css('display','block');
                        }
                        swal(result.title, result.message, result.icon);

                        if (result.icon === "success") {
                            $('#editForm').bootstrapValidator('resetForm', true);
                            $('.upto_amount').css('display','none');
                            $('.promo').css('display','none');
                            $('#myModal').modal('hide');
                            $("#btn_save").html("Save");
                            $('#description').val("");
                            $('#id').val("");
                            $('#myTable').DataTable().ajax.reload(null,false);
                        }else{
                            $('.promo').css('display','none');
                            $("#error-edit").html("Something Wrong Try Again...");
                            $("#error-edit").css("display", "block");
                        }
                        }
                        
                    }
                });
        });

        //edit promocode set data
        $(document).on('click', '.edit_plan', function(e) {
            e.preventDefault();
            /* Act on the event */
            var id = $(this).data('id');
            var promocode = $(this).data('promocode');
            var order_type = $(this).data('order_type');
            var code_type = $(this).data('code_type');
            var code_time = $(this).data('code_time');
            var perc_price = $(this).data('perc_price');
            var description = $(this).data('description');
            var upto_amount = $(this).data('upto_amount');
            var enddate = $(this).data('enddate');
            var one_day = $(this).data('one_day');
            var min_amount = $(this).data('min_amount');
            // alert(one_day);
            enddatearr = enddate.split("-");
            enddate = moment(new Date(enddatearr[2],enddatearr[1]-1,enddatearr[0])).format('YYYY-MM-DD');

            $("#id").val(id);
            $("#promocode").val(promocode);
            $("#order_type").val(order_type);
            $("#code_type").val(code_type).trigger('change');
            $("#code_time").val(code_time);
            $("#one_day").val(one_day);
            $("#perc_price").val(perc_price);
            $("#upto_amount").val(upto_amount);
            $("#enddate").val(enddate);
            $("#description").val(description);
            $("#min_amount").val(min_amount);
            $("#btn_save").html("Update");

        });

        $(document).on('click', '#closeModal', function(e) {
            e.preventDefault();
            /* Act on the event */
            $('.perc_price').html('Percentage/Price');
            $('#description').val("");
            $('#editForm').bootstrapValidator('resetForm', true);
            $('.upto_amount').css('display','none');
            $('.promo').css('display','none');
            $('#error').css('display','none');
            $('#id').val("");
            setTimeout(() => {
                $("#btn_save").html("Save");
            }, 100);
        });
    });

   $(document).ready(function() {
        $table = $("#myTable").DataTable({
            'serverMethod': 'post',
            "ajax": {
                'url':"{{ route('promocode.show') }}",
                "data":{"_token":"{{csrf_token()}}"},
            },
            "processing": true,
            "serverSide": true,
            'stateSave' : true,
            "ordering": false,
            "columns": [
              /*{ "data": "id",
                render:function(data,type,row,meta){
                  // console.log(meta);
                    return meta.row + 1;
                }
            },*/
            {
                data: "id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1
                }
            },
            { "data": "promocode" },
            { "data": "order_type",
                render:function(data,type,row,meta){
                    return `<span class="text-uppercase">${row.order_type}</span>`
                }
            },
            { "data": "code_type",
                render:function(data,type,row,meta){
                    return (row.code_type==="price")?"<i class='fas fa-rupee-sign text-danger' aria-hidden='true'></i>":"<i class='fa fa-percent text-danger' aria-hidden='true'></i>";
                }
            },
            { "data": "code_time" },
            { "data": "perc_price" },
            { "data": "min_order_amount",
                render:function(data,type,row,meta){
                    return (row.min_order_amount)?row.min_order_amount:"-";
                }
            },
            { "data": "upto_amount",
                render:function(data,type,row,meta){
                    return (row.upto_amount)?row.upto_amount:"-";
                }
            },
            { "data": "one_day"},
            { "data": "created_at" },
            { "data": "enddate" },
            { "data": "id",
            render:function(data,type,row,meta){
                $id =btoa(row.id);
                $html = '<div class="dropdown mo-mb-2">'+
                              '<a href="#" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                                  'Action'+
                              '</a>'+

                              '<div class="dropdown-menu" x-placement="bottom-start" style="cursor: pointer;">'+
                                  '<a class="dropdown-item edit_plan" data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#myModal" data-id="'+row.id+'"data-order_type="'+row.order_type+'"data-code_type="'+row.code_type+'"data-code_time="'+row.code_time+'"data-promocode="'+row.promocode+'"data-description="'+row.description+'"data-one_day="'+row.one_day+'"data-upto_amount="'+row.upto_amount+'"data-min_amount="'+row.min_order_amount+'"data-enddate="'+row.enddate+'"data-perc_price="'+row.perc_price+'"><i class="fa fa-edit text-primary"></i> Edit</a>'+' ';

                                 $html+= '<a class="dropdown-item btn_delete" data-id="'+row.id+'"><i class="fa fa-trash text-danger"></i> Delete</a>' ;
                              '</div>'+
                          '</div>';
                  return $html;
                }
            },
            ]
        });
    });

   $(document).on("click", '.btn_delete', function(event) {
        var id =$(this).attr('data-id');
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this Promocode.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {

                $.ajax({
                    url: '{{ route('promocode.destroy') }}',
                    type: 'post',
                    data: {promo_id: id,_token:"{{csrf_token()}}"},
                    dataType: 'json',
                success: function(response) {
                    if(response.status == 'unauthorized_access'){
                        swal("Error","Unauthorized access.",{icon:'error'});
                    }
                    else if (response.status == "success") {
                        swal(response.message, {
                            icon: 'success',
                        }).then((result) => {
                            $('#myTable').DataTable().ajax.reload(null,false);
                        });
                    } else {
                        swal("Error","Something Wrong try agian.",{icon:'error'});
                    }
                }
            });


            } else {
                swal("Your PromoCode is Safe.");
            }
        });
    });

</script>
@endpush
