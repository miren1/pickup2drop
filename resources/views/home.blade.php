@extends('layouts.app')

@push('css')

@endpush

@section('content')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Dashboard</h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <!--end::Actions-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Dashboard-->
            <!--begin::Row-->
            <div class="row">
                <div class="col-lg-12 col-xxl-12">
                    <!--begin::Mixed Widget 1-->
                    <div class="card card-custom bg-gray-100 card-stretch gutter-b">
                        <!--begin::Header-->
                        <div class="card-header border-0 bg-danger py-5">
                            <h3 class="card-title font-weight-bolder text-white">Pickup To Drop</h3>
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->
                        <div class="card-body p-0 position-relative overflow-hidden">
                            <!--begin::Chart-->
                            <div id="kt_mixed_widget_1_chart" class="card-rounded-bottom bg-danger" style="height: 100px"></div>
                            <!--end::Chart-->
                            <!--begin::Stats-->
                            <div class="card-spacer mt-n25">
                                <!--begin::Row-->
                                <div class="row m-0">
                                    <div class="col bg-light-warning px-6 py-8 rounded-xl mr-7 mb-7">
                                        <a href="pickup?status=pending">
                                            <span class="svg-icon svg-icon-3x svg-icon-warning d-block my-2">
                                                <i class="fa fa-clock fa-3x text-warning">&nbsp;&nbsp;</i>
                                            </span>
                                            <span  class="text-warning font-weight-bold font-size-h6">Pending Order: {{ $pending }}</span>
                                        </a>
                                    </div>
                                    <div class="col bg-light-primary px-6 py-8 rounded-xl mr-7 mb-7">
                                        <a href="pickup?status=process">
                                            <span class="svg-icon svg-icon-3x svg-icon-primary d-block my-2">
                                                <i class="fa fa-spinner fa-3x text-primary">&nbsp;&nbsp;</i>
                                            </span>
                                            <span  class="text-primary font-weight-bold font-size-h6">Processed Order: {{ $process }}</span>
                                        </a>
                                    </div>
                                    <div class="col bg-light-success px-6 py-8 rounded-xl mr-7 mb-7">
                                        <a href="pickup?status=complete">
                                            <span class="svg-icon svg-icon-3x svg-icon-primary d-block my-2">
                                                <i class="fa fa-tasks fa-3x text-primary">&nbsp;&nbsp;</i>
                                            </span>
                                            <span class="text-primary font-weight-bold font-size-h6">Completed Order: {{ $complete }}</span>
                                        </a>
                                    </div>


                                </div>
                                <!--end::Row-->

                                <!--begin::Row-->
                                <div class="row m-0">
                                    <div class="col bg-light-success px-6 py-8 rounded-xl mr-7 mb-7">
                                        <a href="{{route('dispute.index')}}">
                                        <span class="svg-icon svg-icon-3x svg-icon- d-block my-2">
                                            <i class="fa fa-gavel fa-3x text-success">&nbsp;&nbsp;</i>
                                        </span>
                                        <span  class="text-primary font-weight-bold font-size-h6">Disputed Order : {{ $dispute }}</span>
                                        </a>
                                    </div>
                                    <div class="col bg-light-danger px-6 py-8 rounded-xl mr-7 mb-7">
                                        <a href="pickup?status=cancel">
                                            <span class="svg-icon svg-icon-3x svg-icon-danger d-block my-2">
                                                <i class="fa fa-times fa-3x text-danger">&nbsp;&nbsp;</i>
                                            </span>
                                            <span class="text-danger font-weight-bold font-size-h6">Cancelled Order : {{ $cancel }}</span>
                                        </a>
                                    </div>
                                    
                                    
                                </div>
                                <!--end::Row-->

                                <!--begin::Row-->
                                <div class="row m-0">
                                    <div class="col bg-light-danger px-6 py-8 rounded-xl mr-7 mb-7">
                                        <a href="{{route('customer.index')}}">
                                        <span class="svg-icon svg-icon-3x svg-icon-danger d-block my-2">
                                            <i class="fa fa-users fa-3x text-danger">&nbsp;&nbsp;</i>
                                        </span>
                                        <span  class="text-danger font-weight-bold font-size-h6">Total Customer : {{ $customer }}</span>
                                        </a>
                                    </div>
                                    <div class="col bg-light-success px-6 py-8 rounded-xl mr-7 mb-7">
                                        <a href="{{route('deliveryboy.index')}}">
                                            <span class="svg-icon svg-icon-3x svg-icon-success d-block my-2">
                                                <img src="{{ asset('/avatars/delivery-boy.png') }}" class="fa-3x text-success" width="50px" height="50px">

                                            </span>

                                            <span class="text-primary font-weight-bold font-size-h6">Total Delivery Agent  : {{ $deliveryboy }}</span>
                                        </a>
                                    </div>
                                </div>



                                <div class="row m-0">

                                    <div class="col bg-light-success px-6 py-8 rounded-xl mr-7 mb-7">
                                        <a href="{{route('subscription.index')}}">
                                            <span class="svg-icon svg-icon-3x svg-icon-success d-block my-2">

                                            <i class="fa fa-subway fa-3x text-danger">&nbsp;&nbsp;</i>

                                            </span>

                                            <span class="text-primary font-weight-bold font-size-h6">Total subscription  : {{ $tiffin }}</span>
                                        </a>
                                    </div>


                                    <div class="col bg-light-danger px-6 py-8 rounded-xl mr-7 mb-7">
                                        <a href="{{url('subscription?active')}}">
                                            <span class="svg-icon svg-icon-3x svg-icon-danger d-block my-2">
                                                <!-- <i class="fa fa-subway fa-3x text-danger">&nbsp;&nbsp;</i> -->
                                                <i class="fa fa-rocket fa-3x text-danger" aria-hidden="true">&nbsp;&nbsp;</i>
                                            </span>
                                            <span  class="text-danger font-weight-bold font-size-h6">Total Active Subscription : {{ $tiffin_subscription }}</span>
                                        </a>
                                    </div>

                                </div>
                                <!--end::Row-->
                            </div>
                            <!--end::Stats-->
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Mixed Widget 1-->
                </div>
            </div>
            <!--end::Row-->
            <!--end::Dashboard-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@push('js')

@endpush
