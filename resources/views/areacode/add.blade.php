@extends('layouts.app')

@section('content')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
  <!--begin::Subheader-->
  <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
    <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
      <!--begin::Info-->
      <div class="d-flex align-items-center flex-wrap mr-2">
        <!--begin::Page Title-->
        <i class="fa fa-map-marker text-danger" aria-hidden="true"></i>
        <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Areacode</h5>
        <!--end::Page Title-->
        <!--begin::Actions-->
        <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

        <!--end::Actions-->
      </div>
      <!--end::Info-->
    </div>
  </div>
  <!--end::Subheader-->
  <!--begin::Entry-->
  <div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container">
      <!--end::Notice-->
      <!--begin::Card-->
      <div class="card card-custom">
        {{-- <div class="card-header">
          <div class="card-title justify-content-between flex-right align-items-right">
            <span class="card-icon">
              <i class="fa fa-hashtag text-danger" aria-hidden="true"></i>
            </span>
            <h3 class="card-label">Areacode</h3>

          </div>
        </div> --}}
        <div class="card-body" style="overflow-x: auto;">

          <div class="container">
            <form id="editForm" action="{{url('areacode/store')}}" method="POST" class="form-horizontal">
             @csrf

                           @if(isset($areacode))
                           <input type="hidden" name="id" value="{{ $areacode->id }}">
                           @endif
                           <div class="row">



                            <div class="col-md-12">

                              <div class="form-group">
                                <label>Area Name</label>
                                <input type="text" id="area_name" name="name" class="form-control" onblur="trimval()" placeholder="Enter Your Name" required
                                data-bv-notempty="true"
                                data-bv-notempty-message="Area Name is required"
                                @if(isset($areacode)) value="{{ $areacode->name }}" @endif>

                              </div>

                              <div class="form-group">
                                <label>Areacode</label>
                                <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57"  onpaste="return false" name="pincode" class="form-control" placeholder="Enter Your Areacode" required
                                data-bv-notempty="true" maxlength="6"
                                data-bv-notempty-message="Areacode is required"
                                @if(isset($areacode)) value="{{ $areacode->pincode }}" @endif>
                              </div>

                                {{--<div class="form-group">
                                    <label>Assign Delivery Agent</label>
                                    <select class="form-control" name="deliveryboy_id" data-bv-notempty="true"
                                            data-bv-notempty-message="The Assign Delivery Agent is required">
                                        <option value="">Select Delivery Agent</option>
                                        @foreach($deliveryboy as $n)
                                            <option
                                                @if(isset($areacode)) @if($n->id==$areacode->deliveryboy_id) selected="selected"
                                                @endif  @endif  value="{{$n->id}}">{{$n->name}}</option>

                                        @endforeach
                                    </select>
                                </div>--}}






                            </div>
                            <div class="col-md-12">
                               <button type="submit" id="btn_save" class="btn btn-danger btn-lg">Save</button>
                               <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" onclick="window.history.go(-1); return false;">Back</a>   
                               {{-- <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" href="{{ route('areacode.index') }}">Back</a>    --}}
                             </div>
                            
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--end::Card-->
                </div>
                <!--end::Container-->
              </div>
              <!--end::Entry-->
            </div>
            <!-- Content Wrapper. Contains page content -->
            @endsection

            @push('js')
            <script>
        //create and store category
        $(function() {
          $('#editForm').bootstrapValidator();
        });

        function trimval(){
          $("#area_name").val($.trim($("#area_name").val()))
        }


      </script>
      @endpush
