@extends('layouts.app')

@section('content')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <i class="fa fa-map-marker text-danger" aria-hidden="true"></i>
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Areacode List</h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

                <a href="{{ url('areacode/add') }}" class="btn btn-light-danger font-weight-bolder btn-sm"
                data-keyboard="false" data-backdrop="static">Add New Areacode</a>
                <!--end::Actions-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--end::Notice-->
            <!--begin::Card-->
            <div class="card card-custom">
                {{-- <div class="card-header">
                    <div class="card-title">
                        <span class="card-icon">
                            <i class="fa fa-hashtag text-danger" aria-hidden="true"></i>
                        </span>
                        <h3 class="card-label">Areacode List</h3>

                         <a href="{{ url('areacode/add') }}" class="btn btn-light-danger font-weight-bolder btn-sm" data-keyboard="false"
                        data-backdrop="static">Add New Areacode</a>
                    </div>
                </div> --}}
                <div class="card-body" style="overflow-x: auto;">
                  <div class="p-3 mb-3 float-right col-md-12 text-right">
                      <button class="btn btn-light-danger font-weight-bolder btn-sm"
                        data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#myModal">Import</button>
                  </div>

                    <!--begin: Datatable-->
                    <table class="table table-hover" id="myTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Area Code</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                    <!--end: Datatable-->
                </div>
            </div>




            <div class="modal fade" id="myModal">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h4 class="modal-title">Import Areacode</h4>
                                        <button type="button" class="close" id="closeModal"
                                            data-dismiss="modal">&times;</button>
                                    </div>
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <h4 id="error-edit" style="display: none;" class='alert alert-danger'></h4>
                                        <form id="editForm" action="areacode/import" method="POST" class="form-horizontal" enctype="multipart/form-data">
                                            @csrf
                                            <div class="row">

                                                <div class="form-group col-md-10">
                                                    <label>City</label>
                                                    <select class="form-control" id="city" name="city"
                                                    required
                                                    data-bv-notempty="true"
                                                    data-bv-notempty-message="City is required">
                                                        <option value=""> -- Select City -- </option>
                                                        @foreach ($cities as $city)
                                                            <option value="{{$city->id}}">{{$city->city_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group col-md-10">
                                                    <input type="hidden" name="id" id="id">
                                                    <label>CSV File</label>
                                                    <input type="file" id="file" name="file" class="form-control" placeholder="Upload file" required
                                                    data-bv-notempty="true"
                                                    data-bv-notempty-message="File is required">
                                                    <span style="display: none;" class="text-danger promo"><small>File Already Exist</small></span>
                                                </div>


                                            </div>
                                            <button type="submit" id="btn_save" class="btn btn-danger btn-lg">Save</button>
                                            <!-- /.card-body -->
                                          </form>
                                    </div>
                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" id="closeModal"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
            <!--end::Card-->


        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!-- Content Wrapper. Contains page content -->
@endsection

@push('js')
<script>

   $(document).ready(function() {
        $table = $("#myTable").DataTable({
            "ajax": "{{ url('areacode/get_all') }}",
            "processing": true,
            "serverSide": true,
            "ordering": false,
            'stateSave' : true,
            "pageLength": 100,
            "columns": [
            {
                data: "id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1
                }
            },
            { "data": "name" },
            { "data": "pincode" },
            { "data": "id",
            render:function(data,type,row,meta){
                console.log(meta);
                $id =btoa(row.id);
                $html = '<div class="dropdown mo-mb-2">'+
                                  '<a href="#" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                                      'Action'+
                                  '</a>'+

                                  '<div class="dropdown-menu" x-placement="bottom-start" style="cursor: pointer;">'+

                                      '<a class="dropdown-item " href="{{ url('areacode/edit') }}/'+$id+'"><i class="fa fa-edit text-primary"></i> Edit</a>'+ ' ';

                                     $html+= '<a class="dropdown-item btn_delete" data-url="areacode/delete" data-id="'+row.id+'"><i class="fa fa-trash text-danger"></i> Delete</a>' ;


                                  '</div>'+
                              '</div>';
                      return $html;
            }
        },
        ]
    });
    } );



   $(document).on("click", '.btn_delete', function(event) {
        var id =$(this).attr('data-id');
        swal({
            title: "Are you sure?",
            text: "You Want To Delete This Areacode,You Will Not Be Able To Recover This Areacode.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {

                $.ajax({
                    url: '{{ url('areacode/destroy') }}/'+id,
                    type: 'get',
                    dataType: 'json',
                //data: {id: 'id'},
                success:function($data){
                    //console.log(data);
                    if($data == "unauthorized_access"){
                        swal("Unauthorized access.", {
                            icon: "error",
                        });
                    }else if($data == true)
                    {
                        swal("Areacode  has been Delete.", {
                            icon: "success",
                        });
                        $('#myTable').DataTable().ajax.reload();
                    }else{
                        swal("Areacode  has been not Delete.", {
                            icon: "error",
                        });
                    }
                    //$('#city').DataTable().ajax.reload();
                }
            });


            } else {
                swal("Your Areacode is Safe.");
            }
        });
    });

</script>
@endpush
