@extends('layouts.app')

@section('content')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
  <!--begin::Subheader-->
  <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
    <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
      <!--begin::Info-->
      <div class="d-flex align-items-center flex-wrap mr-2">
        <!--begin::Page Title-->
        <i class="fa fa-money-check-alt text-danger" aria-hidden="true"></i>
        <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Price</h5>
        <!--end::Page Title-->
        <!--begin::Actions-->
        <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

        <!--end::Actions-->
      </div>
      <!--end::Info-->
    </div>
  </div>
  <!--end::Subheader-->
  <!--begin::Entry-->
  <div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container">
      <!--end::Notice-->
      <!--begin::Card-->
      <div class="card card-custom">
        {{-- <div class="card-header">
          <div class="card-title justify-content-between flex-right align-items-right">
            <span class="card-icon">
              <i class="fa fa-hashtag text-danger" aria-hidden="true"></i>
            </span>
            <h3 class="card-label">Areacode</h3>

          </div>
        </div> --}}
        <div class="card-body" style="overflow-x: auto;">

          <div class="container">
            <form id="editForm" action="{{url('price/store')}}" method="POST" class="form-horizontal">
             @csrf

                           @if(isset($price))
                           <input type="hidden" name="id" value="{{ $price->id }}">
                           @endif
                           <div class="row">



                            <div class="col-md-12">

                              <div class="form-group">
                                <label>PickUpType</label>
                                <select class="form-control @error('price') is-invalid @enderror" name="pickuptype" required
                                data-bv-notempty="true"
                                data-bv-notempty-message="Type is required"  id="Picktype"
                                >
                                <option value="">Select a Type</ option>
                                  <option  @if(isset($price) && $price->pickuptype =='pickupdrop') selected="" @endif value="pickupdrop">PickUpDrop</option>
{{--                                  <option @if(isset($price) && $price->pickuptype =='grocery') selected="" @endif value="grocery">Grocery</option> --}}
                                  <option @if(isset($price) && $price->pickuptype =='medicine') selected="" @endif value="medicine">Medicine</option>
                                  <option @if(isset($price) && $price->pickuptype =='subscription') selected="" @endif value="subscription">Subscription</option>
                                </select>
                              </div>

                              <div class="form-group DateDiv">
                                <label>Minimum KM.</label>
                                <input type="number" id="min_km"  name="min_km" class="form-control @error('min_km') is-invalid @enderror" placeholder="Minimum KM" required
                                data-bv-notempty="true"
                                data-bv-notempty-message="Minimum KM is required"
                                data-bv-max-message="Minimum KM less than required"
                                @if(isset($price)) value="{{ $price->min_km }}" @endif>
                                @error('min_km')
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                              </div>


                              <div class="form-group DateDiv">
                                <label>Maximum KM</label>
                                <input type="number" id="max_km"  name="max_km" class="form-control @error('max_km') is-invalid @enderror" placeholder="Maximum KM" required
                                data-bv-notempty="true"
                                data-bv-notempty-message="Maximum KM is required"
                                 @if(isset($price)) value="{{ $price->max_km }}" @endif>
                                @error('max_km')
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                              </div>

                              <div class="form-group DateDiv typeDiv">
                                <label>Type</label>
                                <select class="form-control @error('price') is-invalid @enderror" id="type" name="type" required
                                data-bv-notempty="true"
                                data-bv-notempty-message="Type is required"
                                >
                                <option value="">Select a Type</option>
                                  <option @if(isset($price) && $price->type =='default') selected="" @endif value="default">Default</option>
                                  <option  @if(isset($price) && $price->type =='urgent') selected="" @endif value="urgent">Urgent</option>
                                  <option @if(isset($price) && $price->type =='normal') selected="" @endif value="normal">Normal</option>
                                </select>


                              </div>

                              <div class="form-group">
                                <label>Price</label>
                                <input type="number"  name="price" class="form-control @error('price') is-invalid @enderror" placeholder="Price" required   data-bv-digits="true"  data-bv-digits-message="The Price is required"
                                data-bv-notempty="true" data-bv-numeric="true"
                                data-bv-notempty-message="Price is required"  @if(isset($price)) value="{{ $price->price }}" @endif>
                                @error('price')
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                              </div>

                              <div class="form-group  DateDiv" id="price_per_km">
                                <label class="col-6 col-form-label">price is per Km or Not?</label>
                                <div class="col-md-2 col-form-label">
                                  <div class="radio-inline">
                                    <label class="radio radio-danger">
                                      <input type="radio" id="True" value="True" name="status_per_km" {{isset($price) && $price->status_per_km == 'True' ? 'checked' : ""}}>
                                      <span></span>Yes</label>
                                      <label class="radio radio-danger">
                                        <input type="radio" id="False" name="status_per_km" value="False" {{isset($price) && $price->status_per_km == 'False' ? 'checked' : (!isset($price) ? 'checked':"")}}>
                                        <span></span>No</label>
                                      </div>
                                      {{-- <span class="form-text text-muted">price is per KM or Not?</span> --}}
                                    </div>
                                  </div>

                            </div>
                            <div class="col-md-12">
                              <button type="submit" id="btn_save" class="btn btn-danger btn-lg">Save</button>
                              <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" onclick="window.history.go(-1); return false;">Back</a>
                              {{-- <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" href="{{ route('price.index') }}">Back</a>    --}}
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--end::Card-->
                </div>
                <!--end::Container-->
              </div>
              <!--end::Entry-->
            </div>
            <!-- Content Wrapper. Contains page content -->
            @endsection

            @push('js')
            <script>
        //create and store category
        $(function() {
          $('#editForm').bootstrapValidator();
        });



      </script>

      <script type="text/javascript">

       $(document).ready(function () {
          priceperkm();
        });

      function priceperkm(){
          if($('#type').val() == 'default'){
          $('#price_per_km').hide();
        }else{
          $('#price_per_km').show();
        }
      }
      $('#type').change(function(){
        priceperkm()
      });

     $('#Picktype').change(function(){
        if($('#Picktype').val() == 'pickupdrop') {
            $('.DateDiv').show();
             $('.glossaryprice').hide();
        } else  if($('#Picktype').val() == 'grocery' || $('#Picktype').val() == 'medicine') {
            $('.glossaryprice').show();
            $('.DateDiv').hide();
        }else if($('#Picktype').val() == 'subscription') {
           $('.DateDiv').show();
             $('.glossaryprice').hide();
             $('.typeDiv').hide();
        }
        else {
            $('.DateDiv').hide();
        }
        priceperkm();
    });


     @if(isset($price))
        getglossary('{{ $price->pickuptype }}');
    @endif
    $('#Picktype').change(function(){
        var type = $('#Picktype').val();
        getglossary(type)
    });

     function getglossary($type)
    {
        if($type == 'grocery') {
         // alert('test');
            $('.glossaryprice').show();
            $('.DateDiv').hide();
        }else if($type ==  'medicine'){
           $('.glossaryprice').show();
            $('.DateDiv').hide();
        }
        else if ($type == 'pickupdrop') {

             $('.DateDiv').show();
             $('.glossaryprice').hide();
             priceperkm();
        }else if ($type == 'subscription') {

             $('.DateDiv').show();
             $('.glossaryprice').hide();
             $('.typeDiv').hide();
        }else{
            $('.glossaryprice').hide();
            $('.DateDiv').hide();
        }
    }
</script>
      @endpush
