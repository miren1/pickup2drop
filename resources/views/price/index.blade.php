@extends('layouts.app')

@section('content')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <i class="fa fa-money-check-alt text-danger" aria-hidden="true"></i>
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Price</h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                {{-- <button class="btn btn-light-danger
                 font-weight-bolder btn-sm"
                data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#myModal">Add New</button> --}}
                 <a href="{{ url('price/add') }}" class="btn btn-light-danger font-weight-bolder btn-sm"
                data-keyboard="false" data-backdrop="static">Add New</a>
                <!--end::Actions-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--end::Notice-->
            <!--begin::Card-->
            <div class="card card-custom">
               {{--  <div class="card-header">
                    <div class="card-title justify-content-between flex-right align-items-right">
                        <span class="card-icon">
                            <i class="fa fa-money-check-alt text-danger" aria-hidden="true"></i>
                        </span>
                        <h3 class="card-label">Price</h3>
                        <button class="btn btn-light-danger font-weight-bolder btn-sm" data-keyboard="false" 
                        data-backdrop="static" data-toggle="modal" data-target="#myModal">Add New</button>
                    </div>
                </div> --}}
                <div class="card-body" style="overflow-x: auto;">
                    <!--begin: Datatable-->
                    <table class="table table-hover" id="dataTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Minimum KM.</th>
                                <th>Maximum KM.</th>
                                <th>Pickuptype</th>
                                <th>Price</th>
                                 <th>Type</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                    <!--end: Datatable-->
                    <div class="container">
                        <div class="modal fade" id="myModal">
                            <div class="modal-dialog ">
                                <div class="modal-content">
                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h4 class="modal-title">Price</h4>
                                        <button type="button" class="close" id="closeModal"
                                            data-dismiss="modal">&times;</button>
                                    </div>
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <h4 id="error-edit" style="display: none;" class='alert alert-danger'></h4>
                                    <form id="editForm" action="{{ url('price/store')}}" method="POST" class="form-horizontal">
                                            @csrf

                                            <div class="form-group">
                                                <label>PickUpType</label>
                                                <select class="form-control @error('price') is-invalid @enderror"{{--  id="type" --}} name="pickuptype" required
                                                data-bv-notempty="true"
                                                data-bv-notempty-message="Type is required"  id="Picktype" 
                                                 >
                                                <option value="">Select a Type</ option> 
                                                <option value="pickupdrop">PickUpDrop</option>
                                                <option value="glossary">Glossary</option> 
                                                <option value="medicine">Medicine</option>      
                                                </select>
                                              
                                               
                                              </div>

                                              <div class="form-group DateDiv">
                                                <label>Minimum KM.</label>
                                                <input type="number" id="min_km"  name="min_km" class="form-control @error('min_km') is-invalid @enderror" placeholder="Minimum KM" required
                                                data-bv-notempty="true"
                                                data-bv-notempty-message="Minimum KM is required"
                                                data-bv-max-message="Minimum KM less than required">
                                                @error('min_km')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                              </div>
                                              <div class="form-group DateDiv">
                                                <label>Maximum KM</label>
                                                <input type="number" id="max_km"  name="max_km" class="form-control @error('max_km') is-invalid @enderror" placeholder="Maximum KM" required
                                                data-bv-notempty="true"
                                                data-bv-notempty-message="Maximum KM is required">
                                                @error('max_km')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                              </div>
                                              <div class="form-group DateDiv">
                                                <label>Type</label>
                                                <select class="form-control @error('price') is-invalid @enderror" id="type" name="type" required
                                                data-bv-notempty="true"
                                                data-bv-notempty-message="Type is required"  
                                                 >
                                                <option value="">Select a Type</ option> 
                                                <option value="urgent">Urgent</option>
                                                <option value="normal">Normal</option>      
                                                 </select>
                                              
                                               
                                              </div>

                                              {{-- <div class="form-group DateDiv">
                                                <label>Price</label>
                                                <input type="number"  name="price" class="form-control @error('price') is-invalid @enderror" placeholder="Price" required
                                                data-bv-notempty="true"
                                                data-bv-notempty-message="Price is required">
                                                @error('price')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                              </div> --}}


                                              <div class="form-group" {{-- style="display: none;" --}}>
                                                <label>Price</label>
                                                <input type="number"  name="price" class="form-control @error('price') is-invalid @enderror" placeholder="Price" required
                                                data-bv-notempty="true"
                                                data-bv-notempty-message="Price is required">
                                                @error('price')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                              </div>

                                              <div class="form-group row DateDiv">
                                                <label class="col-6 col-form-label">price is per Km or Not?</label>
                                                <div class="col-6 col-form-label">
                                                    <div class="radio-inline">
                                                        <label class="radio radio-danger">
                                                        <input type="radio" id="True" value="True" name="status_per_km" />
                                                        <span></span>Yes</label>
                                                        <label class="radio radio-danger">
                                                        <input type="radio" id="False" name="status_per_km" value="False" checked="checked" />
                                                        <span></span>No</label>
                                                    </div>
                                                    <span class="form-text text-muted">price is per KM or Not?</span>
                                                </div>
                                            </div>
                                                <button type="submit" id="btn_save" class="btn btn-danger btn-lg">Save</button>
                                            
                                            <!-- /.card-body -->
                                          </form>
                                    </div>
                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" id="closeModal"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!-- Content Wrapper. Contains page content -->
@endsection

@push('js')
<script>
        //create and store category
        $(function() {
            $('#editForm').bootstrapValidator();

            $(document).on('click', '#closeModal', function(e) {
                e.preventDefault();
                /* Act on the event */
                $('#editForm').bootstrapValidator('resetForm', true);
                $('#True').prop('checked', false);
                $('#False').prop('checked', true);
                $('#error').css('display','none');
                
            });
        });

    //load data in dataTable

     $(document).ready(function() {
        $table = $("#dataTable").DataTable({
            "ajax": "{{ url('price/show') }}",
            "processing": true,
            "serverSide": true,
            "ordering": false,
            'stateSave' : true,
            "columns": [
              { "data": "id",
                render:function(data,type,row,meta){
                    return meta.row + 1;
                }
            },
            { "data": "min_km" },
            { "data": "max_km" },
            { "data": "pickuptype",
                render:function(data,type,row,meta){
                    return row.pickuptype.toUpperCase();
                }
        },
            { "data": "price" },
            //{ "data": "type" },
            { "data": "id",
                render:function(data,type,row,meta){
                  //console.log(row);
                  if(row.type=="default"){
                        return '<span>Default</span>';
                  }else if (row.type=="normal") {
                        return '<span>Normal</span>';
                  } 
                  else {
                        return '<span>Urgent</span>';
                  }
                    
                }
            },
            { "data": "id",
            render:function(data,type,row,meta){
                $id =btoa(row.id);
                $html = '<div class="dropdown mo-mb-2">'+
                                  '<a href="#" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                                      'Action'+
                                  '</a>'+

                                  '<div class="dropdown-menu" x-placement="bottom-start" style="cursor: pointer;">'+
                                      
                                      '<a class="dropdown-item " href="{{ url('price/edit') }}/'+$id+'"><i class="fa fa-edit text-primary"></i> Edit</a>'+ ' ';
                                      
                                     $html+= '<a class="dropdown-item btn_delete" data-url="price/delete" data-id="'+row.id+'"><i class="fa fa-trash text-danger"></i> Delete</a>' ;
                                    

                                  '</div>'+
                              '</div>';
                      return $html;
            }
        },
        ]
    });
    } );


        //delete category
        $(document).on("click", '.btn_delete', function() {
                //e.preventDefault();
                var price_id = $(this).attr('data-id');

               swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this Plan.",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {

                            $.ajax({
                                type: "POST",
                                url: "{{url('price/destroy')}}",
                                data: {
                                    "id": price_id,
                                    "_token": "{{ csrf_token() }}"
                                },
                                success: function(response) {
                                    //alert(response.title);
                                    if (response.title == "Success!") {
                                        swal(response.message, {
                                            icon: 'success',
                                        });
                                        setTimeout(function () {
                            window.location.href = "{{ url('price') }}"; //will redirect to your blog page (an ex: blog.html)
                        },2000);
                                    } else {
                                        swal(response.title,response.message,{icon:response.icon});
                                    }
                                }
                            });
                        }
                    });
            });
   



</script>

<script type="text/javascript">
     $('#Picktype').change(function(){
        //alert('hello');
        if($('#Picktype').val() == 'pickupdrop') {
            $('.DateDiv').show();
             $('.glossaryprice').hide();
        } else  if($('#Picktype').val() == 'glossary' || $('#Picktype').val() == 'medicine') {
            $('.glossaryprice').show();
            $('.DateDiv').hide();
        }
        else {
            $('.DateDiv').hide();
            //$('.add_services').hide(); 
        } 
    });
</script>
@endpush
