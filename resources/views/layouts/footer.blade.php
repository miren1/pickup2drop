<!--begin::Footer-->
<div class="footer bg-white py-4 d-flex flex-lg-column text-center" id="kt_footer">
    <!--begin::Container-->
    <div class="container-fluid flex-md-row">
        <!--begin::Copyright-->
        <div class="text-dark order-2 order-md-1">
            <span class="text-muted font-weight-bold mr-2">Copyright © 2022-{{date('Y')}}</span>
            <a href="{{route('home')}}" class="text-dark-75 text-hover-primary">Pickup To Drop</a>
        </div>
        <!--end::Copyright-->
    </div>
    <!--end::Container-->
</div>
<!--end::Footer-->