<!--begin::Wrapper-->
<div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
    <!--begin::Header-->
    <div id="kt_header" class="header header-fixed">
        <!--begin::Container-->
        <div class="container-fluid d-flex align-items-stretch justify-content-between">
            <!--begin::Header Menu Wrapper-->
            <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">

            </div>
            <!--end::Header Menu Wrapper-->
            <!--begin::Topbar-->
            <div class="topbar">
                <!--begin::User-->
                <div class="topbar-item">


                    <li class="nav-item avatar dropdown cursor-pointer">
                        <a class="nav-link  waves-effect waves-light" id="navbarDropdownMenuLink-5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="fas fa-bell" style="font-size:20px"></i>
                        <span class="badge badge-danger ml-2 top-0 position-absolute" style="right: 6px;font-size: 0.65em;">
                            {{count(Auth()->user()->unreadNotifications)}}
                        </span>
                        </a>
                        <div style="max-height: 484px; overflow-y: auto; " class="dropdown-menu dropdown-menu-lg-right dropdown-secondary mt-0 pt-0" aria-labelledby="navbarDropdownMenuLink-5">
                            <div style="padding: 15px 0; cursor:default;" class="bg-gray-100 text-center">
                                <a class="p-2" href="{{url('admin-notification/mark/read/all')}}">mark all as read</a>
                            </div>
                            @if(Auth()->user()->unreadNotifications->count() > 0)
                              @foreach (Auth()->user()->unreadNotifications as $notification)
                                <a href="/admin-notification/show/{{base64_encode($notification->id)}}" class="dropdown-item waves-effect waves-light cursor-pointer" href="#">{{($notification->data['message'])}}</a>
                            @endforeach
                            @else
                            <div class="col-md-12">
                                <a  class="dropdown-item waves-effect waves-light cursor-pointer" href="#">There are no notifications at present</a>
                            </div>

                            @endif

                        </div>
                    </li>

                    <div class="btn btn-icon btn-icon-mobile w-auto btn-clean d-flex align-items-center btn-lg px-2" id="kt_quick_user_toggle">
                        {{-- <span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1">Hi,</span>
                    <span class="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3">{{Auth()->user()->name}}</span> --}}
                        <div class="d-flex align-items-stretch justify-content-between flex-lg-grow-1">
							@include('nav')
						</div>

                        {{-- <span class="symbol symbol-lg-35 symbol-25 symbol-light-success">
                            <span class="symbol-label font-size-h5 font-weight-bold"></span>
                        </span> --}}
                    </div>
                </div>
                <!--end::User-->
            </div>
            <!--end::Topbar-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Header-->

    <script>

    </script>
