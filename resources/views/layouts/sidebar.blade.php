
<div class="d-flex flex-column flex-root">
    <!--begin::Page-->
    <div class="d-flex flex-row flex-column-fluid page">
        <!--begin::Aside-->
        <div class="aside aside-left aside-fixed d-flex flex-column flex-row-auto" id="kt_aside">
            <!--begin::Brand-->
            <div class="brand flex-column-auto" id="kt_brand">
                <!--begin::Logo-->
            <a href="{{url('/home')}}" onclick="$table.state.clear();" class="brand-logo">
                <img alt="Logo" src="{{asset('/assets/media/PickupandDroplogo_white.png')}}" style="height: 60px; margin-top: 10px;" />
            </a>
                <!--end::Logo-->
                <!--begin::Toggle-->
                <button class="brand-toggle btn btn-sm px-0" id="kt_aside_toggle">
                    <span class="svg-icon svg-icon svg-icon-xl">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-left.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <polygon points="0 0 24 0 24 24 0 24" />
                                <path d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z" fill="#000000" fill-rule="nonzero" transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999)" />
                                <path d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999)" />
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span>
                </button>
                <!--end::Toolbar-->
            </div>
            <!--end::Brand-->
            <!--begin::Aside Menu-->
            <div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper">
                <!--begin::Menu Container-->
                <div id="kt_aside_menu" class="aside-menu my-4 scroll ps ps--active-y" data-menu-vertical="1" data-menu-scroll="1" data-menu-dropdown-timeout="500">
                    <!--begin::Menu Nav-->
                    <ul class="menu-nav">
                        <li class="menu-item menu-item-{{ (request()->segment(1) == 'home') ? 'active' : '' }}" aria-haspopup="true">
                            <a href="{{route('home')}}" onclick="$table.state.clear();" class="menu-link">
                                <span class="svg-icon menu-icon">
                                    <i class="icon-dashboard {{ (request()->segment(1) == 'home') ? 'text-danger' : '' }}"></i>
                                </span>
                                <span class="menu-text">Dashboard</span>
                            </a>
                        </li>
                        <li class="menu-section">
                            <h4 class="menu-text">menu</h4>
                            <i class="menu-icon ki ki-bold-more-hor icon-md"></i>
                        </li>
                        
                        @can('Customer-list')
                            <li class="menu-item menu-item-{{ (request()->segment(1) == 'customers') ? 'active' : '' }}" aria-haspopup="true">
                                <a href="{{url('customer')}}" onclick="$table.state.clear();" class="menu-link">
                                    <span class="svg-icon menu-icon">
                                        <i class="fa fa-users {{ (request()->segment(1) == 'customer') ? 'text-danger' : '' }}"></i>
                                    </span>
                                    <span class="menu-text">Customers</span>
                                </a>
                            </li>
                        @endcan

                        @canany(['Deliveryboy-List','Deliveryboy-Add','Deliveryboy-Edit','Deliveryboy-Show','Deliveryboy-Delete','Deliveryboy-Document'])
                            <li class="menu-item menu-item-{{ (request()->segment(1) == 'deliveryboy') ? 'active' : '' }}" aria-haspopup="true">
                                <a href="{{ url('deliveryboy')}}" onclick="$table.state.clear();" class="menu-link">
                                    <span class="svg-icon menu-icon">
                                        <i class="fa fa-truck {{ (request()->segment(1) == 'deliveryboy') ? 'text-danger' : '' }}" style="transform: rotateY(160deg);"></i>

                                    </span>
                                    <span class="menu-text">Delivery Agent</span>
                                </a>
                            </li>
                        @endcan
                        @canany(['Tds-Show','TDS-Upload'])
                            <li class="menu-item menu-item-{{ (request()->segment(1) == 'tds-certificate') ? 'active' : '' }}" aria-haspopup="true">
                                <a href="{{ url('tds-certificate')}}" onclick="$table.state.clear();" class="menu-link">
                                    <span class="svg-icon menu-icon">
                                        <i class="fa icon-file-text {{ (request()->segment(1) == 'tds-certificate') ? 'text-danger' : '' }}" style="transform: rotateY(160deg);"></i>

                                    </span>
                                    <span class="menu-text">TDS Certificate</span>
                                </a>
                            </li>
                        @endcan

                        @canany(['Order-List','Order-Show'])
                            <li class="menu-item menu-item-{{ (request()->segment(1) == 'pickup') ? 'active' : '' }}" aria-haspopup="true">
                                <a href="{{ url('pickup')}}" onclick="$table.state.clear();" class="menu-link">
                                    <span class="svg-icon menu-icon">
                                        <i class="fa fa-truck {{ (request()->segment(1) == 'pickup') ? 'text-danger' : '' }}"></i>
                                    </span>
                                    <span class="menu-text">Orders</span>
                                </a>
                            </li>
                        @endcan
                        {{--<li class="menu-item menu-item-{{ (request()->segment(1) == 'deliveryboy_assign') ? 'active' : '' }}" aria-haspopup="true">
                            <a href="{{ url('deliveryboy_assign')}}" class="menu-link">
                                <span class="svg-icon menu-icon">
                                    <i class="fa fa-hashtag {{ (request()->segment(1) == 'deliveryboy_assign') ? 'text-danger' : '' }}"></i>
                                </span>
                                <span class="menu-text">Delivery Agent Assign Area</span>
                            </a>
                        </li>--}}

                        @canany(['Subscription-List','Subscription-Show'])
                            <li class="menu-item menu-item-{{ (request()->segment(1) == 'subscription') ? 'active' : '' }}" aria-haspopup="true">
                                <a href="{{ url('subscription')}}" onclick="$table.state.clear();" class="menu-link">
                                    <span class="svg-icon menu-icon">
                                        <i class="fa fa-subway {{ (request()->segment(1) == 'subscription') ? 'text-danger' : '' }}"></i>
                                    </span>
                                    <span class="menu-text">Subscription</span>
                                </a>
                            </li>
                        @endcan

                        @canany(['Wallet-List','Wallet-Add','Wallet-Delete'])
                            <li class="menu-item menu-item-{{ (request()->segment(1) == 'wallet') ? 'active' : '' }}" aria-haspopup="true">
                                <a href="{{ route('wallet.index')}}" onclick="$table.state.clear();" class="menu-link">
                                    <span class="svg-icon menu-icon">
                                        <i class="fas fa-wallet {{ (request()->segment(1) == 'wallet') ? 'text-danger' : '' }}"></i>
                                    </span>
                                    <span class="menu-text">Wallet</span>
                                </a>
                            </li>
                        @endcan

                        @can('Send-Notification')
                            <li class="menu-item menu-item-{{ (request()->segment(1) == 'notification') ? 'active' : '' }}" aria-haspopup="true">
                                <a href="{{ url('notification')}}" onclick="$table.state.clear();" class="menu-link">
                                    <span class="svg-icon menu-icon">
                                        <i class="fas fa-bullhorn {{ (request()->segment(1) == 'notification') ? 'text-danger' : '' }}"></i>
                                    </span>
                                    <span class="menu-text">Notification</span>
                                </a>
                            </li>
                        @endcan

                        @canany(['Notification-List','Notification-Show'])
                        <li class="menu-item menu-item-{{ (request()->segment(1) == 'admin-notification') ? 'active' : '' }}" aria-haspopup="true">
                            <a href="{{ route('notification.index')}}" onclick="$table.state.clear();" class="menu-link">
                                <span class="svg-icon menu-icon">
                                    <i class="fas fa-bell {{ (request()->segment(1) == 'admin-notification') ? 'text-danger' : '' }}"></i>
                                </span>
                                <span class="menu-text">Admin Notification</span>
                            </a>
                        </li>
                        @endcan

                        @canany(['Dispute-List','Dispute-Show'])
                            <li class="menu-item menu-item-{{ (request()->segment(1) == 'dispute-order') ? 'active' : '' }}" aria-haspopup="true">
                                <a href="{{ url('dispute-order')}}" onclick="$table.state.clear();" class="menu-link">
                                    <span class="svg-icon menu-icon">
                                    <!-- <i class="fas fa-shipping-fast"></i> -->
                                    <!--   <i class="fas fa-shopping-cart {{ (request()->segment(1) == 'dispute-order') ? 'text-danger' : '' }}"></i> -->
                                    <i class="fas fa-shipping-fast {{ (request()->segment(1) == 'dispute-order') ? 'text-danger' : '' }}"></i>
                                    </span>
                                    <span class="menu-text">Dispute Order</span>
                                </a>
                            </li>
                        @endcan

                        @canany(['Transaction-History-List','Transaction-History-Show'])
                            <li class="menu-item menu-item-{{ (request()->segment(1) == 'transaction-history') ? 'active' : '' }}" aria-haspopup="true">
                                <a href="{{ url('transaction-history')}}" onclick="$table.state.clear();" class="menu-link">
                                    <span class="svg-icon menu-icon">
                                        <i class="fas fa-clock  {{ (request()->segment(1) == 'transaction-history') ? 'text-danger' : '' }}"></i>
                                    </span>
                                    <span class="menu-text">Transaction History</span>
                                </a>
                            </li>
                        @endcan

                        @canany('GST-Report-List')
                            <li class="menu-item menu-item-{{ (request()->segment(1) == 'gst-report') ? 'active' : '' }}" aria-haspopup="true">
                                <a href="{{ url('gst-report')}}" onclick="$table.state.clear();" class="menu-link">
                                    <span class="svg-icon menu-icon">
                                        <i class="fas fa-clock  {{ (request()->segment(1) == 'gst-report') ? 'text-danger' : '' }}"></i>
                                    </span>
                                    <span class="menu-text">GST Report</span>
                                </a>
                            </li>
                        @endcan

                        @if(auth()->user()->type == 'Admin')
                            <li class="menu-item menu-item-sub menu-item-{{ (request()->segment(1) == 'user') || (request()->segment(1) == 'roles-permission') ? 'open' : '' }}" aria-haspopup="true" data-menu-toggle="hover">
									<a href="javascript:;" class="menu-link menu-toggle">
                                        <span class="svg-icon menu-icon">
										<i class="fa fa-cog {{ (request()->segment(1) == 'user') ? 'text-danger' : '' }}"></i>
                                        </span>
										<span class="menu-text">User Management</span>
									</a>
									<div class="menu-submenu" kt-hidden-height="80" style="">
										<i class="menu-arrow"></i>
										<ul class="menu-subnav">

                                            <li class="menu-item menu-item-{{ (request()->segment(1) == 'user') ? 'active' : '' }}" aria-haspopup="true">
                                                <a href="{{ url('user')}}" onclick="$table.state.clear();" class="menu-link">
                                                    <span class="svg-icon menu-icon">
                                                        <i class="fa fa-user {{ (request()->segment(1) == 'user') ? 'text-danger' : '' }}"></i>
                                                    </span>
                                                    <span class="menu-text">Users</span>
                                                </a>
                                            </li>
                                            <li class="menu-item menu-item-{{ (request()->route()->getName() == 'role_permission.index') ? 'active' : '' }}" aria-haspopup="true">
                                                <a href="{{ url('roles-permission')}}" onclick="$table.state.clear();" class="menu-link">
                                                    <span class="svg-icon menu-icon">
                                                        <i class="fa fa-universal-access {{ (request()->route()->getName() == 'role_permission.index') ? 'text-danger' : '' }}"></i>
                                                    </span>
                                                    <span class="menu-text">Roles</span>
                                                </a>
                                            </li>
                                            <li class="menu-item menu-item-{{ (request()->route()->getName() == 'role_permission.assign') ? 'active' : '' }}" aria-haspopup="true">
                                                <a href="{{ url('roles-permission/assign')}}" onclick="$table.state.clear();" class="menu-link">
                                                    <span class="svg-icon menu-icon">
                                                        <i class="fa fa-unlock-alt {{ (request()->route()->getName() == 'role_permission.assign') ? 'text-danger' : '' }}"></i>
                                                    </span>
                                                    <span class="menu-text">Assign Permission</span>
                                                </a>
                                            </li>


                                         

										</ul>
									</div>
                                </li> 
                            @endif

                            @canany(['City-List','City-Add','City-Edit','City-Show','City-Delete','Area-List','Area-Add','Area-Edit','Area-Show','Area-Delete','Price-List','Price-Add','Price-Edit','Price-Show','Price-Delete','Promocode-List','Promocode-Add','Promocode-Edit','Promocode-Show','Promocode-Delete','Slider-List','Slider-Add','Slider-Edit','Slider-Show','Slider-Delete','Update-Order-Commission','Radius-Setting','GST-Setting'])
                                <li class="menu-item menu-item-sub menu-item-{{ (request()->segment(1) == 'general-settings') || (request()->segment(1) == 'commission-order') || (request()->segment(1) == 'areacode') || (request()->segment(1) == 'city') || (request()->segment(1) == 'price') || (request()->segment(1) == 'promocode') || (request()->segment(1) == 'manage-slider') ? 'open' : '' }}" aria-haspopup="true" data-menu-toggle="hover">
									<a href="javascript:;" class="menu-link menu-toggle">
                                        <span class="svg-icon menu-icon">
										<i class="fa fa-cog {{ (request()->segment(1) == 'commission-order') || (request()->segment(1) == 'areacode') || (request()->segment(1) == 'city') || (request()->segment(1) == 'price') || (request()->segment(1) == 'promocode') || (request()->segment(1) == 'manage-slider') ? 'text-danger' : '' }}"></i>
                                        </span>
										<span class="menu-text">Settings</span>
									</a>
									<div class="menu-submenu" kt-hidden-height="80" style="">
										<i class="menu-arrow"></i>
										<ul class="menu-subnav">

                                            @canany(['City-List','City-Add','City-Edit','City-Show','City-Delete'])
                                                <li class="menu-item menu-item-{{ (request()->segment(1) == 'city') ? 'active' : '' }}" aria-haspopup="true">
                                                    <a href="{{ url('city')}}" onclick="$table.state.clear();" class="menu-link">
                                                        <span class="svg-icon menu-icon">
                                                            <i class="fa fa-city {{ (request()->segment(1) == 'city') ? 'text-danger' : '' }}"></i>
                                                        </span>
                                                        <span class="menu-text">City</span>
                                                    </a>
                                                </li>
                                            @endcan

                                            @canany(['Area-List','Area-Add','Area-Edit','Area-Show','Area-Delete'])
                                            	<li class="menu-item menu-item-{{ (request()->segment(1) == 'areacode') ? 'active' : '' }}" aria-haspopup="true">
                                                    <a href="{{ url('areacode')}}" onclick="$table.state.clear();" class="menu-link">
                                                        <span class="svg-icon menu-icon">
                                                            <i class="fa fa-map-marker {{ (request()->segment(1) == 'areacode') ? 'text-danger' : '' }}"></i>
                                                        </span>
                                                        <span class="menu-text">Areacode</span>
                                                    </a>
                                                </li>
                                            @endcan

                                            @canany(['Price-List','Price-Add','Price-Edit','Price-Show','Price-Delete'])
                                                <li class="menu-item menu-item-{{ (request()->segment(1) == 'price') ? 'active' : '' }}" aria-haspopup="true">
                                                    <a href="{{ url('price')}}" onclick="$table.state.clear();" class="menu-link">
                                                        <span class="svg-icon menu-icon">
                                                            <i class="fa fa-money-check-alt {{ (request()->segment(1) == 'price') ? 'text-danger' : '' }}"></i>
                                                        </span>
                                                        <span class="menu-text">Price</span>
                                                    </a>
                                                </li>
                                            @endcan

                                            @canany(['Promocode-List','Promocode-Add','Promocode-Edit','Promocode-Show','Promocode-Delete'])
                                                <li class="menu-item menu-item-{{ (request()->segment(1) == 'promocode') ? 'active' : '' }}" aria-haspopup="true">
                                                    <a href="{{ url('promocode')}}" onclick="$table.state.clear();" class="menu-link">
                                                        <span class="svg-icon menu-icon">
                                                            <i class="fa fa-gift {{ (request()->segment(1) == 'promocode') ? 'text-danger' : '' }}"></i>
                                                        </span>
                                                        <span class="menu-text">Promocodes</span>
                                                    </a>
                                                </li>
                                            @endcan

                                            @canany(['Slider-List','Slider-Add','Slider-Edit','Slider-Show','Slider-Delete'])
                                                <li class="menu-item menu-item-{{ (request()->segment(1) == 'manage-slider') ? 'active' : '' }}" aria-haspopup="true">
                                                    <a href="{{ url('manage-slider')}}" onclick="$table.state.clear();" class="menu-link">
                                                        <span class="svg-icon menu-icon">
                                                            <i class="fas fa-sliders-h {{ (request()->segment(1) == 'manage-slider') ? 'text-danger' : '' }}"></i>
                                                        </span>
                                                        <span class="menu-text">Slider</span>
                                                    </a>
                                                </li>
                                            @endcan

                                            @canany('Update-Order-Commission')
                                                <li class="menu-item menu-item-{{ (request()->segment(1) == 'commission-order') ? 'active' : '' }}" aria-haspopup="true">
                                                    <a href="{{ url('commission-order')}}" onclick="$table.state.clear();" class="menu-link">
                                                        <span class="svg-icon menu-icon">
                                                            <i class="fas fa-percent {{ (request()->segment(1) == 'commission-order') ? 'text-danger' : '' }}"></i>
                                                        </span>
                                                        <span class="menu-text">Order Commission</span>
                                                    </a>
                                                </li>
                                            @endcan

                                            {{-- @if(auth()->user()->can('Radius-Setting') || auth()->user()->can('edit articles')) --}}
                                            @canany(['Radius-Setting','GST-Setting'])
                                                <li class="menu-item menu-item-{{ (request()->segment(1) == 'general-settings') ? 'active' : '' }}" aria-haspopup="true">
                                                    <a href="{{ url('general-settings')}}" onclick="$table.state.clear();" class="menu-link">
                                                        <span class="svg-icon menu-icon">
                                                            <i class="fas fa-cogs {{ (request()->segment(1) == 'general-settings') ? 'text-danger' : '' }}"></i>
                                                        </span>
                                                        <span class="menu-text">General Settings</span>
                                                    </a>
                                                </li>
                                            @endcan

										</ul>
									</div>
								</li>
                            @endcan
                                 {{-- <li class="menu-item menu-item" aria-haspopup="true">
                            <a href="{{route('logout')}}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();" class="menu-link">
                                <span class="svg-icon menu-icon">
                                    <i class="icon-signout"></i>
                                </span>
                                <span class="menu-text">Logout</span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li> --}}

</ul>


                    <!--end::Menu Nav-->
                </div>
                <!--end::Menu Container-->
            </div>
            <!--end::Aside Menu-->
        </div>
        <!--end::Aside-->
