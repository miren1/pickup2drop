<!DOCTYPE html>
    <html>
    <head>
      <meta charset='utf-8'>
      <meta name='viewport' content='width=device-width'>
      <title>Privacy Policy</title>
      <style> body { font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; padding:1em; } </style>
    </head>
    <body>
    <strong>Privacy Policy</strong>
    <p>This page is used to inform visitors regarding our policies regarding the collection, use, and disclosure of Personal Information if anyone decided to use our Service.</p>
    <p>If you choose to use our Service, then you agree to the collection and use of information about this policy. The Personal Information that we collect is used for providing and improving the Service. We will not use or share your information with anyone except as described in this Privacy Policy.</p>
    <p>The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which are accessible at <strong>Pickup To Drop- Delivery Agent</strong> unless otherwise defined in this Privacy Policy.</p>
    <p><strong>Information Collection and Use</strong></p>
    <p>For a better experience, while using our Service, we may require you to provide us with certain personally identifiable information. The information that we request will be retained by us and used as described in this privacy policy.</p> <div>
    <p>The app does use third-party services that may collect information used to identify you.</p>
    <p>Link to the privacy policy of third-party service providers used by the app</p>
    <ul><li><a href="https://www.google.com/policies/privacy/" target="_blank" rel="noopener noreferrer">Google Play Services</a></li>
      <!----><!----><!----><!----><!----><!----><!----><!----><!----><!----><!----><!----><!----><!----><!----><!----><!----><!----><!----><!----><!----><!----><!----><!----><!----><!----><!---->
    </ul></div> <p><strong>Information we collect automatically</strong></p>
    <li>Location information</li>
    <p>
       We collect Users’ location data to enable rides, for user support, for safety and fraud detection purposes, and to satisfy legal requirements. We collect location information (including GPS coordinates and WiFi data) based on your App settings, device permissions, and whether you are using it the App as a Rider or a Driver (where applicable):
       -  Drivers:  We collect your device’s precise location when the App is running in the foreground (App open and on-screen) and when the App is running in the background (App open but not on-screen) in the Driver mode. We may also collect precise location for a limited time after you exit Driver mode in order to detect and investigate ride incidents.
    </p>
    <p>
      Pick up & Drop - Delivery Agent  app collects location data to enable live location of your even when the app is closed or not in use.
    </p>
    <li>Transaction information</li>
    <p>We collect transaction information related to the use of our Services, including the type of Services requested or provided, order details, payment transaction information, date and time the service was provided, amount charged, route and distance traveled, and payment method.</p>
    
    <li>Usage information</li>
    <p>We collect data about your use of our Services. This may include such data as access dates and times, App features or pages viewed, App or Website crashes and other system activity. We may also collect and use your data for marketing purposes related to third-party sites you have visited or services you used before interacting with our Services.</p>
    
    <li>Communications information</li>
    <p>We enable Users to communicate with each other and with us through the App. To facilitate this service, we receive data about the time and date of the communications and the content. We may also use this data for user support, safety and security purposes, to resolve disputes between users, to improve our Services, and for analytics (such as to measure and improve the effectiveness of our website).</p>
    
    <li>Device information</li>
    <p></p>
     We collect information about the device you use to access the Services, such as your device name, brand and model, user agent, IP address, mobile carrier, network type, time zone settings, language settings, advertising identifiers, browser type, operating system and its version, screen parameters, battery state, and installed applications that can be used for authentication purposes. We may also collect mobile sensor data, such as speed, direction, altitude, acceleration, deceleration, and other technical data.
    <br><br>
    Cookies, Analytics, and Third-Party Technologies. We collect information through the use of cookies, pixels, tags and other similar tracking technologies (“Cookies”). Cookies are small text files that web servers place on your device. They automatically collect information, measure and analyze how you use websites and applications, and identify which web pages and ads you click on. The information we obtain through cookies helps us improve your inDriver experience (for example, by saving your preferences) and our marketing programs. Our service providers and business partners may use this information to display ads across your devices tailored to your preferences and characteristics. You can delete cookies and manage cookie settings in your web browser settings. Nevertheless, some cookies cannot be disabled since the functionality of some of our features depends on them. To learn more about these technologies, please see our Cookie Policy. 
    </p>
    </ul></div> <p><strong>Log Data</strong></p>
    <p>We want to inform you that whenever you use our Service, in a case of an error in the app we collect data and information (through third-party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol (“IP”) address, device name, operating system version, the configuration of the app when utilizing our Service, the time and date of your use of the Service, and other statistics.</p>
    <p><strong>Cookies</strong></p>
    <p>Cookies are files with a small amount of data that are commonly used as anonymous unique identifiers. These are sent to your browser from the websites that you visit and are stored on your device's internal memory.</p>
    <p>This Service does not use these “cookies” explicitly. However, the app may use third-party code and libraries that use “cookies” to collect information and improve their services. You have the option to either accept or refuse these cookies and know when a cookie is being sent to your device. If you choose to refuse our cookies, you may not be able to use some portions of this Service.</p>
    <p><strong>Service Providers</strong></p>
    <p>We may employ third-party companies and individuals due to the following reasons:</p>
      <ul>
          <li>To facilitate our Service;</li>
          <li>To provide the Service on our behalf;</li>
          <li>To perform Service-related services; or</li>
          <li>To assist us in analyzing how our Service is used.</li>
      </ul>
    <p>We want to inform users of this Service that these third parties have access to their Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose.</p>
    <p><strong>Refund Policy</strong></p>
    <p>Should the Delivery Agent find the package to be overweight when picking up the goods, the customer will not get a refund and the driver will get the full amount.</p>
    <p><strong>Security</strong></p>
    <p>We value your trust in providing us with your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.</p>
    <p><strong>Links to Other Sites</strong></p>
    <p>This Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by us. Therefore, we strongly advise you to review the Privacy Policy of these websites. We have no control over and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.</p>
    <p><strong>Children’s Privacy</strong></p>
    <div>
    <p>
      These Services do not address anyone under the age of 13. We do not knowingly collect personally identifiable information from children under 13 years of age. In the case we discover that a child under 13 has provided us with personal information, we immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact us so that we will be able to do the necessary actions.</p>
    </div> <!---->
    <p><strong>Changes to This Privacy Policy</strong></p>
    <p>We may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on this page.</p>
    <p>This policy is effective as of 2022-05-05</p>
    <p><strong>Contact Us</strong></p>
    <p>If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us at <strong>info@pickuptodrop.com</strong>.</p>
    </body>
    </html>
