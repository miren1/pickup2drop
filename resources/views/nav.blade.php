<div class="dropdown menu-nav">
  <span class="d-flex justify-content-center align-items-center" style="max-height: 60px;max-width:80px;width:100%;overflow:hidden" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    {{-- <i class="fa fa-user"></i> --}}
	<img src="/{{Auth::user()->profile_photo_path ? Auth::user()->profile_photo_path : 'uploads/default_user.png'}}"
	 alt="{{Auth::user()->name}}" srcset="" width="50">
  </span>
  <div class="dropdown-menu " style="width:max-content; left:-50px;" aria-labelledby="dropdownMenuButton">
		<span class="d-block rounded p-4 text-center text-white font-bold {{ (request()->segment(2) == 'profile') ? 'text-danger' : 'bg-danger text-white' }}">Hi, {{auth()->user()->name}}</span>
		
		<li class="border rounded menu-item {{ (request()->segment(2) == 'profile') ? 'bg-danger text-white' : '' }}" aria-haspopup="true">
		<a href="{{route('admin.profile')}}" class="menu-item menu-item p-4 d-block">
			<span class="svg-icon menu-icon">
			<i class="fa fa-cog {{ (request()->segment(2) == 'profile') ? 'text-white' : 'text-danger' }}"></i>
			</span>
			<span class="menu-text {{ (request()->segment(2) == 'profile') ? 'text-white' : 'text-danger' }}">&nbsp;Profile Settings</span>
		</a>
		</li>
		
		
		<li class="menu-item menu-item p-4" aria-haspopup="true">
			<a href="{{route('logout')}}" onclick="event.preventDefault();
			document.getElementById('logout-form').submit();" class="menu-link">
				<span class="svg-icon menu-icon">
					<i class="icon-signout" style="color:#f64e60"></i>
				</span>
				<span class="menu-text" style="color:#f64e60">&nbsp; Logout</span>
			</a>
			<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
				@csrf
			</form>
		</li>
  </div>
</div>