@extends('layouts.app')

@section('content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <!-- <i class="fas fa-sliders-h"></i> -->
                <i class="fas fa-clock text-danger" aria-hidden="true"></i>
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Transaction History</h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

                <!-- <a href="{{ url('subscription/add') }}" class="btn btn-light-danger font-weight-bolder btn-sm"
                data-keyboard="false" data-backdrop="static">Add New</a>   -->
                <!-- <a href="" class="btn btn-light-danger font-weight-bolder btn-sm"
                data-keyboard="false" data-backdrop="static">Add New Slider</a> -->
                <!--  <button class="btn btn-light-danger font-weight-bolder btn-sm"
                data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#myModal">Add New Slider</button> -->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--end::Notice-->
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="header-menu-wrapper mt-10">
                    <h5 class="text-dark font-weight-bold mt-10 mb-2 ml-9" style="display: inline"><i class="fas fa-wallet text-danger"></i>&nbsp;Wallet History</h5>
                    <h5 class="mr-9" style="display: inline;float: right">Payable Amount : ₹&nbsp;{{ $balance }}</h5>
                </div>

                <div class="card-body" style="overflow-x: auto;">
                    <!--begin: Datatable-->
                    <input type="hidden" name="id" id="id" value="{{$Id}}">
                    <table class="table table-hover" id="myTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Order Id</th>
                                <th>Order Name</th>
                                <th>Amount</th>
                                <th>Mobile Number</th>
                                <th>Status</th>
                                <!-- <th>Max Amount</th> -->
                                <!-- <th style="width: 80px;">End Date</th> -->
                                <!-- <th>One Day</th> -->
                                <th style="width: 75px;">Date</th>
                                <!-- <th>Action</th> -->
                            </tr>
                        </thead>
                    </table>
                    <!--end: Datatable-->

                </div>
            </div>
            <!--end::Card-->
            &nbsp;
             <!--begin::Card-->
            <div class="card card-custom">
                <div class="header-menu-wrapper">
                        <h5 class="text-dark font-weight-bold mt-10 mb-2 ml-9"><i class="fas fa-clock fa-spin text-danger"></i>&nbsp;Bank Transfer History</h5>
                </div>
                <div class="card-body" style="overflow-x: auto;">
                    <!--begin: Datatable-->
                    <input type="hidden" name="id" id="id" value="{{$Id}}">
                    <table class="table table-hover" id="BankTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Delivery Agent Name</th>
                                <th>Transaction Id</th>
                                <th>Total Amount</th>
                                <th>TDS</th>
                                <th>Paid Amount</th>
                                <th>Status</th>
                                <!-- <th>Max Amount</th> -->
                                <!-- <th style="width: 80px;">End Date</th> -->
                                <!-- <th>One Day</th> -->
                                <th style="width: 75px;">Date</th>
                                <!-- <th>Action</th> -->
                            </tr>
                        </thead>
                    </table>
                    <!--end: Datatable-->
                </div>
                <div class="p-5">
                    <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" onclick="window.history.go(-1); return false;">Back</a>
                </div>
                
            </div>
            
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
</div>

@endsection

@push('js')
<script>
    $(function() {

    	$('#editForm').bootstrapValidator();
        $(document).on('click', '#closeModal', function(e) {
            e.preventDefault();
            /* Act on the event */
            $('.perc_price').html('Percentage/Price');
            $('#description').val("");
            $('#editForm').bootstrapValidator('resetForm', true);
            $('.upto_amount').css('display','none');
            $('.promo').css('display','none');
            $('#error').css('display','none');
            $("#btn_save").html("Save");
            $('#id').val("");
        });





        $(document).ready(function() {
            var id = $("#id").val();
            $table = $("#myTable").DataTable({
                'serverMethod': 'get',
                "ajax": {
                    'url':"{{ route('transaction.data') }}",
                    "data":{
                        "_token":"{{csrf_token()}}",
                        "id": id,
                    },
                },
                "processing": true,
                "serverSide": true,
                "ordering": false,
                "columns": [
                  /*{ "data": "id",
                    render:function(data,type,row,meta){
                        return meta.row + 1;
                    }
                },*/
                {
                    data: "id",
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1
                        }
                },
                { "data": "order_id" ,
                render: function(data, type, row, meta) {
                            return `<a href="${'/pickup/view/'+btoa(row.order_id)}">${row.order_id}</a>`;
                    }
                },
                { "data": "order_name",
                        render: function(data, type, row, meta) {
                            if(row.orders)
                            {
                                return row.orders.item_name;
                            }else {
                                return '-';
                            }
                        }
                },
                { "data": "amount"
                },
                { "data": "mobile_number" },
                { "data": "status" },

                // { "data": "file",
                //     render: function(data,type,row,meta){
                //      var data = row.file;
                //      if(data.length >= 20)
                //      {
                //         return data.substr(0,20) + '....';
                //      }

                //      return  data;
                //      // console.log('df',row.description);
                //    }
                // },
                { "data": "created_at" },
                // { "data": "id",
                // render:function(data,type,row,meta){
                //     var id =btoa(row.id);
                //     $html = '<div class="dropdown mo-mb-2">'+
                //                   '<a href="#" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                //                       'Action'+
                //                   '</a>'+

                //                   '<div class="dropdown-menu" x-placement="bottom-start" style="cursor: pointer;">'

                //                     $html+= '<a href="{{ url('transaction-history/view') }}/'+id+'" class="dropdown-item" data-id="'+row.id+'"><i class="fa fa-eye text-primary"></i>  View History</a>' ;
                //                   '</div>'+
                //               '</div>';
                //       return $html;
                //     }
                // },
                ]
            });

            $table = $("#BankTable").DataTable({
                'serverMethod': 'get',
                "ajax": {
                    'url':"{{ route('bank.transaction') }}",
                    "data":{
                        "_token":"{{csrf_token()}}",
                        "id": id,
                    },
                },
                "processing": true,
                "serverSide": true,
                "ordering": false,
                "columns": [
                  /*{ "data": "id",
                    render:function(data,type,row,meta){
                        return meta.row + 1;
                    }
                },*/
                {
                    data: "id",
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1
                        }
                },
                { "data": "user_name",
                        render: function(data, type, row, meta) {
                            if(row.customers)
                            {
                                return row.customers.name;
                            }else {
                                return '-';
                            }
                        }
                },
                { "data": "transaction_id" },
                { "data": "total_amount",
                    render: function(data, type, row, meta) {
                        return row.tds+row.amount
                    }
                },
                { "data": "tds"},
                { "data": "amount"},
                { "data": "status" },

                // { "data": "file",
                //     render: function(data,type,row,meta){
                //      var data = row.file;
                //      if(data.length >= 20)
                //      {
                //         return data.substr(0,20) + '....';
                //      }

                //      return  data;
                //      // console.log('df',row.description);
                //    }
                // },
                { "data": "created_at" },
                // { "data": "id",
                // render:function(data,type,row,meta){
                //     var id =btoa(row.id);
                //     $html = '<div class="dropdown mo-mb-2">'+
                //                   '<a href="#" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                //                       'Action'+
                //                   '</a>'+

                //                   '<div class="dropdown-menu" x-placement="bottom-start" style="cursor: pointer;">'

                //                     $html+= '<a href="{{ url('transaction-history/view') }}/'+id+'" class="dropdown-item" data-id="'+row.id+'"><i class="fa fa-eye text-primary"></i>  View History</a>' ;
                //                   '</div>'+
                //               '</div>';
                //       return $html;
                //     }
                // },
                ]
            });
        });

        $(document).on('click', '.edit_plan', function(e) {
            // document.querySelector('#file').required = false;
            // // $('#file').prop('required',false);
            // $('#file').removeAttr('data-bv-notempty');
            // $('#file').removeAttr('bv-notempty-message');
            // $('#file').removeAttr('data-bv-field');
            $('#editForm').bootstrapValidator('enableFieldValidators', 'file', false);

            e.preventDefault();
            /* Act on the event */
            var id = $(this).data('id');
            var title = $(this).data('title');
            var description = $(this).data('description');
            var slider_type = $(this).data('slider_type');
            var image_url = $(this).data('image_url');
            var file = $(this).data('file');

            $("#id").val(id);
            $("#title").val(title);
            $("#description").val(description);
            $("#slider_type").val(slider_type).trigger('change');
            $("#image_url").val(image_url);
            $("#file").val(file);
            $("#btn_save").html("Edit");

        });




        $(document).on("click", '.btn_delete', function(event) {
            var id =$(this).attr('data-id');
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this Slider.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: '',
                        type: 'post',
                        data: {
                            slider_id: id,_token:"{{csrf_token()}}"
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (response.status == "success") {
                                swal(response.message, {
                                    icon: 'success',
                                }).then((result) => {
                                    $('#myTable').DataTable().ajax.reload(null,false);
                                });
                            } else {
                                swal("Error","Something Wrong try agian..",{icon:'error'});
                            }
                        }
                    });
                } else {
                    swal("Your Slider is Safe.");
                }
            });
        });



    });
</script>
@endpush
