@extends('layouts.app')
<style>
    .sorting::after,.sorting::before{
        visibility: hidden;
    }
</style>
@section('content')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-2">
                    <!--begin::Page Title-->
                    <i class="fa fa-truck text-danger"  aria-hidden="true"></i>
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;GST Report</h5>
                    <!--end::Page Title-->
                    <!--begin::Actions-->
                    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

                {{--  <a href="{{ url('deliveryboy/add') }}" class="btn btn-light-warning font-weight-bolder btn-sm"
                 data-keyboard="false" data-backdrop="static">Add New Deliveryboy</a> --}}
                <!--end::Actions-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        @php
            $totalcgst = 0;
            $totalsgst = 0;
        @endphp
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--end::Notice-->
                <!--begin::Card-->
                <div class="card card-custom">
                    {{-- <div class="card-header">
                        <div class="card-title">
                            <span class="card-icon">
                                <i class="fa fa-truck text-danger" aria-hidden="true"></i>
                            </span>
                            <h3 class="card-label">Pickup List</h3>

                             <a href="{{ url('deliveryboy/add') }}" class="btn btn-light-danger font-weight-bolder btn-sm" data-keyboard="false"
                            data-backdrop="static">Add New Deliveryboy</a>
                        </div>
                    </div> --}}
                    <div class="card-body" style="overflow-x: auto;">
                        <div class="row mb-10">
                            <div class="col-md-4">
                                <form action="{{ route('gstreport.index') }}" id="frmgst">
                                    <input type="text" class="form-control" name="daterangepicker" id="daterangepicker"/>
                                </form>
                            </div>
                            <div class="col-md-4">
                                <a href="javascript:void(0)" class="btn btn-warning" id="exportpdf">Export</a>
                            </div>
                        </div>
                        <table class="table table-hover" id="myTable">
                            <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Mobile Number</th>
                                <th>Customer Name</th>
                                <th>Item Name</th>
                                <th>Payment Type</th>
                                <th>Price</th>
                                <th>CGST</th>
                                <th>SGST</th>
                                <th class="text-center">Date/Time</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($pickup as $single)
                                    @php
                                        $totalcgst += $single->cgst;
                                        $totalsgst += $single->sgst;
                                    @endphp
                                    <tr>
                                        <td>{{$single->id}}</td>
                                        <td>{{ ($single->customer && $single->customer->mobile_number)?$single->customer->mobile_number:""}}</td>
                                        <td>{{ ($single->customer && $single->customer->name)?$single->customer->name:""}}</td>
                                        <td class="d-inline-block w-105px text-wrap itemname">{{$single->item_name}}</td>
                                        @php
                                            $paymenttype = '';
                                            if($single->payment_id != null && $single->wallet_amount == 0)
                                                $paymenttype = 'Online';
                                            else if($single->payment_id != null && $single->wallet_amount != 0)
                                                $paymenttype = 'Online+Wallet';
                                            else if($single->payment_id == null && $single->wallet_amount != 0)
                                                $paymenttype = 'Wallet';
                                            else if($single->promo_amount > 0 && $single->v2 == 'true' && ($single->paid_amount == 0 || $single->paid_amount == null))
                                                $paymenttype = 'Discount';
                                            else if($single->promo_amount > 0 && $single->v2 == 'false' && $single->original_amount == 0)
                                                $paymenttype = 'Discount';
                                            else
                                                $paymenttype = 'Online';

                                        @endphp
                                        <td>{{$paymenttype}}</td>
                                        <td>{{$single->original_amount}}</td>
                                        <td>{{$single->cgst}}</td>
                                        <td>{{$single->sgst}}</td>
                                        <td>{{ date("d-m-Y H:i:s",strtotime($single->created_at)) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 ml-9 mb-9">
                            <h4>Total CGST : ₹ {{ $totalcgst }}</h4>
                            <h4>Total SGST : ₹ {{ $totalsgst }}</h4>
                        </div>
                    </div>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!-- Content Wrapper. Contains page content -->
@endsection
@push('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endpush
@push('js')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script>
        $(document).ready(function() {
            $(".itemname").each(function( index ) {
                if($(this).text().length > 20){
                    $(this).attr("title", $(this).text());
                    var item = $(this).text().substring(0,20)+'...'
                    $(this).html(item)
                    $(this).attr("data-toggle", "tooltip");
                    $(this).attr("data-theme", "dark");
                }
            });



            var start = moment().startOf('month');
            var end = moment();

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }
            $('#daterangepicker').daterangepicker({
                startDate: start,
                endDate: end,
                maxDate: end,
                timePicker: false,
                locale: {
                    format: 'DD/MM/YYYY'
                },
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);


            $("#myTable").DataTable({
                "sorting": false,
            });

            $('#daterangepicker').on('change.datepicker', function(ev){
                $("#frmgst").submit();
            });

            $(document).on('click',"#exportpdf",function(e){
                var date = $('#daterangepicker').val();
                window.location.href = "/gst-report/exportCsv?date="+date;
            });

            function setdefaultdate(){
                var uri = location.href;
                if(uri.indexOf("?daterangepicker") > 0){
                    var newdate = uri.split('?daterangepicker=');
                    $('#daterangepicker').val(decodeURIComponent(newdate[1]).replaceAll("+"," "));

                }
            }
            setdefaultdate();
        });
    </script>
@endpush
