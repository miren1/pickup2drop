@extends('layouts.app')
@push('css')
<style type="text/css">
* {
	box-sizing: border-box;
}

input[type="file"] {
	position: absolute;
	right: -9999px;
	visibility: hidden;
	opacity: 0;
}
input[type="submit"] {
	position: relative;
	padding: 1rem 3rem;
	background: #0c8fda;
	display: inline-block;
	text-align: center;
	overflow: hidden;
	border-radius: 10px;
	border: 0;
	color:#fff;
	&:hover {
		background: darken(#0c8fda, 5);
		color: #fff;
		cursor: pointer;
		transition: 0.2s all;
	}
}
.label {
	position: relative;
	padding: 3rem 3rem;
	background: #eee;
	display: inline-block;
	text-align: center;
	overflow: hidden;
    font-size: 20px;
	border-radius: 10px;
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
	&:hover {
		background: #0c8fda;
		color: #fff;
		cursor: pointer;
		transition: 0.2s all;
	}
}

div {
	&.files {
		background: #eee;
		padding: 1rem;
		margin:1rem 0;
		border-radius:10px;
		ul{
			list-style:none;
			padding:0;
			max-height:150px;
			overflow:auto;
			li{
				padding:0.5rem 0;
				padding-right:2rem;
				position:relative;
				i{
					cursor:pointer;
					position:absolute;
					top:50%;
					right:0;
					transform:translatey(-50%);
				}
			}
		}
	}
	&.container {
		width: 100%;
		padding: 0 2rem;
	}
}

span.file-size {
	color: #999;
	padding-left: 0.5rem;
}

    td{
    vertical-align:middle !important;
}
  body {
  background: whitesmoke;
  /* font-family: 'Open Sans', sans-serif; */
}
</style>

@endpush
@section('content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <i class="fa icon-file-text text-danger" style="transform: rotateY(160deg);" aria-hidden="true"></i>
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;TDS Certificate Upload</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
            </div>
        </div>
    </div>
    <div class="d-flex flex-column-fluid">
        <div class="container">
             <div class="card card-custom">
                <div class="card-body row" >
                    <div class="col-md-12">
                         <form id="tdsForm" action="{{url('tds-certificate/tdsformstore')}}" method="POST"
                                    enctype="multipart/form-data" class="form-horizontal">
                                    @csrf
                                    <div class="my-3 form-group">
                                         <Label>Financial Year</Label>
                                        <select id="financial_year" class="form-control"
                                                    name="financial_year" required data-bv-notempty="true" data-bv-notempty-message="Please select financial year">
                                        <option value="">Select Financial Year</option>
                                            @for ($year = date('Y')-1; $year < date('Y') -1 + 20; $year++)
                                                <option value="{{$year}} - {{$year+1}}">{{$year}} - {{$year+1}}</option>
                                            @endfor
                                        </select>
                                    </div>
                               	<div class="my-3 form-group">
                                    <label for="uploadTds" class="label col-md-12 form-control">
                                        <input type="file" name="tds_image[]" class="form-control" accept="application/pdf" id="uploadTds" multiple >
                                    Browse Files
                                    </label>
                                    <p class="text-danger my-2 pdf-error">* Only pdf files are supported</p>
                                </div>
                                <div class="files my-5 d-none previewFile" >
                                    <h6>File List</h6>
                                    <ul id="filelist" class="list-group" style="max-height:350px;overflow-x:auto"></ul>
                                </div>
                                {{-- <input type="submit" value="Submit" name="submit" id="submit"> --}}
                                <div class="col-md-12 mt-3">
                                    <button type="submit" id="btn_save" class="btn btn-danger btn-lg">Save</button>
                                    <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" href="{{ route('tds-certificate.index') }}">Back</a>
                                        {{-- <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" href="{{ route('deliveryboy.index') }}">Back</a> --}}
                                </div>
                             <input type="hidden" name="actualFileList" id="actualFileList" />
                          </form>
                    </div>
                    {{-- <div class="mt-5 mb-2 col-md-12">
                      <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" href="{{ route('tds-certificate.index') }}">Back</a>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('js')
<script>
    $(document).ready(function() {
      $('#tdsForm').bootstrapValidator()
    });
// no react or anything
let state = {};

// state management
function updateState(newState) {
	state = { ...state, ...newState };
}

// event handlers
$("#uploadTds").change(function(e) {
	let files = document.getElementById("uploadTds").files;
	let filesArr = Array.from(files);
	if(filesArr.length > 100){
		filesArr = filesArr.slice(0,100)
		$(".pdf-error").html('* You can not upload more than 100 files.');
	}
	updateState({ files: files, filesArr: filesArr });

	renderFileList();
});

$(".files").on("click", "li > i", function(e) {
	let key = $(this)
		.parent()
		.attr("key");
	let curArr = state.filesArr;

	curArr.splice(key, 1);
	updateState({ filesArr: curArr });
	renderFileList();
    if(curArr.length == 0){
        $(".previewFile").removeClass('d-block');
    }
});

// $("form").on("submit", function(e) {
// 	e.preventDefault();
// 	console.log(state);
// 	renderFileList();
// });

// render functions
function renderFileList() {
    $("#actualFileList").val([]);
    let tempArr = [];
	let fileMap = state.filesArr.map((file, index) => {
          $(".previewFile").addClass('d-block');

		let suffix = "bytes";
		let size = file.size;
		if (size >= 1024 && size < 1024000) {
			suffix = "KB";
			size = Math.round(size / 1024 * 100) / 100;
		} else if (size >= 1024000) {
			suffix = "MB";
			size = Math.round(size / 1024000 * 100) / 100;
		}
        tempArr.push(`${file.name}`);
		return `<li class="p-3 list-group-item" key="${index}"><span class="col-md-2 mx-2 w-100px">${index+1}</span> ${
			file.name
		} <span class="file-size">${size} ${suffix}</span><i class="fa fa-trash text-danger float-right cursor-pointer"></i></li>`;
	});
	$("#filelist").html(fileMap);
    $("#actualFileList").val(tempArr);
}

</script>
@endpush

