@extends('layouts.app')

@section('content')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-2">
                    <!--begin::Page Title-->
                    <i class="fa fa-truck text-danger" style="transform: rotateY(160deg);" aria-hidden="true"></i>
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Delivery Agent List</h5>

                    <!--end::Page Title-->
                    <!--begin::Actions-->
                    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

                    <a href="{{ url('deliveryboy/add') }}"
                       class="btn btn-light-danger font-weight-bolder btn-sm float-right"
                       data-keyboard="false" data-backdrop="static">Add New Delivery Agent</a>
                    <!--end::Actions-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--end::Notice-->
                <!--begin::Card-->
                <div class="card card-custom">
                    <div class="card-body" style="overflow-x: auto;">
                        <div class="row">
                            <div class="col">
                                <div class="mb-4 float-left d-block w-50">
                                    <select class="form-control form-checkbox" id="sel_city">
                                        <option value="all">----All----</option>
                                        @foreach($cities as $city)
                                            <option value="{{$city->id}}">{{$city->city_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="mb-4 float-right d-block w-100 text-right">
                                    <a class="btn btn-outline-primary mr-2" onclick="setOrderType('all')">&nbsp;&nbsp;&nbsp;&nbsp;
                                        All &nbsp;&nbsp;&nbsp;&nbsp;</a>
                                    <a class="btn btn-outline-success mr-2" onclick="setOrderType('active')">Active</a>
                                    <a class="btn btn-outline-danger mr-2"
                                       onclick="setOrderType('deactive')">Deactive</a>
                                </div>
                            </div>
                        </div>


                        <!--begin: Datatable-->
                        <table class="table table-hover" id="myTable">
                            <thead>
                            <tr>
                                <th width="10%" class="sorting_disabled">No</th>
                                <th width="20%">Name</th>
                                <th class="min-w-125px text-center" width="25%">Mobile</th>
                                <th class="min-w-125px text-center">City</th>
                                <th class="min-w-125px text-center"
                                    aria-label="Role: activate to sort column ascending">Wallet
                                </th>
                                <th width="25%" class="text-center">status</th>
                                <th width="20%">Action</th>
                            </tr>
                            </thead>
                        </table>
                        <!--end: Datatable-->
                        <div class="container">
                            <div class="modal fade" id="myModal">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h4 class="modal-title">Delivery Agent Details</h4>
                                            <button type="button" class="close" id="closeModal"
                                                    data-dismiss="modal">&times;
                                            </button>
                                        </div>
                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            <h4 id="error-edit" style="display: none;" class='alert alert-danger'></h4>
                                            <form class="form-horizontal">
                                                @csrf
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <input type="hidden" name="id" id="id">
                                                        <label>Name</label>
                                                        <input type="text" id="name" name="name" class="form-control"
                                                               readonly>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Email</label>
                                                        <input type="email" id="email" name="email" class="form-control"
                                                               readonly>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label>Birth Date</label>
                                                        <input type="text" name="birthdate" id="birthdate"
                                                               class="form-control" readonly>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Joining Date</label>
                                                        <input type="text" name="joining_date" id="joining_date"
                                                               class="form-control" readonly>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <!-- <input type="hidden" name="id" id="id"> -->
                                                        <label>Phone Number</label>
                                                        <input type="text" id="mobile_number" name="mobile_number"
                                                               class="form-control" readonly>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Slider Type</label>
                                                        <select class="form-control" id="type" name="type" disabled>
                                                            <option value="">Select Type</option>
                                                            <option value="Deliveryboy">Delivery Agent</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Gender</label><br>
                                                        <div class="radio-inline">
                                                            <label class="radio radio-danger">
                                                                <input type="radio" id="True" value="male" name="gender"
                                                                       onclick="return false;"/>
                                                                <span></span>Male</label>
                                                            <label class="radio radio-danger">
                                                                <input type="radio" id="False" name="gender"
                                                                       value="female" onclick="return false;"/>
                                                                <span></span>Female</label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <!-- <input type="hidden" name="id" id="id"> -->
                                                        <label>Bank Name</label>
                                                        <input type="text" id="bank_name" name="bank_name"
                                                               class="form-control" readonly>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <!-- <input type="hidden" name="id" id="id"> -->
                                                        <label>Bank Account Number</label>
                                                        <input type="text" id="bank_account_number"
                                                               name="bank_account_number" class="form-control" readonly>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <!-- <input type="hidden" name="id" id="id"> -->
                                                        <label>Account Holder Name</label>
                                                        <input type="text" id="bank_account_name"
                                                               name="bank_account_name" class="form-control" readonly>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <!-- <input type="hidden" name="id" id="id"> -->
                                                        <label>Bank Account IFSC</label>
                                                        <input type="text" id="bank_account_ifsc"
                                                               name="bank_account_ifsc" class="form-control" readonly>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <!-- <input type="hidden" name="id" id="id"> -->
                                                        <label>Bank Account Status</label>
                                                        <input type="text" id="bank_details_status"
                                                               name="bank_details_status" class="form-control" readonly>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Address</label>
                                                        <textarea type="text" id="address" name="address"
                                                                  class="form-control" readonly></textarea>
                                                    </div>
                                                </div>
                                                <!-- /.card-body -->
                                            </form>
                                        </div>
                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" id="closeModal"
                                                    data-dismiss="modal">Close
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            {{-- Edit wallet amount modal start --}}
                            <div class="modal fade" id="walletModal" tabindex="-1" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered mw-650px">
                                    <div class="modal-content">
                                        <div class="modal-header" id="wallet_header">
                                            <h2 class="fw-bolder">Wallet Amount</h2>
                                            <div class="btn btn-icon btn-sm btn-active-icon-primary"
                                                 data-dismiss="modal"
                                                 data-kt-users-modal-action="close">
                                                <span class="svg-icon svg-icon-1">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                 height="24" viewBox="0 0 24 24" fill="none">
                                                                <rect opacity="0.5" x="6" y="17.3137" width="16"
                                                                      height="2" rx="1"
                                                                      transform="rotate(-45 6 17.3137)"
                                                                      fill="black"></rect>
                                                                <rect x="7.41422" y="6" width="16" height="2"
                                                                      rx="1" transform="rotate(45 7.41422 6)"
                                                                      fill="black"></rect>
                                                            </svg>
                                                        </span>
                                            </div>
                                        </div>
                                        <div class="modal-body scroll-y mx-5 mx-xl-15 my-7">
                                            <form id="wallet_form"
                                                  class="form fv-plugins-bootstrap5 fv-plugins-framework"
                                                  method="post"
                                                  action="{{ route('update.wallet') }}"
                                            >
                                                @csrf
                                                <input type="hidden" name="da_id" id="da_id">
                                                <div class="d-flex flex-column scroll-y me-n7 pe-7"
                                                     id="wallet_scroll" data-kt-scroll="true"
                                                     data-kt-scroll-activate="{default: false, lg: true}"
                                                     data-kt-scroll-max-height="auto"
                                                     data-kt-scroll-dependencies="#wallet_header"
                                                     data-kt-scroll-wrappers="#wallet_scroll"
                                                     data-kt-scroll-offset="300px" style="max-height: 373px;">
                                                    <div class="fv-row mb-7 fv-plugins-icon-container">
                                                        <label class="required fw-bold fs-6 mb-2">Enter Amount</label>
                                                        <input required type="text" id="wallet_amount"
                                                               name="wallet_amount"
                                                               class="form-control"
                                                               placeholder="Enter Amount" autocomplete="off">
                                                        <div
                                                            class="fv-plugins-message-container invalid-feedback"></div>
                                                    </div>
                                                    <div id="wallet_error" class="text-danger"></div>
                                                </div>
                                                <div class="text-center pt-15">
                                                    <button type="submit" class="btn btn-danger mx-2" id="wallet_submit"
                                                            data-kt-users-modal-action="submit">
                                                        <span class="indicator-label">Submit</span>
                                                    </button>
                                                    <button type="button" class="btn btn-light mx-2"
                                                            data-dismiss="modal"
                                                            data-kt-users-modal-action="cancel">Discard
                                                    </button>

                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- Edit wallet amount modal end --}}


                        </div>
                    </div>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!-- Content Wrapper. Contains page content -->
@endsection

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
    <script>

        function setOrderType(ffilter) {
            $table.state.clear()
            var uri = location.href;
            var filter = 'all';
            if (uri.indexOf("?") > 0) {
                if (uri.includes('&status')) {
                    var new_url = uri.split('&status');
                    window.location.replace(`${new_url[0]}&status=${ffilter}`);
                } else if (uri.includes('?status')) {
                    var new_url = uri.split('?status');
                    window.location.replace(`${new_url[0]}?status=${ffilter}`);
                } else {
                    window.location.replace(`${uri}&status=${ffilter}`);
                }
            } else {
                window.location.replace(`?status=${ffilter}`);
            }
            $(`#driver_status option[value="${ffilter}"]`)
        }

        $(document).on('change', '#sel_city', function (e) {
            $table.state.clear()
            var uri = location.href;
            var city_id = $(this).val();

            var status_uri_part = '';
            if (uri.includes("status")) {
                status_uri_part = '&status=' + uri.split("status=")[1];
            }

            if (uri.includes('&city_id')) {
                var new_url = uri.split('&city_id');
                window.location.replace(`deliveryboy&city_id=` + city_id + status_uri_part);
            } else if (uri.includes('?city_id')) {
                var new_url = uri.split('?city_id');
                window.location.replace(`deliveryboy?city_id=` + city_id + status_uri_part);
            } else {
                window.location.replace(`deliveryboy?city_id=` + city_id + status_uri_part);
            }
        });

        $(document).ready(function () {
            var uri = location.href;
            var filter = 'all';
            if (uri.indexOf("?") > 0)
                var filter = uri.substring(uri.indexOf("?") + 1);
            //$(`#driver_status option[value="${filter}"]`).attr("selected", "selected")

            if(uri.indexOf("city_id") > 0){
                var new_f = uri.substring(uri.indexOf("?") + 1);
                if(new_f.indexOf("&") > 0){
                    var uriArr = new_f.split("&");
                    $.each(uriArr,function(k,v){
                       if(v.indexOf("city_id") > -1){
                           var new_filter = v.split("=");
                           $(`#sel_city option[value="${new_filter[1]}"]`).attr("selected", "selected")
                       }
                    });
                }else{
                    var new_filter = new_f.split("=");
                    $(`#sel_city option[value="${new_filter[1]}"]`).attr("selected", "selected")
                }
            }

            $table = $("#myTable").DataTable({
                "ajax": `{{ url('deliveryboy/get_all?${filter}') }}`,
                "processing": true,
                "serverSide": true,
                "ordering":true,
                order: [[0, 'desc']],
                'stateSave': true,
                "pageLength": 100,
                "columns": [
                    /*{ "data": "id",
                      render:function(data,type,row,meta){
                        console.log(meta);
                          return meta.row + 1;
                      }
                  },*/

                    {"data": "id"},
                    {"data": "name"},
                    {"data": "mobile_number", class: 'text-center'},
                    {"data": "city.city_name"},
                    {"data": "wallet", class: 'text-center'},
                    {
                        "data": "driver_status", name: 'driverdoc.driver_status',
                        render: function (data, type, row, meta) {
                            var status = row.driver_status == 'active' ? "" : 'checked';
                            return `<div class="toggle-button-cover d-flex justify-content-center align-items-center">
                    <div>
                        <div class="button r my-auto" id="button-1">
                        <input id="update_status" data-id="${row.id}" type="checkbox" ${status} class="checkbox" />
                        <div class="knobs"></div>
                        <div class="layer"></div>
                        </div>
                    </div>
                    </div>`;
                        },
                    },
                    {
                        "data": "id",
                        render: function (data, type, row, meta) {
                            var address = row.default_home_addresses && row.default_home_addresses.address ? row.default_home_addresses.address : '';
                            $id = btoa(row.id);
                            $html = '<div class="dropdown mo-mb-2">' +
                                '<a href="#" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                                'Action' +
                                '</a>' +

                                '<div class="dropdown-menu" x-placement="bottom-start" style="cursor: pointer;">' +

                                '<a class="dropdown-item d-block" href="{{ url('deliveryboy/edit') }}/' + $id + '"><i class="fa fa-edit text-primary"></i>&nbsp; Edit</a>' + ' ';

                            $html += '<a class="dropdown-item btn_delete d-block" data-url="user/delete" data-id="' + row.id + '"><i class="fa fa-trash text-danger"></i>&nbsp; Delete</a>' + '';

                            $html += '<a class="dropdown-item view_deliverboy d-block" data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#myModal" data-bank_name="' + row.bank_name + '"data-id="' + row.id + '"data-name="' + row.name + '"data-mobile_number="' + row.mobile_number + '" data-birthdate="' + row.birthdate + '" data-email="' + row.email + '" data-type="' + row.type + '" data-gender="' + row.gender + '" data-bank_account_number="' + row.bank_account_number + '" data-bank_account_name="' + row.bank_account_name + '" data-bank_account_ifsc="' + row.bank_account_ifsc + '" data-bank_details_status="' + row.bank_details_status + '" data-address="' + address + '" data-joining_date="' + row.driverdoc.joining_date + '"><i class="fa fa-eye text-primary"></i>&nbsp; View</a>' + ' ';

                            $html += '<a class="dropdown-item btn_docs d-block"  data-id="' + row.id + '"><i class="fa fa-file text-info"></i>&nbsp; Docs</a>' + '';

                            $html += '<a class="dropdown-item wallet_edit d-block" data-backdrop="static" data-toggle="modal" data-target="#walletModal" data-id="' + row.id + '" data-wallet="' + row.wallet + '"><i class="fa fa-wallet text-warning"></i>&nbsp; Edit Wallet</a>' + '';
                            '</div>' +
                            '</div>';
                            return $html;
                        }
                    },
                ],
                'columnDefs': [{orderable: false, targets: [1,2,3,5,6]}],
            });
        });

        $(document).on("click", '.btn_docs', function (event) {
            var id = $(this).attr('data-id');
            window.location.replace('deliveryboy/docs/' + btoa(id));
        });
        $(document).on("click", '.btn_delete', function (event) {
            var id = $(this).attr('data-id');
            swal({
                title: "Are you sure?",
                text: "You Want To Delete This Delivery Agent,You Will Not Be Able To Recover This Delivery Agent.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {

                        $.ajax({
                            url: '{{ url('deliveryboy/destroy') }}/' + id,
                            type: 'get',
                            dataType: 'json',
                            //data: {id: 'id'},
                            success: function ($data) {
                                if ($data == 'unauthorized_access') {
                                    swal("Unauthorized access.", {
                                        icon: "error",
                                    });
                                } else if ($data == true) {
                                    swal("Delivery Agent has been Deleted.", {
                                        icon: "success",
                                    });
                                    $('#myTable').DataTable().ajax.reload();
                                } else {
                                    swal("Delivery Agent has been not Deleted.", {
                                        icon: "error",
                                    });

                                }
                                //$('#city').DataTable().ajax.reload();
                            }
                        });


                    } else {
                        swal("Your Delivery Agent is Safe.");
                    }
                });
        });

        var wallet = 0;
        $(document).on('click', '.wallet_edit', function (e) {
            e.preventDefault();
            wallet = $(this).data('wallet');
            var id = $(this).data('id');

            $("#da_id").val(id);
            $("#wallet_amount").val(wallet);
            $('#wallet_error').html("");
            $("#wallet_submit").removeAttr("disabled");
        });


        $("#wallet_amount").on("keyup", function (e) {
            var wallet_amount = $('.wallet_edit').data('wallet');
            var enter_amount = $("#wallet_amount").val();

            if (wallet < enter_amount) {
                $('#wallet_error').html("Please enter amount bellow wallet amount");
                $("#wallet_submit").attr("disabled", true);
            } else {
                $('#wallet_error').html("");
                $("#wallet_submit").removeAttr("disabled");
            }
        });

        $(document).on('click', '.view_deliverboy', function (e) {
            $(document).on('change', '#type', function () {
                return false;
            });

            e.preventDefault();
            /* Act on the event */
            var id = $(this).data('id');
            var name = $(this).data('name');
            var mobile_number = $(this).data('mobile_number');
            var birthdate = $(this).data('birthdate');
            var email = $(this).data('email');
            var type = $(this).data('type');
            var gender = $(this).data('gender');
            var bank_details_status = $(this).data('bank_details_status');
            var bank_account_number = $(this).data('bank_account_number');
            var bank_account_name = $(this).data('bank_account_name');
            var bank_account_ifsc = $(this).data('bank_account_ifsc');
            var address = $(this).data('address');
            var bank_name = $(this).data('bank_name');
            var joining_date = $(this).data('joining_date');

            if (joining_date)
                joining_date = moment(joining_date).format('DD/MM/YYYY');

            $("#id").val(id);
            $("#name").val(name);
            $("#mobile_number").val(mobile_number);
            $("#type").val(type).trigger('change');
            $("#birthdate").val(birthdate);
            $("#email").val(email);
            $("#bank_details_status").val(bank_details_status);
            $("#bank_account_number").val(bank_account_number);
            $("#bank_account_name").val(bank_account_name);
            $("#bank_account_ifsc").val(bank_account_ifsc);
            $("#address").val(address);
            $("#bank_name").val(bank_name);
            $("#joining_date").val(joining_date);


            if (gender == 'Male') {
                $("#True").prop("checked", true);
            } else {
                $("#False").prop("checked", true);
            }

        });

        $(document).on("click", '#update_status', function (event) {
            var id = $(this).attr('data-id');
            var status;
            if (event.currentTarget.checked) {
                status = 'suspended';
            } else {
                status = 'active';
            }
            $.ajax({
                url: `deliveryboy/toogle-delivery-agent/${id}/${status}`,
                type: 'get',
                success: function (response) {

                    if (response.status == "success") {
                        swal(response.message, {
                            icon: 'success',
                        }).then((result) => {
                            $('#myTable').DataTable().ajax.reload(null, false);
                        });
                    } else {
                        swal("Error", response.message, {icon: 'error'});
                        $('#myTable').DataTable().ajax.reload(null, false);
                    }
                }
            });


        });

    </script>
@endpush
<style>


    .toggle-button-cover {
        display: table-cell;
        position: relative;
        /* width: 100px; */
        top: 12%;
        box-sizing: border-box;
    }


    .button-cover:before {
        counter-increment: button-counter;
        content: counter(button-counter);
        position: absolute;
        right: 0;
        bottom: 0;
        color: #d7e3e3;
        font-size: 12px;
        line-height: 1;
        padding: 5px;
    }

    .button-cover,
    .knobs,
    .layer {
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        height: 13px;
    }

    .button {
        position: relative;
        top: 50%;
        width: 80px;
        height: 25px;
        margin: -20px auto 0 auto;
        overflow: hidden;
    }

    .button.r,
    .button.r .layer {
        border-radius: 100px;
    }

    .button.b2 {
        border-radius: 2px;
    }

    .checkbox {
        position: relative;
        width: 100%;
        height: 100%;
        padding: 0;
        margin: 0;
        opacity: 0;
        cursor: pointer;
        z-index: 3;
    }

    .knobs {
        z-index: 2;
    }

    .layer {
        width: 100%;
        background-color: #ebf7fc;
        transition: 0.9s ease all;
        z-index: 1;
    }

    /* Button 1 */
    #button-1 .knobs:before {
        content: "  Active  ";
        position: absolute;
        top: -3px;
        left: 0px;
        width: 80px;
        height: 40px;
        color: #fff;
        font-size: 11px;
        font-weight: 600;
        text-align: center;
        line-height: 1;
        padding: 9px 4px;
        background-color: #1BC5BD;
        border-radius: 1%;
        transition: 0.9s cubic-bezier(0.18, 0.89, 0.35, 1.15) all;
    }

    #button-1 .checkbox:checked + .knobs:before {
        content: "Deactive ";
        left: 00px;
        background-color: #F64E60;
    }

    #button-1 .checkbox:checked ~ .layer {
        background-color: #fcebeb;
    }

    #button-1 .knobs,
    #button-1 .knobs:before,
    #button-1 .layer {
        transition: 0.9s ease all;
    }

</style>
