@extends('layouts.app')

@section('content')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-2">
                    <!--begin::Page Title-->
                    <i class="fa fa-truck text-danger" style="transform: rotateY(160deg);" aria-hidden="true"></i>
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Delivery Agent</h5>
                    <!--end::Page Title-->
                    <!--begin::Actions-->
                    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

                    <!--end::Actions-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--end::Notice-->
                <!--begin::Card-->
                <div class="card card-custom">
                    {{-- <div class="card-header">
                        <div class="card-title justify-content-between flex-right align-items-right">
                            <span class="card-icon">
                                <i class="fa fa-hashtag text-danger" aria-hidden="true"></i>
                            </span>
                            <h3 class="card-label">Delivery Agent</h3>

                        </div>
                    </div> --}}
                    <div class="card-body" style="overflow-x: auto;">

                        <div class="container">
                            <form id="editForm" action="{{url('deliveryboy/store')}}" method="POST"
                                  enctype="multipart/form-data" class="form-horizontal">
                                @csrf

                                @if(isset($deliveryboy))
                                    <input type="hidden" name="id" value="{{ $deliveryboy->id }}">
                                @endif
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Name</label><span class="text-danger">*</span>
                                            <input type="text" name="name" class="form-control"
                                                   placeholder="Enter Your Name" required
                                                   data-bv-notempty="true"
                                                   data-bv-notempty-message="Name is required"
                                                   @if(isset($deliveryboy)) value="{{ $deliveryboy->name }}" @endif>

                                        </div>

                                        <div class="form-group">
                                            <label>Email</label><span class="text-danger">*</span>
                                            <input type="email" name="email" class="form-control"
                                                   placeholder="Enter Your Email" required
                                                   data-bv-notempty="true"
                                                   data-bv-notempty-message="Email is required"
                                                   @if(isset($deliveryboy)) value="{{ $deliveryboy->email }}" @endif>

                                        </div>

                                        <div class="form-group">
                                            <label>Type</label><span class="text-danger">*</span>
                                            <select class="form-control" name="type" data-bv-notempty="true"
                                                    data-bv-notempty-message="Type is required">
                                                <option value="">Select Type</option>
                                                <option
                                                    @if(isset($deliveryboy) && $deliveryboy->type=="Deliveryboy") selected=""
                                                    @endif value="Deliveryboy">Delivery Agent
                                                </option>
                                            </select>


                                        </div>

                                        <div class="form-group">
                                            <span class="form-text text-muted">Gender</span><br>
                                            <div class="radio-inline">
                                                <label class="radio radio-danger">
                                                    <input type="radio" id="True" value="male" name="gender"
                                                           @if(isset($deliveryboy) && $deliveryboy->gender =='Male') checked=""
                                                           @endif checked="checked"/>
                                                    <span></span>Male</label>
                                                <label class="radio radio-danger">
                                                    <input type="radio" id="False" name="gender" value="female"
                                                           @if(isset($deliveryboy) && $deliveryboy->gender =='Female') checked="" @endif />
                                                    <span></span>Female</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Birth Date</label><span class="text-danger">*</span>
                                            <div class="input-group">
                                                <input type="text" readonly class="form-control" id="datepicker"
                                                       name="birthdate" placeholder="Select a Date" required="required"
                                                       data-bv-notempty="true"
                                                       data-bv-notempty-message="Birthdate is required"
                                                       @if(isset($deliveryboy)) value="{{ $deliveryboy->birthdate }}" @endif>
                                                <label class="input-group-btn" for="datepicker">
                                        <span class="btn btn-default">
                                             <i class="fa fa-calendar"></i>
                                        </span>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Phone Number</label><span class="text-danger">*</span>
                                            <input type="text" name="mobile_number" class="form-control"
                                                   placeholder="Enter Your Phone Number" required
                                                   data-bv-notempty="true"
                                                   data-bv-stringlength="true"
                                                   data-bv-integer="true"
                                                   maxlength="10"
                                                   minlength="10"
                                                   onkeypress="return event.charCode>=48 && event.charCode<=57"
                                                   data-bv-notempty-message="Phone Number  is required"
                                                   @if(isset($deliveryboy)) value="{{ $deliveryboy->mobile_number }}" @endif>

                                        </div>

                                        @if(!isset($deliveryboy))
                                            <div class="form-group">
                                                <label>Password</label><span class="text-danger">*</span>
                                                <input type="password" name="password" class="form-control"
                                                       placeholder="Enter Your Password" required
                                                       data-bv-notempty="true"
                                                       data-bv-stringlength="true"
                                                       data-bv-stringlength-min="6"
                                                       data-bv-notempty-message="Password is required">

                                            </div>
                                        @else
                                            <div class="form-group">
                                                <label>Change Password</label>
                                                {{--<small>if you can change than only type</small>--}}
                                                <input type="password" name="password" class="form-control"
                                                       placeholder="Enter Your Password">

                                            </div>
                                        @endif
                                        <div class="form-group">
                                            <label>Address</label>
                                            <textarea name="address" id="" class="form-control"
                                                      placeholder="Enter Your Address ">@if(isset($deliveryboy) && $deliveryboy->defaultHomeAddresses && $deliveryboy->defaultHomeAddresses->address){{ trim($deliveryboy->defaultHomeAddresses->address) }}@endif</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12 my-3">
                                        <h4>Vehicle Details</h4>
                                        <hr>
                                        <div class="form-group">
                                            <label>Vehicle Model</label>
                                            <input type="text" name="vehicle_model" class="form-control"
                                                   placeholder="Enter Your Vehicle Model"
                                                   @if(isset($deliveryboy) && $deliveryboy->driverdoc && $deliveryboy->driverdoc->vehicle_model) value="{{ $deliveryboy->driverdoc->vehicle_model }}" @endif>
                                        </div>
                                        <div class="form-group">
                                            <label>Vehicle Number</label>
                                            <input type="text" name="vehicle_number" class="form-control"
                                                   placeholder="Enter Your Vehicle Number"
                                                   @if(isset($deliveryboy) && $deliveryboy->driverdoc && $deliveryboy->driverdoc->vehicle_number) value="{{ $deliveryboy->driverdoc->vehicle_number }}" @endif>
                                        </div>
                                        <div class="form-group">
                                            <label>Vehicle Color</label>
                                            <input type="text" name="vehicle_colour" class="form-control"
                                                   placeholder="Enter Your Vehicle Color"
                                                   @if(isset($deliveryboy) && $deliveryboy->driverdoc && $deliveryboy->driverdoc->vehicle_colour) value="{{ $deliveryboy->driverdoc->vehicle_colour }}" @endif>
                                        </div>
                                    </div>
                                    <div class="col-md-12 my-3">
                                        <h4>Documents</h4>
                                        <hr>
                                        <div class="form-group">
                                            <label>Driver Photo</label>
                                            <div class="border border-secondary">
                                                <input type="file" id="file" accept=".jpg,.jpeg,.png"
                                                       name="driver_photo" class="form-control" placeholder="File">
                                            </div>
                                            @if(isset($deliveryboy) && $deliveryboy->driverdoc && $deliveryboy->driverdoc->driver_photo)
                                                <img class="img-radius p-3"
                                                     src="/{{$deliveryboy->driverdoc->driver_photo}}" width="160">
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>RC Front Photo</label>
                                            <div class="border border-secondary">
                                                <input type="file" id="file" accept=".jpg,.jpeg,.png" name="rc_front"
                                                       class="form-control" placeholder="File"
                                                       value="{{isset($deliveryboy) && $deliveryboy->driverdoc && $deliveryboy->driverdoc->rc_front ? '/'.$deliveryboy->driverdoc->rc_front : ''}}">
                                            </div>
                                            @if(isset($deliveryboy) && $deliveryboy->driverdoc && $deliveryboy->driverdoc->rc_front)
                                                <img class="img-radius p-3" src="/{{$deliveryboy->driverdoc->rc_front}}"
                                                     width="160">
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>RC Back Photo</label>
                                            <div class="border border-secondary">
                                                <input type="file" id="file" accept=".jpg,.jpeg,.png" name="rc_back"
                                                       class="form-control" placeholder="File">
                                            </div>
                                            @if(isset($deliveryboy) && $deliveryboy->driverdoc && $deliveryboy->driverdoc->rc_back)
                                                <img class="img-radius p-3" src="/{{$deliveryboy->driverdoc->rc_back}}"
                                                     width="160">
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Aadhaar Front Photo</label>
                                            <div class="border border-secondary">
                                                <input type="file" id="file" accept=".jpg,.jpeg,.png"
                                                       name="addhar_front" class="form-control" placeholder="File">
                                            </div>
                                            @if(isset($deliveryboy) && $deliveryboy->driverdoc && $deliveryboy->driverdoc->addhar_front)
                                                <img class="img-radius p-3"
                                                     src="/{{$deliveryboy->driverdoc->addhar_front}}" width="160">
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Aadhaar Back Photo</label>
                                            <div class="border border-secondary">
                                                <input type="file" id="file" accept=".jpg,.jpeg,.png" name="addhar_back"
                                                       class="form-control" placeholder="File">
                                            </div>
                                            @if(isset($deliveryboy) && $deliveryboy->driverdoc && $deliveryboy->driverdoc->addhar_back)
                                                <img class="img-radius p-3"
                                                     src="/{{$deliveryboy->driverdoc->addhar_back}}" width="160">
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Pancard Photo</label>
                                            <div class="border border-secondary">
                                                <input type="file" id="file" accept=".jpg,.jpeg,.png" name="pan_card"
                                                       class="form-control" placeholder="File">
                                            </div>
                                            @if(isset($deliveryboy) && $deliveryboy->driverdoc && $deliveryboy->driverdoc->pan_card)
                                                <img class="img-radius p-3" src="/{{$deliveryboy->driverdoc->pan_card}}"
                                                     width="160">
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Pan Number</label>
                                            <div class="border border-secondary">
                                                <input type="text" name="pan_number" class="form-control" placeholder="Pan Number"
                                                    @if(isset($deliveryboy) && $deliveryboy->driverdoc && $deliveryboy->driverdoc->pan_number) value="{{ $deliveryboy->driverdoc->pan_number }}" @endif>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Insurance Photo</label>
                                            <div class="border border-secondary">
                                                <input type="file" id="file" accept=".jpg,.jpeg,.png"
                                                       name="insurance_certificate" class="form-control"
                                                       placeholder="File">
                                            </div>
                                            @if(isset($deliveryboy) && $deliveryboy->driverdoc && $deliveryboy->driverdoc->insurance_certificate)
                                                <img class="img-radius p-3"
                                                     src="/{{$deliveryboy->driverdoc->insurance_certificate}}"
                                                     width="160">
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Driving License Photo</label>
                                            <div class="border border-secondary">
                                                <input type="file" id="file" accept=".jpg,.jpeg,.png" 
                                                       name="tds_certificate" class="form-control" placeholder="File"
                                                       @if(isset($deliveryboy) && $deliveryboy->driverdoc && $deliveryboy->driverdoc->tds_certificate)
                                                        data-bv-notempty="false"
                                                       @else
                                                        data-bv-notempty="true"
                                                        data-bv-notempty-message="Driving License photo is required"
                                                       @endif
                                                       >
                                            </div>
                                            @if(isset($deliveryboy) && $deliveryboy->driverdoc && $deliveryboy->driverdoc->tds_certificate)
                                                <img class="img-radius p-3"
                                                     src="/{{$deliveryboy->driverdoc->tds_certificate}}" width="160">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" id="btn_save" class="btn btn-danger btn-lg">Save</button>
                                        <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" onclick="window.history.go(-1); return false;">Back</a>
                                        {{-- <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" href="{{ route('deliveryboy.index') }}">Back</a> --}}
                                    </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
    </div>
    <!-- Content Wrapper. Contains page content -->
@endsection
@push('css')
    <style type="text/css">
        .datepicker {
            width: auto !important;
        }
    </style>
@endpush
@push('js')
    <script>
        //create and store category
        $(function () {
            $('#editForm').bootstrapValidator();
            // $('#editForm').bootstrapValidator('resetForm', true);
            // $('#birthdate').click(function(){
            //     let now = new Date();
            //     const format = "YYYY-MM-DD";
            //     var dateTime = moment(now).format(format);
            //     // alert(dateTime);
            //     $("#birthdate").attr('max',dateTime);
            // });
            $(document).ready(function () {
                $("#datepicker").datepicker({
                    autoclose: true,
                    todayHighlight: false,
                    format: 'dd/mm/yyyy',
                    max: new Date(new Date().getFullYear() - 18, 12, 31),
                    min: new Date(new Date().getFullYear() - 100, 12, 31),
                    defaultViewDate: new Date(new Date().getFullYear() - 18, 12, 31),
                    endDate: "-18y",
                    startDate: "-100y",
                }).on('change', function (e) {
                    // Revalidate the date field
                    $('#editForm').bootstrapValidator('revalidateField', 'datepicker');
                });
                ;
            });


        });
    </script>
@endpush
