@extends('layouts.app')
@push('css')
<style type="text/css">
    td{
    vertical-align:middle !important;
}
</style>

@endpush
@section('content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <i class="fa fa-truck text-danger" style="transform: rotateY(160deg);" aria-hidden="true"></i>
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Delivery Agent Documents</h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
            @if($deliveryboy_docs->driver_status != "active")
                <a href="{{ route('approve.delivery.agent',[$deliveryboy_docs->id,'active']) }}" class="btn btn-light-danger font-weight-bolder btn-sm float-right"
                data-keyboard="false" data-backdrop="static">Active Delivery Agent</a>
            @else
                  <a href="{{ route('approve.delivery.agent',[$deliveryboy_docs->id,'deactive']) }}" class="btn btn-light-danger font-weight-bolder btn-sm float-right"
                data-keyboard="false" data-backdrop="static">Deactive Delivery Agent</a>
            @endif
                <!--end::Actions-->
            </div>
            <!--end::Info-->
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--end::Notice-->
            <!--begin::Card-->
            <div class="card card-custom">
                {{-- <div class="card-header">
                    <div class="card-title">
                        <span class="card-icon">
                            <i class="icon-building text-danger" aria-hidden="true"></i>
                        </span>
                        <h3 class="card-label">Deliveryboy List</h3>

                         <a href="{{ url('deliveryboy/add') }}" class="btn btn-light-danger font-weight-bolder btn-sm" data-keyboard="false"
                        data-backdrop="static">Add New Deliveryboy</a>
                    </div>
                </div> --}}
                <div class="card-body" style="overflow-x: auto;">
                     <table class="table table-hover border" id="myTable">
                        <thead>
                            <tr>
                                <th>Vehicle Number</th>
                                <th>Vehicle Model</th>
                                <th>Vehicle Color</th>
                            </tr>
                        </thead>
                        <tbody>
                            <td>
                                {{$deliveryboy_docs->vehicle_number}}
                            </td>
                            <td>
                                {{$deliveryboy_docs->vehicle_model}}
                            </td>
                            <td>
                                {{$deliveryboy_docs->vehicle_colour}}
                            </td>
                        </tbody>
                     </table>
                </div>
                <div class="card-body" style="overflow-x: auto;">
                     <table class="table table-hover" id="myTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Document Name</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $count = 0; @endphp
                            @if($deliveryboy_docs && $deliveryboy_docs->driver_photo)
                                <tr>
                                    <td>
                                        {{++$count}}
                                    </td>
                                    <td>
                                        Driver Photo
                                    </td>

                                    <td>
                                        <span class="badge p-3 text-white {{$deliveryboy_docs->driver_photo_status == "approve" ? 'badge-success' : ($deliveryboy_docs->driver_photo_status == "reject" ? 'badge-danger' : 'badge-warning')}}">
                                        {{ucfirst($deliveryboy_docs->driver_photo_status)}}
                                        </span>
                                    </td>
                                    <td>
                                        <a class="btn  col-md-4 btn-outline-primary" onclick="setModalImage('{{$deliveryboy_docs->driver_photo}}','{{$deliveryboy_docs->id}}','driver_photo','{{$deliveryboy_docs->driver_photo_status}}')" data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#myModal">View</a>
                                    </td>
                                <tr>
                            @endif
                            @if($deliveryboy_docs && $deliveryboy_docs->addhar_front)
                                <tr>
                                    <td>
                                        {{++$count}}
                                    </td>
                                    <td>
                                        Aadhaar Front Photo
                                    </td>

                                    <td>
                                        <span class="badge p-3 text-white {{$deliveryboy_docs->addhar_front_status == "approve" ? 'badge-success' : ($deliveryboy_docs->addhar_front_status == "reject" ? 'badge-danger' : 'badge-warning')}}">
                                        {{ucfirst($deliveryboy_docs->addhar_front_status)}}
                                        </span>
                                    </td>
                                    <td>
                                        <a class="btn  col-md-4 btn-outline-primary" onclick="setModalImage('{{$deliveryboy_docs->addhar_front}}','{{$deliveryboy_docs->id}}','addhar_front','{{$deliveryboy_docs->addhar_front_status}}')" data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#myModal">View</a>
                                    </td>
                                </tr>
                            @endif
                            @if($deliveryboy_docs && $deliveryboy_docs->addhar_back)
                                <tr>
                                    <td>
                                        {{++$count}}
                                    </td>
                                    <td>
                                        Aadhaar Back Photo
                                    </td>

                                    <td>
                                       <span class="badge p-3 text-white {{$deliveryboy_docs->addhar_back_status == "approve" ? 'badge-success' : ($deliveryboy_docs->addhar_back_status == " reject " ? 'badge-danger' : 'badge-warning')}}">
                                        {{ucfirst($deliveryboy_docs->addhar_back_status)}}
                                        </span>
                                    </td>
                                    <td>
                                        <a class="btn  col-md-4 btn-outline-primary" onclick="setModalImage('{{$deliveryboy_docs->addhar_back}}','{{$deliveryboy_docs->id}}','addhar_back','{{$deliveryboy_docs->addhar_back_status}}')" data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#myModal">View</a>
                                    </td>
                                </tr>
                            @endif
                            @if($deliveryboy_docs && $deliveryboy_docs->rc_front)
                                <tr>
                                    <td>
                                        {{++$count}}
                                    </td>
                                    <td>
                                        RC Front Photo
                                    </td>

                                    <td>
                                        <span class="badge p-3 text-white {{$deliveryboy_docs->rc_front_status == "approve" ? 'badge-success' : ($deliveryboy_docs->rc_front_status == "reject" ? 'badge-danger' : 'badge-warning')}}">
                                        {{ucfirst($deliveryboy_docs->rc_front_status)}}
                                        </span>
                                    </td>
                                    <td>
                                        <a class="btn  col-md-4 btn-outline-primary" onclick="setModalImage('{{$deliveryboy_docs->rc_front}}','{{$deliveryboy_docs->id}}','rc_front','{{$deliveryboy_docs->rc_front_status}}')" data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#myModal">View</a>
                                    </td>
                                </tr>
                            @endif
                            @if($deliveryboy_docs && $deliveryboy_docs->rc_back)
                                <tr>
                                    <td>
                                        {{++$count}}
                                    </td>
                                    <td>
                                        RC Back Photo
                                    </td>

                                    <td>
                                        <span class="badge p-3 text-white {{$deliveryboy_docs->rc_back_status == "approve" ? 'badge-success' : ($deliveryboy_docs->rc_back_status == "reject" ? 'badge-danger' : 'badge-warning')}}">
                                        {{ucfirst($deliveryboy_docs->rc_back_status)}}
                                        </span>
                                    </td>
                                    <td>
                                        <a class="btn  col-md-4 btn-outline-primary" onclick="setModalImage('{{$deliveryboy_docs->rc_back}}','{{$deliveryboy_docs->id}}','rc_back','{{$deliveryboy_docs->rc_back_status}}')" data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#myModal">View</a>
                                    </td>
                                </tr>
                            @endif
                            @if($deliveryboy_docs && $deliveryboy_docs->pan_card)
                                <tr>
                                    <td>
                                        {{++$count}}
                                    </td>
                                    <td>
                                        Pancard Photo
                                    </td>

                                    <td>
                                        <span class="badge p-3 text-white {{$deliveryboy_docs->pan_card_status == "approve" ? 'badge-success' : ($deliveryboy_docs->pan_card_status == "reject" ? 'badge-danger' : 'badge-warning')}}">
                                        {{ucfirst($deliveryboy_docs->pan_card_status)}}
                                        </span>
                                    </td>
                                    <td>
                                        <a class="btn  col-md-4 btn-outline-primary" onclick="setModalImage('{{$deliveryboy_docs->pan_card}}','{{$deliveryboy_docs->id}}','pan_card','{{$deliveryboy_docs->pan_card_status}}')" data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#myModal">View</a>
                                    </td>
                                </tr>
                            @endif
                            @if($deliveryboy_docs && $deliveryboy_docs->insurance_certificate)
                                <tr>
                                    <td>
                                        {{++$count}}
                                    </td>
                                    <td>
                                        Insurance Certificate
                                    </td>

                                    <td>
                                        <span class="badge p-3 text-white {{$deliveryboy_docs->insurance_certificate_status == "approve" ? 'badge-success' : ($deliveryboy_docs->insurance_certificate_status == "reject" ? 'badge-danger' : 'badge-warning')}}">
                                        {{ucfirst($deliveryboy_docs->insurance_certificate_status)}}
                                        </span>
                                    </td>
                                    <td>
                                        <a class="btn  col-md-4 btn-outline-primary" onclick="setModalImage('{{$deliveryboy_docs->insurance_certificate}}','{{$deliveryboy_docs->id}}','insurance_certificate','{{$deliveryboy_docs->insurance_certificate_status}}')" data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#myModal">View</a>
                                    </td>
                                </tr>
                            @endif
                            @if($deliveryboy_docs && $deliveryboy_docs->tds_certificate)
                                <tr>
                                    <td>
                                        {{++$count}}
                                    </td>
                                    <td>
                                        Driving License Photo
                                    </td>

                                    <td>
                                        <span class="badge p-3 text-white {{$deliveryboy_docs->tds_certificate_status == "approve" ? 'badge-success' : ($deliveryboy_docs->tds_certificate_status == "reject" ? 'badge-danger' : 'badge-warning')}}">
                                        {{ucfirst($deliveryboy_docs->tds_certificate_status)}}
                                        </span>
                                    </td>
                                    <td>
                                        <a class="btn  col-md-4 btn-outline-primary" onclick="setModalImage('{{$deliveryboy_docs->tds_certificate}}','{{$deliveryboy_docs->id}}','tds_certificate','{{$deliveryboy_docs->tds_certificate_status}}')" data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#myModal">View</a>
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="col-md-12">
{{--                            <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" onclick="window.history.go(-1); return false;">Back</a>--}}
                             <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" href="{{ route('deliveryboy.index') }}">Back</a>
                    </div>
                </div>
            </div>
            <div class="container">
                        <div class="modal fade" id="myModal">
                            <div class="modal-dialog modal-lg modal-content" style="background: #2015163b">
                                <div class="float-right">
                                    <button type="button" class="btn btn-danger col-md-1 float-right" id="closeModal"
                                            data-dismiss="modal">X</button>
                                </div>

                                  <div id="showImagediv" class="d-flex justify-content-center align-items-center p-3 pb-5" style="height: 75vh">
                                    <img id="showImage" alt=""  style="max-width: 42%;max-height:74vh" >
                                  </div>
                                    <!-- Modal footer -->
                                    <div class="modal-footer d-flex justify-content-center align-items-center p-3" id="modalButton">
                                        <a type="button" class="btn btn-success col-md-5" id="approveButton"
                                            >Approve</a>
                                        <a type="button" class="btn btn-danger col-md-5" id="rejectButton"
                                            >Reject</a>
                                    </div>
                            </div>
                        </div>
                    </div>
        </div>
    </div>
</div>

@endsection
@push('js')
<script>
    function setModalImage(image,id,field_name,field_status){
        document.getElementById("showImage").src= "/"+image;
        document.getElementById("approveButton").href= "/deliveryboy/docs/approve/"+id+"/"+field_name;
        document.getElementById("rejectButton").href= "/deliveryboy/docs/reject/"+id+"/"+field_name;
        console.log(field_status);
        if(field_status != "pending"){
            $('#modalButton').attr('style', 'display:none !important');
        }else{
            $('#modalButton').attr('style', 'display:flex !important');
        }

    }
</script>

@endpush
