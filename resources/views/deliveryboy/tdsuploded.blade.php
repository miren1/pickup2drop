@extends('layouts.app')
@push('css')
<style type="text/css">
td{
    vertical-align:middle !important;
}
.img_wrp {
    display: inline-block;
    position: relative;
}
.fa-times {
    position: absolute;
    top: -10px;
    right: -10px;
}
</style>

@endpush
@section('content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <i class="fa fa-truck text-danger" style="transform: rotateY(160deg);" aria-hidden="true"></i>
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Delivery Agent TDS Certificate</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
            </div>
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">{{ $user_details->name }}&nbsp;({{ $user_details->mobile_number }})</h5>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
             <div class="card card-custom">
                <div class="card-body row" style="overflow-x: auto;">
                    
                    @foreach ($files as $key=>$filelist)
                    <div class="col-md-12">
                        <label for=""> <strong>Financial Year : </strong></label>
                        <span>{{$key}}</span>
                    </div>
                    
                    @foreach ($filelist as $file)
                    <div class="col-md-3 text-center div_cert_{{$file->id}} my-4">
                        <a href="{{ env("DIGITALOCEAN_SPACES_VIEW_ENDPOINT")."/".$file->tds_cert}}" target="_blank" rel="noopener noreferrer">
                            <div class="img_wrp">
                                <img src="{{ asset('assets/media/pdf.svg') }}" alt="" srcset="">
                                <i class="fas fa-times text-danger delete-file" data-id="{{ $file->id }}"></i>
                            </div>
                            <div class="p-2">
                                {{$file->image_name}}
                            </div>
                        </a>
                    </div>
                    @endforeach
                    @endforeach
                    <div class="col-md-12">
                        {{-- <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" onclick="window.history.go(-1); return false;">Back</a> --}}
                        <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" href="{{ route('tds-certificate.index') }}">Back</a>
                    </div>
                </div>
            </div>
            <!--end::Notice-->
            <!--begin::Card-->
        </div>
    </div>
</div>

@endsection

@push('js')
    <script type="text/javascript">
        $(document).on('click','.delete-file',function(){
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                    if (willDelete) {
                        var tds_id = $(this).data('id');
                        $.ajax({
                            url: '{{ route('tdscertificate.delete') }}',
                            type: 'delete',
                            data: {'id':tds_id,"_token": "{{ csrf_token() }}"},
                            dataType: 'json',
                            success:function(data){
                                swal("Your file has been deleted!", {
                                    icon: "success",
                                });
                                $(".div_cert_"+tds_id).hide();
                                 if(data.tds_file == 0){
                                    window.location.replace('/tds-certificate');
                                }
                            },
                            error:function (error){
                                swal("Error!", error.responseJSON.error, "error");
                            }
                        });
                    }
                });
            return false;
        });
    </script>
@endpush
