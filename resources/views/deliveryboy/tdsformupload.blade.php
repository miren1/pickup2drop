@extends('layouts.app')

@section('content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <i class="fa fa-truck text-danger" style="transform: rotateY(160deg);" aria-hidden="true"></i>
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Delivery Agent List</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                    <a href="{{route('tdsuploadedfile')}}"  class="btn btn-light-danger font-weight-bolder btn-sm float-right" >Upload TDS Certificate</a>
                </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        <div class="container">
            <div class="modal fade" id="tdsUploadModal">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h4 class="modal-title">Upload TDS Certificate</h4>
                            <button type="button" class="close" id="closeModal"
                                data-dismiss="modal">&times;</button>
                        </div>

                        <div class="modal-body">
                            <h4 id="error-edit" style="display: none;" class='alert alert-danger'></h4>
                            <form id="tdsForm" action="{{url('tds-certificate/tdsformstore')}}" method="POST"
                                    enctype="multipart/form-data" class="form-horizontal ">
                                              @csrf
                              <input id="input-id" type="file" name="tds_image[]" class="file" multiple data-preview-file-type="text">
                          </form>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" id="closeModal"
                                data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="card card-custom">
                <div class="card-body" style="overflow-x: auto;">

                    <table class="table table-hover" id="myTable">
                        <thead>
                            <tr>
                                <th width="10%">No</th>
                                <th width="20%">Name</th>
                                <th width="25%">Mobile</th>
                                <th width="25%">Status</th>
                                <th width="25%">Pan Number</th>
                                <th width="20%">Action</th>
                            </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>

   $(document).ready(function() {
    
        $table = $("#myTable").DataTable({
            "ajax": "{{ url('tds-certificate/Getalldeliveryagent') }}",
            "processing": true,
            "serverSide": true,
            "ordering": false,
            'stateSave' : true,
            "columns": [
            {
                data: "id",
                render: function(data, type, row, meta) {
                    return row.id;
                }
            },
            { "data": "name" },
            { "data": "mobile_number" },
            { "data": "driver_status",name:'driverdoc.driver_status',
                render:function(data,type,row,meta){
                    var status = row.driver_status == 'active' ? "":'checked';
                    return `<div class="toggle-button-cover">
                    <div>
                        <div class="button r my-auto" id="button-1">
                        <input id="update_status" type="checkbox" ${status} class="checkbox" onclick="return false"/>
                        <div class="knobs"></div>
                        <div class="layer"></div>
                        </div>
                    </div>
                    </div>`;
                }
            },
            { "data": "pan_number" ,name:'driverdoc.pan_number'},
            { "data": "id",
            render:function(data,type,row,meta){
                $id =btoa(row.id);
                $html = '<a class="dropdown-item btn_tds_docs d-block cursor-pointer" href="tds-certificate/tdsuploaded/'+btoa(row.id)+'"  data-id="'+row.id+'"><i class="fa fa-file text-info"></i>&nbsp; TDS Certificate</a>';
                      return $html;
            }
        },
        ]
    });
  } );
</script>
@endpush
<style>

.toggle-button-cover {
  display: table-cell;
  position: relative;
  top:12%;
  box-sizing: border-box;
}

.bi-plus-lg{
  display: none;
}

.button-cover:before {
  counter-increment: button-counter;
  content: counter(button-counter);
  position: absolute;
  right: 0;
  bottom: 0;
  color: #d7e3e3;
  font-size: 12px;
  line-height: 1;
  padding: 5px;
}

.button-cover,
.knobs,
.layer {
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  height:13px;
}
.button {
  position: relative;
  top: 50%;
  width: 80px;
  height: 25px;
  margin: -20px auto 0 auto;
  overflow: hidden;
}

.button.r,
.button.r .layer {
  border-radius: 100px;
}

.button.b2 {
  border-radius: 2px;
}

.checkbox {
  position: relative;
  width: 100%;
  height: 100%;
  padding: 0;
  margin: 0;
  opacity: 0;
  cursor: pointer;
  z-index: 3;
}

.knobs {
  z-index: 2;
}

.layer {
  width: 100%;
  background-color: #ebf7fc;
  transition: 0.9s ease all;
  z-index: 1;
}

#button-1 .knobs:before {
  content: "  Active  ";
  position: absolute;
  top: -3px;
  left: 0px;
  width: 80px;
  height: 40px;
  color: #fff;
  font-size: 11px;
  font-weight: 600;
  text-align: center;
  line-height: 1;
  padding: 9px 4px;
  background-color: #1BC5BD;
  border-radius: 1%;
  transition: 0.9s cubic-bezier(0.18, 0.89, 0.35, 1.15) all;
}

#button-1 .checkbox:checked + .knobs:before {
  content: "Deactive ";
  left: 00px;
  background-color: #F64E60;
}

#button-1 .checkbox:checked ~ .layer {
  background-color: #fcebeb;
}

#button-1 .knobs,
#button-1 .knobs:before,
#button-1 .layer {
  transition: 0.9s ease all;
}
</style>
