@extends('layouts.app')

@section('content')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <i class="fas fa-wallet text-danger" aria-hidden="true"></i>
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Wallet</h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

                <a href="{{ route('wallet.create') }}" class="btn btn-light-danger font-weight-bolder btn-sm"
                data-keyboard="false" data-backdrop="static">Add New Item</a>
                <!--end::Actions-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--end::Notice-->
            <!--begin::Card-->
            <div class="card card-custom">
                {{-- <div class="card-header">
                    <div class="card-title">
                        <span class="card-icon">
                            <i class="fa fa-hashtag text-danger" aria-hidden="true"></i>
                        </span>
                        <h3 class="card-label">Deliveryboy assign List</h3>

                         <a href="{{ url('deliveryboy_assign/add') }}" class="btn btn-light-danger font-weight-bolder btn-sm" data-keyboard="false"
                        data-backdrop="static">Add New deliveryboy_assign</a>
                    </div>
                </div> --}}
                <div class="card-body" style="overflow-x: auto;">
                    <!--begin: Datatable-->
                    <table class="table table-hover" id="myTable">
                        <thead>
                            <tr>
                                <th>Mobile Number</th>
                                <th>User</th>
                                <th>Type</th>
                                <th>Amount</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!-- Content Wrapper. Contains page content -->
@endsection

@push('js')
<script>

   $(document).ready(function() {
        $table = $("#myTable").DataTable({
            "ajax": "{{ route('wallet.show') }}",
            "processing": false,
            "serverSide": false,
            "ordering": false,
            "columns": [
              { "data": "mobile_number",name:'user.mobile_number'
            },
            //{ "data": "boy.name" },
             { "data": "user",
                render:function(data, row, alldata){
                    if(alldata.user)
                    {
                        return alldata.user.name;
                    }
                    else if(alldata.user_id==0)
                    {
                        return '<span class="label label-lg font-weight-bold label-light-info label-inline">All Users</span>';
                    }
                    else
                    {
                        return '<span class="label label-lg font-weight-bold label-light-primary label-inline">New Users</span>';
                    }
                }
            },
            { "data": "type",
                render:function(data, row, alldata){
                    if(alldata.type=="default")
                    {
                        return '<span class="label label-lg font-weight-bold label-light-primary label-inline">Default</span>';
                    }
                    else if(alldata.type=="add_to_all")
                    {
                        return '<span class="label label-lg font-weight-bold label-light-info label-inline">All Users</span>';
                    }
                    else if(alldata.type=="add_to_one")
                    {
                        return '<span class="label label-lg font-weight-bold label-light-warning label-inline">One User</span>';
                    }
                    else if(alldata.type=="referral")
                    {
                        return '<span class="label label-lg font-weight-bold label-light-dark label-inline">Referral User</span>';
                    }
                    else
                    {
                        /*return '<span class="label label-lg font-weight-bold label-light-dark label-inline">Reffered User</span>';*/
                        return '<span class="label label-lg font-weight-bold label-light-dark label-inline">Referral User</span>';

                    }
                }
            },
            
            { "data": "amount"},
            { "data": "created_at"},
            { "data": "id",
            render:function(data,type,row,meta){
                // console.log(meta);
                $id =btoa(row.id);
                $html = '<div class="mo-mb-2">'
                                  '<div class="dro1pdown-menu" x-placement="botto1m-start" style="cursor: pointer;">'+ '';
                                    $html+= '<a class="px-3 btn_delete" title="Delete" data-id="'+$id+'"><i class="fa fa-trash text-danger"></i></a>' ;
                                      if(row.type=="default")
                                      {
                                        $html+='<a class="px-3" title="Edit" href="{{ url('wallet/edit') }}/'+$id+'"><i class="fa fa-edit text-primary"></i></a>'+ ' ';
                                      }



                                  '</div>'+
                              '</div>';
                      return $html;
            }
        },
        ]


    });

        $( document ).ajaxComplete(function() {
            // Required for Bootstrap tooltips in DataTables
            $('body').tooltip({
                 selector: '[data-toggle="tooltip"]',
                 container: 'body',
                 "delay": {"show": 100, "hide": 0},
            });
        });

    });

   $(document).on("click", '.btn_delete', function(event) {
        var id =$(this).attr('data-id');
        swal({
            title: "Are you sure?",
            text: "Do you want to delete the wallet amount? You won't be able to recover the amount.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {

                $.ajax({
                    url: "{{ route('wallet.destroy') }}",
                    type: 'post',
                    dataType: 'json',
                data: {id:id,"_token":"{{csrf_token()}}"},
                success:function($data){
                    //console.log(data);
                    if($data == 'unauthorized_access'){
                        swal("Unauthorized access.", {
                            icon: "error",
                        });
                    }
                    else if($data == true)
                    {
                        swal("Wallet has been deleted.", {
                            icon: "success",
                        });
                        $('#myTable').DataTable().ajax.reload();
                    }else{
                        swal("Unable to update wallet.", {
                            icon: "error",
                        });
                    }
                    //$('#city').DataTable().ajax.reload();
                }
            });


            } else {
                swal("Your Wallet is Safe.");
            }
        });
    });

</script>
@endpush
