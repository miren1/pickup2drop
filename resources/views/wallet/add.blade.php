@extends('layouts.app')

@section('content')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
  <!--begin::Subheader-->
  <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
    <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
      <!--begin::Info-->
      <div class="d-flex align-items-center flex-wrap mr-2">
        <!--begin::Page Title-->
        <i class="fas fa-wallet text-danger" aria-hidden="true"></i>
        <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Create Wallet Plan</h5>
        <!--end::Page Title-->
        <!--begin::Actions-->
        <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

        <!--end::Actions-->
      </div>
      <!--end::Info-->
    </div>
  </div>
  <!--end::Subheader-->
  <!--begin::Entry-->
  <div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container">
      <!--end::Notice-->
      <!--begin::Card-->
      <div class="card card-custom">
       {{--  <div class="card-header">
          <div class="card-title justify-content-between flex-right align-items-right">
            <span class="card-icon">
              <i class="fa fa-hashtag text-danger" aria-hidden="true"></i>
            </span>
            <h3 class="card-label">Deliveryboy assign List</h3>

          </div>
        </div> --}}
        <div class="card-body" style="overflow-x: auto;">

          <div class="container">
            <form id="editForm" action="{{route('wallet.store')}}" method="POST" class="form-horizontal">
             @csrf 

                    @if(isset($wallet))
                    <input type="hidden" name="id" value="{{ $wallet->id }}">
                    @endif   
                    <div class="row">
                    <div class="col-md-12">
                      <div class="form-group" id="title" style="display: none;">
                        <label>Title</label>

                        <input type="title" value="@if(isset($wallet)){{$wallet->title}}@endif" class="form-control" id="ref_title" name="title" placeholder="Title" data-bv-notempty="true"
                        data-bv-notempty-message="Title is required"/>
                      </div>
                      <div class="form-group">
                        <label>Amount</label>

                        <input type="number" value="@if(isset($wallet)){{$wallet->amount}}@endif" class="form-control" id="amount" name="amount" placeholder="Amount" data-bv-notempty="true"
                        data-bv-notempty-message="Amount is required" min=0 />
                      </div>
                        @if(!isset($wallet))
                          <div class="form-group">
                            <label>Select Type</label>

                            <select class="form-control select2" id="type" name="type" data-bv-notempty="true"
                            data-bv-notempty-message="Type is required">
                            <option value="">Select a Type</option>
                            @if(!isset($default))
                            <option value="default">Default</option>
                            @endif
                            @if(!isset($referral))
                            <option value="referral">Referral User</option>
                            @endif
                            @if(!isset($by_referral))
                            <option value="by_referral">Referred User</option>
                            @endif
                            <option value="add_to_one">Add Balance to One User</option>
                            <option value="add_to_all">Add Balance to All User</option>
                            </select>
                          </div>
                        @else
                          <div class="form-group">
                            <label>Select Type</label>

                            <select class="form-control select2" id="type" name="type" data-bv-notempty="true"
                            data-bv-notempty-message="Type is required">
                            <option value="">Select a Type</option>
                            <option value="default" selected>Default</option>
                            </select>
                          </div>
                        @endif
                        @if(!isset($wallet))
                        <div class="form-group" id="s_user" style="display: none;">
                          <label>Select User</label>
                          <select id="user_id" class="form-control select2"  {{-- multiple="multiple" --}} name="user_id"data-bv-notempty="true"
                          data-bv-notempty-message="User is required">
                          <option value=""> -- Select a User -- </option>
                          
                          @foreach($users as $n)

                          <option value="{{$n->id}}">{{$n->name}} || {{ $n->mobile_number }}</option>

                          @endforeach
                          </select>
                        </div>
                        @endif
                    </div>

                    <div class="col-md-12">
                      <button type="submit" id="btn_save" class="btn btn-danger btn-lg">Save</button> 
                      <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" onclick="window.history.go(-1); return false;">Back</a>
                      {{-- <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" href="{{ route('wallet.index') }}">Back</a> --}}
                    </div>
                    
                    </form>
                </div>
                </div>
            </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!-- Content Wrapper. Contains page content -->
@endsection

@push('js')
<script>
    //create and store category
    $(function() {
        $('#editForm').bootstrapValidator();
        $(".select2").select2();

        $(document).on('change','#type',function(){
          
          var type = $('#type').val();
          // alert(type);
          if(type=="add_to_one")
          {
            $('#title').css('display','none');
            $('#s_user').css('display','block');
            $('#ref_title').val(null);
          }
          else if(type=="add_to_all")
          {
            $('#title').css('display','none');
            $('#s_user').css('display','none');
            $('#user_id').val(null).trigger('change');
            $('#ref_title').val(null);
          }
          else if(type=="referral")
          {
            $('#s_user').css('display','none');
            $('#user_id').val(null).trigger('change');
            $('#title').css('display','block');
          }
          else if(type=="by_referral")
          {
            $('#s_user').css('display','none');
            $('#user_id').val(null).trigger('change');
            $('#title').css('display','block');
          }
          else
          {
            $('#title').css('display','none');
            $('#s_user').css('display','none');
            $('#user_id').val(null).trigger('change');
            $('#ref_title').val(null);
          }
        });
    });
    $(document).on('click','.select2',function(){
      // alert('asad');
      $('body').css('overflow-x','hidden');
		});
</script>
@endpush
