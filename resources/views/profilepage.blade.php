@extends('layouts.app')

@section('content')

    <style type="text/css">
        .small, small {
            font-size: .875em;
            color: red !important;
        }
    </style>
    <div class="toolbar" id="kt_toolbar">
        <!--begin::Container-->
        <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
            <!--begin::Page title-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Mobile Toggle-->
                <button class="burger-icon burger-icon-left mr-4 d-inline-block d-lg-none"
                        id="kt_subheader_mobile_toggle">
                    <span></span>
                </button>
                <!--end::Mobile Toggle-->
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <!-- <h5 class="text-dark font-weight-bold my-1 mr-5">Campaign</h5> -->
                    <!--end::Page Title-->

                    &nbsp;
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{route('home')}}" class="text-muted">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">My Profile</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Page title-->

        </div>
        <!--end::Container-->
    </div>

    <div class="post d-flex flex-column-fluid" id="kt_post">
        <div id="kt_content_container" class="container">
            <div class="card mb-5 mb-xl-10" id="kt_profile_details_view">

                <div class="card-body p-9">
                    <div class="row d-block">
                        <div class="row">
                            <div class="col-12">
                                <ul class="nav nav-tabs">
                                    <li class="nav-item">
                                        <a class="nav-link active text-danger" data-toggle="tab" role="tab" href="#home">Edit Profile</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-danger" data-toggle="tab" role="tab" href="#profile">Change Password</a>
                                    </li>
                                </ul>
                                <div id="myTabContent" class="tab-content">
                                    <div class="tab-pane fade show active" id="home">
                                        <div class="card">
                                            <div class="card-body">
                                                <form id="profile_update" method="POST"
                                                      action="{{url('/admin/updateprofile')}}" class="form" enctype="multipart/form-data" role="form">
                                                    @csrf
                                                    <div class="card-body">
                                                        <div class="row mb-6">
                                                            <label
                                                                class="col-lg-4 col-form-label d-flex align-items-center required fw-bold fs-6">Profile Photo</label>
                                                            <div class="col-lg-8 fv-row">
                                                                <input type="file" name="profile_photo_path" id="profile_photo_path">
                                                                @if(Auth::user()->profile_photo_path)
                                                                    <img src="/{{Auth::user()->profile_photo_path}}" width="100" alt="{{Auth::user()->name}}" srcset="">
                                                                @endif
                                                                 <div class="d-block">
                                                                <p class="text-danger mt-2">
                                                                    Note : Please upload image with the aspect ratio of 1:1 for better result.
                                                                </p>
                                                            </div>
                                                            </div>
                                                           
                                                        </div>
                                                        <div class="row mb-6">
                                                            <label
                                                                class="col-lg-4 col-form-label required fw-bold fs-6">Name</label>
                                                            <div class="col-lg-8 fv-row">
                                                                <input type="text" name="name"
                                                                       class="form-control form-control-lg form-control-solid"
                                                                       value="{{Auth::user()->name}}"
                                                                       data-bv-notempty-message="The Name is required"
                                                                       required/>

                                                            </div>
                                                        </div>
                                                        <div class="row mb-6">
                                                            <label
                                                                class="col-lg-4 col-form-label required fw-bold fs-6">Email</label>
                                                            <div class="col-lg-8 fv-row">
                                                                <input type="text" name="email"
                                                                       class="form-control form-control-lg form-control-solid"
                                                                       value="{{Auth::user()->email}}"
                                                                       data-bv-emailaddress="true"
                                                                       data-bv-notempty-message="The Email is required"
                                                                       required/>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-footer d-flex justify-content-end py-6 px-9">
                                                        <a href="{{route('home')}}"
                                                                class="btn btn-primary mx-3" title="Back">
                                                            <i class="fa fa-angle-double-left mt-1">&nbsp;<span class="font-weight-light">Back</span></i>
                                                        </a>
                                                        {{-- <button type="submit" class="btn btn-primary"
                                                                id="kt_account_profile_details_submit">Update
                                                        </button> --}}
                                                        <button type="submit" id="btn_save" title="Update Profile" class="btn btn-danger btn-lg">Update Profile</button>
                                                    </div>
                                                </form>
                                            </div> <!-- end card body-->
                                        </div> <!-- end card -->
                                    </div>
                                    <div class="tab-pane fade" id="profile">
                                        <div class="row d-block m-auto">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row d-block">
                                                        <form id="changes_password" method="POST"
                                                              action="{{url('admin/updatepassword')}}" class="form">
                                                            @csrf
                                                            <div class="card-body">
                                                                <div class="row mb-6">
                                                                    <label
                                                                        class="col-lg-4 col-form-label required fw-bold fs-6 ">Old
                                                                        Password</label>
                                                                    <div class="col-lg-8 fv-row">
                                                                        <input type="password" name="old_password" id="old_password"
                                                                               class="form-control form-control-lg form-control-solid password-check"
                                                                               required
                                                                               data-bv-notempty-message="The Password is required"
                                                                               placeholder="Enter Your Old Password"/>

                                                                    </div>
                                                                    <div class="col-lg-4">
                                                                    </div>
                                                                    <div  class="col-lg-8 mt-2">
                                                                        <p id="CheckoldPasswordMatch"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="row mb-6">
                                                                    <label
                                                                        class="col-lg-4 col-form-label required fw-bold fs-6">New
                                                                        Password</label>
                                                                    <div class="col-lg-8 fv-row">
                                                                        <input type="password" name="password"
                                                                               class="form-control form-control-lg form-control-solid password-check"
                                                                               required
                                                                               id="password"
                                                                               placeholder="Enter Your New Password"
                                                                               data-identical="true"
                                                                               data-identical-field="confirm_password"
                                                                               data-identical-message="Enter the same password as new password"
                                                                               data-bv-notEmpty-message="new password is required"/>

                                                                    </div>
                                                                </div>
                                                                <div class="row mb-6">
                                                                    <label class="col-lg-4 col-form-label fw-bold fs-6">
                                                                        <span class="required">Confirm Password</span>
                                                                        <i class="fas fa-exclamation-circle ms-1 fs-7"
                                                                           data-bs-toggle="tooltip"
                                                                           title="Enter the same password as new password"></i>
                                                                    </label>
                                                                    <div class="col-lg-8 fv-row">
                                                                        <input type="password"
                                                                               name="password_confirmation"
                                                                               class="form-control form-control-lg form-control-solid password-check"
                                                                               required
                                                                               id="confirm_password"
                                                                               placeholder="Enter Your Confirm Password"
                                                                               data-notEmpty-message="Confirm password is required"
                                                                               data-identical="true"
                                                                               data-identical-field="password"
                                                                               data-identical-message="Enter the same password as new password"/>

                                                                    </div>
                                                                     <div class="col-lg-4">
                                                                    </div>
                                                                    <div  class="col-lg-8 mt-2">
                                                                        <p id="CheckPasswordMatch"></p>
                                                                    </div>
                                                                </div>

                                                               

                                                            </div>
                                                            <div
                                                                class="card-footer d-flex justify-content-end py-6 px-9">
                                                                <a href="{{route('home')}}" class="btn btn-primary mx-3" title="Back">
                                                                    <i class="fa fa-angle-double-left mt-1">&nbsp;<span class="font-weight-light">Back</span></i>
                                                                </a>
                                                                {{-- <button type="submit" class="btn btn-primary"
                                                                        id="kt_account_profile_details_submit">Update
                                                                </button> --}}
                                                                <button type="submit" id="btn_save_password" class="btn btn-danger btn-lg">Update Password</button>
                                                            </div>
                                                        </form>
                                                    </div> <!-- end card body-->
                                                </div> <!-- end card -->
                                            </div>
                                        </div>
                                        <!-- end row -->
                                    </div>
                                </div>
                            </div>
                        </div><!-- end col-->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
        $("#btn_save_password").attr("disabled", true);
         $(".password-check").on('keyup', function() {
        //  $(".password-check").on('blur', function() {
            var attr_id = $(this).attr('id');
            var password = $("#password").val();
            var old_password = $("#old_password").val();
            if(old_password == ""){
                $("#CheckoldPasswordMatch").show() 
                $("#CheckoldPasswordMatch").html("Please enter Original Password before procced!").css("color", "red");
            }else{         
                $("#CheckoldPasswordMatch").hide()   
                if(password.length <= 7 && attr_id == "password"){
                    $("#CheckPasswordMatch").html("Please enter Password atleast 8 character!").css("color", "red");
                }else{
                     var confirmPassword = $("#confirm_password").val();
                    if(confirmPassword != ""){
                        if (password != confirmPassword){
                            $("#CheckPasswordMatch").html("Password Does Not Matched !").css("color", "red");
                            $("#btn_save_password").attr("disabled", true);
                        }            
                        else{
                            if(password == "" || password.length <= 7){
                                $("#CheckPasswordMatch").html("Please enter Password atleast 8 character!").css("color", "red");
                            }else{
                                $("#CheckPasswordMatch").html("Password Matched !").css("color", "green");
                                $("#btn_save_password").attr("disabled", false);
                            }
                        }
                    }
                }
            }
            
        });
        });
    </script>
    

@endsection
