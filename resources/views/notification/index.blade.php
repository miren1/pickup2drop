@extends('layouts.app')

@section('content')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
  <!--begin::Subheader-->
  <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
    <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
      <!--begin::Info-->
      <div class="d-flex align-items-center flex-wrap mr-2">
        <!--begin::Page Title-->
        <i class="fas fa-bullhorn text-danger" aria-hidden="true"></i>
        <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Notification</h5>
        <!--end::Page Title-->
        <!--begin::Actions-->
        <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

        <!--end::Actions-->
      </div>
      <!--end::Info-->
    </div>
  </div>
  <!--end::Subheader-->
  <!--begin::Entry-->
  <div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container">
      <!--end::Notice-->
      <!--begin::Card-->
      <div class="card card-custom">
        <div class="card-body" style="overflow-x: auto;">
          <div class="container">
            <form id="editForm" action="{{route('notification.store')}}" method="POST" class="form-horizontal">
             @csrf
               <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Select Type</label>

                    <select class="form-control" id="type" name="type" data-bv-notempty="true"
                    data-bv-notempty-message="Type is required">
                    <option value="">Select a Type</option>
                    <option value="delivery_agent">Notification For All Delivery Agent</option>
                    <option value="customer">Notification For All Customer</option>
                    {{-- <option value="one">Notification For One User</option>
                    <option value="area">Area Wise User</option> --}}
                    <option value="all">Notification For All User</option>
                    </select>
                  </div>
                  <div class="form-group" id="s_user" style="display: none;">
                    <label>Select User</label>
                    <select id="user_id" class="form-control select2" name="user_id"data-bv-notempty="true"
                    data-bv-notempty-message="User is required">
                    <option value=""> -- Select a User -- </option>
                    
                    @foreach($users as $user)

                    <option value="{{$user->id}}">{{$user->name}} || {{ $user->mobile_number }}</option>

                    @endforeach
                    </select>
                  </div>
                  <div class="form-group" id="s_area" style="display: none;">
                    <label>Select Area</label>
                    <select id="area" class="form-control select2" name="area"data-bv-notempty="true"
                    data-bv-notempty-message="Area is required">
                    <option value=""> -- Select a Area -- </option>
                    
                    @foreach($areas as $area)

                    <option value="{{$area->pincode}}">{{$area->name}} || {{ $area->pincode }}</option>

                    @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Select Time Status</label>

                    <select class="form-control" id="status" name="status" data-bv-notempty="true"
                    data-bv-notempty-message="Status is required">
                      <option value="">Select a Status</option>
                      <option value="now">Now</option>
                      <option value="time">Schedule</option>
                    </select>
                  </div>
                  <div class="form-group" id="s_date" style="display: none;">
                    <label>Date</label>

                    <input class="form-control" type="date" min="{{date('Y-m-d')}}" id="date" name="date" onkeydown="return false"  data-bv-notempty="true"
                    data-bv-notempty-message="Date is required" />
                  </div>
                  <div class="form-group" id="s_time" style="display: none;">
                    <label>Time</label>

                    <input class="form-control" type="time" id="time" name="time" data-bv-notempty="true"
                    data-bv-notempty-message="Time is required" />
                  </div>
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" name="title" class="form-control" placeholder="Title" required data-bv-notempty="true" data-bv-notempty-message="Title is required">
                  </div>
                  <div class="form-group">
                    <label>Description</label>
                    <textarea name="description" rows="5" class="form-control" placeholder="Description" required data-bv-notempty="true" data-bv-notempty-message="Description is required"></textarea>
                  </div>
                </div>
                <div class="col-md-12">
                    <button type="submit" id="btn_save" class="btn btn-danger btn-lg">Send Notification</button>
                    {{-- <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" href="{{ url()->previous() }}">Back</a>    --}}
                </div>
                
              </form>
            </div>
          </div>
        </div>
      </div>
      <!--end::Card-->
    </div>
    <!--end::Container-->
  </div>
  <!--end::Entry-->
</div>
<!-- Content Wrapper. Contains page content -->
@endsection

@push('js')
<script>

  //create and store category
  $(function() {

    $('.select2').select2();

    $('#type').on('change',function(){
      var type = $(this).val();
      if(type == "one")
      {
        $('#s_user').css('display','block');
        $('#s_area').css('display','none');
        $('#area').val(null).trigger('change');
      }
      else if(type == "area")
      {
        $('#s_user').css('display','none');
        $('#user_id').val(null).trigger('change');
        $('#s_area').css('display','block');
      }
      else
      {
        $('#s_user').css('display','none');
        $('#user_id').val(null).trigger('change');
        $('#s_area').css('display','none');
        $('#area').val(null).trigger('change');
      }
    });

    $('#status').on('change',function(){
      var status = $(this).val();
      if(status == "time")
      {
        $('#s_time').css('display','block');
        $('#s_date').css('display','block');
      }
      else
      {
        $('#s_time').css('display','none');
        $('#s_date').css('display','none');
        $('#time').val(null);
        $('#date').val(null);
      }
    });

    $('#editForm').bootstrapValidator();
  });
</script>
@endpush
