@extends('layouts.app')

@section('content')

@push('css')
<style type="text/css">
  h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
    margin-bottom: 2.5rem;
    font-weight: 500;
    line-height: 1.2;
}
</style>
@endpush
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
  <!--begin::Subheader-->
  <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
    <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
      <!--begin::Info-->
      <div class="d-flex align-items-center flex-wrap mr-2">
        <!--begin::Page Title-->
        <i class="fa fa-truck text-danger"  aria-hidden="true"></i>
        <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp; Order Information view</h5>


        <!--end::Page Title-->
        <!--begin::Actions-->
        <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

        <!--end::Actions-->
      </div>
      <!--end::Info-->
    </div>
  </div>
  <!--end::Subheader-->
  <!--begin::Entry-->
  <div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container">
      <!--end::Notice-->
      <!--begin::Card-->
      <div class="card card-custom">
        {{-- <div class="card-header">
          <div class="card-title justify-content-between flex-right align-items-right">
            <span class="card-icon">
              <i class="fa fa-hashtag text-danger" aria-hidden="true"></i>
            </span>
            <h3 class="card-label">Areacode</h3>

          </div>
        </div> --}}
        <div class="card-body" style="overflow-x: auto;">

          <div class="container">

            <div class="row">
              <div class="col-md-12">
                <h4 style="margin-bottom: 0.5rem;">Type:-{{ $pickup->type }}</h4>
                      <hr>
                @if($pickup->type=="pickup")
                <div class="card-block">

                  <div class="row">

                    <div class="col-xl-3 col-md-6">
                      <label>Item Name</label>
                      {{-- @php dd($pickup); @endphp --}}
                      <h5 class="m-b-30 f-w-700"> {{ $pickup->item_name }}
                        <span class="text-c-green m-l-10"></span></h5>

                    </div>

                    <div class="col-xl-3 col-md-6">
                      <label>Customer Name</label>
                      {{-- @php dd($pickup); @endphp --}}
                      <h5 class="m-b-30 f-w-700"> {{ $pickup->customer && $pickup->customer->name ? $pickup->customer->name : " - " }}
                        <span class="text-c-green m-l-10"></span></h5>

                    </div>

                      <div class="col-xl-3 col-md-6">
                      <label>Description</label>

                      <h5 class="m-b-30 f-w-700"> {{ $pickup->description }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>


                      <div class="col-xl-3 col-md-6">
                      <label>Pick Address</label>

                      <h5 class="m-b-30 f-w-100"> {{$pickup['Pickupaddress']->office_shop_no}}, {{ $pickup['Pickupaddress']->address }} @if($pickup['Pickupaddress'] && $pickup['Pickupaddress']->name)<br> Name : {{$pickup['Pickupaddress']->name}} @endif<br> Mobile No. : {{$pickup['Pickupaddress']->phone}}
                        <span class="text-c-green m-l-10"></span></h5>


                      </div>

                      <div class="col-xl-3 col-md-6">
                      <label>Drop Address</label>

                      <h5 class="m-b-30 f-w-700">{{$pickup['Dropaddress']->office_shop_no}}, {{ $pickup['Dropaddress']->address }} @if($pickup['Dropaddress'] && $pickup['Dropaddress']->name)<br> Name : {{$pickup['Dropaddress']->name}} @endif <br> Mobile No. : {{$pickup['Dropaddress']->phone}}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>

                      <div class="col-xl-3 col-md-6">
                      <label>PinCode</label>

                      <h5 class="m-b-30 f-w-700"> {{ $pickup->pincode }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>

                      <div class="col-xl-3 col-md-6">
                      <label>Status</label>

                      <h5 class="m-b-30 f-w-700">
                          @if($pickup && $pickup->status == 'Request Accept')
                              {{'Pickup Requested'}}
                          @elseif($pickup && $pickup->status == 'Out Of Pickup')
                              {{'Out For Pickup'}}
                          @elseif($pickup && $pickup->status == 'Out Of Delivery')
                              {{'Out For Delivery'}}
                          @elseif($pickup && $pickup->status == 'Cancel Request')
                              {{'Request Cancelled'}}
                          @else
                            {{$pickup->status}}
                          @endif
                        <span class="text-c-green m-l-10"></span></h5>
                      </div>
                      @if($pickup['boy'] != null)
                      <div class="col-xl-3 col-md-6">
                      <label>Delivery Agent</label>

                      <h5 class="m-b-30 f-w-700"> {{ $pickup['boy']->name }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>
                      @endif

                      <div class="col-xl-3 col-md-6">
                      <label>Request Type</label>

                      <h5 class="m-b-30 f-w-700">
                        <span class="badge badge-primary text-capitalize">{{$pickup->request_type}}</span>
                      </div>

                      <div class="col-xl-3 col-md-6">
                      <label>Amount</label>

                      <h5 class="m-b-30 f-w-700"> {{ $pickup->amount }}
                        <span class="text-c-green m-l-10"></span></h5>
                      </div>

                      <div class="col-xl-3 col-md-6">
                          <label>Original Amount</label>
                          <h5 class="m-b-30 f-w-700"> 
                            @if($pickup->promo_amount > 0)
                              {{ $pickup->original_amount  + $pickup->promo_amount}}
                            @else
                              {{ $pickup->original_amount }}
                            @endif
                              <span class="text-c-green m-l-10"></span>
                          </h5>
                      </div>

                      <div class="col-xl-3 col-md-6">
                          <label>SGST</label>
                          <h5 class="m-b-30 f-w-700"> {{ $pickup->sgst }}
                              <span class="text-c-green m-l-10"></span>
                          </h5>
                      </div>

                      <div class="col-xl-3 col-md-6">
                          <label>CGST</label>
                          <h5 class="m-b-30 f-w-700"> {{ $pickup->cgst }}
                              <span class="text-c-green m-l-10"></span>
                          </h5>
                      </div>

                      <div class="col-xl-3 col-md-6">
                        <label>Promocode</label>

                        <h5 class="m-b-30 f-w-700"> {{ ($pickup->promocode)?$pickup->promocode:"-" }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>

                      <div class="col-xl-3 col-md-6">
                      </label>Payment Status</label>

                      <h5 class="m-b-30 f-w-700"> {{-- {{ $pickup->payment_status }}
                        <span class="text-c-green m-l-10"></span> --}}

                        @if($pickup->payment_status =="pending" && $pickup->payment_status !='' && $pickup->payment_status != null)

                          <span class="badge badge-warning">Pending</span>

                        @elseif($pickup->payment_status =="complete")

                          <span class="badge badge-success">Complete</span>

                        @else

                          <span class="badge badge-danger">Failed</span>

                        @endif
                      </h5>

                      </div>


                      <div class="col-xl-3 col-md-6">
                      </label>Payment Type</label>

{{--                      <h5 class="m-b-30 f-w-700"> {{ $pickup->payment_type }}--}}
                      <h5 class="m-b-30 f-w-700"> Online
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>

                      <div class="col-xl-3 col-md-6">
                      </label>Customer Mobile no.</label>

                      <h5 class="m-b-30 f-w-700"> {{  $pickup->customer && $pickup->customer->mobile_number ? $pickup->customer->mobile_number : ' - ' }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>

                         <div class="col-xl-3 col-md-6">
                      <label>Reciever Mobile no.</label>

                      <h5 class="m-b-30 f-w-700"> {{  $pickup->boy && $pickup->boy->mobile_number ? $pickup->boy->mobile_number : ' - '}}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>


                      <div class="col-xl-3 col-md-6">
                      </label>Distance (Km).</label>

                      <h5 class="m-b-30 f-w-700"> {{ $pickup->distance }} km.
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>

                    </div>
                </div>
                @endif



              @if($pickup->type=="medicine")

               {{-- //<h1>{{ $pickup->type }}</h1> --}}
                <div class="card-block">
                  <div class="row">

                    <div class="col-xl-3 col-md-6">
                      </label>Item Name</label>

                      <h5 class="m-b-30 f-w-700"> {{ $pickup->item_name }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>

                      <div class="col-xl-3 col-md-6">
                      <label>Customer Name</label>
                      <!-- {{-- @php dd($pickup); @endphp --}} -->
                      <h5 class="m-b-30 f-w-700"> {{ $pickup->customer->name }}
                        <span class="text-c-green m-l-10"></span></h5>

                    </div>

                      <div class="col-xl-3 col-md-6">
                      </label>Description</label>

                      <h5 class="m-b-30 f-w-700"> {{ $pickup->description }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>


                      <div class="col-xl-3 col-md-6">
                      </label>Dropaddress</label>

                      <h5 class="m-b-30 f-w-700">{{$pickup['Dropaddress']->office_shop_no}}, {{ $pickup['Dropaddress']->address }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>

                      <div class="col-xl-3 col-md-6">
                      </label>PinCode</label>

                      <h5 class="m-b-30 f-w-700"> {{ $pickup->pincode }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>

                      <div class="col-xl-3 col-md-6">
                      <label>Status</label>

                      <h5 class="m-b-30 f-w-700"> 
                        @if($pickup && $pickup->status == 'Request Accept')
                              {{'Pickup Requested'}}
                          @elseif($pickup && $pickup->status == 'Out Of Pickup')
                              {{'Out For Pickup'}}
                          @elseif($pickup && $pickup->status == 'Out Of Delivery')
                              {{'Out For Delivery'}}
                          @elseif($pickup && $pickup->status == 'Cancel Request')
                              {{'Request Cancelled'}}
                          @else
                            {{$pickup->status}}
                          @endif
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>

                      {{-- <div class="col-xl-3 col-md-6">
                      </label>Request Type</label>

                      <h5 class="m-b-30 f-w-700"> {{ $pickup->request_type }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div> --}}

                      <div class="col-xl-3 col-md-6">
                      <label>Request Type</label>

                      <h5 class="m-b-30 f-w-700">
                        <span class="badge badge-primary text-capitalize">{{$pickup->request_type}}</span>
                        <!-- {{-- <span class="text-c-green m-l-10"></span> --}} -->
                      </h5>

                      </div>

                      {{-- <div class="col-xl-3 col-md-6">
                      </label>Type</label>

                      <h5 class="m-b-30 f-w-700"> {{ $pickup->type }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div> --}}

                       <div class="col-xl-3 col-md-6">

                               </label>Media</label>

                               <h5 class="m-b-30 f-w-700">
                               <!-- {{-- <a href="{{url('public/images')}}/{{$pickup['image']->img_path}}" target="_blank"> --}} -->
                               @if($pickup['image'])
                                  <img class="img-radius" src="{{$pickup['image']->img_path}}" width="60" height="60">
                                <!-- </a> -->
                                @else
                                -
                                @endif
                                <span class="text-c-green m-l-10"></span></h5>


                            </div>

                        <div class="col-xl-3 col-md-6">
                        </label>Distance (Km).</label>

                        <h5 class="m-b-30 f-w-700"> {{ $pickup->distance }} km.
                          <span class="text-c-green m-l-10"></span></h5>

                        </div>

                    </div>


                  </div>
                  @endif

              @if($pickup->type=="tiffin" || $pickup->type=="subscription")
                <div class="card-block">

                  <div class="row">

                    <div class="col-xl-3 col-md-6">
                      <label>Item Name</label>
                      {{-- @php dd($pickup); @endphp --}}
                      <h5 class="m-b-30 f-w-700"> {{ $pickup->item_name }}
                        <span class="text-c-green m-l-10"></span></h5>

                    </div>

                    <div class="col-xl-3 col-md-6">
                      <label>Customer Name</label>
                      {{-- @php dd($pickup); @endphp --}}
                      <h5 class="m-b-30 f-w-700"> {{ $pickup->customer->name }}
                        <span class="text-c-green m-l-10"></span></h5>

                    </div>

                    <div class="col-xl-3 col-md-6">
                      <label>Customer Name</label>
                      {{-- @php dd($pickup); @endphp --}}
                      <h5 class="m-b-30 f-w-700"> {{ $pickup->customer->name }}
                        <span class="text-c-green m-l-10"></span></h5>

                    </div>

                    <div class="col-xl-3 col-md-6">
                      <label>Customer Mobile No.</label>
                      {{-- @php dd($pickup); @endphp --}}
                      <h5 class="m-b-30 f-w-700"> {{ $pickup->customer->mobile_number }}
                        <span class="text-c-green m-l-10"></span></h5>

                    </div>

                      <div class="col-xl-3 col-md-6">
                      <label>Description</label>

                      <h5 class="m-b-30 f-w-700"> {{ $pickup->description }}
                        <span class="text-c-green m-l-10"></span></h5>
                      </div>

                      <div class="col-xl-3 col-md-6">
                      <label>Pick Address</label>

                      <h5 class="m-b-30 f-w-100">{{$pickup['Pickupaddress']->office_shop_no}}, {{ $pickup['Pickupaddress']->address }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>

                      <div class="col-xl-3 col-md-6">
                      <label>Dropaddress</label>

                      <h5 class="m-b-30 f-w-700">{{$pickup['Dropaddress']->office_shop_no}}, {{ $pickup['Dropaddress']->address }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>

                      <div class="col-xl-3 col-md-6">
                      <label>PinCode</label>

                      <h5 class="m-b-30 f-w-700"> {{ $pickup->pincode }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>

                      <div class="col-xl-3 col-md-6">
                      <label>Status</label>

                      <h5 class="m-b-30 f-w-700"> @if($pickup->is_status =="active")

                         <span class="badge badge-success">Active</span>
                      @else
                         <span class="badge badge-danger">Deactive</span>
                      @endif
                        <span class="text-c-danger m-l-10"></span></h5>

                      </div>
                      @if($pickup['boy'] != null)
                      <div class="col-xl-3 col-md-6">
                      <label>Deliveryboy</label>

                      <h5 class="m-b-30 f-w-700"> {{ $pickup['boy']->name }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>
                      @endif

                      <div class="col-xl-3 col-md-6">
                      </label>Amount</label>

                      <h5 class="m-b-30 f-w-700"> {{ $pickup->amount }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>

                      <div class="col-xl-3 col-md-6">
                        </label>Promocode</label>

                        <h5 class="m-b-30 f-w-700"> {{ ($pickup->promocode)?$pickup->promocode:"-" }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>

                    </div>
                </div>
                @endif


            @if(!isset($pickup->dispute))
                <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" onclick="window.history.go(-1); return false;">Back</a>
                {{-- <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" href="{{ route('pickup.index') }}">Back</a> --}}
            @endif

              </div>

            </div>
          </div>
        </div>
      </div>
      <!--end::Card-->
    </div>
    <!--end::Container-->
  </div>

  {{-- Dispute order start --}}
  @if(isset($pickup->dispute))
  <div class="d-flex flex-column-fluid my-3">
    <!--begin::Container-->
    <div class="container">
      <!--end::Notice-->
      <!--begin::Card-->
      <div class="card card-custom">
        <div class="card-body" style="overflow-x: auto;">

          <div class="container">

            <div class="row">
              <div class="col-md-12">

                <div class="col-xl-3 col-md-6">
                      <h4 style="margin-bottom: 0.5rem;">Dispute</h4>
                      <hr>
                </div>

                    <div class="col-xl-12 col-md-12 ">
                      <h5 class="f-w-100">Dispute Description</h5>
                        <p class="m-b-30 text-justify">{{$pickup->dispute->dispute ? $pickup->dispute->dispute : ' - '}}<span class="text-c-green m-l-10"></span></p>
                    </div>


                    <div class="col-xl-3 col-md-6">
                      <h5>Dispute Status</h5>
                        @if($pickup->dispute->status == 'Valid')
                            <span class="badge badge-success">&nbsp;&nbsp;&nbsp;&nbsp; Valid &nbsp;&nbsp;&nbsp;&nbsp;</span>
                        @elseif($pickup->dispute->status == 'Invalid')
                            <span class="badge badge-warning text-white">&nbsp;&nbsp;&nbsp; Invalid &nbsp;&nbsp;</span>
                        @elseif($pickup->dispute->status == 'Pending')
                            <span class="badge badge-danger">&nbsp; Pending &nbsp;</span>
                        @else
                            <p> - </p>
                        @endif
                    </div>


                  <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" onclick="window.history.go(-1); return false;">Back</a>
                  {{-- <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" href="{{ route('pickup.index') }}">Back</a> --}}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endif

  <!--end::Entry-->
</div>
<!-- Content Wrapper. Contains page content -->
@endsection

@push('js')
<script>
        //create and store category
        $(function() {
          $('#editForm').bootstrapValidator();
        });



      </script>
      @endpush
