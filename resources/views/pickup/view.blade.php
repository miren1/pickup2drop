@extends('layouts.app')

@section('content')

@push('css')
<style type="text/css">
  h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
    margin-bottom: 2.5rem;
    font-weight: 500;
    line-height: 1.2;
}
.select2-container--default .select2-results>.select2-results__options{
  overflow-x:clip;
}
.select2-container--default .select2-results>.select2-results__options{
  overflow-x:clip;
}
.select2-container .select2-selection--single{
  height:40px;
}
.select2-container--default .select2-selection--single .select2-selection__arrow{
  top: 15px;
}
</style>
@endpush
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
  <!--begin::Subheader-->
  <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
    <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
      <!--begin::Info-->
      <div class="d-flex align-items-center flex-wrap mr-2">
        <!--begin::Page Title-->
        <i class="fa fa-truck text-danger"  aria-hidden="true"></i>
        <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp; Order Information view</h5>


        <!--end::Page Title-->
        <!--begin::Actions-->
        <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

        <!--end::Actions-->
      </div>
      <!--end::Info-->
    </div>
  </div>
  <!--end::Subheader-->
  <!--begin::Entry-->
  <div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container">

      <!--end::Notice-->
      <!--begin::Card-->
      <div class="card card-custom">
        <div class="card-body" style="overflow-x: auto;">

          <div class="container">

            <div class="row">


              <div class="col-md-12">
                <div>
                <span class="d-inline-block"><h4 style="margin-bottom: 0.5rem;" id="span_order_id" data-order_id="{{$pickup->id}}">Order Id : {{ $pickup->id }} ({{ $pickup->type }})</h4></span>


                <span class="d-inline-block float-right"><h6><strong>Status :
                         @if($pickup && $pickup->status == 'Request Accept')
                              {{'Pickup Requested'}}
                          @elseif($pickup && $pickup->status == 'Out Of Pickup')
                              {{'Out For Pickup'}}
                          @elseif($pickup && $pickup->status == 'Out Of Delivery')
                              {{'Out For Delivery'}}
                          @elseif($pickup && $pickup->status == 'Cancel Request')
                              {{'Request Cancelled'}}
                          @else
                            {{$pickup->status}}
                          @endif
                        </strong>  </h6></span>
                </div>
                <p>{{$pickup->created_at->format('d-m-Y H:i:s')}}</p>
                <hr>
                @if($pickup->type=="pickup" || true)
                <div class="card-block">

                  <div class="row">
                      <div class="col-xl-6 col-md-6 d-inline-block">
                      <label>Pick Address</label>

                      <h5 class="m-b-30 f-w-100 text-justify pr-5"> {{$pickup['Pickupaddress']->office_shop_no}}, {{ $pickup['Pickupaddress']->address }} @if($pickup['Pickupaddress'] && $pickup['Pickupaddress']->name)<br> Name : {{$pickup['Pickupaddress']->name}} @endif<br> Mobile No. : {{$pickup['Pickupaddress']->phone}}
                        <span class="text-c-green m-l-10"></span></h5>


                      </div>

                      <div class="col-xl-6 col-md-6  float-right">
                      <label>Drop Address</label>

                      <h5 class="m-b-30 f-w-700 text-justify pr-5">{{$pickup['Dropaddress']->office_shop_no}}, {{ $pickup['Dropaddress']->address }} @if($pickup['Dropaddress'] && $pickup['Dropaddress']->name)<br> Name : {{$pickup['Dropaddress']->name}} @endif <br> Mobile No. : {{$pickup['Dropaddress']->phone}}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>
                      <div class="col-xl-3 col-md-6">
                        <label>Request Type</label>

                        <h5 class="m-b-30 f-w-700">
                          <span class="badge badge-primary text-capitalize">{{$pickup->request_type}}</span></h5>
                      </div>
                      @if($pickup['boy'] != null)
                        <div class="col-xl-3 col-md-6 deliveryboy_name">
                            <label>Delivery Agent</label>
                            @if($pickup->status != "Cancel Request")
                              <a class="cursor-pointer" onclick="Showdeliveryboy()">&nbsp;&nbsp;<i class="fa fa-edit text-primary"></i></a>
                            @endif
                            <h5 class="m-b-30 f-w-700"> {{ $pickup['boy']->name }} ({{  $pickup->boy && $pickup->boy->mobile_number ? $pickup->boy->mobile_number : ' - '}})
                              <span class="text-c-green m-l-10"></span></h5>
                        </div>
                        @else
                        <div class="col-xl-3 col-md-6 deliveryboy_name">
                            @if($pickup->status != "Cancel Request")
                              <a class="cursor-pointer" onclick="Showdeliveryboy()" >&nbsp;&nbsp;<i class="fa fa-edit text-primary"></i></a>
                            @endif
                            <label>Delivery Agent</label>
                            <h5 class="m-b-30 f-w-700 ">  -
                              <span class="text-c-green m-l-10"></span></h5>
                        </div>
                      @endif

                      <div id="deliveryboy_Dropdown" class="col-xl-3 col-md-6 d-none">
                        <label class="d-block">Delivery Agent</label>
                        <select class="select2_dropdown" id="da_id">
                            <option value=null>--- Select DeliveryBoy---</option>
                            @foreach ($deliveryboy as $item)
                              <option value="{{$item->id}}" @if($pickup->boy && $pickup->boy->id && $pickup->boy->id == $item->id) {{'selected'}} @endif>
                                {{$item->name}}
                                @if($item->delivery_agent_status == '1')
                                   {{'(Online)'}}
                                @elseif($item->delivery_agent_status == '0')
                                {{'(Offline)'}}
                                @endif
                              </option>
                            @endforeach
                        </select>
                      </div>

                          <div class="col-xl-3 col-md-6">
                      <label>Delivery Agent Commission</label>

                      <h5 class="m-b-30 f-w-700">
                        <span class="text-c-green m-l-10">{{ number_format($pickup->order_commission,2)}}</span></h5>

                      </div>
                      <div class="col-xl-3 col-md-6">
                      <label>Distance (Km.)</label>
                      <h5 class="m-b-30 f-w-700"> {{ $pickup->distance }} KM
                        <span class="text-c-green m-l-10"></span></h5>
                    </div>

                    <hr>

                    <div class="col-xl-3 col-md-6">
                      <label>Customer Name</label>
                      <h5 class="m-b-30 f-w-700"> {{ $pickup->customer && $pickup->customer->name ? $pickup->customer->name : " - " }}
                        <span class="text-c-green m-l-10"></span></h5>
                    </div>

                    <div class="col-xl-3 col-md-6">
                      <label>Item Name</label>
                      <h5 class="m-b-30 f-w-700"> {{ $pickup->item_name }}
                        <span class="text-c-green m-l-10"></span></h5>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <label>Description</label>
                          <h5 class="m-b-30 f-w-700"> {{ $pickup->description }}
                            <span class="text-c-green m-l-10"></span></h5>
                      </div>
                    <div class="col-xl-3 col-md-6">
                      <label>Item Weight</label>
                      <h5 class="m-b-30 f-w-700"> {{ $pickup->item_weight }}
                        <span class="text-c-green m-l-10"></span></h5>
                    </div>



                      <div class="col-xl-3 col-md-6">
                      <label>Payment Status</label>

                      <h5 class="m-b-30 f-w-700">

                        @if($pickup->payment_status =="pending" && $pickup->payment_status !='' && $pickup->payment_status != null)

                          <span class="badge badge-warning">Pending</span>

                        @elseif($pickup->payment_status =="complete")

                          <span class="badge badge-success">Complete</span>

                        @else

                          <span class="badge badge-danger">Failed</span>

                        @endif
                      </h5>

                      </div>


                      <div class="col-xl-3 col-md-6">
                      <label>Payment Type</label>

{{--                      <h5 class="m-b-30 f-w-700"> {{ $pickup->payment_type }}--}}
                      <h5 class="m-b-30 f-w-700">
						    @if($pickup->payment_id != null && $pickup->wallet_amount == 0)
							  Online
							@elseif($pickup->payment_id != null && $pickup->wallet_amount != 0)
							  Online+Wallet
							@elseif($pickup->payment_id == null && $pickup->wallet_amount != 0)
							  Wallet
							@elseif($pickup->promo_amount > 0 && $pickup->v2 == true && $pickup->paid_amount == 0)
							  Discount
							@elseif($pickup->promo_amount > 0 && $pickup->v2 == false && $pickup->original_amount == 0)
							  Discount
							@else
							  Online
							@endif
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>

                      @if(!empty($pickup->note))
                      <div class="col-xl-3 col-md-6">
                          <label>Notes</label>
                          <h5 class="m-b-30 f-w-700"> {{ $pickup->note }}</h5>
                      </div>
                      @endif
                      <div class="col-md-12 text-right">
                        <div class="col-md-6 float-right">
                               @if($pickup->v2 == 'true')
                              <div>
                                    <label class="col-md-4 ">Fare Amount <span class="fs-10" style="font-size: 10px">(Without GST)</span> : </label>
                                    <h6 class="m-b-30 f-w-700 d-inline-block w-80px  mb-3"><i class="icon-inr text-dark"></i> {{ number_format($pickup->original_amount,2) }}<span class="text-c-green m-l-10"></span></h6>
                              </div>
                              @else
                              <div>
                                    <label class="col-md-3 ">Fare Amount : </label>
                                    <h6 class="m-b-30 f-w-700 d-inline-block mb-3 align-items-center w-80px "><i class="icon-inr text-dark"></i>
                                      @if($pickup->promo_amount > 0)
                                        {{ number_format($pickup->original_amount  + $pickup->promo_amount - $pickup->sgst - $pickup->cgst,2)}}
                                      @else
                                        {{ number_format($pickup->original_amount - $pickup->sgst - $pickup->cgst,2) }}
                                      @endif
                                      <span class="text-c-green m-l-10"></span></h6>
                              </div>
                              @endif
                              <div>
                                  <label class="col-md-3 text-danger">Promo Amount : </label>
                                    @if($pickup->promo_amount > 0)
                                      <h6 class="m-b-30 f-w-700 d-inline-block w-80px  mb-3 text-danger">- <i class="icon-inr text-danger"></i> {{ number_format($pickup->promo_amount,2) }} <span class="text-c-green m-l-10"></span></h6>
                                      <p class="text-danger">({{$pickup->promocode}})</p>
                                    @else
                                      <h6 class="m-b-30 f-w-700 d-inline-block w-80px text-danger mb-3"><i class="icon-inr text-danger"></i> 0.00 <span class="text-c-green m-l-10"></span></h6>
                                    @endif
                              </div>

                              <hr>
                              <div>
                                    <label class="col-md-3">Amount : </label>
                                    @if($pickup->v2 == 'true')
                                      <h6 class="m-b-30 f-w-700 d-inline-block  w-80px  mb-3"><i class="icon-inr text-dark"></i> {{ number_format($pickup->original_amount  - $pickup->promo_amount,2)}}<span class="text-c-green m-l-10"></span></h6>
                                    @else
                                      <h6 class="m-b-30 f-w-700 d-inline-block  w-80px  mb-3"><i class="icon-inr text-dark"></i> {{ number_format($pickup->original_amount - $pickup->sgst - $pickup->cgst,2)}}<span class="text-c-green m-l-10"></span></h6>
                                    @endif
                              </div>
                              <div>
                                    <label class="col-md-3">SGST : </label>
                                    <h6 class="m-b-30 f-w-700 d-inline-block w-80px  mb-3"><i class="icon-inr text-dark"></i> {{ number_format($pickup->sgst,2) }}<span class="text-c-green m-l-10"></span></h6>
                              </div>
                              <div>
                                    <label class="col-md-3">CGST : </label>
                                    <h6 class="m-b-30 f-w-700 d-inline-block w-80px   mb-3"><i class="icon-inr text-dark"></i> {{ number_format($pickup->cgst,2) }}<span class="text-c-green m-l-10"></span></h6>
                              </div>
                              <hr>
                              <div>
                                    <label class="col-md-3">Total Amount : </label>
                                    @if($pickup->v2 == 'true')
                                      <h6 class="m-b-30 f-w-700 d-inline-block w-80px   mb-3"><i class="icon-inr text-dark"></i> {{ number_format($pickup->original_amount + $pickup->cgst + $pickup->sgst - $pickup->promo_amount,2)}}<span class="text-c-green m-l-10"></span></h6>
                                    @else
                                      <h6 class="m-b-30 f-w-700 d-inline-block w-80px   mb-3"><i class="icon-inr text-dark"></i> {{ number_format($pickup->original_amount,2)}}<span class="text-c-green m-l-10"></span></h6>
                                    @endif
                              </div>
                              <div>
                                    <label class="col-md-3">Wallet Amount : </label>
                                    @if($pickup->wallet_amount > 0)
                                      <h6 class="m-b-30 f-w-700 d-inline-block w-80px  mb-3">- <i class="icon-inr text-dark"></i> {{ number_format($pickup->wallet_amount,2)}}<span class="text-c-green m-l-10"></span></h6>
                                    @else
                                      <h6 class="m-b-30 f-w-700 d-inline-block w-80px  mb-3"><i class="icon-inr text-dark"></i> 0.00 <span class="text-c-green m-l-10"></span></h6>
                                    @endif
                              </div>
                              <hr>
                              <div>
                                    <label class="col-md-3">Paid Amount : </label>
                                    @if($pickup->v2 == 'true')
                                      <h6 class="m-b-30 f-w-700 d-inline-block w-80px   mb-3"><i class="icon-inr text-dark"></i> {{ number_format($pickup->original_amount + $pickup->cgst + $pickup->sgst - $pickup->promo_amount - $pickup->wallet_amount,2)}}<span class="text-c-green m-l-10"></span></h6>
                                    @else
                                      <h6 class="m-b-30 f-w-700 d-inline-block w-80px   mb-3"><i class="icon-inr text-dark"></i> {{ number_format($pickup->original_amount,2)}}<span class="text-c-green m-l-10"></span></h6>
                                    @endif
                              </div>

                        </div>
                      </div>


                    </div>
                </div>
                @endif



              @if($pickup->type=="medicine")

               {{-- //<h1>{{ $pickup->type }}</h1> --}}
                <div class="card-block">
                  <div class="row">

                    <div class="col-xl-3 col-md-6">
                      <label>Item Name</label>

                      <h5 class="m-b-30 f-w-700"> {{ $pickup->item_name }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>

                      <div class="col-xl-3 col-md-6">
                      <label>Customer Name</label>
                      <!-- {{-- @php dd($pickup); @endphp --}} -->
                      <h5 class="m-b-30 f-w-700"> {{ $pickup->customer->name }}
                        <span class="text-c-green m-l-10"></span></h5>

                    </div>

                      <div class="col-xl-3 col-md-6">
                      <label>Description</label>

                      <h5 class="m-b-30 f-w-700"> {{ $pickup->description }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>


                      <div class="col-xl-3 col-md-6">
                      <label>Dropaddress</label>

                      <h5 class="m-b-30 f-w-700">{{$pickup['Dropaddress']->office_shop_no}}, {{ $pickup['Dropaddress']->address }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>

                      <div class="col-xl-3 col-md-6">
                      <label>PinCode</label>

                      <h5 class="m-b-30 f-w-700"> {{ $pickup->pincode }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>

                      <div class="col-xl-3 col-md-6">
                      <label>Status</label>

                      <h5 class="m-b-30 f-w-700">
                        @if($pickup && $pickup->status == 'Request Accept')
                              {{'Pickup Requested'}}
                          @elseif($pickup && $pickup->status == 'Out Of Pickup')
                              {{'Out For Pickup'}}
                          @elseif($pickup && $pickup->status == 'Out Of Delivery')
                              {{'Out For Delivery'}}
                          @elseif($pickup && $pickup->status == 'Cancel Request')
                              {{'Request Cancelled'}}
                          @else
                            {{$pickup->status}}
                          @endif
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>

                      {{-- <div class="col-xl-3 col-md-6">
                      <label>Request Type</label>

                      <h5 class="m-b-30 f-w-700"> {{ $pickup->request_type }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div> --}}

                      <div class="col-xl-3 col-md-6">
                      <label>Request Type</label>

                      <h5 class="m-b-30 f-w-700">
                        <span class="badge badge-primary text-capitalize">{{$pickup->request_type}}</span>
                        <!-- {{-- <span class="text-c-green m-l-10"></span> --}} -->
                      </h5>

                      </div>

                      {{-- <div class="col-xl-3 col-md-6">
                      <label>Type</label>

                      <h5 class="m-b-30 f-w-700"> {{ $pickup->type }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div> --}}

                       <div class="col-xl-3 col-md-6">

                               <label>Media</label>

                               <h5 class="m-b-30 f-w-700">
                               <!-- {{-- <a href="{{url('public/images')}}/{{$pickup['image']->img_path}}" target="_blank"> --}} -->
                               @if($pickup['image'])
                                  <img class="img-radius" src="{{$pickup['image']->img_path}}" width="60" height="60">
                                <!-- </a> -->
                                @else
                                -
                                @endif
                                <span class="text-c-green m-l-10"></span></h5>


                            </div>

                        <div class="col-xl-3 col-md-6">
                        <label>Distance (Km).</label>

                        <h5 class="m-b-30 f-w-700"> {{ $pickup->distance }} km.
                          <span class="text-c-green m-l-10"></span></h5>

                        </div>

                    </div>


                  </div>
                  @endif

              @if($pickup->type=="tiffin" || $pickup->type=="subscription")
                <div class="card-block">

                  <div class="row">

                    <div class="col-xl-3 col-md-6">
                      <label>Item Name</label>
                      {{-- @php dd($pickup); @endphp --}}
                      <h5 class="m-b-30 f-w-700"> {{ $pickup->item_name }}
                        <span class="text-c-green m-l-10"></span></h5>

                    </div>

                    <div class="col-xl-3 col-md-6">
                      <label>Customer Name</label>
                      {{-- @php dd($pickup); @endphp --}}
                      <h5 class="m-b-30 f-w-700"> {{ $pickup->customer->name }}
                        <span class="text-c-green m-l-10"></span></h5>

                    </div>

                    <div class="col-xl-3 col-md-6">
                      <label>Customer Name</label>
                      {{-- @php dd($pickup); @endphp --}}
                      <h5 class="m-b-30 f-w-700"> {{ $pickup->customer->name }}
                        <span class="text-c-green m-l-10"></span></h5>

                    </div>

                    <div class="col-xl-3 col-md-6">
                      <label>Customer Mobile No.</label>
                      {{-- @php dd($pickup); @endphp --}}
                      <h5 class="m-b-30 f-w-700"> {{ $pickup->customer->mobile_number }}
                        <span class="text-c-green m-l-10"></span></h5>

                    </div>

                      <div class="col-xl-3 col-md-6">
                      <label>Description</label>

                      <h5 class="m-b-30 f-w-700"> {{ $pickup->description }}
                        <span class="text-c-green m-l-10"></span></h5>
                      </div>

                      <div class="col-xl-3 col-md-6">
                      <label>Pick Address</label>

                      <h5 class="m-b-30 f-w-100">{{$pickup['Pickupaddress']->office_shop_no}}, {{ $pickup['Pickupaddress']->address }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>

                      <div class="col-xl-3 col-md-6">
                      <label>Dropaddress</label>

                      <h5 class="m-b-30 f-w-700">{{$pickup['Dropaddress']->office_shop_no}}, {{ $pickup['Dropaddress']->address }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>

                      <div class="col-xl-3 col-md-6">
                      <label>PinCode</label>

                      <h5 class="m-b-30 f-w-700"> {{ $pickup->pincode }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>

                      <div class="col-xl-3 col-md-6">
                      <label>Status</label>

                      <h5 class="m-b-30 f-w-700"> @if($pickup->is_status =="active")

                         <span class="badge badge-success">Active</span>
                      @else
                         <span class="badge badge-danger">Deactive</span>
                      @endif
                        <span class="text-c-danger m-l-10"></span></h5>

                      </div>
                      @if($pickup['boy'] != null)
                      <div class="col-xl-3 col-md-6">
                      <label>Deliveryboy</label>

                      <h5 class="m-b-30 f-w-700"> {{ $pickup['boy']->name }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>
                      @endif

                      <div class="col-xl-3 col-md-6">
                      <label>Amount</label>

                      <h5 class="m-b-30 f-w-700"> {{ $pickup->amount }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>

                      <div class="col-xl-3 col-md-6">
                        <label>Promocode</label>

                        <h5 class="m-b-30 f-w-700"> {{ ($pickup->promocode)?$pickup->promocode:"-" }}
                        <span class="text-c-green m-l-10"></span></h5>

                      </div>

                    </div>
                </div>
                @endif


            @if(!isset($pickup->dispute))
                <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3" onclick="window.history.go(-1); return false;">Back</a>
                {{-- <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" href="{{ route('pickup.index') }}">Back</a> --}}
            @endif

              </div>

            </div>
          </div>
        </div>
      </div>
      <!--end::Card-->
    </div>
    <!--end::Container-->
  </div>

  {{-- Dispute order start --}}
  @if(isset($pickup->dispute))
  <div class="d-flex flex-column-fluid my-3">
    <!--begin::Container-->
    <div class="container">
      <!--end::Notice-->
      <!--begin::Card-->
      <div class="card card-custom">
        <div class="card-body" style="overflow-x: auto;">

          <div class="container">

            <div class="row">
              <div class="col-md-12">

                <div class="col-xl-3 col-md-6">
                      <h4 style="margin-bottom: 0.5rem;">Dispute</h4>
                      <hr>
                </div>

                    <div class="col-xl-12 col-md-12 ">
                      <h5 class="f-w-100">Dispute Description</h5>
                        <p class="m-b-30 text-justify">{{$pickup->dispute->dispute ? $pickup->dispute->dispute : ' - '}}<span class="text-c-green m-l-10"></span></p>
                    </div>


                    <div class="col-xl-3 col-md-6">
                      <h5>Dispute Status</h5>
                        @if($pickup->dispute->status == 'Valid')
                            <span class="badge badge-success">&nbsp;&nbsp;&nbsp;&nbsp; Valid &nbsp;&nbsp;&nbsp;&nbsp;</span>
                        @elseif($pickup->dispute->status == 'Invalid')
                            <span class="badge badge-warning text-white">&nbsp;&nbsp;&nbsp; Invalid &nbsp;&nbsp;</span>
                        @elseif($pickup->dispute->status == 'Pending')
                            <span class="badge badge-danger">&nbsp; Pending &nbsp;</span>
                        @else
                            <p> - </p>
                        @endif
                    </div>

                  <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" onclick="window.history.go(-1); return false;">Back</a>
                  {{-- <a type="button" id="back" class="btn btn-outline-danger btn-lg ml-3 float-right" href="{{ route('pickup.index') }}">Back</a> --}}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endif

  <!--end::Entry-->
</div>
<!-- Content Wrapper. Contains page content -->
@endsection

@push('js')
<script>

        $(document).ready( function () {
          $('#deliveryboy_Dropdown').addClass('d-none');
				  $('.select2_dropdown').select2();
        });

        function Showdeliveryboy(){
          $('.deliveryboy_name').addClass('d-none');
          $('#deliveryboy_Dropdown').addClass('d-block');
        }
        //create and store category
        $(function() {
          $('#editForm').bootstrapValidator();
        });

        $("#da_id").change(function(){
          var da_id = $("#da_id").val();
          if(da_id != "null"){
            var originalda = `{{$pickup->boy && $pickup->boy->id ? $pickup->boy->id : 'Assign'}}`;
            var textMessage = '';
            if(originalda != 'Assign')
              textMessage = 'Reassign DeliveryBoy !'
            else
              textMessage = 'Assign DeliveryBoy !'

            var order_id = $("#span_order_id").data('order_id');
              swal({
                  title: "Are you sure?",
                  text: textMessage,
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
              }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                          url: '{{ route('deliveryboy.assign') }}',
                          type: 'post',
                          data: {
                              order_id: order_id,_token:"{{csrf_token()}}",
                              da_id: da_id
                          },
                          dataType: 'json',
                      success: function(response) {
                          if (response.status == "success") {
                              swal(response.message, {
                                  icon: 'success',
                              }).then((result) => {
                                  window.location.reload();
                              });
                          }else if(response.status == "error"){
                                swal(response.message, {
                                  icon: 'error',
                              }).then((result) => {
                                  window.location.reload();
                              });
                          } else {
                              swal("Error","Something Wrong try agian..",{icon:'error'});
                          }
                      }
                  });
                }else{
                  $("#da_id").val({{$pickup->boy && $pickup->boy->id ? $pickup->boy->id : null}});
                }
              });
          }
        });


      </script>
      @endpush
