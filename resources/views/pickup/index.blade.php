@extends('layouts.app')

@section('content')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <i class="fa fa-truck text-danger"  aria-hidden="true"></i>
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">&nbsp;Orders List</h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

               {{--  <a href="{{ url('deliveryboy/add') }}" class="btn btn-light-warning font-weight-bolder btn-sm"
                data-keyboard="false" data-backdrop="static">Add New Deliveryboy</a> --}}
                <!--end::Actions-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--end::Notice-->
            <!--begin::Card-->
            <div class="card card-custom">
                {{-- <div class="card-header">
                    <div class="card-title">
                        <span class="card-icon">
                            <i class="fa fa-truck text-danger" aria-hidden="true"></i>
                        </span>
                        <h3 class="card-label">Pickup List</h3>

                         <a href="{{ url('deliveryboy/add') }}" class="btn btn-light-danger font-weight-bolder btn-sm" data-keyboard="false"
                        data-backdrop="static">Add New Deliveryboy</a>
                    </div>
                </div> --}}
                <div class="card-body" style="overflow-x: auto;">
                    <!--begin: Datatable-->
                    <div class="mb-4 float-right d-block w-100 text-right">
                        <label for="" class="mx-4 border border-primary p-2 rounded text-primary">Order Status  </label>
                        <a class="btn btn-outline-success mr-2" onclick="setOrderType('all')">&nbsp;&nbsp;&nbsp;&nbsp; All &nbsp;&nbsp;&nbsp;&nbsp;</a>
                        <a class="btn btn-outline-warning mr-2" onclick="setOrderType('pending')">Pending</a>
                        <a class="btn btn-outline-primary mr-2" onclick="setOrderType('process')">In Progress</a>
                        <a class="btn btn-outline-info mr-2" onclick="setOrderType('complete')">Completed</a>
                        <a class="btn btn-outline-danger mr-2" onclick="setOrderType('cancel')">Cancelled</a>
                        <a class="btn btn-outline-danger mr-2" onclick="setOrderType('dispute')">Disputed</a>
                    </div>

                    <table class="table table-hover" id="myTable">
                        <thead>
                            <tr>
                                {{-- <th>No</th> --}}
                                <th>Order ID</th>
                                <th>Mobile Number</th>
                                <th>Customer Name</th>
                                <th class="w-90px text-wrap">Item Name</th>
                                <th class="w-70px">Status</th>
                                <th class="text-center">Request Type</th>
                                <th>Payment Status</th>
                                <th>Payment Type</th>
                                <th class="w-80px">price <span style="font-size: 10px">(Without GST)</span> </th>
                                <th class="text-center">Date/Time</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                    <!--end: Datatable-->
                    <div class="container">
                        <div class="modal fade" id="myModal">
                            <div class="modal-dialog ">
                                <div class="modal-content">
                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h4 class="modal-title">Change Delivery Agent</h4>
                                        <button type="button" class="close" id="closeModal"
                                            data-dismiss="modal">&times;</button>
                                    </div>
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <h4 id="error-edit" style="display: none;" class='alert alert-danger'></h4>
                                        <form id="editForm" method="POST" class="form-horizontal">
                                            @csrf
                                            <div class="form-group">
                                                <input type="hidden" name="id" id="id">
                                                <label>Delivery Agent</label>
                                                <select class="form-control select2" id="deliveryboy_id" name="deliveryboy_id" required
                                                data-bv-notempty="true"
                                                data-bv-notempty-message="Delivery Agent Required">
                                                    <option value=""> -- Select Delivery Agent -- </option>
                                                    @foreach ($deliverys as $delivery)
                                                        <option value="{{$delivery->id}}">{{$delivery->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                    </div>
                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" id="closeModal"data-dismiss="modal">Close</button>
                                        <button type="submit" id="btn_save" class="btn btn-danger">Change</button>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!-- Content Wrapper. Contains page content -->
@endsection

@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<script>
    function setOrderType(ffilter){
        $table.state.clear()
        var uri = location.href;
        var filter = 'all';
        if(uri.indexOf("?") > 0){
            var check = uri.includes('&status');
            if(uri.includes('&status')){
                var new_url = uri.split('&status');
                window.location.replace(`${new_url[0]}&status=${ffilter}`);
            }else if(uri.includes('?status')){
                var new_url = uri.split('?status');
                window.location.replace(`${new_url[0]}?status=${ffilter}`);
            }
            else{
                window.location.replace(`${uri}&status=${ffilter}`);
            }
        }else{
            window.location.replace(`?status=${ffilter}`);
        }

        $(`#order_type option[value="${ffilter}"]`)


    }
    $(document).ready(function() {
        $('.select2').select2();
    });

    $(document).ready(function() {
        var uri = location.href;
        var filter = 'all';
        if(uri.indexOf("?") > 0)
            var filter = uri.substring(uri.indexOf("?")+1);
        $(`#order_type option[value="${filter}"]`).attr("selected", "selected")

        $table = $("#myTable").DataTable({
            "ajax": `{{ url('pickup/get_all/${filter}') }}`,
            "processing": true,
            "serverSide": true,
            "ordering": false,
            retrieve: true,
            'stateSave' : true,
            "columns": [
              /*{ "data": "id",
                render:function(data,type,row,meta){
                    return meta.row + 1;
                }
            },
            {
                data: "id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1
                }
            },*/
            { "data": "id",
                render:function(data,type,row,meta){
                //   return (row.customer && row.customer.mobile_number ? row.customer.mobile_number : ' - ');
                    // return 1000 + data;
                    return data;
                }
            },
            { "data": "mobile_number" ,name:"customer.mobile_number"},
            { "data": "id",
               render:function(data,row,alldata){
               var name ="";
                //alert(alldata.contactsdata.length != 0);
               if(alldata.customer!= null){

                 name+=alldata.customer.name;
                 return name;
               }
               return '-';
               }
             },
            { "data": "item_name",
                render:function(data,type,row,meta){
                        if (data.length > 20)
                        return '<span  data-toggle="tooltip" data-theme="dark" title="'+data+'">'+data.substring(0,20) + '...'+'</span>';
                        else
                        return data;
                }
            },
            /*{ "data": "item_weight" },*/
            { "data": "status" },

            /*{ "data": "id",
               render:function(data,row,alldata){
               var name ="";
                //alert(alldata.contactsdata.length != 0);
               if(alldata.boy != null){

                 name+=alldata.boy.name;
               }
               return '<span  data-toggle="tooltip" title="'+name+'">'+name.substring(0,20) + '...'+'</span>';
             } },*/
             //{ "data": "request_type" },
             {
               data: "request_type",
                    render: function(data, row, alldata){
                        //if (alldata.request_type =="normal") {
                            return `<span class="label label-lg text-capitalize label-light-default py-4 label-inline" style="width:120px !important">${alldata.request_type}</span>`;
                        /*}else {
                            return '<span class="label label-lg font-weight-bold label-light-danger label-inline">Urgent</span>';
                        }*/
                    }
                },
             //{ "data": "customer.name" },


             {
               data: "payment_status",
                    render: function(data, row, alldata){
                        if (alldata.payment_status =="pending" && alldata.payment_status !='' && alldata.payment_status != null) {
                            return '<span class="label label-lg font-weight-bold label-light-warning label-inline">Pending</span>';
                        }else if (alldata.payment_status =="complete") {

                            return '<span class="label label-lg font-weight-bold label-light-success label-inline">Completed</span>';

                        }else {
                            return '<span class="label label-lg font-weight-bold label-light-danger label-inline">Failed</span>';
                        }
                    }
                },
            { "data": "payment_type",render:function(data,type,row,meta){
				var paymenttype = '';
                    if(row.payment_id != null && row.wallet_amount == 0)
                        paymenttype = 'Online';
                    else if(row.payment_id != null && row.wallet_amount != 0)
                        paymenttype = 'Online+Wallet';
                    else if(row.payment_id == null && row.wallet_amount != 0)
                        paymenttype = 'Wallet';
                    else if(row.promo_amount > 0 && row.v2 == 'true' && (row.paid_amount == 0 || row.paid_amount == null))
                        paymenttype = 'Discount';
                    else if(row.promo_amount > 0 && row.v2 == 'false' && row.original_amount == 0)
                        paymenttype = 'Discount';
                    else
                        paymenttype = 'Online';

                    return paymenttype;
                }
            },
            { "data": "original_amount",
                render : function (data,type,row,meta) {
                     if(row.v2 == 'false' && row.promo_amount > 0){
                        $amount = row.original_amount + row.promo_amount - row.sgst - row.cgst;
                        return $amount.toFixed(2);
                    }else{
						if(row.original_amount > 0)
                        	return (row.original_amount).toFixed(2);
						else
							return '0.00';
                    }
                }
            },
            { "data": "created_at",class:"text-center",
                render : function (data,type,row,meta) {
//return row.created_at;
//return "test";
                    $html = "<span>"+moment(row.created_at).format('DD-MM-YYYY')+"</span></br><span>"+moment(row.created_at).format('HH:mm:ss')+"</span>";
                    // $html = "<span>"+moment(row.created_at).format('DD-MM-YYYY')+"</span></br><span>"+moment(row.created_at).format('H:m:s')+"</span>";
                    return $html;
                }
            },
            //{ "data": "boy.name" },
            { "data": "id",
            render:function(data,type,row,meta){
                    $id =btoa(row.id);
                    $html = '<div class="mo-mb-2"><a title="View" class="text-center" href="{{ url('pickup/view') }}/'+$id+'"><i class="fa fa-eye text-primary mr-1"></i></a></div>';
                    return $html;
                }
            },
            ]
        });
      });


$( document ).ajaxComplete(function() {
            // Required for Bootstrap tooltips in DataTables
            $('body').tooltip({
                 selector: '[data-toggle="tooltip"]',
                 container: 'body',
                 "delay": {"show": 100, "hide": 0},
            });
    });
   $(function(){
        $('#editForm').bootstrapValidator()
            .on('success.form.bv', function(e) {
            $.ajax({
                type: "POST",
                url: "{{ route('deliveryboy.change') }}",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function(result) {
                    swal(result.title, result.message, result.icon);
                    if (result.icon=="success") {
                        $('#id').val('');
                        $('#deliveryboy_id').val('').trigger('change');
                        $('#editForm').bootstrapValidator('resetForm', true);
                        $('#error').css('display','none');
                        $('#myModal').modal('hide');
                        $('#myTable').DataTable().ajax.reload();
                    }
                }
            });
        });

        $(document).on('click','.edit',function(e){
            e.preventDefault();
            var id = $(this).data('id');
            var d_id = $(this).data('d_id');
            // alert(id);
            $('#deliveryboy_id').val(d_id).trigger('change');
            $('#id').val(id);
        });

        $(document).on('click', '#closeModal', function(e) {
            e.preventDefault();
            /* Act on the event */
            $('#id').val('');
            // $('#deliveryboy_id').val(null);
            $('#deliveryboy_id').val(null).trigger('change');
            $('#editForm').bootstrapValidator('resetForm', true);
            $('#error').css('display','none');
        });
   });

</script>
@endpush
