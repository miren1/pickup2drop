<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $permissions = [
        //     ["name" => "Pemissions-management" , "guard_name"=>"web" ],
        //     ["name" => "Roles-management","guard_name"=>"web"],
        //     ["name" => "Users-management","guard_name"=>"web"],
        //     ["name" => "Customers-management","guard_name"=>"web"],
        //     ["name" => "DeliveryAgent-management","guard_name"=>"web"],
        //     ["name" => "Orders-management","guard_name"=>"web"],
        //     ["name" => "Subscription-management","guard_name"=>"web"],
        //     ["name" => "Wallet-management","guard_name"=>"web"],
        //     ["name" => "Notification-management","guard_name"=>"web"],
        //     ["name" => "AdminNotification-management","guard_name"=>"web"],
        //     ["name" => "DisputeOrder-management","guard_name"=>"web"],
        //     ["name" => "Transaction History-management","guard_name"=>"web"],
        //     ["name" => "GSTReport-management","guard_name"=>"web"],
        //     ['name' => "Settings-management","guard_name"=>"web"],
        // ];

        $permissions = [
            ['module_name' => 'Customer-Management' ,'name' =>  'Customer-list',"guard_name"=>"web"],
            ['module_name' => 'Deliveryboy-Management' ,'name' =>  'Deliveryboy-List',"guard_name"=>"web"],
            ['module_name' => 'Deliveryboy-Management' ,'name' =>  'Deliveryboy-Add',"guard_name"=>"web"],
            ['module_name' => 'Deliveryboy-Management' ,'name' =>  'Deliveryboy-Edit',"guard_name"=>"web"],
            ['module_name' => 'Deliveryboy-Management' ,'name' =>  'Deliveryboy-Show',"guard_name"=>"web"],
            ['module_name' => 'Deliveryboy-Management' ,'name' =>  'Deliveryboy-Delete',"guard_name"=>"web"],
            ['module_name' => 'Deliveryboy-Management' ,'name' =>  'Deliveryboy-Document',"guard_name"=>"web"],
            ['module_name' => 'Tds-Certificate-Management', 'name' => 'Tds-Show',"guard_name"=>"web"],
            ['module_name' => 'Tds-Certificate-Management', 'name' => 'TDS-Upload',"guard_name"=>"web"],
            ['module_name' => 'Order-Management', 'name' => 'Order-List',"guard_name"=>"web"],
            ['module_name' => 'Order-Management', 'name' => 'Order-Show',"guard_name"=>"web"],
            ['module_name' => 'Subscription-Management', 'name' => 'Subscription-List',"guard_name"=>"web"],
            ['module_name' => 'Subscription-Management', 'name' => 'Subscription-Show',"guard_name"=>"web"],
            ['module_name' => 'Wallet-Management', 'name' =>  'Wallet-List',"guard_name"=>"web"],
            ['module_name' => 'Wallet-Management', 'name' =>  'Wallet-Add',"guard_name"=>"web"],
            ['module_name' => 'Wallet-Management', 'name' =>  'Wallet-Delete',"guard_name"=>"web"],
            ['module_name' => 'Notification-Management', 'name' =>  'Send-Notification',"guard_name"=>"web"],
            ['module_name' => 'Admin-Notification-Management', 'name' =>  'Notification-List',"guard_name"=>"web"],
            ['module_name' => 'Admin-Notification-Management', 'name' =>  'Notification-Show',"guard_name"=>"web"],
            ['module_name' => 'Dispute-Management', 'name' =>  'Dispute-List',"guard_name"=>"web"],
            ['module_name' => 'Dispute-Management', 'name' =>  'Dispute-Show',"guard_name"=>"web"],
            ['module_name' => 'Transaction-History-Management', 'name' =>  'Transaction-History-List',"guard_name"=>"web"],
            ['module_name' => 'Transaction-History-Management', 'name' =>  'Transaction-History-Show',"guard_name"=>"web"],
            ['module_name' => 'GST-Report-Management', 'name' =>  'GST-Report-List',"guard_name"=>"web"],
            ['module_name' => 'City-Management','name' => 'City-List',"guard_name"=>"web"],
            ['module_name' => 'City-Management','name' => 'City-Add',"guard_name"=>"web"],
            ['module_name' => 'City-Management','name' => 'City-Edit',"guard_name"=>"web"],
            ['module_name' => 'City-Management','name' => 'City-Show',"guard_name"=>"web"],
            ['module_name' => 'City-Management','name' => 'City-Delete',"guard_name"=>"web"],
            ['module_name' => 'Area-Management','name' => 'Area-List',"guard_name"=>"web"],
            ['module_name' => 'Area-Management','name' => 'Area-Add',"guard_name"=>"web"],
            ['module_name' => 'Area-Management','name' => 'Area-Edit',"guard_name"=>"web"],
            ['module_name' => 'Area-Management','name' => 'Area-Show',"guard_name"=>"web"],
            ['module_name' => 'Area-Management','name' => 'Area-Delete',"guard_name"=>"web"],
            ['module_name' => 'Price-Management','name' => 'Price-List',"guard_name"=>"web"],
            ['module_name' => 'Price-Management','name' => 'Price-Add',"guard_name"=>"web"],
            ['module_name' => 'Price-Management','name' => 'Price-Edit',"guard_name"=>"web"],
            ['module_name' => 'Price-Management','name' => 'Price-Show',"guard_name"=>"web"],
            ['module_name' => 'Price-Management','name' => 'Price-Delete',"guard_name"=>"web"],
            ['module_name' => 'Promocode-Management','name' => 'Promocode-List',"guard_name"=>"web"],
            ['module_name' => 'Promocode-Management','name' => 'Promocode-Add',"guard_name"=>"web"],
            ['module_name' => 'Promocode-Management','name' => 'Promocode-Edit',"guard_name"=>"web"],
            ['module_name' => 'Promocode-Management','name' => 'Promocode-Show',"guard_name"=>"web"],
            ['module_name' => 'Promocode-Management','name' => 'Promocode-Delete',"guard_name"=>"web"],
            ['module_name' => 'Slider-Management','name' => 'Slider-List',"guard_name"=>"web"],
            ['module_name' => 'Slider-Management','name' => 'Slider-Add',"guard_name"=>"web"],
            ['module_name' => 'Slider-Management','name' => 'Slider-Edit',"guard_name"=>"web"],
            ['module_name' => 'Slider-Management','name' => 'Slider-Show',"guard_name"=>"web"],
            ['module_name' => 'Slider-Management','name' => 'Slider-Delete',"guard_name"=>"web"],
            ['module_name' => 'Order-Commission-Management','name' => 'Update-Order-Commission',"guard_name"=>"web"],
            ['module_name' => 'General-Setting-Management','name' => 'GST-Setting',"guard_name"=>"web"],
            ['module_name' => 'General-Setting-Management','name' => 'Radius-Setting',"guard_name"=>"web"],

        ];

        $d = collect($permissions)->pluck('name');
        $user = User::where('type','Admin')->first();
        $role = Role::create(['name' => 'admin']);
        $role->giveAllPermissions($d);
        $user->assignRole($role);

        Permission::insert($permissions);
        dd('done');

        // hasAllPermissions($permissions);
    }
}
/*['User-Management','User-List'],
            ['User-Management','User-Add'],
            ['User-Management','User-Edit'],
            ['User-Management','User-Show'],
            ['User-Management','User-Delete'],
            ['Role-Management', 'Role-List'],
            ['Role-Management', 'Role-Add'],
            ['Role-Management', 'Role-Edit'],
            ['Role-Management', 'Role-Show'],
            ['Role-Management', 'Role-Delete'],
            ['Permission-Management','Assign-Permission'],*/
