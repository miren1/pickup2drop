<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('referral_id')->nullable();
            $table->string('by_referral_id')->nullable();
            $table->enum('gender',['Male','Female'])->nullable();
            $table->date('birthdate')->nullable();
            $table->string('mobile_number',15)->nullable();
            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->text('two_factor_secret')->nullable();
            $table->text('two_factor_recovery_codes')->nullable();
            $table->rememberToken()->nullable();    
            $table->unsignedBigInteger('current_team_id');
            $table->foreign('current_team_id')->references('id')->on('teams')->onDelete('cascade');
            $table->text('profile_photo_path')->nullable();
            $table->text('fcm_token')->nullable();
            $table->text('api_token')->nullable();
            $table->double('wallet',15, 8)->default(0);
            $table->enum('type',['Admin','Deliveryboy','Customer'])->default('Customer');
            $table->string('payment_contact_id')->nullable();
            $table->string('fund_account_id')->nullable();
            $table->enum('bank_details_status',['Not add','Pending','Verified','Not Verified'])->default('Not add');
            $table->string('bank_account_name')->nullable();
            $table->string('bank_account_ifsc')->nullable();
            $table->string('bank_account_number')->nullable();
            $table->string('bank_name')->nullable();  
            $table->enum('is_delete',['true','false'])->default('false');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
