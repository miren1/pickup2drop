<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromocodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promocodes', function (Blueprint $table) {
            $table->id();
            $table->string('promocode');
            $table->text('description')->nullable();
            $table->enum('order_type',['pickup normal','pickup urgent','medicine','grocery','all']);
            $table->enum('code_type',['percentage','price']);
            $table->enum('one_day',['true','false']);
            $table->double('perc_price');
            $table->string('code_time');
            $table->boolean('is_delete')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promocodes');
    }
}
