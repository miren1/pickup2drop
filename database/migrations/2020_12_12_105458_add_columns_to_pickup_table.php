<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToPickupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pickups', function (Blueprint $table) {
            $table->enum('type',['pickup','grossery','medicine','tiffin'])->after('payment_type');
            $table->string('start_date')->after('type')->nullable();
            $table->string('end_date')->after('start_date')->nullable();
            $table->enum('is_status',['active','deactive'])->nullable()->after('end_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pickups', function (Blueprint $table) {
            $table->dropColumn(['type','start_date','end_date','is_status']);
        });
    }
}
