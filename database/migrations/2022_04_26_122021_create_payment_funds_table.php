<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentFundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_funds', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('fund_id');
            $table->string('contact_id');
            $table->string('bank_account_name');
            $table->string('bank_account_ifsc');
            $table->string('bank_name');
            $table->string('bank_account_number');
            $table->enum('account_verify',['pending','verified','not_verified'])->default('pending');
            $table->enum('fund_validation_status',['Pending','Created','Completed','Failed']);
            $table->integer('amount');
            $table->string('currency');
            $table->string('account_status');
            $table->string('payout_id');
            $table->string('payout_status');
            $table->string('failure_reason');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_funds');
    }
}
