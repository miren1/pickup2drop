<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriverDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_documents', function (Blueprint $table) {
            $table->id();
            $table->foreignId('driver_id');
            $table->text('driver_photo')->nullable();
            $table->text('driver_photo_status')->nullable();
            $table->text('addhar_front')->nullable();
            $table->text('addhar_front_status')->nullable();
            $table->text('addhar_back')->nullable();
            $table->text('addhar_back_status')->nullable();
            $table->text('pan_card')->nullable();
            $table->text('pan_card_status')->nullable();
            $table->text('tds_certificate')->nullable();
            $table->text('tds_certificate_status')->nullable();
            $table->text('rc_front')->nullable();
            $table->text('rc_front_status')->nullable();
            $table->text('rc_back')->nullable();
            $table->text('rc_back_status')->nullable();
            $table->text('insurance_certificate')->nullable();
            $table->text('insurance_certificate_status')->nullable();
            $table->text('vehicle_number')->nullable();
            $table->text('vehicle_model')->nullable();
            $table->text('vehicle_colour')->nullable();
            $table->text('driver_status')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_documents');
    }
}
