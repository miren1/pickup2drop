<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePickupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pickups', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->string('item_name')->nullable();
            $table->decimal('item_weight',10,3)->nullable();
            $table->foreignId('pickaddress_id');
            $table->foreignId('dropaddress_id');
            $table->enum('status',['Pickup Requested','Out Of Pickup','Picked Up','Out Of Delivery','Delivered','Request Accept','Cancel Request']);
            $table->foreignId('deliveryboy_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pickups');
    }
}
