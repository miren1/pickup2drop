<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPaymentsToPickupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pickups', function (Blueprint $table) {
            $table->enum('request_type',['normal','urgent'])->default('normal')->after('deliveryboy_id');
            $table->string('payment_id')->nullable()->after('request_type');
            $table->double('amount')->nullable()->after('payment_id');
            $table->enum('payment_status',['pending','complete','faild'])->nullable()->after('amount');
            $table->enum('payment_type',['online','cash'])->nullable()->after('payment_status');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pickups', function (Blueprint $table) {
            $table->dropColumn(['request_type','payment_id','amount','payment_status','payment_type']);
        });
    }
}
