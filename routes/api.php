<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\ApiController;
use App\Http\Controllers\api\UserController;
use App\Http\Controllers\api\SliderController;
use App\Http\Controllers\api\CustomerController;
use App\Http\Controllers\api\DeliveryboyController;
use App\Http\Controllers\api\WalletController;
use App\Http\Controllers\api\DisputeController;
use App\Http\Controllers\api\PaymentController;
use App\Models\GeneralSetting;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//AUTH
Route::post('login',[ApiController::class,'login']);
Route::post('Deliveryboylogin',[ApiController::class,'Deliveryboylogin']);
Route::post('CheckMobile',[ApiController::class,'CheckMobile']);
Route::middleware('auth:api')->post('reffer',[ApiController::class,'Reffer']);
Route::post('ChangePassword',[ApiController::class,'ChangePassword']);

//All Users APIS
Route::middleware('auth:api')->post('GetProfile',[UserController::class,'GetProfile']);
Route::middleware('auth:api')->post('Updateuser',[UserController::class,'Updateuser']);
Route::post('verify/agent',[UserController::class,'verifyDeliveryAgent']);
Route::post('reset/agent/password',[UserController::class,'resetAgentPassword']);


Route::middleware('auth:api')->post('reset-password', [UserController::class, 'resetPassword']);
Route::middleware('auth:api')->post('customer-update-profile', [UserController::class, 'customerUpdateProfile']);
//Customer API

Route::middleware('auth:api', 'throttle:1,' . (1 / 10))->post('pickRequest', [CustomerController::class, 'pickRequest']);
Route::middleware('auth:api', 'throttle:1,' . (1 / 10))->post('pickRequestv2', [CustomerController::class, 'pickRequestv2']);
Route::middleware('auth:api')->post('GetRequest',[CustomerController::class,'GetRequest']);
Route::middleware('auth:api')->post('pickuplist',[CustomerController::class,'pickuplist']);
Route::middleware('auth:api')->post('getprice',[CustomerController::class,'getPrice']);
Route::middleware('auth:api')->post('getpricev2',[CustomerController::class,'getPricev2']);

Route::middleware('auth:api')->post('pickupdetail',[CustomerController::class,'pickupdetail']);
Route::middleware('auth:api')->post('pickupcomplete',[CustomerController::class,'pickupcomplete']);
Route::middleware('auth:api')->post('promocode_list',[CustomerController::class,'promocode_list']);
Route::middleware('auth:api')->post('apply_promocode',[CustomerController::class,'ApplyPromocode']);
Route::middleware('auth:api')->post('grossery_medicine_request',[CustomerController::class,'Grossery_Medicine_Request']);
Route::middleware('auth:api')->post('tiffin_request',[CustomerController::class,'Tiffin_Request']);
Route::middleware('auth:api')->post('grossery_medicine_getprice',[CustomerController::class,'Grossery_Medicine_Getprice']);
Route::middleware('auth:api')->post('tiffin_list',[CustomerController::class,'Tiffin_List']);
Route::middleware('auth:api')->post('tiffin_detail',[CustomerController::class,'Tiffin_Detail']);

Route::middleware('auth:api')->post('update-note', [CustomerController::class, 'updateNote']);

//cancel pickup and refund
Route::middleware('auth:api')->post('pickup-cancel',[CustomerController::class,'PaymentCancel']);
Route::middleware('auth:api')->post('default-address',[CustomerController::class, 'default_address']);

//deliveryboy API
Route::middleware('auth:api')->post('delivetyboyacceptrequest',[DeliveryboyController::class,'delivetyboyacceptrequest']);
Route::middleware('auth:api')->post('deliveryboypickuplist',[DeliveryboyController::class,'deliveryboypickuplist']);
Route::middleware('auth:api')->post('deliveryboypickupdetail',[DeliveryboyController::class,'deliveryboypickupdetail']);
Route::middleware('auth:api')->post('pickupnotaccept',[DeliveryboyController::class,'pickupnotaccept']);
Route::middleware('auth:api')->post('deliveryboypaymentstatus',[DeliveryboyController::class,'deliveryboypaymentstatus']);
Route::middleware('auth:api')->post('deliveryboylocation',[DeliveryboyController::class,'deliveryboylocation']);
Route::middleware('auth:api')->post('deliveryboycompletepickup',[DeliveryboyController::class,'deliveryboycompletepickup']);
Route::middleware('auth:api')->post('boy_tiffin_list',[DeliveryboyController::class,'Boy_Tiffin_List']);
Route::middleware('auth:api')->post('boy_tiffin_detail',[DeliveryboyController::class,'Boy_Tiffin_Detail']);
Route::middleware('auth:api')->post('boy_status',[DeliveryboyController::class, 'DeliveryAgentStatus']);
Route::middleware('auth:api')->post('subscription-price-old',[DeliveryboyController::class, 'SubscriptionPrice']);
Route::post('delivery-boy-registration',[DeliveryboyController::class, 'DeliveryBoyRegistration']);
Route::middleware('auth:api')->post('delivery-boy-documents-upload',[DeliveryboyController::class, 'DeliveryBoyDocumentsUpload']);
Route::middleware('auth:api')->get('delivery-boy-profile',[DeliveryboyController::class, 'getDeliveryAgentProfile']);
Route::get('get-cities',[DeliveryboyController::class, 'getCity']);
Route::middleware('auth:api')->post('subscription-price',[CustomerController::class, 'SubscriptionPrice']);

//slider API
Route::middleware('auth:api')->get('slider_list',[SliderController::class,'Slider']);
Route::middleware('auth:api')->get('slider_listv2',[SliderController::class,'Sliderv2']);

//WalletHistory api
Route::middleware('auth:api')->post('addwallet',[WalletController::class,'add']);
Route::middleware('auth:api')->post('viewwallet',[WalletController::class,'view']);

//addresslist
Route::middleware('auth:api')->post('addresslist',[WalletController::class,'addresslist']);
Route::middleware('auth:api')->post('deleteaddress',[WalletController::class,'deleteaddress']);
Route::any('razorpay/webhook',[DeliveryboyController::class,'webhookResponse']);

//dispute confirmation
Route::middleware('auth:api', 'throttle:1,'.(1/10))->post('dispute/add',[DisputeController::class,'addDispute']);

//razorpay bank account validation for deliveryboy
Route::middleware('auth:api')->post('check/bankDetail',[PaymentController::class,'checkBankDetail']);

Route::middleware('auth:api')->post('logout',[UserController::class,'logout']);

//number using get object of delivery boy
Route::post('getUser',[DeliveryboyController::class,'objectGet'])->name('objectGet');



//weekly cron
Route::any('weekly/transaction',[PaymentController::class,'weeklyTransaction'])->name('weekly.transaction');
Route::middleware('auth:api')->get('transaction/history',[PaymentController::class,'transactionHistory'])->name('transaction.history');
Route::middleware('auth:api')->post('transaction/detail',[PaymentController::class, 'deliveryboypayoutHistory'])->name('transaction.detail');
Route::middleware('auth:api')->get('tds_certificate_download',[PaymentController::class, 'TdsCertificateDownload'])->name('tds.certificate.download');

//delivery boy wallet history
Route::middleware('auth:api')->get('deliveryboy/wallet/history',[PaymentController::class,'deliveryboyWalletHistory'])->name('deliveryboyWalletHistory');

Route::get('get_update', function () {
    $maintanace_mode = GeneralSetting::where('name', 'customer_maintance_mode')->first()->value;
    $data = [
        "android_current_version" => "21",
        "android_minimum_version" => "18",
        "android_force_update" => true,
        "android_under_maintenance" => $maintanace_mode,
        "ios_current_version" => "10",
        "ios_minimum_version" => "10",
        "ios_force_update" => true,
        "ios_under_maintenance" => $maintanace_mode,
    ];
    return response()->json($data);
});

Route::get('get_da_update', function () {
    $maintanace_mode = GeneralSetting::where('name', 'da_maintance_mode')->first()->value;
    $data = [
        "current_version" => "14",
        "minimum_version" => "14",
        "force_update" => true,
        "under_maintenance" => $maintanace_mode,
    ];
    return response()->json($data);
});
