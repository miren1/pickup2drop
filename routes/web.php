<?php

use App\Http\Controllers\AdminNotificationController;
use App\Http\Controllers\AdminUserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\PriceController;
use App\Http\Controllers\PickupController;
use App\Http\Controllers\TiffinController;
use App\Http\Controllers\WalletController;
use App\Http\Controllers\AreacodeController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\DeliveryController;
use App\Http\Controllers\PromocodeController;
use App\Http\Controllers\DeliveryboyassignController;
use App\Http\Controllers\NotificationCotroller;
use App\Http\Controllers\SliderController;
use App\Http\Controllers\api\DisputeController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\CommissionController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TransactionHistoryController;
use App\Http\Controllers\GeneralSettingsController;
use App\Http\Controllers\GstController;
use App\Http\Controllers\RolePermissionController;
use App\Models\GeneralSetting;

Route::get('/', function () {
    return redirect('login');
});

Route::get('temp-password', function () {
    dd(\Illuminate\Support\Facades\Hash::make("Admin@123"));
});
Route::get('/cron', [NotificationCotroller::class, 'cron']);
Route::get('notificationcron', function () {
    Artisan::call('schedule:run');
});

Auth::routes();

Route::any('tiffin_subscription', [App\Http\Controllers\HomeController::class, 'FnGetSubscription']);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('auth', 'admin');
Route::middleware('auth')->prefix('customer')->group(function () {
    Route::get('/', [CustomerController::class, 'index'])->name('customer.index');
    Route::get('show', [CustomerController::class, 'show'])->name('show');
});

Route::middleware('auth')->prefix('admin')->group(function () {
    Route::get('profile', [HomeController::class, 'profile'])->name('admin.profile');
    Route::post('updateprofile', [HomeController::class, 'store'])->name('profile.store');
    Route::post('updatepassword', [HomeController::class, 'updatePassword'])->name('profile.updatePassword');
});
Route::middleware('auth')->prefix('price')->group(function () {
    Route::get('/', [PriceController::class, 'index'])->name('price.index');
    Route::get('add', [PriceController::class, 'create'])->name('create');
    Route::get('show', [PriceController::class, 'show'])->name('show');
    Route::post('store', [PriceController::class, 'store'])->name('store');
    Route::get('edit/{id}', [PriceController::class, 'edit'])->name('edit');
    Route::post('destroy', [PriceController::class, 'destroy'])->name('destroy');
});

Route::middleware('auth')->prefix('general-settings')->group(function () {
    Route::get('/', [GeneralSettingsController::class, 'index'])->name('generalsettings.index');
    Route::get('getdata', [GeneralSettingsController::class, 'getdata'])->name('generalsettings.getdata');
    Route::post('storedata', [GeneralSettingsController::class, 'storedata'])->name('generalsettings.storedata');
    Route::post('storearearadius', [GeneralSettingsController::class, 'storearearadius'])->name('generalsettings.storearearadius');
    Route::post('appsetting', [GeneralSettingsController::class, 'AppSetting'])->name('generalsettings.appsetting');
});
Route::middleware('auth')->prefix('deliveryboy')->group(function () {
    Route::get('/', [DeliveryController::class, 'index'])->name('deliveryboy.index');
    Route::get('add', [DeliveryController::class, 'create'])->name('create');
    Route::get('get_all', [DeliveryController::class, 'Getall'])->name('Getall');
    Route::post('store', [DeliveryController::class, 'store'])->name('store');
    Route::get('edit/{id}', [DeliveryController::class, 'edit'])->name('edit');
    Route::get('destroy/{id}', [DeliveryController::class, 'destroy'])->name('destroy');
    Route::get('docs/{id}', [DeliveryController::class, 'DocumentList'])->name('docs');
    Route::post('update/wallet', [DeliveryController::class, 'updateWallet'])->name('update.wallet');
    Route::get('approve-delivery-agent/{id}/{status}', [DeliveryController::class, 'approveDeliveryAgent'])->name('approve.delivery.agent');
    Route::get('toogle-delivery-agent/{id}/{status}', [DeliveryController::class, 'toggleDeliveryAgent'])->name('toogle.delivery.agent');
    Route::get('docs/approve/{id}/{field_name}', [DeliveryController::class, 'ApproveDocs'])->name('docs.approve');
    Route::get('docs/reject/{id}/{field_name}', [DeliveryController::class, 'ApproveDocs'])->name('docs.reject');
});

Route::middleware('auth')->prefix('pickup')->group(function () {
    Route::get('/', [PickupController::class, 'index'])->name('pickup.index');
    Route::get('get_all/{filter}', [PickupController::class, 'Get_all'])->name('Get_all');
    Route::get('view/{id}', [PickupController::class, 'create'])->name('pickup.create');
    Route::post('deliveryboy/change', [PickupController::class, 'DeliveryBoyChange'])->name('deliveryboy.change');
    Route::post('deliveryboy/assign', [PickupController::class, 'assignDeliveryBoy'])->name('deliveryboy.assign');
});

Route::middleware('auth')->prefix('areacode')->group(function () {
    Route::get('/', [AreacodeController::class, 'index'])->name('areacode.index');
    Route::get('add', [AreacodeController::class, 'create'])->name('create');
    Route::get('get_all', [AreacodeController::class, 'Getall'])->name('Getall');
    Route::post('store', [AreacodeController::class, 'store'])->name('store');
    Route::get('edit/{id}', [AreacodeController::class, 'edit'])->name('edit');
    Route::get('destroy/{id}', [AreacodeController::class, 'destroy'])->name('destroy');
    Route::post('/import', [AreacodeController::class, 'importAreaCode'])->name('import.area');
});

Route::middleware('auth')->prefix('deliveryboy_assign')->group(function () {
    Route::get('/', [DeliveryboyassignController::class, 'index'])->name('index');
    Route::get('add', [DeliveryboyassignController::class, 'create'])->name('create');
    Route::get('get_all', [DeliveryboyassignController::class, 'Getall'])->name('Getall');
    Route::post('store', [DeliveryboyassignController::class, 'store'])->name('store');
    Route::get('edit/{id}', [DeliveryboyassignController::class, 'edit'])->name('edit');
    Route::get('destroy/{id}', [DeliveryboyassignController::class, 'destroy'])->name('destroy');
});

Route::middleware('auth')->prefix('subscription')->group(function () {
    Route::get('/', [TiffinController::class, 'index'])->name('subscription.index');
    Route::get('add', [TiffinController::class, 'create'])->name('create');
    Route::get('show/{filter}', [TiffinController::class, 'Get_all'])->name('show');
    Route::post('store', [TiffinController::class, 'store'])->name('store');
    Route::get('edit/{id}', [TiffinController::class, 'edit'])->name('edit');
    Route::get('destroy/{id}', [TiffinController::class, 'destroy'])->name('destroy');
    Route::get('view/{id}', [TiffinController::class, 'view'])->name('view');
});

Route::middleware('auth')->prefix('promocode')->group(function () {
    Route::get('/', [PromocodeController::class, 'index'])->name('promocode.index');
    Route::post('show', [PromocodeController::class, 'show'])->name('promocode.show');
    Route::post('store', [PromocodeController::class, 'store'])->name('promocode.store');
    Route::post('update', [PromocodeController::class, 'update'])->name('promocode.update');
    Route::post('destroy', [PromocodeController::class, 'destroy'])->name('promocode.destroy');
});

Route::middleware('auth')->prefix('wallet')->group(function () {
    Route::get('/', [WalletController::class, 'index'])->name('wallet.index');
    Route::get('show', [WalletController::class, 'show'])->name('wallet.show');
    Route::get('create', [WalletController::class, 'Create'])->name('wallet.create');
    Route::post('store', [WalletController::class, 'store'])->name('wallet.store');
    Route::get('edit/{id}', [WalletController::class, 'Edit'])->name('wallet.edit');
    Route::post('destroy', [WalletController::class, 'destroy'])->name('wallet.destroy');
});

Route::middleware('auth')->prefix('notification')->group(function () {
    Route::get('/', [NotificationCotroller::class, 'index'])->name('notification1.index');
    Route::post('store', [NotificationCotroller::class, 'store'])->name('notification.store');
});

Route::middleware('auth')->prefix('manage-slider')->group(function () {
    Route::get('/', [SliderController::class, 'index'])->name('slider.index');
    Route::get('show', [SliderController::class, 'show'])->name('slider.show_list');
    Route::post('store', [SliderController::class, 'store'])->name('slider.store');
    Route::post('update', [SliderController::class, 'update'])->name('slider.update');
    Route::post('destroy', [SliderController::class, 'destroy'])->name('slider.destroy');
});

Route::middleware('auth')->prefix('dispute-order')->group(function () {
    Route::get('/', [DisputeController::class, 'index'])->name('dispute.index');
    Route::get('show', [DisputeController::class, 'show'])->name('dispute.show_list');
    Route::post('dispute/check', [DisputeController::class, 'disputeCheck'])->name('dispute.check');
    Route::get('/show/{id}', [DisputeController::class, 'showDetail'])->name('notification.show.detail');
});

Route::middleware('auth')->prefix('gst-report')->group(function () {
    Route::get('/', [GstController::class, 'index'])->name('gstreport.index');
    Route::get('/get_all', [GstController::class, 'getAll'])->name('gstreport.get_all');
    Route::get('/exportCsv', [GstController::class, 'exportCsv'])->name('gstreport.exportCsv');
});
Route::middleware('auth')->prefix('commission-order')->group(function () {
    Route::get('/', [CommissionController::class, 'index'])->name('commission.index');
    Route::post('commission/store', [CommissionController::class, 'store'])->name('commission.store');
});

Route::middleware('auth')->prefix('tds-certificate')->group(function () {
    Route::get('/', [DeliveryController::class, 'TDSUploadofDeliveryAgent'])->name('tds-certificate.index');
    Route::get('Getalldeliveryagent', [DeliveryController::class, 'GetallDeliveryAgent'])->name('Getalldeliveryagent');
    Route::get('tdsuploaded/{id}', [DeliveryController::class, 'tdsuploaded'])->name('tdsuploaded');
    Route::post('tdsformstore', [DeliveryController::class, 'tdsformstore'])->name('tdsformstore');
    Route::get('tdsuploadedfile', [DeliveryController::class, 'tdsuploadedfile'])->name('tdsuploadedfile');
    Route::delete('deletetds', [DeliveryController::class, 'deleteTDSCetificate'])->name('tdscertificate.delete');
});

Route::middleware('auth')->prefix('transaction-history')->group(function () {
    Route::get('/', [TransactionHistoryController::class, 'index'])->name('transaction.index');
    Route::get('show', [TransactionHistoryController::class, 'show'])->name('transaction.show_list');
    Route::get('view/{id}', [TransactionHistoryController::class, 'view'])->name('transaction.view');
    Route::get('transaction/data', [TransactionHistoryController::class, 'transactionData'])->name('transaction.data');
    Route::get('transaction/bank', [TransactionHistoryController::class, 'bankTransaction'])->name('bank.transaction');
});
Route::middleware('auth')->prefix('admin-notification')->group(function () {
    Route::get('/', [AdminNotificationController::class, 'index'])->name('notification.index');
    Route::get('/get-notification', [AdminNotificationController::class, 'Getall'])->name('notification.get');
    Route::get('/show/{id}', [AdminNotificationController::class, 'show'])->name('notification.show');
    Route::get('/mark/read/all', [AdminNotificationController::class, 'markAllRead'])->name('notification.read.all');

});

Route::prefix('roles-permission')->group(function () {
    Route::get('/', [RolePermissionController::class, 'index'])->name('role_permission.index');
    Route::get('/getall', [RolePermissionController::class, 'Getall'])->name('role_permission.Getall');
    Route::get('/assign', [RolePermissionController::class, 'assign'])->name('role_permission.assign');
    Route::get('/assign/{id}', [RolePermissionController::class, 'checkAssign'])->name('role_permission.check.assign');
    Route::post('/role/store', [RolePermissionController::class, 'storerole'])->name('role.store');
    Route::get('/role/getPermission/{id}', [RolePermissionController::class, 'getPermission'])->name('role_permission.getpermission');
    Route::post('/role/storepermission', [RolePermissionController::class, 'storePermission'])->name('role_permission.storepermission');
    Route::post('/destroy/{id}', [RolePermissionController::class, 'destoryRole'])->name('destroy.role');
});

Route::prefix('user')->group(function () {
    Route::get('/', [AdminUserController::class, 'index'])->name('user.index');
    Route::get('/create', [AdminUserController::class, 'create'])->name('user.create');
    Route::post('/store', [AdminUserController::class, 'store'])->name('user.store');
    Route::get('/edit/{id}', [AdminUserController::class, 'edit'])->name('user.edit');
    Route::get('/show/{id}', [AdminUserController::class, 'show'])->name('user.show');
    Route::post('/update/{id}', [AdminUserController::class, 'update'])->name('user.update');
    Route::delete('/{id}', [AdminUserController::class, 'destory'])->name('user.destroy');
});


Route::get('/all-city', [CityController::class, 'Getall'])->middleware('auth');
Route::resource('/city', CityController::class)->middleware('auth');

Route::get('/privacy-policy', [CustomerController::class, 'privacyPolicy'])->name('privacyPolicy');
Route::get('/driver-privacy-policy', [CustomerController::class, 'driverPrivacyPolicy'])->name('driverPrivacyPolicy');
Route::view('/about-us', 'newpages.aboutus');

Route::get('/terms-condition', [CustomerController::class, 'termsCondition'])->name('termsCondition');

Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return "Cache is clear";
});
