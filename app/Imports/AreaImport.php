<?php

namespace App\Imports;

use App\Models\Areacode;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\FromCollection;

class AreaImport implements ToModel, WithStartRow, WithCustomCsvSettings, FromCollection

{

    public function __construct($city){
        $this->city = $city;
    }
    public function startRow(): int
    {
        return 2;
    }

    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ';',
            'delimiter' => ','
        ];
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Areacode([
            'name'     => $row['0'],
            'pincode'    => $row['4'],
            'city_id' => $this->city
        ]);
    }
    public function collection()
    {
        return $this->city;
    }
}
