<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;

class DeliveryboyWalletHistory extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','order_id','amount', 'status'];


    public function Orders()
    {
        return $this->hasOne('App\Models\Pickup','id','order_id');
    }

    public function Pickupaddress() {
        return $this->hasOne('App\Models\Address','id','pickaddress_id');
    }
    public function Dropaddress() {

        return $this->hasOne('App\Models\Address','id','dropaddress_id');
    }

    public function boy() {
        return $this->hasOne('App\Models\User','id','deliveryboy_id');
    }

    public function areas() {
        return $this->hasOne('App\Models\Area','pincode','pincode');
    }

    public function customer() {
        return $this->hasOne('App\Models\User','id','user_id');
    }

    public function image() {
        return $this->hasOne('App\Models\Image','pickup_id','id');
    }

    public function customers() {
        return $this->belongsTo('App\Models\User','user_id','id');
    }

        /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

}
