<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TDSCertificate extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'tds_certificates';
    protected $fillable = [
        'delivery_agent_id',
        'pan_number',
        'tds_cert',
        'image_name',
        'financial_year'
    ];
}
