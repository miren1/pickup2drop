<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;

class Promocode extends Model
{
    use HasFactory;

    protected $fillable = [
    	'promocode','description','order_type','code_type','code_time','one_day','perc_price','upto_amount','min_order_amount','enddate','is_delete'
    ];

      /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
