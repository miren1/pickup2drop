<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DriverDocument extends Model
{
    use HasFactory;

    protected $fillable = [
        'driver_id',
        'driver_photo',
        'driver_photo_status',
        'addhar_front',
        'addhar_front_status',
        'addhar_back',
        'addhar_back_status',
        'pan_card',
        'pan_number',
        'pan_card_status', 
        'tds_certificate',
        'tds_certificate_status',
        'rc_front',
        'rc_front_status',
        'rc_back',
        'rc_back_status',
        'insurance_certificate',
        'insurance_certificate_status',
        'vehicle_number',
        'vehicle_model',
        'vehicle_colour',
        'driver_status',
        'joining_date'
    ];
}
