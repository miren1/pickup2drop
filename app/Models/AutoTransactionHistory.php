<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;
class AutoTransactionHistory extends Model
{
    use HasFactory;
    protected $fillable = ['user_id','transaction_id','fund_account_id','amount','status','tds'];

    public function customers() {
        return $this->belongsTo('App\Models\User','user_id','id');
    }

           /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
