<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;

class DeliveryArea extends Model
{
    use HasFactory;

    protected $fillable = ['area_id','deliveryboy_id'];

    public function Area()
    {
        return $this->hasOne('App\Models\Area','id','area_id');
    }

    public function Boy()
    {
        return $this->hasOne('App\Models\User','id','deliveryboy_id');
    }

    public function dBoy()
    {
        return $this->hasMany('App\Models\Delivery_fcm_token','deliveryboy_id','deliveryboy_id');
    }

    public function Areacode()
    {
        return $this->belongsTo('App\Models\Area','area_id','id');
    }

         /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
