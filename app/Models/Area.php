<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;

class Area extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'pincode',
        'delete_status'
    ];

    public function deliveryareas()
    {
        return $this->hasOne('App\Models\DeliveryArea','area_id','id')->with('boy');
    }

           /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
