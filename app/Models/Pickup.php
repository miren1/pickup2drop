<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pickup extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id', 'item_name','description', 'item_weight','pickaddress_id','dropaddress_id','pincode','distance','status','deliveryboy_id','v2','paid_amount'
        ,'request_type','payment_id','payment_status','amount','original_amount','wallet_amount','promocode','payment_type','type','start_date','end_date','is_status','dispute_status','order_commission',
        'start_time','end_time','cgst','sgst','promo_amount', 'note'
    ];

    public function Pickupaddress() {
        return $this->hasOne('App\Models\Address','id','pickaddress_id');
    }
    public function Dropaddress() {

        return $this->hasOne('App\Models\Address','id','dropaddress_id');
    }

    public function boy() {
        return $this->hasOne('App\Models\User','id','deliveryboy_id');
    }

    public function areas() {
        return $this->hasOne('App\Models\Area','pincode','pincode');
    }

    public function customer() {
        return $this->hasOne('App\Models\User','id','user_id');
    }

    public function image() {
        return $this->hasOne('App\Models\Image','pickup_id','id');
    }

    public function customers() {
        return $this->belongsTo('App\Models\User','user_id','id');
    }
    public function dispute() {
        return $this->hasOne('App\Models\Dispute', 'order_id', 'id');
    }


    protected $casts = [
//    'created_at'  => 'datetime:Y-m-d H:i:s',
  //  'updated_at' => 'datetime:Y-m-d H:i:s',
];
}
