<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;

class Wallet extends Model
{
    use HasFactory;

    protected $fillable = ['title','amount','type','user_id','is_delete'];

    public function user()
    {
        return $this->hasOne('App\Models\User','id','user_id');
    }

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function getTitleAttribute($value)
    {
        return ucfirst($value);
    }
}
