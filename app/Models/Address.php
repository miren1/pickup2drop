<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\DeliveryArea;
use DateTimeInterface;

class Address extends Model
{
    use HasFactory;
    protected $fillable = [
        'address', 'office_shop_no', 'apartment_bulding_name','landmark','pincode','is_delete','phone','longitude','latiude','user_id','type','name'
    ];

    public function areas()
    {
        return $this->hasOne('App\Models\Area','pincode','pincode')->with("deliveryareas");
    }

    public function area()
    {
        return $this->hasOne('App\Models\Area','pincode','pincode');
    }

    public function deliveryareas()
    {
        return $this->hasOne('App\Models\DeliveryArea','area_id','id');
    }

           /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
