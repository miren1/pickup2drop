<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;

class PaymentFund extends Model
{
    use HasFactory;
     protected $fillable = ['user_id','fund_id','contact_id','bank_account_name','bank_account_ifsc','bank_name','bank_account_number','account_verify'];


       /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
