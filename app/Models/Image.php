<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;

class Image extends Model
{
    use HasFactory;

    protected $fillable = [
        'pickup_id',
        'img_path'
    ];

    public function getImgPathAttribute($value)
    {
        return url('/').'/'.$value;
    }

       /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function pickup()
    {
        return $this->belongsTo('App\Models\Pickup', 'pickup_id', 'id');
    }
}
