<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;

class Price extends Model
{
    use HasFactory;

    protected $fillable = [
        'min_km',
        'max_km',
        'price',
        'status_per_km',
        'delete_status',
        'pickuptype',
        'type',
    ];

    protected $hidden = [
        'delete_status'
    ];


     /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
