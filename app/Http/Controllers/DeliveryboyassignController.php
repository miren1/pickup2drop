<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Areacode;
use App\Models\User;
use App\Models\DeliveryArea;
Use Alert;
class DeliveryboyassignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return view('deliveryboy_assign/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['areacode'] = Areacode::get();
        $data['deliveryboy'] = User::where('type','Deliveryboy')->get();
         return view('deliveryboy_assign/add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());

        $ArrData = array(
        'area_id'=>$request->area_id,
        'deliveryboy_id'=>$request->deliveryboy_id,
        );

        if ($request->id) {
            $where = array('id'=>$request->id);
            $isupdate = DeliveryArea::where($where)->update($ArrData);
            Alert::success('Success !', 'delivery Agent assign Update Successfully.');
        }else{
             $isInsert = DeliveryArea::create($ArrData);

             Alert::success('Success !', 'delivery Agent assign Created Successfully.');

           return redirect('deliveryboy_assign');

        }
          return redirect('deliveryboy_assign');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Getall()
    {
      /* $data= DeliveryArea::with('Areacode','Boy')->get();
       dd($data);*/
      return datatables()->of(DeliveryArea::with('Areacode','Boy'))->toJson();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['deliveryboy_assign'] = DeliveryArea::find(base64_decode($id));
        $data['areacode'] = Areacode::get();
        $data['deliveryboy'] = User::where('type','Deliveryboy')->get();
        return view('deliveryboy_assign/add',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = DeliveryArea::destroy($id);
        if ($deleted) {
            $status = true;
        }else{
            $status = false;
        }
        return response()->json($status);
    }
}
