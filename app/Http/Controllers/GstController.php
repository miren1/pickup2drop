<?php

namespace App\Http\Controllers;

use App\Models\Pickup;
use Illuminate\Http\Request;
use Alert;

class GstController extends Controller
{
    public function index(Request $request){
        if (!auth()->user()->can('GST-Report-List')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        $pickup = Pickup::with(['customer', 'boy', 'dispute'])->where('status', 'Delivered')->orderBy('id', 'DESC');

        if($request->daterangepicker){
            $splitdate = explode(" - ",$request->daterangepicker);
            $pickup = $pickup->whereBetween('created_at',[date("Y-m-d 00:00:00",strtotime(str_replace("/",'-',$splitdate[0]))),date("Y-m-d 23:59:59",strtotime(str_replace("/","-",$splitdate[1])))]);
        }else{
            $pickup = $pickup->whereBetween('created_at',[date("Y-m-01 00:00:00"),date("Y-m-d 23:59:59")]);
        }
        $pickup = $pickup->get();
        return view('gst.index',['pickup'=>$pickup]);
    }

    public function getAll(Request $request){
        if (!auth()->user()->can('GST-Report-List')) {
            return response()->json('Error','Unauthorized access');
        }
        $pickup = Pickup::with(['customer', 'boy', 'dispute'])->where('status', 'Delivered')->orderBy('id', 'DESC');

        return datatables()->of($pickup)
            ->addColumn('mobile_number',function($data){
                return $data->customer && $data->customer->mobile_number ? $data->customer->mobile_number : '-';
            })->toJson();
    }

    public function exportCsv(Request $request)
    {
        try {
            if (!auth()->user()->can('GST-Report-List')) {
                Alert::error('Error !', 'Unauthorized access.');
                return redirect()->route('home');
            }
            $fileName = time() . '.csv';
            $tasks = Pickup::with(['customer', 'boy', 'dispute'])->where('status', 'Delivered')->orderBy('id', 'DESC');

            if ($request->date) {
                $splitdate = explode(" - ", $request->date);
                $tasks = $tasks->whereBetween('created_at', [date("Y-m-d 00:00:00", strtotime(str_replace("/", '-', $splitdate[0]))), date("Y-m-d 23:59:59", strtotime(str_replace("/", "-", $splitdate[1])))]);
            }
            $tasks = $tasks->get();

            $headers = array(
                "Content-type" => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma" => "no-cache",
                "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                "Expires" => "0"
            );

            $columns = array('Order ID', 'Mobile Number', 'Customer Name', 'Item Name', 'Payment Type', 'Price', 'CGST', 'SGST', 'Date/Time');

            $callback = function () use ($tasks, $columns) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);
                $totalcgst = 0;
                $totalsgst = 0;

                foreach ($tasks as $task) {
                    $row['Order ID'] = $task->id;
                    $row['Mobile Number'] = ($task->customer && $task->customer->mobile_number) ? $task->customer->mobile_number : "";
                    $row['Customer Name'] = ($task->customer && $task->customer->name) ? $task->customer->name : "";
                    $row['Item Name'] = $task->item_name;
                    $row['Payment Type'] = $task->payment_type;
                    $row['Price'] = $task->original_amount;
                    $row['CGST'] = $task->cgst;
                    $row['SGST'] = $task->sgst;
                    $row['Date/Time'] = date("d-m-Y H:i:s", strtotime($task->created_at));

                    $totalcgst += $task->cgst;
                    $totalsgst += $task->sgst;

                    fputcsv($file, array($row['Order ID'], $row['Mobile Number'], $row['Customer Name'], $row['Item Name'], $row['Payment Type'], $row['Price'], $row['CGST'], $row['SGST'], $row['Date/Time']));
                }
                $total = ["","","","","",""];
                $total['total CGST'] = "Total CGST : ₹ ".$totalcgst;
                $total['total SGST'] = "Total SGST : ₹ ".$totalsgst;
                fputcsv($file, $total);
                fclose($file);
            };

            return response()->stream($callback, 200, $headers);
        }catch (\Exception $e){
            dd($e->getMessage());
        }
    }
}
