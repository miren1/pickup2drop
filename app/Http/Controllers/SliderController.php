<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Slider;
use Intervention\Image\Facades\Image;
use Alert;

class SliderController extends Controller
{
    public function index()
    {
        if (!auth()->user()->can('Slider-List')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        return view('slider/index');
    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('Slider-Add')) {
            $data['message'] = "Aunthorized access.";
            $data['title'] = "Error!";
            $data['icon'] = "error";
            $data['status'] = "aunthorized_access";
            return response()->json($data);
        }
        $validator = Validator::make($request->all(), [
            'title'=> 'required',
            'description'=> 'required',
            'file' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ],[
            'file.mimes' => 'Please upload only .jpeg, .jpg and png!',
        ]);    
        if ($validator->fails()) {
            $data['message'] = "Slider Not Add Try Again..";
            $data['title'] = "Error !";
            $data['icon'] = "error";
        }        
        $imageName = time().'_'.$request->file->getClientOriginalName(); 
        // $imageName = time().'.'.$request->file->getClientOriginalName(); 
        $add = new Slider;
        $add->title = $request->title;
        $add->description = $request->description;
        $add->image_url = $request->image_url;
        $add->file = $imageName;
        
        
        $img = Image::make(request()->file('file'));

        $img->resize(1600, 900, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });


        $img->save('uploads'.'/'. $imageName);


        $imgPath = url('uploads').'/'.$imageName;
        $add->image_path = $imgPath; 
        $its_save = $add->save(); 
        if($its_save){
            $data['message'] = "Slider Added Successfully.";
            $data['title'] = "Success !";
            $data['icon'] = "success";
            $data['status'] = "success";
        }else{
            $data['message'] = "Slider Not Add Try Again..";
            $data['title'] = "Error !";
            $data['icon'] = "error";
            $data['status'] = "error";
        }
        return response()->json($data);
    }

    public function show(Request $request)
    {
        if (!auth()->user()->can('Slider-List')) {
            return response()->json('Unauthorized access.');
        }
        if ($request->ajax()) {
            
            $data = Slider::all();
            return \DataTables::of($data)
                    ->editColumn('created_at', function($row) {   
                           $date = date('d-m-Y',strtotime($row->created_at));
                           return $date;
                    })
                    ->make(true);
        }
    }

    public function update(Request $request)
    {
        if (!auth()->user()->can('Slider-Edit')) {
            $data['message'] = "Unauthorized access";
            $data['title'] = "Error !";
            $data['icon'] = "error";
            $data['status'] = "aunthorized_access";
            return response()->json($data);
        }

        $id = $request->id;
        $update = Slider::find($id);
        $update->title = $request->title;
        $update->description = $request->description;
        $update->image_url = $request->image_url;
        $update->slider_type = $request->slider_type;
        if($request->file){
            $imageName = time().'.'.$request->file->getClientOriginalName();
            $img = Image::make(request()->file('file'));

            $img->resize(1600, 900, function ($constraint) {
                $constraint->aspectRatio();
            });


            $img->save('uploads' . '/' . $imageName);

            $imgPath = url('uploads') . '/' . $imageName;
            $update->image_path = $imgPath; 

            // $request->file->move(public_path('uploads'), $imageName);
            $update->file = $imageName;
        }else{
            $update->file = $update->file;
        }
        $its_save = $update->save();
        if($its_save){
            $data['message'] = "Slider Updated Successfully.";
            $data['title'] = "Success !";
            $data['icon'] = "success";
            $data['status'] = "success";
        }else{
            $data['message'] = "Slider Not Updated Try Again..";
            $data['title'] = "Error !";
            $data['icon'] = "error";
            $data['status'] = "error";
        }
        return response()->json($data);

    }    

    public function destroy(Request $request)
    {
        if (!auth()->user()->can('Slider-Delete')) {
            $data['message'] = "Aunthorized access.";
            $data['title'] = "Error!";
            $data['icon'] = "error";
            $data['status'] = "aunthorized_access";
            return response()->json($data);
        }

        $slider =  Slider::find($request->slider_id);
        $is_delete = $slider->delete();
        if($is_delete)
        {
            $data['message'] = "Slider Deleted Successfully.";
            $data['title'] = "Success!";
            $data['icon'] = "success";
            $data['status'] = "success";
        }
        else
        {
            $data['message'] = " Not Deleted";
            $data['title'] = "Error !";
            $data['icon'] = "error";
            $data['status'] = "error";
        }

        return response()->json($data);
    }

    

}
