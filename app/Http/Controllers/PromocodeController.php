<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Promocode;
use Alert;

class PromocodeController extends Controller
{
    public function index()
    {
        if (!auth()->user()->can('Promocode-List')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
    	return view('promocode.promocodes');
    }

    public function store(Request $request)
    {

        if (!auth()->user()->can('Promocode-Add')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        if ($request->code_type==="percentage" && $request->perc_price>100) {
            $data['status'] = "more";
            return response()->json($data);
        }
        // dd($request->all());
    	$exist = Promocode::where(['promocode'=>$request->promocode,'order_type'=>$request->order_type,'is_delete'=>0])->get();
    	if(!$exist->isEmpty())
    	{
    		$data['status'] = "exist";
    		return response()->json($data);
    	}
        $promocode = Promocode::create($request->all());

        if($promocode)
        {
            $data['message'] = "Promocode Added Successfully.";
            $data['title'] = "Success !";
            $data['icon'] = "success";
        }
        else
        {
            $data['message'] = "Promocode Not Add Try Again..";
            $data['title'] = "Error !";
            $data['icon'] = "error";
        }

        return response()->json($data);
    }

    public function show(Request $request)
    {
        if (!auth()->user()->can('Promocode-List')) {
            return response()->json('Unauthorized access');
        }
    	if ($request->ajax()) {
           $data = Promocode::where('is_delete',0)->get();
            return \DataTables::of($data)
                    ->editColumn('created_at', function($row) {
                           return date('d-m-Y',strtotime($row->created_at));
                    })
                    ->editColumn('enddate', function($row){
                        return date('d-m-Y',strtotime($row->enddate));
                    })
                    ->make(true);
        }
    }

    public function update(Request $request)
    {
        if (!auth()->user()->can('Promocode-Edit')) {
            $data['message'] = "Unauthorized access.";
            $data['title'] = "error !";
            $data['icon'] = "error";
            $data['status'] = "unauthorized_access";
            return response()->json($data);
        }

    	$exist = Promocode::where(['promocode'=>$request->promocode,'order_type'=>$request->order_type,'is_delete'=>0])->whereNotIn('id',[$request->id])->get();

    	if(!$exist->isEmpty())
    	{
    		$data['status'] = "exist";
    		return response()->json($data);
    	}
    	$where = $request->except('id','_token');
        $promocode = Promocode::where('id',$request->id)->update($where);

        if($promocode)
        {
            $data['message'] = "Promocode Updated Successfully.";
            $data['title'] = "Success !";
            $data['icon'] = "success";
        }
        else
        {
            $data['message'] = "Promocode Not Update Try Again..";
            $data['title'] = "Error !";
            $data['icon'] = "error";
        }

        return response()->json($data);
    }

    public function destroy(Request $request)
    {
        if (!auth()->user()->can('Promocode-Delete')) {
            $data['status'] = "unauthorized_access";
            $data['message'] = "Unauthorized access";
            return response()->json($data);
        }
        $promocode = Promocode::where('id',$request->promo_id)
        		->update(['is_delete'=>1]);
        if($promocode)
        {
            $data['status'] = "success";
            $data['message'] = "Promocode Deleted.";
        }
        else
        {
            $data['status'] = "error";
            $data['message'] = "Promocode Not Deleted";
        }

        return response()->json($data);
    }
}
