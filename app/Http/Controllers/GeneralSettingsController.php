<?php

namespace App\Http\Controllers;

use App\Models\GeneralSetting;
use Illuminate\Http\Request;
use Alert;

class GeneralSettingsController extends Controller
{
    public function index(){
        if (!auth()->user()->canany(['GST-Setting', 'Radius-Setting'])) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        return view('general-settings.index');
    }

    public function getdata(Request $request){
        if (!auth()->user()->canany(['GST-Setting', 'Radius-Setting'])) {
            return response()->json('Error','Unauthorized access');
        }
        if($request->ajax()){
            $generalSettings = GeneralSetting::all();
            if(count($generalSettings) > 0  )
                return response()->json(['data'=>$generalSettings],200);

            return response()->json(['message'=>'No Data Found.'],302);
        }
        return redirect()->back();
    }

    public function storedata(Request $request){
        try{
            if (!auth()->user()->can('GST-Setting')) {
                return response()->json(['status'=>"error",'message' => "Unauthorized access"]);
            }

            $request->validate([
                'sgst'=>"required",
                'cgst'=>"required"
            ]);

            $sgst = GeneralSetting::updateOrCreate(
                ["name" => 'sgst'],
                ['value'=> $request->sgst]
            );

            $cgst = GeneralSetting::updateOrCreate(
                ["name" => 'cgst'],
                ['value'=> $request->cgst]
            );

            if($cgst && $sgst)
                return response()->json(["icon" => "success", "status" => "success", 'message'=>"GST updated successfully"]);

            return response()->json(["icon" => 'error', 'status' => "error", 'message'=>"something went wrong"],320);
        }catch(\Exception $e){
            return response()->json(["icon" => 'error', 'status' => "error", "message"=>$e->getMessage()],302);
        }
    }

    public function storearearadius(Request $request){
        try{
          if (!auth()->user()->can('Radius-Setting')) {
                return response()->json(["icon"=>'error','status' => "error", 'message' => "Unauthorized access"]);
            }

            $request->validate([
                'covered_area'=>"required",
            ]);

            $area = GeneralSetting::updateOrCreate(
                ["name" => 'Covered Area'],
                ['value' => $request->covered_area]
            );
            if ($area)
                return response()->json(["icon"=>"success", "status" => "success",'message' => "Covered area radius updated successfully"]);

            return response()->json(["icon" => "error", "status" => 320, 'message' => "something went wrong"]);
        }catch(\Exception $e){
            return response()->json(["icon" => 'error', 'status' => "error", "message"=>$e->getMessage()],302);
        }
    }

    public function AppSetting(Request $request)
    {
        try{
                $area = GeneralSetting::updateOrCreate(
                    ["name" => 'customer_maintance_mode'],
                    ['value' => $request->customer_maintance_mode]
                );
                $area = GeneralSetting::updateOrCreate(
                    ["name" => 'da_maintance_mode'],
                    ['value' => $request->da_maintance_mode]
                );
                return response()->json(["icon" => "success", "status" => "success", 'message' => "App settings updated successfully"]);
        }catch(\Throwable $th){
                return response()->json(["icon" => 'error', 'status' => "error", "message" => $th->getMessage()], 302);
        }
    }
}
