<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\Price;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
Use Alert;

class PriceController extends Controller
{
    public function index()
    {
        if (!auth()->user()->can('Price-List')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }

        return view('price/index');
    }


    public function create()
    {
        if (!auth()->user()->can('Price-Add')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }

        return view('price/add');
    }

    public function show(Request $request)
    {
        if (!auth()->user()->can('Price-List')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        /*if ($request->ajax()) {
            $data = Price::where('delete_status','0')->get();
            // return $data;
            return Datatables::of($data)
                    ->editColumn('created_at', function($row) {   
                        $date = date('d-m-Y',strtotime($row->created_at));
                        return $date;
                    })
                    ->make(true);
        }*/
        return datatables()->of(Price::where('delete_status','0')->orderBy('id', 'DESC'))->toJson();
    }

    
    public function store(Request $request)
    {
        if (!auth()->user()->canany(['Price-Add','Price-Edit'])) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        if($request->type != null)
        {
            $arrData['type'] = $request->type;
        } 
        else{
            $arrData['type'] = 'normal';
        }
        if($request->min_km != null)
        {
            $arrData['min_km'] = $request->min_km;
        }else{
             $arrData['min_km'] = 0;
        }
        
        if($request->max_km != null)
        {
            $arrData['max_km'] = $request->max_km;
        }else{
            $arrData['max_km'] = 0;
        }

        // $arrData = array(
        //  'price'=>$request->price,
        //  'pickuptype'=>$request->pickuptype,
        //  'status_per_km'=>$request->status_per_km,   
        // );

        $arrData['price'] = $request->price;
        $arrData['pickuptype'] = $request->pickuptype;
        $arrData['status_per_km'] = $request->status_per_km;
        if ($request->id) {
              // dd($arrData); 
            $where = array('id'=>$request->id);
            $isupdate = Price::where($where)->update($arrData);
            Alert::success('Success !','Price Updated Successfully.');
        }else{
                
           $isInsert = Price::create($arrData);
           Alert::success('Success !','Price Created Successfully.');
           return redirect('price');
        }
        return redirect('price');
    }


    public function edit($id)
    {
        if (!auth()->user()->can('Price-Edit')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }

       $data['price'] = Price::find(base64_decode($id));
       return view('price/add',$data);
    }

    public function destroy(Request $request)
    {
        if (!auth()->user()->can('Price-Destroy')) {
            $data['title'] = 'Error!';
            $data['icon'] = 'error';
            $data['message'] = 'Unauthorized access.';
            return response()->json($data);
        }
        $price_data = Price::where('id', $request->id)->first();

        $max = DB::select('select id,max_km from prices where max_km = (SELECT MAX(`max_km`) FROM prices where pickuptype = "' . $price_data->pickuptype . '" and type =  "' . $price_data->type .'" and delete_status = 0) and pickuptype = "' . $price_data->pickuptype .'" and type =  "' . $price_data->type . '" and delete_status = 0');

        if($request->id==$max[0]->id)
        {
            $price = Price::where('id',$request->id)->update(['delete_status'=>1]);
            if($price)
            {
                $data['title'] = 'Success!';
                $data['icon'] = 'success';
                $data['message'] = 'Price Deleted Successfully.';
            }
            else
            {
                $data['title'] = 'Error !';
                $data['icon'] = 'error';
                $data['message'] = 'Something Wrong try Again..';
            }
        }
        else {
            $data['title'] = 'Error!';
            $data['icon'] = 'error';
            $data['message'] = 'Please first delete maximum km.';
        }
        
        return response()->json($data);
    }
}
