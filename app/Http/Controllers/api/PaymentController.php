<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\api\Base\BaseController;
use App\Models\PaymentFund;
use App\Helpers\DeliveryboyMail;
use App\Models\User;
use App\Models\AutoTransactionHistory;
use App\Models\DeliveryboyWalletHistory;
use App\Models\TDSCertificate;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class PaymentController extends BaseController
{
    public function checkBankDetail(Request $request)
    {
        // dd($request->user()->payment_contact_id);
        $url  = 'https://api.razorpay.com/v1/fund_accounts';
        $data = '{
                  "contact_id": "'.$request->user()->payment_contact_id.'",
                  "account_type": "bank_account",
                  "bank_account": {
                    "name": "'.$request->bank_account_name.'",
                    "ifsc": "'.$request->bank_account_ifsc.'",
                    "account_number": "'.$request->bank_account_number.'"
                  }
                }';

        $curlData = DeliveryboyMail::curl_post($url,$data);
        if($curlData && $curlData['response']){

			if($curlData['response'] && isset($curlData['response']->error)){
				return $this->ErrorResponse(__($curlData['response']->error->description));
				exit;
			}
            $userUpdateFundId = User::where('payment_contact_id',$request->user()->payment_contact_id)->update(['fund_account_id'=>$curlData['response']->id]);

            $updateData = new PaymentFund;
            $updateData->user_id = $request->user()->id;
            $updateData->fund_id = $curlData['response']->id;
            $updateData->contact_id = $curlData['response']->contact_id;
            $updateData->bank_account_name = $curlData['response']->bank_account->name;
            $updateData->bank_account_ifsc =  $curlData['response']->bank_account->ifsc;
            $updateData->bank_name = $curlData['response']->bank_account->bank_name;
            $updateData->bank_account_number = $curlData['response']->bank_account->account_number;
            $its_save = $updateData->save();
            if($its_save){
                $url = 'https://api.razorpay.com/v1/fund_accounts/validations';
                $data = '{
                          "account_number": "'.env('RAZORPAY_X_ACCOUNT_NUMBER_VIRTUAL').'",
                          "fund_account": {
                            "id": "'.$updateData->fund_id.'"
                          },
                          "amount": 100,
                          "currency": "INR",
                          "notes": {
                            "random_key_1": "Make it so.",
                            "random_key_2": "Tea. Earl Grey. Hot."
                          }
                        }';
                $curlData_1 = DeliveryboyMail::curl_post($url,$data);
                if($curlData_1 && $curlData_1['response']){

                    $userUpdateBank = User::where('fund_account_id',$updateData->fund_id)->update(['bank_account_name'=>$curlData_1['response']->fund_account->details->name,'bank_account_ifsc'=>$curlData_1['response']->fund_account->details->ifsc,'bank_account_number'=>$curlData_1['response']->fund_account->details->account_number,'bank_name'=>$curlData_1['response']->fund_account->details->bank_name]);
                    // dd($userUpdateBank);
                    $update = PaymentFund::where('fund_id',$updateData->fund_id)->update(['fund_validation_status'=>'Created','amount'=>1,
                                'currency'=>'INR','account_status'=>$curlData_1['response']->results->account_status]);
                    $url =  'https://api.razorpay.com/v1/payouts';
                    $data = '{
                              "account_number": "'.env('RAZORPAY_X_ACCOUNT_NUMBER').'",
                              "fund_account_id": "'.$updateData->fund_id.'",
                              "amount": 100,
                              "currency": "INR",
                              "mode": "IMPS",
                              "purpose": "refund",
                              "reference_id": "Acme Transaction ID 12345",
                              "narration": "Acme Corp Fund Transfer",
                              "notes": {
                                "notes_key_1":"Tea, Earl Grey, Hot",
                                "notes_key_2":"Tea, Earl Grey… decaf."
                              }
                            }';
                    $curlData_2 = DeliveryboyMail::curl_post($url,$data);
                    if($curlData_2 && $curlData_2['response']){
                        $Dataupdate = PaymentFund::where('fund_id',$curlData_2['response']->fund_account_id)->update(['payout_id'=>$curlData_2['response']->id,'payout_status'=>$curlData_2['response']->status,'failure_reason'=>$curlData_2['response']->failure_reason]);
                        $userUpdate = User::where('id',$request->user()->id)->update(['bank_details_status'=>'Pending','fund_account_id'=>$curlData_2['response']->fund_account_id]);
                        return response()->json(['error' => 'false','message'=>'Account Added Successfully!','data'=> ''], 200);
                    }
                }
            }
        }else{
            return $this->ErrorResponse(__('Try Again...'));
        }
    }

    public function weeklyTransaction(Request $request)
    {
        $data = User::where('type','Deliveryboy')
                    ->where('is_delete','false')
                    ->where('bank_details_status','Verified')->get();
        $transaction = [];
        foreach($data as $value)
        {
            if($value->wallet > 0)
            {
                $tds = ($value->wallet * 0.05);
                $amount = (float)($value->wallet - $tds);
                $url =  'https://api.razorpay.com/v1/payouts';
                $data = '{
                          "account_number": "'.env('RAZORPAY_X_ACCOUNT_NUMBER').'",
                          "fund_account_id": "'.$value->fund_account_id.'",
                          "amount": '.($amount * 100). ',
                          "currency": "INR",
                          "mode": "IMPS",
                          "purpose": "salary",
                          "reference_id": "'.time().'",
                            "narration": "Smart Merchant Service Ltd",
                            "notes": {
                                "notes_key_1":"Tea, Earl Grey, Hot",
                                "notes_key_2":"Tea, Earl Grey… decaf."
                            }
                        }';

                $curlData_2 = DeliveryboyMail::curl_post($url,$data);
                if($curlData_2){
                    $dataUpdate = User::where('fund_account_id',$value->fund_account_id)->update(['wallet'=>0]);
                    $arrayData = ['user_id'=>$value->id,'transaction_id'=> $curlData_2['response']->id,'fund_account_id'=>$curlData_2['response']->fund_account_id,'amount'=>($curlData_2['response']->amount/100),'status'=>'Pending','tds'=>$tds];
                    $transaction[] = AutoTransactionHistory::create($arrayData);
                }
            }
        }
        return response()->json(['error'=>false,'statusCode'=>200,'data'=>$transaction]);
    }

    public function transactionHistory(Request $request)
    {
        $data = AutoTransactionHistory::where('user_id',$request->user()->id)->get();
        if($data){
            return response()->json(['error' => 'false','message'=>'weekly transaction get successfully!','data'=> $data], 200);
        }else{
            return $this->ErrorResponse(__('Record Not Found!'));
        }

    }

    public function deliveryboyWalletHistory(Request $request)
    {
        $data = DeliveryboyWalletHistory::with(['Orders','Pickupaddress'])->where('user_id',$request->user()->id)->orderby('id','DESC')->get();
        if($data){
            return response()->json(['error' => 'false','message'=>'Deliveryboy wallet transaction get successfully!','data'=> $data], 200);
        }else{
            return $this->ErrorResponse(__('Record Not Found!'));
        }
    }

    public function deliveryboypayoutHistory(Request $request)
    {
        $transactionHistory = AutoTransactionHistory::with('customers')->where('id',$request->transaction_id)->first();
        $data['bank_account_name'] = $transactionHistory->customers->bank_account_name;
        $data['bank_account_number'] = $transactionHistory->customers->bank_account_number;
        $data['bank_account_ifsc'] = $transactionHistory->customers->bank_account_ifsc;
        $data['timeperiod'] =  Carbon::parse($transactionHistory->created_at)->subDay(7)->format('d-m-Y') .' to '. $transactionHistory->created_at->format('d-m-Y');
        $data['total_payout'] = $transactionHistory->amount + $transactionHistory->tds;
        $data['amount'] = $transactionHistory->amount;
        $data['tds'] = $transactionHistory->tds;
        $data['transfer_on'] = Carbon::parse($transactionHistory->created_at)->format('d M Y h:s A');

        if ($data) {
            return response()->json(['error' => 'false', 'message' => 'Deliveryboy Payout details get successfully!', 'data' => $data], 200);
        } else {
            return $this->ErrorResponse(__('Record Not Found!'));
        }
    }
    public function TdsCertificateDownload(){
        $data = TDSCertificate::where('delivery_agent_id', auth()->user()->id)->get();
        if(count($data) > 0)
            return response()->json(['error' => 'false', 'message' => 'TDS Certificate get successfully!', 'data' => $data], 200);
        else
            return $this->ErrorResponse(__('Record Not Found!'));

    }


}
