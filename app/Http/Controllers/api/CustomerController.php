<?php

namespace App\Http\Controllers\api;

use App\Helpers\PushNotification;
use App\Models\GeneralSetting;
use Illuminate\Support\Facades\Log;
use Notification;
use App\Models\Area;
use App\Models\User;
use Razorpay\Api\Api;
use App\Models\Image;
use App\Models\Price;
use App\Models\Pickup;
use App\Models\Address;
use App\Models\Promocode;
use App\Models\Delivery_fcm_token;
use Illuminate\Http\File;
use App\Models\DeliveryArea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\PickupRequest;
use App\Http\Requests\PickupIdRequest;
use App\Models\WalletHistory;
use App\Http\Controllers\API\Base\BaseController;
use App\Models\Commission;
use Carbon\Carbon;

class CustomerController extends BaseController
{
    public function pickRequest(PickupRequest $request)
    {
			//return $this->ErrorResponse(__('We apologize for the inconvenience Your application is outdated. We suggest updating your app so you can enjoy a better user experience.'));
        $pick_address_check = Address::where('user_id', $request->user_id)->where('address', $request->pickaddress[0]['address'])->where('is_delete',0)->count();
        $drop_address_check = Address::where('user_id', $request->user_id)->where('address', $request->dropaddress[0]['address'])->where('is_delete',0)->count();

        $pickaddress = $request->pickaddress[0];
        $pickaddress['user_id'] = $request->user_id;
        $dropaddress = $request->dropaddress[0];
        $dropaddress['user_id'] = $request->user_id;
        $amount = ($request->amount) * 100;
        if ($request->payment_type == "cash") {
            $request['payment_status'] = "complete";
        } else if ($request->payment_type == "online") {
            $request['payment_status'] = "complete";
            $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
            $payment = $api->payment->fetch($request->payment_id)->capture(array('amount' => $amount, 'currency' => 'INR'));
        }
        $Input = $request->except('pickaddress', 'dropaddress','promocodedata');
        //        if ($pick_address_check && !is_null($pickaddress['id']) && $pickaddress['id'] != 'null') {
        if ($pick_address_check > 0) {
            $pick_up = $request->pickaddress[0]['address'];
            $query = Address::where('user_id', $request->user_id)->where('address', $pick_up)->where('is_delete', 0)->first();
            Address::where('id', $query->id)->update(['name' => $request->pickaddress[0]['name'],'phone' => $request->pickaddress[0]['phone']]);
            $old_pickup_id = $query->id;
            //$update_pickup = Address::where('id', $old_pickup_id)->update($pickaddress);
            $Input['pickaddress_id'] = $old_pickup_id;
        } else {
            $Input['pickaddress_id'] = Address::create($pickaddress)->id;
        }

        //       if ($drop_address_check && !is_null($dropaddress['id']) && $dropaddress['id'] != 'null') {
        if ($drop_address_check) {
            $drop_up = $request->dropaddress[0]['address'];
            $query = Address::where('user_id', $request->user_id)->where('address', $drop_up)->where('is_delete', 0)->first();
            Address::where('id', $query->id)->update(['name' => $request->dropaddress[0]['name'],'phone' => $request->dropaddress[0]['phone']]);
            $old_dropup_id = $query->id;
            //$update_dropup = Address::where('id', $old_dropup_id)->update($dropaddress);
            $Input['dropaddress_id'] = $old_dropup_id;
        } else {
            $Input['dropaddress_id'] = Address::create($dropaddress)->id;
        }

        $Input['pincode'] = $request->pickaddress[0]['pincode'];

        if (isset($Input['start_date']) && $Input['start_date'] != '') {
            $Input['start_date'] = date('Y-m-d', strtotime($Input['start_date']));
            $Input['end_date'] = date('Y-m-d', strtotime($Input['end_date']));
        }
        //GST Calculation
        $commission_amt = 0;
        $cgst = GeneralSetting::where('name', 'cgst')->first();
        $sgst = GeneralSetting::where('name', 'sgst')->first();
        $gst = 0;
        if ($cgst && $sgst) {
            $gst = number_format((($Input['amount'] * ($cgst->value + $sgst->value)) / 100), 2);
        }
        /*$sgst = GeneralSetting::where('name', 'sgst')->first();
        if ($sgst) {
            $Input['sgst'] = number_format($Input['original_amount'] - ($Input['original_amount'] * (100/($sgst->value + 100))), 2);
        }*/
        if ($gst != 0) {
            //$Input['sgst'] = number_format($gst / 2, 2);
            //$Input['cgst'] = number_format($gst / 2, 2);

			$Input['sgst'] = number_format($request->sgst, 2);
            $Input['cgst'] = number_format($request->cgst, 2);

            $commission_amt = number_format(($Input['original_amount'] - $Input['cgst'] - $Input['sgst']), 2);
        }

        $percenatge_commisssion = Commission::first();

        if (isset($Input['promocode']) && $Input['promocode'] != "") {
            /*$promocode = Promocode::where("promocode",$Input['promocode'])->where('is_delete', '0')->first();
            if($promocode){
                if($promocode->code_type == 'percentage'){
                    $Input['promo_amount'] = number_format(($Input['original_amount'] * ($promocode->perc_price / 100)),2);
                }else{
                    $Input['promo_amount'] = number_format($promocode->perc_price,2);
                }*/
            $Input['promo_amount'] = $request->promocodedata['amount'];
            //$commission_amt = number_format(($Input['original_amount'] + $Input['promo_amount']), 2);
            //$Input['original_amount'] = $Input['original_amount'] + $Input['promo_amount'];
            $commission_amt += $Input['promo_amount'];
            //}
        }

        $Input['order_commission'] = number_format(((float)$commission_amt - ((float)$commission_amt * (float)$percenatge_commisssion->commission) / 100), 2);

        $pickrequest = Pickup::create($Input);

        if ($pickrequest) {
            $pickup = Pickup::with('Pickupaddress.area')->find($pickrequest->id);
            $deliverys = DeliveryArea::with('dBoy')->where('area_id', $pickup->Pickupaddress->area->id)->get();
            $allDeliveryAgent = User::where(['type' => 'Deliveryboy', 'is_delete' => 'false', 'delivery_agent_status' => '1'])->get();

            $allowedAgents = [];

            $radius = GeneralSetting::where('name', 'Covered Area')->first();
            if ($radius)
                $radius =  $radius->value;

            $radius = $radius ? (int)$radius : 10;

            foreach ($allDeliveryAgent as $deliveryAgent) {

                $distance = calculate_distance($deliveryAgent->latitude, $deliveryAgent->longitude, $request->pickaddress[0]['latiude'], $request->pickaddress[0]['longitude']);

                if ($distance >= 0 && $distance < $radius) {
                    $allowedAgents[] = $deliveryAgent;
                }
            }

            if (count($allowedAgents) == 0) {
                return $this->ErrorResponse(__('No delivery agent found in your area.'));
            }

            foreach ($allowedAgents as $agent) {

                $agntArr = collect($agent)->toArray();
                $usertoken = $agntArr['fcm_token'];
                $addtion = array(
                    'Type' => 'Find One Pickup Request',
                    'user' => $agntArr['name'],
                );
                $msg = array(
                    'body' => 'Pickup Request Occurs Check Fast',
                    'title' => 'Pickup Request',
                );

                PushNotification::Push($msg, $usertoken, $addtion);
            }

            if ($request->wallet_amount) {
                User::where('id', $request->user_id)->decrement('wallet', $request->wallet_amount);
                if ($request->wallet_amount > 0) {
                    if ($request->payment_type == "cash") {
                        $payment_id = 'pickup request debit';
                    } else {
                        $payment_id = $request->payment_id;
                    }
                    $add_wallet = [
                        'user_id' => $request->user_id,
                        'amount' => $request->wallet_amount,
                        'payment_id' => $payment_id,
                        'title' => 'Pickup Request Debit',
                        'status' => 'debit',
                    ];
                    $wallet_history = WalletHistory::forceCreate($add_wallet);
                }
            }
            if ($request->request_type == "urgent") {
                return $this->SuccessResponse($pickrequest, __('Emergency Pickup request send successfully!'));
            }

            return $this->SuccessResponse($pickrequest, __('Pickup request send successfully!'));
        } else {
            return $this->ErrorResponse(__('Pickup request Not send'));
        }
    }

    public function pickRequestv2(PickupRequest $request)
    {
        $pick_address_check = Address::where('user_id', $request->user_id)->where('address', $request->pickaddress[0]['address'])->where('is_delete', 0)->count();
        $drop_address_check = Address::where('user_id', $request->user_id)->where('address', $request->dropaddress[0]['address'])->where('is_delete', 0)->count();

        $pickaddress = $request->pickaddress[0];
        $pickaddress['user_id'] = $request->user_id;
        $dropaddress = $request->dropaddress[0];
        $dropaddress['user_id'] = $request->user_id;
        $amount = ($request->amount) * 100;
        if ($request->payment_type == "cash") {
            $request['payment_status'] = "complete";
        } else if ($request->payment_type == "online") {
            $request['payment_status'] = "complete";
            $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
            $payment = $api->payment->fetch($request->payment_id)->capture(array('amount' => $amount, 'currency' => 'INR'));
        }
        $Input = $request->except('pickaddress', 'dropaddress', 'promocodedata');

        //        if ($pick_address_check && !is_null($pickaddress['id']) && $pickaddress['id'] != 'null') {
        if ($pick_address_check > 0) {
            $pick_up = $request->pickaddress[0]['address'];
            $query = Address::where('user_id', $request->user_id)->where('address', $pick_up)->where('is_delete', 0)->first();
            Address::where('id', $query->id)->update(['name' => $request->pickaddress[0]['name'],'phone' => $request->pickaddress[0]['phone']]);
            $old_pickup_id = $query->id;
            //$update_pickup = Address::where('id', $old_pickup_id)->update($pickaddress);
            $Input['pickaddress_id'] = $old_pickup_id;
        } else {
            $Input['pickaddress_id'] = Address::create($pickaddress)->id;
        }

        //       if ($drop_address_check && !is_null($dropaddress['id']) && $dropaddress['id'] != 'null') {
        if ($drop_address_check) {
            $drop_up = $request->dropaddress[0]['address'];
            $query = Address::where('user_id', $request->user_id)->where('address', $drop_up)->where('is_delete', 0)->first();
            Address::where('id', $query->id)->update(['name' => $request->dropaddress[0]['name'],'phone' => $request->dropaddress[0]['phone']]);
            $old_dropup_id = $query->id;
            //$update_dropup = Address::where('id', $old_dropup_id)->update($dropaddress);
            $Input['dropaddress_id'] = $old_dropup_id;
        } else {
            $Input['dropaddress_id'] = Address::create($dropaddress)->id;
        }

        $Input['pincode'] = $request->pickaddress[0]['pincode'];

        if (isset($Input['start_date']) && $Input['start_date'] != '') {
            $Input['start_date'] = date('Y-m-d', strtotime($Input['start_date']));
            $Input['end_date'] = date('Y-m-d', strtotime($Input['end_date']));
        }
        //GST Calculation
        $commission_amt = 0;
        $cgst = GeneralSetting::where('name', 'cgst')->first();
        $sgst = GeneralSetting::where('name', 'sgst')->first();
        $gst = 0;
        if ($cgst && $sgst) {
            $gst = number_format((($Input['original_amount'] * ($cgst->value + $sgst->value)) / 100), 2);
        }
        /*$sgst = GeneralSetting::where('name', 'sgst')->first();
        if ($sgst) {
            $Input['sgst'] = number_format($Input['original_amount'] - ($Input['original_amount'] * (100/($sgst->value + 100))), 2);
        }*/
        if ($gst != 0) {
            //$Input['sgst'] = number_format($gst / 2, 2);
            //$Input['cgst'] = number_format($gst / 2, 2);
            $commission_amt = number_format(($Input['original_amount']), 2);
        }
        $percenatge_commisssion = Commission::first();

        if ($request->promocodedata && isset($Input['promocode']) && $Input['promocode'] != "") {
            /*$promocode = Promocode::where("promocode",$Input['promocode'])->where('is_delete', '0')->first();
            if($promocode){
                if($promocode->code_type == 'percentage'){
                    $Input['promo_amount'] = number_format(($Input['original_amount'] * ($promocode->perc_price / 100)),2);
                }else{
                    $Input['promo_amount'] = number_format($promocode->perc_price,2);
                }*/

            $Input['promo_amount'] = $request->promocodedata['amount'];
            $commission_amt = number_format(($Input['original_amount']), 2);
            //$Input['original_amount'] = $Input['original_amount'] + $Input['promo_amount'];
            //$commission_amt += $Input['promo_amount'];
            //}
        }else{
			$Input['promocode'] = null;
		}

        $Input['order_commission'] = number_format(((float)$commission_amt - ((float)$commission_amt * (float)$percenatge_commisssion->commission) / 100), 2);

		$Input['paid_amount'] = $Input['amount'] + $Input['wallet_amount'];
		$Input['v2'] = 'true';

        $pickrequest = Pickup::create($Input);

        if ($pickrequest) {
            $pickup = Pickup::with('Pickupaddress.area')->find($pickrequest->id);
            $deliverys = DeliveryArea::with('dBoy')->where('area_id', $pickup->Pickupaddress->area->id)->get();
            $allDeliveryAgent = User::where(['type' => 'Deliveryboy', 'is_delete' => 'false', 'delivery_agent_status' => '1'])->get();

            $allowedAgents = [];

            $radius = GeneralSetting::where('name', 'Covered Area')->first();
            if ($radius)
                $radius =  $radius->value;

            $radius = $radius ? (int)$radius : 10;

            foreach ($allDeliveryAgent as $deliveryAgent) {

                $distance = calculate_distance($deliveryAgent->latitude, $deliveryAgent->longitude, $request->pickaddress[0]['latiude'], $request->pickaddress[0]['longitude']);

                if ($distance >= 0 && $distance < $radius) {
                    $allowedAgents[] = $deliveryAgent;
                }
            }

            if (count($allowedAgents) == 0) {
                return $this->ErrorResponse(__('No delivery agent found in your area.'));
            }

            foreach ($allowedAgents as $agent) {

                $agntArr = collect($agent)->toArray();
                $usertoken = $agntArr['fcm_token'];
                $addtion = array(
                    'Type' => 'Find One Pickup Request',
                    'user' => $agntArr['name'],
                );
                $msg = array(
                    'body' => 'Pickup Request Occurs Check Fast',
                    'title' => 'Pickup Request',
                );

                PushNotification::Push($msg, $usertoken, $addtion);
            }

            if ($request->wallet_amount) {
                User::where('id', $request->user_id)->decrement('wallet', $request->wallet_amount);
                if ($request->wallet_amount > 0) {
                    if ($request->payment_type == "cash") {
                        $payment_id = 'pickup request debit';
                    } else {
                        $payment_id = $request->payment_id;
                    }
                    $add_wallet = [
                        'user_id' => $request->user_id,
                        'amount' => $request->wallet_amount,
                        'payment_id' => $payment_id,
                        'title' => 'Pickup Request Debit',
                        'status' => 'debit',
                    ];
                    $wallet_history = WalletHistory::forceCreate($add_wallet);
                }
            }
            if ($request->request_type == "urgent") {
                return $this->SuccessResponse($pickrequest, __('Emergency Pickup request send successfully!'));
            }

            return $this->SuccessResponse($pickrequest, __('Pickup request send successfully!'));
        } else {
            return $this->ErrorResponse(__('Pickup request Not send'));
        }
    }

    public function GetRequest(Request $request)
    {

        $pickrequest = Pickup::with('Pickupaddress', 'Dropaddress', 'boy')->where('user_id', $request->id)->orderby('id', 'desc')->get();
        if ($pickrequest) {
            return $this->SuccessResponse($pickrequest, __('Request List'));
        } else {
            return $this->ErrorResponse(__('Not Found'));
        }
    }

    public function pickuplist(Request $request)
    {
        $pickups = Pickup::with(['Pickupaddress', 'Dropaddress', 'boy', 'image' => function ($q) {
            $q->whereHas('pickup', function ($q2) {
                $q2->where('type', 'medicine');
            });
        }, 'dispute'])
            ->where('user_id', $request->user_id)
            ->where('type', '!=', 'tiffin')
            ->orderByRaw("status = 'Delivered' ASC")->orderBy('id', 'DESC')->get();

        return $this->SuccessResponse($pickups, __('Pickup List'));
    }

	public function getPrice(PickupRequest $request)
    {
		return $this->ErrorResponse(__("We apologize for the inconvenience Your application is outdated. We suggest updating your app so you can enjoy a better user experience"));

		$pickuppin = $request['pickaddress'][0]['pincode'];
        $droppin = $request['dropaddress'][0]['pincode'];
        $picarea = Area::where('pincode', $pickuppin)->get();
        $droparea = Area::where('pincode', $droppin)->get();
        $request['wallet'] = (int)User::where('id', $request->user_id)->first()->wallet;


        if (!$picarea->isEmpty() && !$droparea->isEmpty()) {
            $from = urlencode($request['pickaddress'][0]['address']);
            $to = urlencode($request['dropaddress'][0]['address']);

            $url = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins=' . $from . '&destinations=' . $to . '&key=AIzaSyDy7EA7d-f4T4hMXA0Y7y-7hrPx6S6YDo0';
            $distance = @file_get_contents($url);

            $distance = json_decode($distance, true);

            if ($distance["rows"][0]["elements"][0]["status"] == "OK") {
                $km = round(explode(" ", $distance["rows"][0]["elements"][0]["distance"]["text"])[0]);
                if ($request->request_type == "normal" || $request->request_type == "food" || $request->request_type == "medicine") {
                    $distance = Price::where(['type' => 'normal', 'delete_status' => 0, 'status_per_km' => 'True'])->where('pickuptype', '!=', 'subscription')->where('max_km', '>=', $km)->where('min_km', '<=', $km)->max('max_km');
                    if (is_null($distance)) {
                        $distance = Price::where(['type' => 'normal', 'delete_status' => 0, 'status_per_km' => 'False'])->where('pickuptype', '!=', 'subscription')->where('max_km', '>=', $km)->where('min_km', '<=', $km)->max('max_km');
                        if (is_null($distance)) {
                            $distance = Price::where(['type' => 'default', 'delete_status' => 0, 'status_per_km' => 'False'])->where('pickuptype', '!=', 'subscription')->max('max_km');
                        }
                    }
                } else {
                    $distance = Price::where(['type' => 'urgent', 'delete_status' => 0])->max('max_km');
                }
                if ($km <= $distance) {

                    $allDeliveryAgent = User::where(['type' => 'Deliveryboy', 'is_delete' => 'false', 'delivery_agent_status' => '1'])->get();
                    $allowedAgents = [];

                    // Set dynamic raidus for delivery agent
                    $radius = GeneralSetting::where('name', 'Covered Area')->first();
                    if ($radius)
                        $radius =  $radius->value;

                    $radius = $radius ? (int)$radius : 10;

                    foreach ($allDeliveryAgent as $deliveryAgent) {
                        $distance = calculate_distance($deliveryAgent->latitude, $deliveryAgent->longitude, $request['pickaddress'][0]['latiude'], $request['pickaddress'][0]['longitude']);
                        if ($distance >= 0 && $distance < $radius) {
                            $allowedAgents[] = $deliveryAgent;
                        }
                    }

                    if (count($allowedAgents) == 0) {
                        return $this->ErrorResponse(__('No delivery agent found in your area.'));
                    }

                    $defualt = false;

                    if ($request->request_type == "normal" || $request->request_type == "food" || $request->request_type == "medicine") {
                        $defualt = Price::where('type', 'default')->where('pickuptype', '!=', 'subscription')->where('delete_status', '0')->first();

                        $plan_price = Price::where('max_km', '>=', $km)
                            ->where('min_km', '<=', $km)
                            ->where(['type' => 'normal', 'delete_status' => 0])
                            ->where('pickuptype', '!=', 'subscription')
                            ->first();
                        $pending_km = abs($defualt->max_km - $km);

                        if ($plan_price) {
                            if ($plan_price->status_per_km == 'True') {
                                $total_price = ($pending_km * $plan_price->price) + $defualt->price;
                            } else {
                                $total_price = $plan_price->price;
                            }
                        } else {
                            $total_price = $defualt->price;
                        }
                        $data = $request->all();
                        $data['distance'] = $km;

                        if (!is_null($request->promocode) && $request->promocode != "") {
//Log::info($request->order_type);
							if(true){
								$uper_code = strtoupper($request->promocode);
								$promocode = Promocode::where("promocode", $request->promocode)->where('is_delete', '0')->first();

                            if ($promocode) {
								$enddate = $promocode->enddate;
								$today = date('Y-m-d');
								if ($enddate < $today) {
									return $this->ErrorResponse(__('Promocode Expired'));
								}
								if ($promocode && isset($promocode->min_order_amount) && ($promocode->min_order_amount >  $request->amount)) {
									return $this->ErrorResponse(__('Minimum order amount to apply this promocode is ' . $promocode->min_order_amount));
								}
								$order_type = $promocode->order_type;
								if ($order_type !== $request->order_type && $order_type !== "all") {
									return $this->ErrorResponse(__('Promocode Valid for ' . $order_type));
								}

								$allpickup = Pickup::where(['user_id' => $request->user_id, 'promocode' => $uper_code])->count();
								if ($allpickup >= $promocode->code_time) {
									return $this->ErrorResponse(__('you used Promocode limits'));
								}

								if ($promocode->one_day === "Yes") {
								$daypickup = Pickup::where(['user_id' => $request->user_id, 'promocode' => $uper_code])
									->whereDate('created_at', '=', date('Y-m-d'))->count();
									if ($daypickup > 0) {
										return $this->ErrorResponse(__('You Can Use this Code Once Per Day'));
									}
								}


                                $data['promocodedata'] = [];
                                if ($promocode->code_type == 'percentage') {
                                    $promo_amount = number_format(($total_price * ($promocode->perc_price / 100)), 2);
                                    $promo_amount = ($promo_amount >= $promocode->upto_amount) ? $promocode->upto_amount : $promo_amount;

                                    $total_price = number_format(($total_price - $promo_amount), 2);
                                    $data['promocodedata']['discount_per'] = $promocode['perc_price'] . "%";
                                    $data['promocodedata']['amount'] = $promo_amount;
                                } else {
                                    $promo_amount = number_format($promocode->perc_price, 2);
                                    if ($promo_amount >= $total_price) {
                                        $promo_amount = $total_price;
                                    }
                                    $total_price = number_format(($total_price - $promo_amount), 2);
                                    $data['promocodedata']['discount'] = $promocode['perc_price'] . " Rs";
                                    $data['promocodedata']['amount'] = $promo_amount;
                                }
							}
							}else{
							 $promocode = Promocode::where("promocode", $request->promocode)->where('is_delete', '0')->first();

                            if ($promocode) {
                                $data['promocodedata'] = [];
                                if ($promocode->code_type == 'percentage') {
                                    $promo_amount = number_format(($total_price * ($promocode->perc_price / 100)), 2);
                                    $promo_amount = ($promo_amount >= $promocode->upto_amount) ? $promocode->upto_amount : $promo_amount;

                                    $total_price = number_format(($total_price - $promo_amount), 2);
                                    $data['promocodedata']['discount_per'] = $promocode['perc_price'] . "%";
                                    $data['promocodedata']['amount'] = $promo_amount;
                                } else {
                                    $promo_amount = number_format($promocode->perc_price, 2);
                                    if ($promo_amount >= $total_price) {
                                        $promo_amount = $total_price;
                                    }
                                    $total_price = number_format(($total_price - $promo_amount), 2);
                                    $data['promocodedata']['discount'] = $promocode['perc_price'] . " Rs";
                                    $data['promocodedata']['amount'] = $promo_amount;
                                }
                              }

							}
                        }

                        //GST Calculation
                        $cgst = GeneralSetting::where('name', 'cgst')->first();
                        if ($cgst) {
                            $data['cgst'] = number_format(($total_price * ($cgst->value / 100)), 2);
                        }
                        $sgst = GeneralSetting::where('name', 'sgst')->first();
                        if ($sgst) {
                            $data['sgst'] = number_format(($total_price * ($sgst->value / 100)), 2);
                        }
                        if ($sgst || $cgst) {
                            $total_price = number_format(($total_price + $data['cgst'] + $data['sgst']), 2);
                        }

                        $data['amount'] = $total_price;
                        return $this->SuccessResponse($data, __('Amount And Distance'));
                    } else {
                        $defualt = Price::where(['type' => 'urgent', 'status_per_km' => 'False', 'delete_status' => 0])->first();
                        //$defualt = Price::where(['type' => 'urgent', 'delete_status' => 0])->first();
                        $plan_price = Price::where('max_km', '>=', $km)
                            ->where('min_km', '<=', $km)
                            ->where(['type' => 'urgent', 'status_per_km' => 'True', 'delete_status' => 0])
                            ->first();

                        $pending_km = $km;

                        if ($defualt)
                            $pending_km = ($km - $defualt->max_km);


                        if ($plan_price) {
                            $total_price = $pending_km * $plan_price->price;
                            if ($defualt) {
                                $total_price += $defualt->price;
                            }
                        } else {
                            $total_price = $defualt->price;
                        }
                        $data = $request->all();
                        $data['distance'] = $km;

                        if (!is_null($request->promocode) && $request->promocode != "") {
                            $promocode = Promocode::where("promocode", $request->promocode)->where('is_delete', '0')->first();
                            if ($promocode) {
                                $data['promocodedata'] = [];
                                if ($promocode->code_type == 'percentage') {
                                    $promo_amount = number_format(($total_price * ($promocode->perc_price / 100)), 2);
                                    $promo_amount = ($promo_amount >= $promocode->upto_amount) ? $promocode->upto_amount : $promo_amount;
                                    $total_price = number_format(($total_price - $promo_amount), 2);
                                    $data['promocodedata']['discount_per'] = $promocode['perc_price'] . "%";
                                    $data['promocodedata']['amount'] = $promo_amount;
                                } else {
                                    $promo_amount = number_format($promocode->perc_price, 2);
                                    if ($promo_amount >= $total_price) {
                                        $promo_amount = $total_price;
                                    }
                                    $total_price = number_format(($total_price - $promo_amount), 2);
                                    $data['promocodedata']['discount'] = $promocode['perc_price'] . " Rs";
                                    $data['promocodedata']['amount'] = $promo_amount;
                                }
                            }
                        }
                        //GST Calculation
                        $cgst = GeneralSetting::where('name', 'cgst')->first();
                        if ($cgst) {
                            $data['cgst'] = number_format(($total_price * ($cgst->value / 100)), 2);
                        }
                        $sgst = GeneralSetting::where('name', 'sgst')->first();
                        if ($sgst) {
                            $data['sgst'] = number_format(($total_price * ($sgst->value / 100)), 2);
                        }
                        if ($sgst || $cgst) {
                            $total_price = number_format(($total_price + $data['cgst'] + $data['sgst']), 2);
                        }
                        //                        $data['amount'] = round($total_price);
                        $data['amount'] = $total_price;
                        return $this->SuccessResponse($data, __('Amount And Distance'));
                    }
                } else {
                    if ($km >= $distance && $request->request_type == "urgent") {
                        return $this->ErrorResponse(__('Emergency Delivery Available Within ' . $distance . ' KM'));
                    }
                    return $this->ErrorResponse(__('Delivery Not avaliable in your location'));
                }
            } else {
                return $this->ErrorResponse(__('Enter Proper Address'));
            }
        } else {
            return $this->ErrorResponse(__('Not Avaliable in your location'));
        }
	}
    public function getPricev2(PickupRequest $request)
    {
		//return $this->ErrorResponse(__("We apologize for the inconvenience Your application is outdated. We suggest updating your app so you can enjoy a better user experience"));

        $pickuppin = $request['pickaddress'][0]['pincode'];
        $droppin = $request['dropaddress'][0]['pincode'];
        $picarea = Area::where('pincode', $pickuppin)->get();
        $droparea = Area::where('pincode', $droppin)->get();
        $request['wallet'] = (int)User::where('id', $request->user_id)->first()->wallet;


        if (!$picarea->isEmpty() && !$droparea->isEmpty()) {
            $from = urlencode($request['pickaddress'][0]['address']);
            $to = urlencode($request['dropaddress'][0]['address']);

            $url = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins=' . $from . '&destinations=' . $to . '&key=AIzaSyDy7EA7d-f4T4hMXA0Y7y-7hrPx6S6YDo0';
            $distance = @file_get_contents($url);

            $distance = json_decode($distance, true);

            if ($distance["rows"][0]["elements"][0]["status"] == "OK") {
                $km = round(explode(" ", $distance["rows"][0]["elements"][0]["distance"]["text"])[0]);
                if ($request->request_type == "normal" || $request->request_type == "food" || $request->request_type == "medicine") {
                    $distance = Price::where(['type' => 'normal', 'delete_status' => 0, 'status_per_km' => 'True'])->where('pickuptype', '!=', 'subscription')->where('max_km', '>=', $km)->where('min_km', '<=', $km)->max('max_km');
                    if (is_null($distance)) {
                        $distance = Price::where(['type' => 'normal', 'delete_status' => 0, 'status_per_km' => 'False'])->where('pickuptype', '!=', 'subscription')->where('max_km', '>=', $km)->where('min_km', '<=', $km)->max('max_km');
                        if (is_null($distance)) {
                            $distance = Price::where(['type' => 'default', 'delete_status' => 0, 'status_per_km' => 'False'])->where('pickuptype', '!=', 'subscription')->max('max_km');
                        }
                    }
                } else {
                    $distance = Price::where(['type' => 'urgent', 'delete_status' => 0])->max('max_km');
                }
                if ($km <= $distance) {

                    $allDeliveryAgent = User::where(['type' => 'Deliveryboy', 'is_delete' => 'false', 'delivery_agent_status' => '1'])->get();
                    $allowedAgents = [];

                    // Set dynamic raidus for delivery agent
                    $radius = GeneralSetting::where('name', 'Covered Area')->first();
                    if ($radius)
                        $radius =  $radius->value;

                    $radius = $radius ? (int)$radius : 10;

                    foreach ($allDeliveryAgent as $deliveryAgent) {
                        $distance = calculate_distance($deliveryAgent->latitude, $deliveryAgent->longitude, $request['pickaddress'][0]['latiude'], $request['pickaddress'][0]['longitude']);
                        if ($distance >= 0 && $distance < $radius) {
                            $allowedAgents[] = $deliveryAgent;
                        }
                    }

                    if (count($allowedAgents) == 0) {
                        return $this->ErrorResponse(__('No delivery agent found in your area.'));
                    }

                    $defualt = false;

                    if ($request->request_type == "pickup to drop" || $request->request_type == "normal" || $request->request_type == "food" || $request->request_type == "medicine") {
                        $defualt = Price::where('type', 'default')->where('pickuptype', '!=', 'subscription')->where('delete_status', '0')->first();

                        $plan_price = Price::where('max_km', '>=', $km)
                            ->where('min_km', '<=', $km)
                            ->where(['type' => 'normal', 'delete_status' => 0])
                            ->where('pickuptype', '!=', 'subscription')
                            ->first();
                        $pending_km = abs($defualt->max_km - $km);

                        if ($plan_price) {
                            if ($plan_price->status_per_km == 'True') {
                                $total_price = ($pending_km * $plan_price->price) + $defualt->price;
                            } else {
                                $total_price = $plan_price->price;
                            }
                        } else {
                            $total_price = $defualt->price;
                        }
                        $data = $request->all();
                        $data['distance'] = $km;

                        if (!is_null($request->promocode) && $request->promocode != "") {
							if(true){
								$uper_code = strtoupper($request->promocode);
								$promocode = Promocode::where("promocode", $request->promocode)->where('is_delete', '0')->first();

                            if ($promocode) {
								$enddate = $promocode->enddate;
								$today = date('Y-m-d');
								if ($enddate < $today) {
									return $this->ErrorResponse(__('Promocode Expired'));
								}
								if ($promocode && isset($promocode->min_order_amount) && ($promocode->min_order_amount >  $request->amount)) {
									return $this->ErrorResponse(__('Minimum order amount to apply this promocode is ' . $promocode->min_order_amount));
								}
								$order_type = $promocode->order_type;
								if ($order_type !== $request->request_type && $order_type !== "all") {
									return $this->ErrorResponse(__('Promocode Valid for ' . $order_type));
								}

								$allpickup = Pickup::where(['user_id' => $request->user_id, 'promocode' => $uper_code])->count();
								if ($allpickup >= $promocode->code_time) {
									return $this->ErrorResponse(__('you used Promocode limits'));
								}

								if ($promocode->one_day === "Yes") {
								$daypickup = Pickup::where(['user_id' => $request->user_id, 'promocode' => $uper_code])
									->whereDate('created_at', '=', date('Y-m-d'))->count();
									if ($daypickup > 0) {
										return $this->ErrorResponse(__('You Can Use this Code Once Per Day'));
									}
								}


                                $data['promocodedata'] = [];
                                if ($promocode->code_type == 'percentage') {
                                    $promo_amount = number_format(($total_price * ($promocode->perc_price / 100)), 2);
                                    $promo_amount = ($promo_amount >= $promocode->upto_amount) ? $promocode->upto_amount : $promo_amount;

                                    $total_price = number_format(($total_price - $promo_amount), 2);
                                    $data['promocodedata']['discount_per'] = $promocode['perc_price'] . "%";
                                    $data['promocodedata']['amount'] = $promo_amount;
                                } else {
                                    $promo_amount = number_format($promocode->perc_price, 2);
                                    if ($promo_amount >= $total_price) {
                                        $promo_amount = $total_price;
                                    }
                                    $total_price = number_format(($total_price - $promo_amount), 2);
                                    $data['promocodedata']['discount'] = $promocode['perc_price'] . " Rs";
                                    $data['promocodedata']['amount'] = $promo_amount;
                                }
							}
							}else{
							 $promocode = Promocode::where("promocode", $request->promocode)->where('is_delete', '0')->first();

                            if ($promocode) {
                                $data['promocodedata'] = [];
                                if ($promocode->code_type == 'percentage') {
                                    $promo_amount = number_format(($total_price * ($promocode->perc_price / 100)), 2);
                                    $promo_amount = ($promo_amount >= $promocode->upto_amount) ? $promocode->upto_amount : $promo_amount;

                                    $total_price = number_format(($total_price - $promo_amount), 2);
                                    $data['promocodedata']['discount_per'] = $promocode['perc_price'] . "%";
                                    $data['promocodedata']['amount'] = $promo_amount;
                                } else {
                                    $promo_amount = number_format($promocode->perc_price, 2);
                                    if ($promo_amount >= $total_price) {
                                        $promo_amount = $total_price;
                                    }
                                    $total_price = number_format(($total_price - $promo_amount), 2);
                                    $data['promocodedata']['discount'] = $promocode['perc_price'] . " Rs";
                                    $data['promocodedata']['amount'] = $promo_amount;
                                }
                              }

							}
                        }

                        //GST Calculation
                        $cgst = GeneralSetting::where('name', 'cgst')->first();
                        if ($cgst) {
                            $data['cgst'] = number_format(($total_price * ($cgst->value / 100)), 2);
                        }
                        $sgst = GeneralSetting::where('name', 'sgst')->first();
                        if ($sgst) {
                            $data['sgst'] = number_format(($total_price * ($sgst->value / 100)), 2);
                        }
                        if ($sgst || $cgst) {
                            $total_price = number_format(($total_price + $data['cgst'] + $data['sgst']), 2);
                        }

                        $data['amount'] = $total_price;
                        return $this->SuccessResponse($data, __('Amount And Distance'));
                    } else {
                        $defualt = Price::where(['type' => 'urgent', 'status_per_km' => 'False', 'delete_status' => 0])->first();
                        //$defualt = Price::where(['type' => 'urgent', 'delete_status' => 0])->first();
                        $plan_price = Price::where('max_km', '>=', $km)
                            ->where('min_km', '<=', $km)
                            ->where(['type' => 'urgent', 'status_per_km' => 'True', 'delete_status' => 0])
                            ->first();

                        $pending_km = $km;

                        if ($defualt)
                            $pending_km = ($km - $defualt->max_km);


                        if ($plan_price) {
                            $total_price = $pending_km * $plan_price->price;
                            if ($defualt) {
                                $total_price += $defualt->price;
                            }
                        } else {
                            $total_price = $defualt->price;
                        }
                        $data = $request->all();
                        $data['distance'] = $km;

                        if (!is_null($request->promocode) && $request->promocode != "") {
                            $promocode = Promocode::where("promocode", $request->promocode)->where('is_delete', '0')->first();
                            if ($promocode) {
                                $data['promocodedata'] = [];
                                if ($promocode->code_type == 'percentage') {
                                    $promo_amount = number_format(($total_price * ($promocode->perc_price / 100)), 2);
                                    $promo_amount = ($promo_amount >= $promocode->upto_amount) ? $promocode->upto_amount : $promo_amount;
                                    $total_price = number_format(($total_price - $promo_amount), 2);
                                    $data['promocodedata']['discount_per'] = $promocode['perc_price'] . "%";
                                    $data['promocodedata']['amount'] = $promo_amount;
                                } else {
                                    $promo_amount = number_format($promocode->perc_price, 2);
                                    if ($promo_amount >= $total_price) {
                                        $promo_amount = $total_price;
                                    }
                                    $total_price = number_format(($total_price - $promo_amount), 2);
                                    $data['promocodedata']['discount'] = $promocode['perc_price'] . " Rs";
                                    $data['promocodedata']['amount'] = $promo_amount;
                                }
                            }
                        }
                        //GST Calculation
                        $cgst = GeneralSetting::where('name', 'cgst')->first();
                        if ($cgst) {
                            $data['cgst'] = number_format(($total_price * ($cgst->value / 100)), 2);
                        }
                        $sgst = GeneralSetting::where('name', 'sgst')->first();
                        if ($sgst) {
                            $data['sgst'] = number_format(($total_price * ($sgst->value / 100)), 2);
                        }
                        if ($sgst || $cgst) {
                            $total_price = number_format(($total_price + $data['cgst'] + $data['sgst']), 2);
                        }
                        //                        $data['amount'] = round($total_price);
                        $data['amount'] = $total_price;
                        return $this->SuccessResponse($data, __('Amount And Distance'));
                    }
                } else {
                    if ($km >= $distance && $request->request_type == "urgent") {
                        return $this->ErrorResponse(__('Emergency Delivery Available Within ' . $distance . ' KM'));
                    }
                    return $this->ErrorResponse(__('Delivery Not avaliable in your location'));
                }
            } else {
                return $this->ErrorResponse(__('Enter Proper Address'));
            }
        } else {
            return $this->ErrorResponse(__('Not Avaliable in your location'));
        }
    }

    public function pickupdetail(PickupIdRequest $request)
    {
        $pickup = Pickup::with(['Pickupaddress', 'Dropaddress', 'boy', 'image' => function ($q) {
            $q->whereHas('pickup', function ($q2) {
                $q2->where('type', 'medicine');
            });
        }, 'dispute'])->find($request->pickup_id);


        if ($pickup) {
            return $this->SuccessResponse($pickup, __('Pickup Details'));
        } else {
            return $this->ErrorResponse(__('Pickup Not Found'));
        }
    }

    public function pickupcomplete(Request $request)
    {
        $user = Pickup::with('Pickupaddress', 'Dropaddress', 'boy', 'image')->where(['user_id' => $request->user_id, 'status' => 'Delivered'])->get();
        return $this->SuccessResponse($user, __('Pickup Complete List'));
    }

    public function Grossery_Medicine_Request(Request $request)
    {
        // if($request->promocode == null)
        // {
        //     $Input['promocode'] = $request->promocode;
        // }
        // if($request->promocode == null)
        // {
        //                    // $request->except(['promocode']);
        //    $request['promocode'] = '';
        // }
        $pincodeD = json_decode($request->dropaddress, true);
        $pincodeD['user_id'] = $request->user_id;
        $file = $request->file('img_path');
        $dropaddress = $pincodeD['address'];
        $amount = ($request->amount) . '00';
        if ($request->payment_type == "cash") {
            $request['payment_status'] = "complete";
        } else if ($request->payment_type == "online") {
            $request['payment_status'] = "complete";
            $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
            $payment = $api->payment->fetch($request->payment_id)->capture(array('amount' => $amount, 'currency' => 'INR'));
        }
        $Input = $request->except('dropaddress', 'img_path');
        $drop_address_check = Address::where('user_id', $request->user_id)->where('address', $pincodeD['address'])->count();
        if ($drop_address_check) {
            $drop_up = $pincodeD['address'];
            $query = Address::where('user_id', $request->user_id)->where('address', $drop_up)->first();
            $old_dropup_id = $query->id;
            $update_dropup = Address::where('id', $old_dropup_id)->update($pincodeD);
            $Input['dropaddress_id'] = $old_dropup_id;
        } else {
            $Input['dropaddress_id'] = Address::create($pincodeD)->id;
        }
        $Input['pickaddress_id'] = 0;
        $Input['pincode'] = $pincodeD['pincode'];

        //GST Calculation
        $commission_amt = 0;
        $cgst = GeneralSetting::where('name', 'cgst')->first();
        $sgst = GeneralSetting::where('name', 'sgst')->first();
        $gst = 0;
        if ($cgst && $sgst) {
            $gst = number_format($Input['original_amount'] - ($Input['original_amount'] * (100 / ($cgst->value + $sgst->value + 100))), 2);
        }
        /*$sgst = GeneralSetting::where('name', 'sgst')->first();
        if ($sgst) {
            $Input['sgst'] = number_format($Input['original_amount'] - ($Input['original_amount'] * (100/($sgst->value + 100))), 2);
        }*/
        if ($gst != 0) {
            $Input['sgst'] = number_format($gst / 2, 2);
            $Input['cgst'] = number_format($gst / 2, 2);
            $commission_amt = number_format(($Input['original_amount'] - $Input['cgst'] - $Input['sgst']), 2);
        }
        $percenatge_commisssion = Commission::first();
        $Input['order_commission'] = number_format(((int)$commission_amt - ((int)$commission_amt * (int)$percenatge_commisssion->commission)) / 100, 2);

        if (isset($Input['promocode']) && $Input['promocode'] != "") {
            $promocode = Promocode::where("promocode", $Input['promocode'])->first();
            if ($promocode) {
                if ($promocode->code_type == 'percentage') {
                    $Input['promo_amount'] = number_format(($Input['original_amount'] * ($promocode->perc_price / 100)), 2);
                } else {
                    $Input['promo_amount'] = number_format($promocode->perc_price, 2);
                }
            }
        }

        $pickrequest = Pickup::create($Input);
        if ($pickrequest) {
            $pickup = Pickup::with('Dropaddress.area')->find($pickrequest->id);
            $deliverys = DeliveryArea::with('dBoy')->where('area_id', $pickup->Dropaddress->area->id)->get();
            foreach ($deliverys as $dBoy) {
                foreach ($dBoy->dBoy as $delivery) {
                    $usertoken = $delivery['fcm_token'];
                    $addtion = array(
                        'Type' => 'Find One ' . ucfirst($request->type) . ' Request',
                        'user' => $delivery['deliveryboy_name'],
                    );
                    $msg = array(
                        'body' => ucfirst($request->type) . ' Request Occurs Check Fast',
                        'title' => ucfirst($request->type) . ' Request',
                    );
                    // Notification::Push($msg,$usertoken,$addtion);
                    PushNotification::push($msg, $usertoken, $addtion);
                }
            }
            if ($file) {
                $img['pickup_id'] = $pickrequest->id;
                $img['img_path'] = $request->file('img_path')->move(
                    'images',
                    $pickrequest->id . '.' . $file->extension()
                );
                $img_path = Image::create($img)->img_path;
                $pickrequest['img_path'] = url('/') . '/' . ($img_path);
            }
            if ($request->wallet_amount && $request->payment_type == "cash") {
                $data = User::where('id', $request->user_id)->decrement('wallet', $request->wallet_amount);
                if ($data > 0) {
                    if ($request->wallet_amount > 0) {
                        if ($request->payment_type == "cash") {
                            $payment_id = $request->type . ' request debit';
                        } else {
                            $payment_id = $request->payment_id;
                        }
                        $add_wallet = [
                            'user_id' => $request->user_id,
                            'amount' => $request->wallet_amount,
                            'payment_id' => $payment_id,
                            'title' => $request->type . ' Request Debit',
                            'status' => 'debit',
                        ];
                        $wallet_history = WalletHistory::forceCreate($add_wallet);
                    }
                }
            }
            return $this->SuccessResponse($pickrequest, __(ucfirst($request->type) . ' request send successfully'));
        } else {
            return $this->ErrorResponse(__(ucfirst($request->type) . ' request Not send'));
        }
    }

    public function Tiffin_Request(Request $request)
    {
        $pickuppin = $request['pickaddress'][0]['pincode'];
        $droppin = $request['dropaddress'][0]['pincode'];
        $picarea = Area::where('pincode', $pickuppin)->get();
        $droparea = Area::where('pincode', $droppin)->get();
        if ($picarea->isEmpty() || $droparea->isEmpty()) {
            return $this->ErrorResponse(__('Delivery Not Available in Your Location'));
        }
        $pickaddress = $request->pickaddress[0];
        $pickaddress['user_id'] = $request->user_id;
        $dropaddress = $request->dropaddress[0];
        $dropaddress['user_id'] = $request->user_id;
        if ($request->payment_type == "cash") {
            $request['payment_status'] = "pending";
        } else if ($request->payment_type == "online") {
            $request['payment_status'] = "complete";
        }
        $Input = $request->except('pickaddress', 'dropaddress');
        $Input['pickaddress_id'] = Address::create($pickaddress)->id;
        $Input['pincode'] = $request->pickaddress[0]['pincode'];
        $Input['dropaddress_id'] = Address::create($dropaddress)->id;
        $pickrequest = Pickup::create($Input);
        if ($pickrequest) {
            return $this->SuccessResponse($pickrequest, __('Tiffin request send successfully'));
        } else {
            return $this->ErrorResponse(__('Tiffin request Not Send'));
        }
    }

    public function Grossery_Medicine_Getprice(Request $request)
    {
        $pincode = Area::where('pincode', $request->pincode)->get();
        if ($pincode) {
            $data = Price::where(['pickuptype' => $request->type, 'delete_status' => 0])->first();
            if ($data == null) {
                return $this->ErrorResponse(__('Something Wrong Try Again..'));
            }
            $response = $request->all();
            $response['amount'] = $data->price;
            $response['wallet'] = (int)User::where('id', $request->user_id)->first()->wallet;

            return $this->SuccessResponse($response, __(ucfirst($request->type) . ' Request Price'));
        } else {
            return $this->ErrorResponse(__('Delivery is not available in your location'));
        }
    }

    public function Tiffin_List(Request $request)
    {
        $tiffin = Pickup::with('Pickupaddress', 'Dropaddress', 'boy')
            ->where('user_id', $request->user_id)
            ->whereIn('type', ['tiffin', 'subscription'])
            ->orderByRaw("status = 'Delivered' ASC")->orderBy('id', 'DESC')
            ->get();
        return $this->SuccessResponse($tiffin, __('Subscription List'));
    }

    public function Tiffin_Detail(PickupIdRequest $request)
    {
        $tiffin = Pickup::with('Pickupaddress', 'Dropaddress', 'boy')
            ->where('id', $request->pickup_id)
            ->whereIn('type', ['tiffin', 'subscription'])->get();
        return $this->SuccessResponse($tiffin, __('Subscription Details'));
    }


	public function PaymentCancel(PickupIdRequest $request)
    {
        $pickup = Pickup::where('id', $request->pickup_id)->whereIn('status', ['Pickup Requested', 'Request Accept'])->get();
        if ($pickup->isEmpty()) {
            return $this->ErrorResponse(__("you're not able to cancel pickup"));
        } else {
            if ($pickup[0]->deliveryboy_id) {
                //$dboys = Delivery_fcm_token::where('deliveryboy_id', $pickup[0]->deliveryboy_id)->get();
                $dboys = User::where('id', $pickup[0]->deliveryboy_id)->get();
                $this->cancel_notify($dboys, $pickup[0]->type);

				$customer = User::where('id', $pickup[0]->user_id)->where('type', 'Customer')->first();

				$customertoken = $customer['fcm_token'];
                        $addtion = array(
                            'Type' => 'Dispute_accept',
                            'user' => $customer['name'],
                        );
                        $msg = array(
                            'title' => 'Order Cancel',
                            'body' => 'Your cancel request has been accepted, As order was accepted by delivery agent we will unable to refund order amount , kindly follow cancellation policy for further information',
                        );

				PushNotification::Push($msg, $customertoken, $addtion);
				$cancel = Pickup::where('id', $request->pickup_id)->update(['status' => 'Cancel Request']);
				if ($cancel) {
                    $response = Pickup::with('Pickupaddress', 'Dropaddress', 'boy', 'image')->find($request->pickup_id);
                    return $this->SuccessResponse($response, __('Request Cancelled'));
                } else {
                    return $this->ErrorResponse(__('Something Wrong Try Again...'));
                }
            }
            if ($pickup[0]->payment_type === "online") {
                if ($pickup[0]->status == 'Pickup Requested') {
                    $customer = User::where('id', $pickup[0]->user_id)->where('type', 'Customer')->first();
                    if ($customer) {
                        if ($pickup[0]->wallet_amount != 0) {
                            $customerWalletHistory = [
                                'title' => 'Cancel Request',
                                'amount' => isset($pickup[0]) ? ($pickup[0]->v2 == 'true' ? $pickup[0]->wallet_amount : $pickup[0]->original_amount) : 0,
                                'status' => 'credit',
                                'payment_id' => 'Payment Refund',
                                'user_id' => $pickup[0]->user_id
                            ];
                            $Wallet_history = WalletHistory::create($customerWalletHistory);
							Log::info(print_r($customer,1));
                            $customer->wallet = ((float)$customer->wallet + (float)$pickup[0]->wallet_amount);
                            //$customer->wallet = ((int)$customer->wallet + (int)$pickup[0]->amount);
                            $its_save = $customer->save();
                        }

                        if(isset($pickup[0]) && $pickup[0]->v2 == 'true'){
                            if ($pickup[0]->paid_amount - $pickup[0]->wallet_amount > 0) {
                                $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
								Log::info(abs((float)$pickup[0]->paid_amount - (float)$pickup[0]->wallet_amount) * 100);
                                $api->payment->fetch($pickup[0]->payment_id)->refund(array("amount" => abs(((float)$pickup[0]->paid_amount - (float)$pickup[0]->wallet_amount) * 100), "speed" => "normal", "receipt" => "Receipt No. " . time()));
                            }
                        }else{
                            if ($pickup[0]->original_amount - $pickup[0]->wallet_amount > 0) {
                                $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
                                $api->payment->fetch($pickup[0]->payment_id)->refund(array("amount" => abs(((float)$pickup[0]->original_amount - (float)$pickup[0]->wallet_amount) * 100), "speed" => "normal", "receipt" => "Receipt No. " . time()));
                            }
                        }


                        $customertoken = $customer['fcm_token'];
                        $addtion = array(
                            'Type' => 'Dispute_accept',
                            'user' => $customer['name'],
                        );
                        $msg = array(
                            'title' => 'Order Cancel',
                            'body' => 'Your cancel request has been accepted. Your deducted amount will be credited to your Account very soon.',
                        );

                        PushNotification::Push($msg, $customertoken, $addtion);
                    }
                }

                $cancel = Pickup::where('id', $request->pickup_id)->update(['status' => 'Cancel Request']);
                if ($cancel) {
                    $response = Pickup::with('Pickupaddress', 'Dropaddress', 'boy', 'image')->find($request->pickup_id);
                    return $this->SuccessResponse($response, __('Request Cancelled'));
                } else {
                    return $this->ErrorResponse(__('Something Wrong Try Again...'));
                }
            } else {
                $cancel = Pickup::where('id', $request->pickup_id)->update(['status' => 'Cancel Request']);
                if ($cancel) {
                    $customer = User::where('id', $pickup[0]->user_id)->where('type', 'Customer')->first();

                    if ($customer) {
                        $customerWalletHistory = [
                            'title' => 'Cancel Request',
                            'amount'=> isset($pickup[0]) ? ($pickup[0]->v2 == 'true' ? $pickup[0]->wallet_amount : $pickup[0]->original_amount) : 0,
                            'status' => 'credit',
                            'payment_id' => 'Payment Refund',
                            'user_id' => $pickup[0]->user_id
                        ];
                        $Wallet_history = WalletHistory::create($customerWalletHistory);
                        if (isset($pickup[0]) && $pickup[0]->v2 == 'true') {
                            $customer->wallet = ((float)$customer->wallet + (float)$pickup[0]->paid_amount);
                        } else {
                            $customer->wallet = ((float)$customer->wallet + (float)$pickup[0]->original_amount);
                        }
                        // $customer->wallet = ((float)$customer->wallet + (float)$pickup[0]->paid_amount);
                        $its_save = $customer->save();
                    }

                    $customertoken = $customer['fcm_token'];
                    $addtion = array(
                        'Type' => 'cancel_request',
                        'user' => $customer['name'],
                    );
                    $msg = array(
                        'title' => 'Order Cancel',
                        'body' => 'Your cancel request has been accepted. Your deducted amount will be credited to your Pickup To Drop wallet very soon.',
                    );

                    PushNotification::Push($msg, $customertoken, $addtion);

                    $response = Pickup::with('Pickupaddress', 'Dropaddress', 'boy', 'image')->find($request->pickup_id);
                    return $this->SuccessResponse($response, __('Request Cancelled'));
                } else {
                    return $this->ErrorResponse(__('Something Wrong Try Again...'));
                }
            }
        }
    }



    public function PaymentCancelold(PickupIdRequest $request)
    {
        $pickup = Pickup::where('id', $request->pickup_id)->whereIn('status', ['Pickup Requested', 'Request Accept'])->get();

        if ($pickup->isEmpty()) {
            return $this->ErrorResponse(__("you're not able to cancel pickup"));
        } else {
            if ($pickup[0]->deliveryboy_id) {
//                $dboys = Delivery_fcm_token::where('deliveryboy_id', $pickup[0]->deliveryboy_id)->get();
                $dboys = User::where('id', $pickup[0]->deliveryboy_id)->get();
                $this->cancel_notify($dboys, $pickup[0]->type);
            }
            if ($pickup[0]->payment_type === "online") {
                if($pickup[0]->status == 'Pickup Requested'){
                $customer = User::where('id', $pickup[0]->user_id)->where('type', 'Customer')->first();
                if ($customer) {
                        if($pickup[0]->wallet_amount != 0){
                    $customerWalletHistory = [
                        'title' => 'Cancel Request',
                        'amount' => $pickup[0]->wallet_amount,
                        'status' => 'credit',
                        'payment_id' => 'Payment Refund',
                        'user_id' => $pickup[0]->user_id
                    ];
                    $Wallet_history = WalletHistory::create($customerWalletHistory);
                    $customer->wallet = ((float)$customer->wallet + (float)$pickup[0]->wallet_amount);
                    //$customer->wallet = ((int)$customer->wallet + (int)$pickup[0]->amount);
                    $its_save = $customer->save();
                }
                if($pickup[0]->original_amount - $pickup[0]->wallet_amount > 0){
                    $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
                    $api->payment->fetch($pickup[0]->payment_id)->refund(array("amount"=> $pickup[0]->original_amount - $pickup[0]->wallet_amount * 100,"speed"=>"normal","receipt"=>"Receipt No. ".time()));
                }

                $customertoken = $customer['fcm_token'];
                $addtion = array(
                    'Type' => 'Dispute_accept',
                    'user' => $customer['name'],
                );
                $msg = array(
                    'title' => 'Order Cancel',
                            'body' => 'Your cancel request has been accepted. Your deducted amount will be credited to your Account very soon.',
                );

                PushNotification::Push($msg, $customertoken, $addtion);
                    }
                }

                $cancel = Pickup::where('id', $request->pickup_id)->update(['status' => 'Cancel Request']);
                if ($cancel) {
                    $response = Pickup::with('Pickupaddress', 'Dropaddress', 'boy', 'image')->find($request->pickup_id);
                    return $this->SuccessResponse($response, __('Request Cancelled'));
                } else {
                    return $this->ErrorResponse(__('Something Wrong Try Again...'));
                }
            } else {
                $cancel = Pickup::where('id', $request->pickup_id)->update(['status' => 'Cancel Request']);
                if ($cancel) {
                    $customer = User::where('id', $pickup[0]->user_id)->where('type', 'Customer')->first();

                    if ($customer) {
                        $customerWalletHistory = [
                            'title' => 'Cancel Request',
                            'amount' => $pickup[0]->original_amount,
                            'status' => 'credit',
                            'payment_id' => 'Payment Refund',
                            'user_id' => $pickup[0]->user_id
                        ];
                        $Wallet_history = WalletHistory::create($customerWalletHistory);
                        $customer->wallet = ((float)$customer->wallet + (float)$pickup[0]->original_amount);
                        $its_save = $customer->save();
                    }

                    $customertoken = $customer['fcm_token'];
                    $addtion = array(
                        'Type' => 'cancel_request',
                        'user' => $customer['name'],
                    );
                    $msg = array(
                        'title' => 'Order Cancel',
                        'body' => 'Your cancel request has been accepted. Your deducted amount will be credited to your Pickup To Drop wallet very soon.',
                    );

                    PushNotification::Push($msg, $customertoken, $addtion);

                    $response = Pickup::with('Pickupaddress', 'Dropaddress', 'boy', 'image')->find($request->pickup_id);
                    return $this->SuccessResponse($response, __('Request Cancelled'));
                } else {
                    return $this->ErrorResponse(__('Something Wrong Try Again...'));
                }
            }
        }
    }

    public function cancel_notify($dboys, $type)
    {
        $temp_arr = [];
        foreach ($dboys as $key => $delivery) {
            if (in_array($delivery['fcm_token'], $temp_arr)) {
                continue;
            }
            $temp_arr[] = $delivery['fcm_token'];
            $usertoken = $delivery['fcm_token'];
            $addtion = array(
                'Type' => 'cancel request',
                'user' => $delivery['name'],
            );
            $msg = array(
                'body' => ucfirst($type) . ' Request has been cancelled',
                'title' => ucfirst($type) . ' Request cancelled',
            );
            PushNotification::Push($msg, $usertoken, $addtion);
        }
    }

    public function promocode_list(Request $request)
    {
			$promocode = Promocode::where('is_delete', 0)->where('enddate', '>=', date('Y-m-d'))->get();
			return $this->SuccessResponse($promocode, __('Promocode List'));
       /* $promocode = Promocode::where('is_delete', 0)->where('enddate', '>=', date('Y-m-d'))->where('id',24)->get();

        $allowed_promocode = [];
        foreach ($promocode as $sPromocode) {
            $pastOrder = Pickup::where(['user_id' => Auth()->user()->id, 'promocode' => $sPromocode->promocode])->count();


            if ($pastOrder < $sPromocode->code_time) {
                if ($sPromocode->one_day === "Yes") {
                    $daypickup = Pickup::where(['user_id' => Auth()->user()->id, 'promocode' => $sPromocode->promocode])
                        ->whereDate('created_at', '=', date('Y-m-d'))->count();
                    if ($daypickup < 1) {
                        $allowed_promocode[] = $sPromocode;
                    }
                } else {
                    $allowed_promocode[] = $sPromocode;
                }
                 return $this->ErrorResponse(__('you used Promocode limits'));
            } */
            /*if ($promocode->one_day === "Yes") {
                $daypickup = Pickup::where(['user_id' => Auth()->user()->id, 'promocode' => $sPromocode->promocode])
                    ->whereDate('created_at', '=', date('Y-m-d'))->count();
                if ($daypickup > 0) {
                    // return $this->ErrorResponse(__('You Can Use this Code Once Per Day'));
                }
            }*/
        //}

        //return $this->SuccessResponse($allowed_promocode, __('Promocode List'));
        //return $this->SuccessResponse($promocode, __('Promocode List'));
    }

    public function ApplyPromocode($request)
    {
        $uper_code = strtoupper($request->promocode);
        $exist_promo = Promocode::where('promocode', $uper_code)->where('is_delete', 0)->first();
        $enddate = $exist_promo->enddate;
        $today = date('Y-m-d');
        if ($enddate < $today) {
            return $this->ErrorResponse(__('Promocode Expired'));
        }
		if ($exist_promo && isset($exist_promo->min_order_amount) && ($exist_promo->min_order_amount >  $request->amount)) {
            return $this->ErrorResponse(__('Minimum order amount to apply this promocode is ' . $exist_promo->min_order_amount));
        }
        if (!$exist_promo) {
            return $this->ErrorResponse(__('Promocode Not Valid'));
        } else {
            $order_type = $exist_promo->order_type;
            if ($order_type !== $request->order_type && $order_type !== "all") {
                return $this->ErrorResponse(__('Promocode Valid for ' . $order_type));
            }
            if ($exist_promo->code_type === "percentage") {
                $price = 0;
                $percenatge = $exist_promo->perc_price;
            } else {
                $percenatge = 0;
                $price = $exist_promo->perc_price;
            }
            $allpickup = Pickup::where(['user_id' => $request->user_id, 'promocode' => $uper_code])->count();
            if ($allpickup == $exist_promo->code_time) {
                return $this->ErrorResponse(__('you used Promocode limits'));
            }
            if ($exist_promo->one_day === "Yes") {
                $daypickup = Pickup::where(['user_id' => $request->user_id, 'promocode' => $uper_code])
                    ->whereDate('created_at', '=', date('Y-m-d'))->count();
                if ($daypickup > 0) {
                    return $this->ErrorResponse(__('You Can Use this Code Once Per Day'));
                }
            }

            if ($price > 0) {
				$amount = $request->original_amount - $request->sgst - $request->cgst;
                $data['amount'] = max(($amount - $price), 0);
                $data['discount'] = $price;
            } else {
				$amount = $request->original_amount - $request->sgst - $request->cgst;
                $dis = ($amount * $percenatge) / 100;
                $upto = $exist_promo->upto_amount;
                $discount = ($dis >= $upto) ? $upto : $dis;
                $data['amount'] = round($amount - $discount);
                $data['amount'] = $discount;
                $data['discount_per'] = $percenatge . "%";
            }
            return $data;
            return response()->json(["data" => $data, "status" => 200]);
            return $this->SuccessResponse($data, __('promocode Amount'));
        }
    }

    public function default_address(Request $request)
    {
        $pickaddress = $request->address;
        $pickaddress['user_id'] = $request->user_id;
        if (isset($request->address_id)) {
            Address::where('id', $request->address_id)->update($pickaddress);
            $user = User::with(['defaultHomeAddresses', 'defaultWorkAddresses'])->find($request->user_id);
            return $this->SuccessResponse($user, __('Address added successfully'));
        }
        $aId = Address::create($pickaddress)->id;
        $user = User::find($request->user_id);
        if ($request->type == 'Home') {
            $user->update(['home_address' => $aId]);
        } else if ($request->type == 'Work') {
            $user->update(['work_address' => $aId]);
        }
        $user = User::with(['defaultHomeAddresses', 'defaultWorkAddresses'])->find($request->user_id);
        return $this->SuccessResponse($user, __('Address added successfully'));
    }

    public function SubscriptionPrice(PickupRequest $request)
    {
        // return $this->ErrorResponse(__('App is under maintanance'));
        $pickuppin = $request['pickaddress'][0]['pincode'];
        $droppin = $request['dropaddress'][0]['pincode'];
        $picarea = Area::where('pincode', $pickuppin)->get();
        $droparea = Area::where('pincode', $droppin)->get();
        $request['wallet'] = (int)User::where('id', $request->user_id)->first()->wallet;

        if (!$picarea->isEmpty() && !$droparea->isEmpty()) {
            $from = urlencode($request['pickaddress'][0]['address']);
            $to = urlencode($request['dropaddress'][0]['address']);
            $url = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins=' . $from . '&destinations=' . $to . '&key=AIzaSyDy7EA7d-f4T4hMXA0Y7y-7hrPx6S6YDo0';
            $distance = @file_get_contents($url);

            $distance = json_decode($distance, true);
            if ($distance["rows"][0]["elements"][0]["status"] == "OK") {
                $km = round(explode(" ", $distance["rows"][0]["elements"][0]["distance"]["text"])[0]);
                if ($request->request_type) {
                    $distance = Price::where(['pickuptype' => $request->request_type, 'delete_status' => 0])->max('max_km');
                }
                if ($km <= $distance) {

                    $allDeliveryAgent = User::where(['type' => 'Deliveryboy', 'is_delete' => 'false', 'delivery_agent_status' => '1'])->get();
                    $allowedAgents = [];

                    // Set dynamic raidus for delivery agent
                    $radius = GeneralSetting::where('name', 'Covered Area')->first();
                    if ($radius)
                        $radius =  $radius->value;

                    $radius = $radius ? (int)$radius : 10;

                    foreach ($allDeliveryAgent as $deliveryAgent) {
                        $distance = calculate_distance($deliveryAgent->latitude, $deliveryAgent->longitude, $request['pickaddress'][0]['latiude'], $request['pickaddress'][0]['longitude']);
                        if ($distance >= 0 && $distance < $radius) {
                            $allowedAgents[] = $deliveryAgent;
                        }
                    }

                    if (count($allowedAgents) == 0) {
                        return $this->ErrorResponse(__('No delivery agent found in your area.'));
                    }


                    $defualt = false;
                    $diff = Carbon::parse($request->start_date)->diffInDays(Carbon::parse($request->end_date)) + 1;
                    if ($request->request_type == "subscription") {
                        // $defualt = Price::where('pickuptype' , $request->request_type)->where('delete_status', '0')->first();

                        $plan_price = Price::where('max_km', '>=', $km)
                            ->where('min_km', '<=', $km)
                            ->where(['type' => 'normal', 'status_per_km' => 'True', 'delete_status' => 0])
                            ->first();

                        $defualt = Price::where('max_km', '>=', $km)
                            ->where('min_km', '<=', $km)
                            ->where(['type' => 'normal', 'status_per_km' => 'False', 'delete_status' => 0])
                            ->first();


                        if ($plan_price) {
                            $pending_km = abs($plan_price->max_km - $km);
                            $total_price = ($km * $plan_price->price);
                        } else {
                            $total_price = $defualt->price;
                        }
                        $data = $request->all();
                        $data['distance'] = $km;
                        $data['amount'] = (int)$total_price * (int)$diff;

                        //GST Calculation
                        $cgst = GeneralSetting::where('name', 'cgst')->first();
                        $sgst = GeneralSetting::where('name', 'sgst')->first();
                        $gst = 0;
                        if ($cgst && $sgst) {
                            $gst = number_format($data['amount'] - ($data['amount'] * (100/($cgst->value + $sgst->value + 100))), 2);
                        }
                        if ($gst != 0) {
                            $data['sgst'] = number_format($gst / 2, 2);
                            $data['cgst'] = number_format($gst / 2, 2);
                            $data['amount'] = round($data['amount'] + $data['sgst'] + $data['cgst']);
                        }

                        return $this->SuccessResponse($data, __('Amount And Distance'));
                    } else {
                        $defualt = Price::where(['type' => 'normal', 'pickuptype' => 'subscription', 'status_per_km' => 'False', 'delete_status' => 0])->first();
                        $plan_price = Price::where('max_km', '>=', $km)
                            ->where('min_km', '<=', $km)
                            ->where(['type' => 'normal', 'pickuptype' => 'subscription', 'status_per_km' => 'True', 'delete_status' => 0])
                            ->first();
                        $pending_km = ($km - $defualt->max_km);
                        if ($plan_price) {
                            $total_price = $pending_km * $plan_price->price + $defualt->price;
                        } else {
                            $total_price = $defualt->price;
                        }
                        $data = $request->all();
                        $data['distance'] = $km;
                        $data['amount'] = (int)$total_price * (int)$diff;
                        //GST Calculation
                        $cgst = GeneralSetting::where('name', 'cgst')->first();
                        $sgst = GeneralSetting::where('name', 'sgst')->first();
                        $gst = 0;
                        if ($cgst && $sgst) {
                            $gst = number_format($data['amount'] - ($data['amount'] * (100/($cgst->value + $sgst->value + 100))), 2);
                        }
                        if ($gst != 0) {
                            $data['sgst'] = number_format($gst / 2, 2);
                            $data['cgst'] = number_format($gst / 2, 2);
                            $data['amount'] = round($data['amount'] + $data['sgst'] + $data['cgst']);
                        }
                        return $this->SuccessResponse($data, __('Amount And Distance'));
                    }
                } else {
                    if ($km >= $distance && $request->request_type == "normal") {
                        return $this->ErrorResponse(__('Emergency Delivery Available Within ' . $distance . ' KM'));
                    }
                    return $this->ErrorResponse(__('Delivery Not avaliable in your location'));
                }
            } else {
                return $this->ErrorResponse(__('Enter Proper Address'));
            }
        } else {
            return $this->ErrorResponse(__('Not Avaliable in your location'));
        }
    }
    public function updateNote(Request $request)
    {
        try {
            $request->validate([
                'order_id' => 'required',
                'note' => 'required'
            ]);
            $order = Pickup::where('id', $request->order_id)->first();
            if ($order) {
                if ($order->status == "Delivered") {
                    $note = Pickup::where('id', $request->order_id)->update(['note' => $request->note]);
                    if ($note) {
                        return $this->SuccessResponse([], __('Notes updated successfully'));
                    }
                    return $this->ErrorResponse(__('Something went wrong'));
                } else {
                    return $this->ErrorResponse(__('Notes can not be updated after order has been delivered'));
                }
            } else {
                return $this->ErrorResponse(__('Order not found'));
            }
        } catch (\Throwable $th) {
            return $this->ErrorResponse(__($th->getMessage()));
        }
    }
}
