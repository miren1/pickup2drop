<?php
namespace App\Http\Controllers\api;
use Str;
use Hash;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Http\Request;
use App\Models\Delivery_fcm_token;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\ReferralRequest;
use App\Http\Requests\Deliveryboyloginrequest;
use App\Models\WalletHistory;
use App\Http\Controllers\api\Base\BaseController;

class ApiController extends BaseController
{
    public function Login(LoginRequest $request){
        $user= User::where(['mobile_number'=>$request->mobile_number,'type'=>'Customer','is_delete'=>'false'])->first();
        if($user){
            $token = base64_encode(json_encode([$user->id,$user->mobile_number,$user->created_at]));
            $user->forceFill([
                'api_token' => hash('sha256', $token),
                'fcm_token' => $request->fcm_token
            ])->save();

            $user['is_first'] = false;

            return $this->LoginResponse($user,$token, __('login successfully'));
        }
        else{
            $wallet = Wallet::where(['type'=>'default','is_delete'=>0])->first();
            if($wallet)
            {
                $request['wallet'] = (int) $wallet->amount;
            }
            else{
                $request['wallet'] = 0;
            }

            $phone_number = $request->mobile_number;
            $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $referral_id = substr($phone_number,0,2). $characters[rand(0, strlen($characters) - 1)] . $characters[rand(0, strlen($characters) - 1)] .mt_rand(10, 99) . $characters[rand(0, strlen($characters) - 1)] . $characters[rand(0, strlen($characters) - 1)] . substr($phone_number,8);
            $request['referral_id'] = $referral_id;

            // dd($request->all());
            $user = User::forceCreate($this->formatParams($request->all()));
            
            if($request['wallet'] > 0)
            {
                $add_wallet = [
                    'user_id'=> User::where(['mobile_number'=>$request->mobile_number,'type'=>'Customer'])->first()->id,
                    'amount'=>$request['wallet'],
                    'payment_id'=>3,
                    'title'=>'Welcome Amount',
                    'status'=>'credit',
                ];
                $wallet_history = WalletHistory::forceCreate($add_wallet);
            }
               
            $token = base64_encode(json_encode([$user->id,$user->mobile_number,$user->created_at]));
            $user->forceFill([
                'api_token' => hash('sha256', $token),
                'fcm_token' => $request->fcm_token
            ])->save();

            $user['is_first'] = true;

            return $this->LoginResponse($user,$token, __('registration successfully'));
        } 
    }
    
    public function Deliveryboylogin(Deliveryboyloginrequest $request){
        $user= User::with('driverdoc')->where(['mobile_number'=>$request->mobile_number,'type'=>'Deliveryboy','is_delete'=>'false'])->first();
        if($user){
            if (!Hash::check($request->password, $user->password)) {
               return $this->ErrorResponse(__('Invalid Password'));
            }
            //192.168.0.111/pickupdrop/public
            $token = base64_encode(json_encode([$user->id,$user->mobile_number,$user->created_at]));
            $user->forceFill([
                'api_token' => hash('sha256', $token),
                'fcm_token' => $request->fcm_token
            ])->save();

            $dbfcm = array(
                'deliveryboy_id'=>$user->id,
                'fcm_token'=>$request->fcm_token,
                'deliveryboy_name'=>$user->name
            ); 
	    $exist = Delivery_fcm_token::where(['deliveryboy_id'=> $user->id, 'fcm_token' => $request->fcm_token])->first();
            if($exist)
                Delivery_fcm_token::where('deliveryboy_id', $user->id)->update(['fcm_token' => $request->fcm_token,'deliveryboy_name' => $user->name]);
            else{
                Delivery_fcm_token::create($dbfcm);
            }
            return $this->LoginResponse($user,$token, __('Login success'));
        }
        else{
            return $this->ErrorResponse(__('Login Failed'));
        } 
    }
    
    public function CheckMobile(Request $request){
        $user= User::where('mobile_number',$request->mobile_number)->first();
        if($user){
            return $this->SuccessResponse($user, __('Alerady Existing'));
        }
        else{
            return $this->ErrorResponse(__('Not Found'));
        } 
    }

    public function ChangePassword(LoginRequest $request){
        $user= User::where('mobile_number',$request->mobile_number)->first();
        if($user){
            $user->forceFill([
                'password' =>Hash::make($request->password),
            ])->save();
            return $this->SuccessResponse($user, __('Password Updated'));
        }
        else{
            return $this->ErrorResponse(__('Invalid Mobile Number'));
        } 
    }

    public function Reffer(Request $request)
    {
        // dd(strtoupper($request->referral_id));
        if(is_null($request->referral_id) && $request->referral_id == ""){
            return $this->ErrorResponse(__('Please Enter Referral Code'));
        }

        $ref_user = User::where('referral_id',strtoupper($request->referral_id))->first();

        if($ref_user)
        {
            $referral = Wallet::where(['type'=>'referral','is_delete'=>false])->first();
            $by_referral = Wallet::where(['type'=>'by_referral','is_delete'=>false])->first();
            
            $amount = ($referral)?$referral->amount:0;
            $by_amount = ($by_referral)?$by_referral->amount:0;
            
            $ref_update = User::where('id',$request->user_id)->increment('wallet',$amount);
            $by_ref_update = User::where('id',$ref_user->id)->increment('wallet',$by_amount);
            $ref_idupdate = User::where('id', $request->user_id)->update(['by_referral_id' => $ref_user->id]);

            if($amount > 0)
            {
                $arrHistory = array(
                    'title' => 'referral amount',
                    'amount' => $amount,
                    'status' => 'credit',
                    'payment_id' => 'referral amount',
                    'user_id' => $request->user_id
                );

                WalletHistory::create($arrHistory);
            }

            if($by_amount > 0)
            {
                $arr = array(
                    'title' => 'your reffered amount',
                    'amount' => $by_amount,
                    'status' => 'credit',
                    'payment_id' => 'your reffered amount',
                    'user_id' => $ref_user->id
                );

                WalletHistory::create($arr);
            }
            $user = User::where('id',$request->user_id)->first();
            return $this->SuccessResponse($user,__('Referral Code Applied'));
        }
        else
        {
            return $this->ErrorResponse(__('Invalid Referral Code'));
        }
    }


    protected function formatParams($params){
        
        return [
            'fcm_token'=>!empty($params['fcm_token']) ? $params['fcm_token'] : "",
            'mobile_number'=>!empty($params['mobile_number']) ? $params['mobile_number'] : "",
            'wallet'=>!empty($params['wallet']) ? $params['wallet'] : 0,
            'referral_id'=>$params['referral_id'],
            'by_referral_id'=>!empty($params['by_referral_id']) ? $params['by_referral_id'] : NULL,
        ];
    }

    protected function fillable(){
         return  ['fcm_token','mobile_number','email','wallet','referral_id','by_referral_id'];

    }
}
