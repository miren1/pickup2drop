<?php

namespace App\Http\Controllers\api;

use App\Models\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\Base\BaseController;
use Illuminate\Support\Facades\Log;

class SliderController extends BaseController
{
    public function Slider()
    {
		return $this->ErrorResponse(__("Please update the application to place the order"));
        $dashboard = Slider::all();
        return $this->SuccessResponse($dashboard,__('Slider List'));
    }
	
	public function Sliderv2()
    {
		$dashboard = Slider::all();
        return $this->SuccessResponse($dashboard,__('Slider List'));
    }
}
