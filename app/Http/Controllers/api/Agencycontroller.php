<?php
namespace App\Http\Controllers\api;
use App\Http\Controllers\api\Base\BaseController;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\EditProductRequest;

use App\Http\Requests\getAgencyrequest;
use App\Http\Requests\Ordergetrequest;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Product;
use App\Models\Orderstimes;
use App\Models\order;
use App\Models\orderhistory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class Agencycontroller extends BaseController
{
    public function Addproduct(ProductRequest $request)
    {
        $Product= Product::where(['name'=>$request->name,'agency_id'=>$request->agency_id])->first();
        if($Product!=null){
               return $this->ErrorResponse(__('already added.'));  
        }
        $Product = Product::forceCreate($this->formatParams($request->all()));
        return $this->SuccessResponse($Product, __('Product Added.'));
    }
    public function Editproduct(EditProductRequest $request)
    {
        $Input=$request->except('id');
        $Product = Product::where('id',$request->id)->update($Input);
        if($Product){
            $Product=Product::find($request->id);
            return $this->SuccessResponse($Product, __('Product Updated.'));
        }
        return $this->ErrorResponse(__('Product not updated.'));  
    }
    public function getProduct(getAgencyrequest $request)
    {
        $Product= Product::where($request->all())->orderBy('name','asc')->get();
        if($Product){
               return $this->SuccessResponse($Product, __('Messages.Product_details'));
        }
        return $this->ErrorResponse(__('Product not found.')); 
    }
    public function getRetailer(getAgencyrequest $request)
    {
        $retailer= User::where('user_type','retailer')
        ->whereHas('retailers', function (Builder $query) use ($request)  {
            $query->where($request->all());
        })->get();
        if($retailer){
               return $this->SuccessResponse($retailer, __('Messages.Retailer_details'));
        }
        return $this->ErrorResponse(__('Retailer not found.')); 
    }

    public function DeleteRetailer(getAgencyrequest $request)
    {
        $retailer=Orderstimes::where($request->all())->delete();
        if($retailer){
               return $this->SuccessResponse($retailer, __('Retailer Deleted'));
        }
        return $this->ErrorResponse(__('Retailer Not Deleted.')); 
    }
    public function getOrder(getAgencyrequest $request)
    {
        $where=$request->except('ordertype');
        $order= order::where($where)->get();
        if($request->ordertype=='Today'){
            $order= order::whereDate('created_at', '=', date('Y-m-d'))->where($where)->get();
        }  
        if($order->isEmpty()){
               return $this->ErrorResponse(__('Order Not found'));  
        }else{
            return $this->SuccessResponse($order, __('Order Details'));
        } 
    }

    public function ChnageOrder(Ordergetrequest $request)
    {
        $where=$request->id;
        $order= order::find($where);
        if(!$order){
               return $this->ErrorResponse(__('Order Not found'));  
        }else{
            $order->status='Delivery';
            $order->save();

            $user=user::find($order->retailer_id);
            $agency=user::find($order->agency_id);
            if($user->fcm_token!=''){
                  $addtion=  array('Type'=>'New Order');
                  $msg = array
                            (
                              'body'  => 'Order Delivery From: '.$agency->shop_name,
                              'title' => 'Order Delivery'
                            );
                  $this->send_notification($msg,$user->fcm_token,$addtion);       
             }
            return $this->SuccessResponse($order, __('Order Delivered'));
        } 
    }
    public function Ordersummary(getAgencyrequest $request)
    {
        $where=$request->all();
        $order= orderhistory::with('prododucts')->whereHas('orders', function (Builder $query) use ($where) {
            $query->whereDate('created_at', date('Y-m-d'))->where($where);
        })->selectRaw('sum(crate_qty) as crate_qty, sum(single_item_price) as single_item_price,product_id, sum(cost) as cost')->groupBy('product_id')->get();
        if($order->isEmpty()){
               return $this->ErrorResponse(__('Order Summary Not found'));  
        }else{
            return $this->SuccessResponse($order, __('Order Summary'));
        } 
    }

    public function Editorder(Request $request) //Orderrequest
    {
        $prodcutid=$request->Products;
        foreach($prodcutid as $Prodkey=>$Prodalue){
            
               $ProdcutData=Product::find($Prodalue['product_id']);
               $cost=$ProdcutData->retailer_price*$Prodalue['crate_qty'];
               $cost=$cost+ ($ProdcutData->single_item_price*$Prodalue['single_item_price']);
               $Prodalue['cost']=$cost;
               orderhistory::where('id',$Prodalue['id'])->update($Prodalue);
               
        }
        return $this->ErrorResponse(__('Order Updated'));  
    }
    public function send_notification($msg,$usertoken,$data)
    {
       
        $fields = array
                    (
                        'to'        => $usertoken,
                        'notification'  => $msg,
                        'data'=>$data
                    );
        $headers = array
                    (
                        'Authorization: key=' . 'AAAAiRwRRGk:APA91bFwTEV0n4MOuJDioh_0uJl_SlzFD--fzHTusuelJno7v1ctTD9F6po6k961IMFCmmuOR2DQbnNDMCm6KiPFBJhXoq5_9tIO1bzZY7uTM7bDSyGTw4n0_4PrcqJ9XaqPobO86WLy',
                        'Content-Type: application/json'
                    );
            
            #Send Reponse To FireBase Server    
            $ch = curl_init();
            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
            $result = curl_exec($ch );
            curl_close( $ch );
    }

    protected function formatParams($params){
        
        return [
            'agency_id'=>!empty($params['agency_id']) ? $params['agency_id'] : "",
            'name'=>!empty($params['name']) ? $params['name'] : "",
            'ada_price'=>!empty($params['ada_price']) ? $params['ada_price'] : "",
            'retailer_price'=>!empty($params['retailer_price']) ? $params['retailer_price'] : "",
            'crate_qty'=>!empty($params['crate_qty']) ? $params['crate_qty'] : "",
            'single_item_price'=>!empty($params['single_item_price']) ? $params['single_item_price'] : "",
        ];
    }

    protected function fillable(){
         return  ['agency_id','name','ada_price','single_item_price','crate_qty','retailer_price'];

    }
}
