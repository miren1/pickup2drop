<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\api\Base\BaseController;
use Illuminate\Http\Request;
use App\Http\Requests\UserStoreRequest;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use PHPUnit\Exception;

class UserController extends BaseController
{
    public function GetProfile(Request $request)
    {
        $user= User::with(['defaultHomeAddresses', 'defaultWorkAddresses'])->find($request->id);
        if(!$user){
               return $this->ErrorResponse(__('User Not found'));
        }else{
            $user=$user->makeHidden(['email_verified_at', 'current_team_id','profile_photo_path']);
            if(!$user->referral_id)
            {
                $phone_number = $user->mobile_number;
                $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $referral_id = substr($phone_number,0,2). $characters[rand(0, strlen($characters) - 1)] . $characters[rand(0, strlen($characters) - 1)] .mt_rand(10, 99) . $characters[rand(0, strlen($characters) - 1)] . $characters[rand(0, strlen($characters) - 1)] . substr($phone_number,8);

                User::where('id',$user->id)->update(['referral_id'=>$referral_id]);
                $user['referral_id'] = $referral_id;
            }

            $wallet = Wallet::where('type','by_referral')->first();
            if($wallet)
            {
                $user['referral_message'] = "Referred";
                // $user['referral_message'] = $wallet->title;
            }
            return $this->SuccessResponse($user, __('User Details'));
        }
    }
    public function Updateuser(UserStoreRequest $request)
    {
        // dd($request->all());
        $Input=$request->except('id');
        $Input['birthdate']=date('Y-m-d',strtotime($request->birthdate));
        $file = $request->file('profile_photo_path');
        if($file){
            $photo = User::find($request->id);
            if($photo->profile_photo_path)
            {
                unlink(public_path($photo->profile_photo_path));
            }
            // $Input['profile_photo_path'] = $request->file('profile_photo_path')->storeAs(
            //     'avatars', $request->id.'.'.$file->extension()
            // );
            $Input['profile_photo_path'] = $request->file('profile_photo_path')->move(
                'avatars', strtotime('now').'_'.$request->id.'.'.$file->extension()
            );
        }
      //Move Uploaded File
        $user= User::where('id',$request->id)->update($Input);
        if(!$user){
               return $this->ErrorResponse(__('User Not found'));
        }else{
            $user=User::with(['defaultHomeAddresses', 'defaultWorkAddresses'])->find($request->id);
            $user=$user->makeHidden(['email_verified_at', 'current_team_id','profile_photo_path']);
            // $user['photo'] = url('public').'/'.$user->profile_photo_path;
            return $this->SuccessResponse($user, __('Profile  Updated'));
        }
    }
    public function resetPassword(Request $request)
    {
        try {
            $existUser = User::where('id', auth()->user()->id)->where('type', 'Deliveryboy')->first();
            if ($existUser) {
            $updateDeliveryBoy = User::where('id', auth()->user()->id)->where('type', 'Deliveryboy')->update(['password' => Hash::make($request->password)]);
                if ($updateDeliveryBoy) {
                    $customer = User::where('id', auth()->user()->id)->where('type', 'Deliveryboy')->first();
                    return $this->SuccessResponse($customer, __('Password updated successfully'));
                }
            }else{
                return $this->ErrorResponse(__('User can not found'));
            }
        } catch (\Throwable $th) {
        }
    }
    public function customerUpdateProfile(Request $request)
    {
        try {
            $request->validate([
                'email'=> 'required|email:rfc,dns',
                'name'=>'required',
                'gender'=>'required',
                'birthdate'=>'required'
            ],[
                'email.required'=>'email is required',
                'email.email'=>'invalid email address',
                'name.required'=>"name is required",
                'gender.required'=>'Gender is required',
                'birthdate.required'=>'birthdate is required'
            ]);
            $existUser = User::where('id', auth()->user()->id)->where('type', 'Deliveryboy')->first();
            if ($existUser) {
                $arrData = array(
                    'name' => $request->name,
                    'email' => $request->email,
                    'gender' => $request->gender,
                    'birthdate' => Carbon::createFromFormat('d/m/Y', $request->birthdate)->format('Y-m-d'),
                );
                $updateDeliveryBoy = User::where('id', auth()->user()->id)->update($arrData);
                if ($updateDeliveryBoy) {
                    $customer = User::where('id', auth()->user()->id)->where('type', 'Deliveryboy')->first();
                    return $this->SuccessResponse($customer, __('User updated successfully'));
                }
            } else {
                return $this->ErrorResponse(__('User can not found'));
            }
        }catch(Exception $e){
            return $this->ErrorResponse(__($e->getMessage()));
        }
    }

    public function verifyDeliveryAgent(Request $request)
    {

        try{
            $request->validate([
                'mobile_number'=> 'required',
                'type'=>'required',

            ],[
                'mobile_number.required'=>'Mobile number is required',
                'type.required'=>"Type is required",
            ]);
            $verifyUser = User::where('mobile_number', $request->mobile_number)->where('type', $request->type)->first();
            if($verifyUser){
                return $this->SuccessResponse(__('Delivery agent exist'));
            }
            else{
                return $this->ErrorResponse(__('Delivery agent does not exist'));

            }
        }catch(Exception $e){
            return $this->ErrorResponse(__($e->getMessage()));
        }

    }
    public function resetAgentPassword(Request $request)
    {
        try {
            $request->validate([
                'mobile_number'=> 'required',
                'password'=>'required',

            ],[
                'mobile_number.required'=>'Mobile number is required',
                'password.required'=>"Password is required",
            ]);
            $existUser = User::where('mobile_number', $request->mobile_number)->first();
            if ($existUser) {
            $updateDeliveryBoy = User::where('mobile_number', $request->mobile_number)->update(['password' => Hash::make($request->password)]);
                if ($updateDeliveryBoy) {
                    return $this->SuccessResponse(('Password reset successfully'));
                }
            }else{
                return $this->ErrorResponse(__('User can not found'));
            }
        }catch(Exception $e){
            return $this->ErrorResponse(__($e->getMessage()));
        }
    }


	public function logout(Request $request){
		User::where('fcm_token',$request->fcm_token)->update(['fcm_token'=>'','delivery_agent_status'=>'0']);
	}
}
