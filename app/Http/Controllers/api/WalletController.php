<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\WalletHistory;
use App\Http\Requests\WalletHistoryStoreRequest;
use App\Http\Controllers\API\Base\BaseController;
use App\Models\User;
use App\Models\Address;
Use Alert;
use Razorpay\Api\Api;

class WalletController extends BaseController
{
    public function add(WalletHistoryStoreRequest $request)
    {
        $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
        $payment = $api->payment->fetch($request->payment_id);
        if($payment['status'] == 'authorized')
            $payment = $payment->capture(array('amount' => number_format($request->amount,2)*100, 'currency' => 'INR'));

        if($payment['status'] == 'captured') {
            $Wallet = WalletHistory::forceCreate($this->formatParams($request->all()));
            if ($Wallet) {
                if ($request->amount) {
                    User::where('id', $request->user_id)->increment('wallet', $request->amount);
                    $user = User::where('id', $request->user_id)->first();
                    return $this->SuccessResponse($user, __('Wallet Credit/debit Success.'));
                }
            }
        }
        return $this->ErrorResponse(__('Something went wrong.'));
    }

    public function view(Request $request)
    {
        $wallet = WalletHistory::where('user_id',$request->user_id)->orderBy('created_at', 'DESC')->get();
        if($wallet){
            return $this->SuccessResponse($wallet, __('Wallet Credit/debit List:'));
        }
        return $this->ErrorResponse(__('Something went wrong.'));
    }

    public function addresslist(Request $request)
    {
        $address = Address::where('user_id',$request->user_id)->where('is_delete',0)->orderBy('created_at', 'desc')->get();
        if($address){
            return $this->SuccessResponse($address, __('Address List:'));
        }
        return $this->ErrorResponse(__('Something went wrong.'));
    }


    public function deleteaddress(Request $request)
    {
       // dd($request->all());
        $address = Address::where('id',$request->address_id)->update(['is_delete'=>1]);
        //dd($wallet);
        if($address){
            return $this->SuccessResponse($address, __('Address has been deleted.'));
        }
        return $this->ErrorResponse(__('Something went wrong.'));
    }

    protected function formatParams($params)
    {
        return [
            'title'=>!empty($params['title']) ? $params['title'] : "",
            'amount'=>!empty($params['amount']) ? $params['amount'] : "",
            'status'=>!empty($params['status']) ? $params['status'] : "",
            'payment_id'=>!empty($params['payment_id']) ? $params['payment_id'] : "",
            'user_id'=>!empty($params['user_id']) ? $params['user_id'] : "",
        ];
    }

    protected function fillable()
    {
         return  ['title','amount','status','payment_id','user_id'];
    }
}
