<?php

namespace App\Http\Controllers\api;

use App\Helpers\PushNotification;
use App\Models\City;
use App\Models\Pickup;
use App\Models\Address;
use App\Models\PaymentContact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\PickupIdRequest;
use App\Http\Requests\DeliveryBoyIdRequest;
use App\Http\Controllers\API\Base\BaseController;
use Notification;
use Illuminate\Support\Facades\Log;
use App\Models\User;
use App\Models\PaymentFund;
use App\Models\AutoTransactionHistory;
use App\Models\Delivery_fcm_token;
use App\Models\DeliveryboyWalletHistory;
use App\Models\DriverDocument;
use App\Models\GeneralSetting;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class DeliveryboyController extends BaseController
{
    public function delivetyboyacceptrequest(Request $request)
    {
        $pickup = Pickup::with('boy','customer')->find($request->pickup_id);
        $body = 'Your ' . ucfirst($pickup->type) . ' Item ' . $request->status;
        if($request->status == 'Delivered')
            $body = 'Your item has been delivered';
        else if($request->status == 'Out Of Delivery')
            $body = 'Your item is out for delivery';
        else if($request->status == 'Picked Up')
            $body = 'Your item has been picked up';
        else if($request->status == 'Out Of Pickup')
            $body = 'The driver is on his way to pick up your item';
        else if($request->status == 'Request Accept')
            $body = 'Your pickup request is accepted';

        if($pickup->deliveryboy_id)
        {
            if($request->status !="Request Accept" && $pickup->status !="Cancel Request")
            {
                $update = Pickup::where('id',$request->pickup_id)->update(['status'=>$request->status]);
                if($update)
                {
                    $pickup = Pickup::with('Pickupaddress','Dropaddress','customer','customer')->find($request->pickup_id);
                    if($pickup->status == 'Delivered')
                    {
                        $walletAdd = User::where('id',$request->deliveryboy_id)->first();
                        // dd($pickup->pickaddress_id);
                        $arrayData = ['user_id'=>$request->user()->id,'order_id'=>$pickup->id,'amount'=>$pickup->order_commission,'status'=>'credit','pickaddress_id'=>$pickup->pickaddress_id,'dropaddress_id'=>$pickup->dropaddress_id,'pincode'=>$pickup->pincode,'deliveryboy_id'=>$pickup->deliveryboy_id];
                        $data = DeliveryboyWalletHistory::create($arrayData);
                        if($data){
                            $walletAdd->wallet = ($pickup->order_commission + $walletAdd->wallet);
                            $its_save = $walletAdd->save();
                        }
                    }
                    $addtion=  array
                        (
                            'Type'=>$request->status,
                            'user'=>$pickup->customer->name,
                        );
                    $msg = array
                        (
                            // 'body'  =>  'Your '.ucfirst($pickup->type).' Item '. $request->status,
                            'body'  =>  $body,
                            // 'title' => $request->status,
                        );
                    PushNotification::Push($msg,$pickup->customer->fcm_token,$addtion);
                    return $this->SuccessResponse($pickup, __('Request Status Updated'));
                }
                else
                {
                    return $this->SuccessResponse($pickup, __('Request Status not Update try agian..'));
                }
            }
            else if($pickup->status ==="Cancel Request")
            {
                return $this->ErrorResponse(__('Request was Cancelled !'));
            }else{
                $deliveryboy = Pickup::where('id',$request->pickup_id)->update(['deliveryboy_id'=>$request->deliveryboy_id,'status'=>$request->status]);
                if ($deliveryboy)
                {
                    $delivery = Pickup::with('Pickupaddress','Dropaddress','boy','image')->find($request->pickup_id);
                    $addtion=  array
                            (
                                'Type'=>$request->status,
                                'user'=>$pickup->customer->name,
                            );
                    $msg = array
                        (
                            'body'  =>  $body,
                            // 'body'  => 'Your '.ucfirst($delivery->type).' '.$request->status,
                            // 'title' => $request->status,
                        );
                    PushNotification::Push($msg,$pickup->customer->fcm_token,$addtion);
                    return $this->SuccessResponse($delivery, __('Request Accepted successfully'));
                }
                else {
                    return $this->ErrorResponse(__('Try Again...'));
                }
            }

        }
        else if($pickup->status ==="Cancel Request")
        {
            return $this->ErrorResponse(__('Request was Cancelled !'));
        }
        else
        {
            $deliveryboy = Pickup::where('id',$request->pickup_id)->update(['deliveryboy_id'=>$request->deliveryboy_id,'status'=>$request->status]);
            if ($deliveryboy)
            {
                $delivery = Pickup::with('Pickupaddress','Dropaddress','boy','image')->find($request->pickup_id);
                $addtion=  array
                        (
                            'Type'=>$request->status,
                            'user'=>$pickup->customer->name,
                        );
                $msg = array
                    (
                        'body'  =>  $body,
                        // 'body'  => 'Your '.ucfirst($delivery->type).' '.$request->status,
                        // 'title' => $request->status,
                    );
                PushNotification::Push($msg,$pickup->customer->fcm_token,$addtion);
                return $this->SuccessResponse($delivery, __('Request Accepted successfully'));
            }
            else {
                return $this->ErrorResponse(__('Try Again...'));
            }
        }
    }

    public function deliveryboypickuplist(DeliveryBoyIdRequest $request)
    {
        $pickup = Pickup::with(['Pickupaddress','Dropaddress','customer', 'image' => function ($q) {
            $q->whereHas('pickup', function ($q2) {
                $q2->where('type', 'medicine');
            });
        }])->where('deliveryboy_id',$request->deliveryboy_id)
        ->where('status','!=','Delivered')
        ->where('type','!=','tiffin')
        ->where('status','!=','pickup Requested')
        ->where('status','!=','Cancel Request')->orderBy('id','DESC')->get();
        // dd($pickup);
        return $this->SuccessResponse($pickup, __('Delivery boy Pickups'));
    }

    public function deliveryboypickupdetail(PickupIdRequest $request)
    {
        $pickup = Pickup::with(['Pickupaddress','Dropaddress','customer','image'=> function($q){
            $q->whereHas('pickup',function($q2){
                $q2->where('type', 'medicine');
            });
        }])->find($request->pickup_id);

        if ($pickup)
        {
            return $this->SuccessResponse($pickup, __('Deliveryboy Pickup Details'));
        }
        else
        {
            return $this->ErrorResponse(__('Deliveryboy Pickup Not Found'));
        }
    }

    public function pickupnotaccept(DeliveryBoyIdRequest $request)
    {
		$user = User::where(['id' => $request->deliveryboy_id, 'type' => 'Deliveryboy', 'is_delete' => 'false'])->first();

            if($user->delivery_agent_status == 0){
                $pickup=[];
                return $this->SuccessResponse($pickup, __('Pickup not accept'));
            }
            $pickups = Pickup::with(['Pickupaddress','areas','Dropaddress','customer', 'image' => function ($q) {
                $q->whereHas('pickup', function ($q2) {
                    $q2->where('type', 'medicine');
                });
            }])
            ->where('type','!=','tiffin')
            ->where('status','=','pickup Requested')
            ->where('dispute_status','Not Add')
            ->orderBy('id','DESC')
            ->get();

            // Set dynamic raidus for delivery agent
            $radius = GeneralSetting::where('name', 'Covered Area')->first();
            if ($radius)
                $radius =  $radius->value;

            $radius = $radius ? (int)$radius : 10;
            $pickup = [];
            foreach($pickups as $pickuporder){
                $distance = calculate_distance($user->latitude, $user->longitude, $pickuporder->Pickupaddress->latiude, $pickuporder->Pickupaddress->longitude);
                    if ($distance > 0 && $distance < $radius) {
                        $pickup[] = $pickuporder;
                    }
            }

        return $this->SuccessResponse($pickup, __('Pickup not accept'));
    }

    public function deliveryboypaymentstatus(PickupIdRequest $request)
    {
        // return $request->all();
        $data = Pickup::where(['id'=>$request->pickup_id,'payment_type'=>'cash'])->update(['amount'=>$request->amount,'payment_status'=>$request->payment_status]);
        if ($data)
        {
            $pickup = Pickup::with('Pickupaddress','Dropaddress','customer')->find($request->pickup_id);
            return $this->SuccessResponse($pickup,__('payment status updated'));
        }else
        {
            return $this->ErrorResponse(__('Payment method is online'));
        }
    }

    public function deliveryboycompletepickup(DeliveryBoyIdRequest $request)
    {
        $pickup = Pickup::with('Pickupaddress','Dropaddress','customer','image')
                ->where('deliveryboy_id',$request->deliveryboy_id)
                ->where('status','Delivered')
                ->orderBy('updated_at','DESC')->get();

        return $this->SuccessResponse($pickup, __('Delivery boy Complete Pickups'));
    }

    public function Boy_Tiffin_List(DeliveryBoyIdRequest $request)
    {
        $tiffin = Pickup::with('Pickupaddress','Dropaddress','customer')
                        ->where('deliveryboy_id',$request->deliveryboy_id)
                        ->where('type','tiffin')
                        ->orderBy('id','DESC')
                        ->get();
        return $this->SuccessResponse($tiffin,__('Subscription List'));
    }

    public function Boy_Tiffin_Detail(PickupIdRequest $request)
    {
        $tiffin = Pickup::with('Pickupaddress','Dropaddress','customer')
        ->where(['id'=>$request->pickup_id,'type'=>'tiffin'])->get();

        return $this->SuccessResponse($tiffin,__('Subscription Details'));
    }

    public function webhookResponse(Request $request)
    {
        if($request->event == 'payout.pending' || $request->event == 'payout.queued')
        {
            $data = PaymentFund::where('payout_id',$request->payload['payout']['entity']['id'])->first();
            if($data)
            {
                $userUpdate = User::where('id',$data->user_id)->update(['bank_details_status'=>'Pending']);
                $data->payout_status = $request->payload['payout']['entity']['status'];
                $data->account_verify = 'pending';
                $its_save = $data->save();
                if($its_save)
                {
                    return response([],200);
                }
            }else{
                $transaction = AutoTransactionHistory::where('transaction_id',$request->payload['payout']['entity']['id'])->first();
                if($transaction)
                {
                    if($request->payload['payout']['entity']['status'] == 'pending' || $request->payload['payout']['entity']['status'] == 'queued'  )
                    {
                        $transaction->status = 'Pending';
                        $its_save = $transaction->save();
                        if ($its_save) {
                            return response([],200);
                        }
                    }
                }
            }
        }

        if($request->event == 'payout.reversed' || $request->event == 'payout.failed' || $request->event == 'payout.rejected')
        {
            $data = PaymentFund::where('payout_id',$request->payload['payout']['entity']['id'])->first();
            if($data)
            {
                //$userUpdate = User::where('id',$data->user_id)->update(['bank_details_status'=>'Not Verified']); // if error than uncomment this code and comment below line
				$userUpdate = User::where('id',$data->user_id)->update(['bank_details_status'=>'Not add']);
                $data->payout_status = $request->payload['payout']['entity']['status'];
                $data->account_verify = 'not_verified';
                $its_save = $data->save();
                if($its_save)
                {
					//code add for invalid account start
					$fcm = User::where('id', $data->user_id)->first();
					$addtion =  array(
					  'Type' => 'Invalid Bank Account',
					  'user' => $fcm->name,
					);
					$msg = array(
					  'body'  => 'Bank account verification failed due to invalid beneficiary account details',
					  'title' => 'Invalid Bank Account',
					);
    				PushNotification::Push($msg, $fcm->fcm_token, $addtion);
					//code add for invalid account end


                    return response([],200);
                }
            }else{
                $transaction = AutoTransactionHistory::where('transaction_id',$request->payload['payout']['entity']['id'])->first();
                if($transaction)
                {
                    if ($request->payload['payout']['entity']['status'] == 'failed' || $request->payload['payout']['entity']['status'] == 'rejected' || $request->payload['payout']['entity']['status'] == 'reversed')
                    {
                        $transaction->status = 'Failed';
                        User::where('id',$transaction->user_id)->update(['wallet'=>($transaction->amount+$transaction->tds)]);
                        $its_save = $transaction->save();
                        if ($its_save) {
                            return response([],200);
                        }
                    }
                }
            }
        }

        if($request->event == 'payout.processed')
        {
            if($request->payload['payout']['entity']['status'] == 'processed'){
                $data = PaymentFund::where('payout_id',$request->payload['payout']['entity']['id'])->first();
                if($data)
                {
                    $userUpdate = User::where('id',$data->user_id)->update(['bank_details_status'=>'Verified']);
                    $data->payout_status = $request->payload['payout']['entity']['status'];
                    $data->account_verify = 'verified';
                    $its_save = $data->save();
                    if($its_save)
                    {
                        return response([],200);
                    }
                }else{
                    $transaction = AutoTransactionHistory::where('transaction_id',$request->payload['payout']['entity']['id'])->first();
                    if($transaction)
                    {
                        $transaction->status = 'Completed';
                        $its_save = $transaction->save();
                        if($its_save)
                        {
                            return response([], 200);
                            // $dataUpdate = User::where('id',$transaction->user_id)->first();
                            // $dataUpdate->wallet = ((int)$dataUpdate->wallet - (int)$transaction->amount);
                            // $its_update = $dataUpdate->save();
                            // if($its_update){
                            //    return response([],200);
                            // }
                        }
                    }

                }
            }
        }

        // if($request->event == 'payout.pending' ||  $request->event == 'payout.rejected'|| $request->event == 'payout.queued' || $request->event == 'payout.initiated' || $request->event == 'payout.processed' || $request->event == 'payout.updated' || $request->event == 'payout.reversed' || $request->event == 'payout.failed')
        // {
        //     if($request->event == 'payout.initiated' || $request->event == 'payout.queued' || $request->event == 'payout.failed' || $request->event == 'payout.rejected' || $request->event == 'payout.reversed')
        //     {
        //         $data = PaymentFund::where('payout_id',$request->payload['payout']['entity']['id'])->first();
        //         if($data)
        //         {
        //             $userUpdate = User::where('id',$data->user_id)->update(['bank_details_status'=>'Not Verified']);
        //             $data->payout_status = $request->payload['payout']['entity']['status'];
        //             $data->account_verify = 'not_verified';
        //             $its_save = $data->save();
        //             if($its_save)
        //             {
        //                 return response([],200);
        //             }
        //         }else{
        //             $transaction = AutoTransactionHistory::where('transaction_id',$request->payload['payout']['entity']['id'])->first();
        //             if($transaction)
        //             {
        //             if($request->payload['payout']['entity']['status'] == 'pending' || $request->payload['payout']['entity']['status'] == 'queued'  ){
        //                     $transaction->status = 'Pending';
        //                     $its_save = $transaction->save();
        //                     if ($its_save) {
        //                         return response([],200);
        //                     }
        //                 }else if ($request->payload['payout']['entity']['status'] == 'failed' || $request->payload['payout']['entity']['status'] == 'rejected' || $request->payload['payout']['entity']['status'] == 'reversed'){
        //                     $transaction->status = 'Failed';
        //                     $its_save = $transaction->save();
        //                     if ($its_save) {
        //                          return response([],200);
        //                     }
        //                 }
        //             }
        //         }
        //     }
        //     if($request->payload['payout']['entity']['status'] == 'processed'){
        //         $data = PaymentFund::where('payout_id',$request->payload['payout']['entity']['id'])->first();
        //         if($data)
        //         {
        //             $userUpdate = User::where('id',$data->user_id)->update(['bank_details_status'=>'Verified']);
        //             $data->payout_status = $request->payload['payout']['entity']['status'];
        //             $data->account_verify = 'verified';
        //             $its_save = $data->save();
        //             if($its_save)
        //             {
        //                 return response([],200);
        //             }
        //         }else{
        //             $transaction = AutoTransactionHistory::where('transaction_id',$request->payload['payout']['entity']['id'])->first();
        //             if($transaction)
        //             {
        //                 $transaction->status = 'Completed';
        //                 $its_save = $transaction->save();
        //                 if($its_save)
        //                 {
        //                     $dataUpdate = User::where('id',$transaction->user_id)->first();
        //                     $dataUpdate->wallet = ((int)$dataUpdate->wallet - (int)$transaction->amount);
        //                     $its_update = $dataUpdate->save();
        //                     if($its_update){
        //                        return response([],200);
        //                     }
        //                 }
        //             }
        //         }

        //     }
        // }
    }


    public function objectGet(Request $request)
    {
        // dd($request->user());
        $token = base64_decode($request->bearerToken());
        $dataToken = json_decode($token);
        $data = User::with('driverdoc')->where('id',$dataToken[0])->first();
        if($data)
        {
            return $this->SuccessResponse($data,__('Deliveryboy Details'));
        }else{
            return $this->ErrorResponse(__('Deliveryboy Not Found'));
        }

    }

     public function deliveryboylocation(Request $request)
    {
        $token = base64_decode($request->bearerToken());
        $dataToken = json_decode($token);
        $userdata = User::where('id',$dataToken[0])->first();
        // dd($userdata);
        if($userdata)
        {
            $data = [

                "latitude"=>$request->latitude,
                "longitude"=>$request->longitude,
            ];

            $user = User::where('id',$dataToken[0])->update($data);

            if($user)
            {
                return $this->SuccessResponse($user,__('Deliveryboy Location Updated'));
            }else{
                return $this->ErrorResponse(__('Deliveryboy Not Updated'));
            }

        }else{
            // dd('df');
            $data = array('error'=>true, 'statusCode'=>500, 'message'=>'Deliveryboy Not Found');
            // return $this->ErrorResponse(__('Deliveryboy Not Found'));
            return response()->json($data);
        }
        // dd($dataToken);

    }
    public function DeliveryBoyRegistration(Request $request)
    {
        $exist_user = User::where('mobile_number', $request->mobile_number)->where('type', 'Deliveryboy')->where('is_delete', 'false')->first();
        if($exist_user){
            return $this->ErrorResponse(__('Delivery Agent already registered'));
        }

        $arrData = array(
            'name' => $request->name,
            'email' => $request->email,
            'type' => $request->type,
            'gender' => $request->gender,
            'city_id' => $request->city_id?:NULL,
            'mobile_number' => $request->mobile_number,
            'password' => Hash::make($request->password),
            'birthdate' => Carbon::createFromFormat('d/m/Y',$request->birthdate)->format('Y-m-d'),
        );

        $user = User::create($arrData);

        DriverDocument::create([
            'driver_id' => $user->id,
            'driver_status' => 'pending'
        ]);

		$curl = curl_init();
		$login = env('RAZORPAY_X_KEY_ID');
		$password = env('RAZORPAY_X_SECRET_KEY');
		$headers = array(
			'Content-Type:application/json',
			'Authorization: Basic ' . base64_encode("$login:$password"),
		);
		$post_fields = [
			"name" => $request->name,
			"email" => $request->email,
			"contact" => $request->mobile_number,
			"type" => "customer"
		];
		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://api.razorpay.com/v1/contacts',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => json_encode($post_fields),
			CURLOPT_HTTPHEADER => $headers,
		));

		$response = curl_exec($curl);
		$statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);
		$resData = json_decode($response);

		if ($statusCode == 200 || $statusCode == 201) {

			$driver_id = $user->id;
			User::where('id', $user->id)->update(['payment_contact_id' => $resData->id]);
			//$mailsent = Mail::to($request->email)->send(new NotificationMail($arrData));
			$mailsent = [];
			if(true || ($mailsent && $mailsent['status'] == 202)){
				if (true) {
					$contactData = new PaymentContact;
					$contactData->user_id = $driver_id;
					$contactData->contactId = $resData->id;
					$contactData->name = $resData->name;
					$contactData->email = $resData->email;
					$contactData->contact = $resData->contact;
					$its_update = $contactData->save();
					if ($its_update) {
						User::where('id', $contactData->user_id)->update(['payment_contact_id' => $resData->id]);
					}
				}
			} else {
				return $this->ErrorResponse(__('Delivery Agent not added.'));
			}
		} else {
			return $this->ErrorResponse(__('Something went wrong.'));
		}

        $token = base64_encode(json_encode([$user->id, $user->mobile_number, $user->created_at]));
        $user->forceFill([
            'api_token' => hash('sha256', $token),
            'fcm_token' => $request->fcm_token
        ])->save();

        $dbfcm = array(
            'deliveryboy_id' => $user->id,
            'fcm_token' => $request->fcm_token,
            'deliveryboy_name' => $user->name
        );
        // $exist = Delivery_fcm_token::where(['deliveryboy_id' => $user->id, 'fcm_token' => $request->fcm_token])->first();
        // if ($exist)
        //     Delivery_fcm_token::where('deliveryboy_id', $user->id)->update(['fcm_token' => $request->fcm_token, 'deliveryboy_name' => $user->name]);
        // else {
        //     Delivery_fcm_token::create($dbfcm);
        // }

        if ($request->address) {
            $Address['address'] = $request->address;
            $Address['type'] = 'home';
            $Address['user_id'] = $user->id;

            $address_id = Address::create($Address)->id;
            User::where('id', $user->id)->update(['home_address' => $address_id]);
        }

        if($user){
            $admins = User::where(['is_delete'=>'false', 'type'=> 'Admin'])->get();
            foreach($admins as $admin){
                $driverdata['message'] = "New driver created " . $request->name .".";
                $admin->notify(new \App\Notifications\NewDriverNotification($driverdata));
            }
            $user = User::with('defaultHomeAddresses')->find($user->id);

            return $this->SuccessResponse([$user, "token"=>$token], __('Deliveryboy Created'));
        }else{
            return $this->ErrorResponse(__('User not found'));
        }
    }

    public function DeliveryBoyDocumentsUpload(Request $request){

        try {
            ini_set('upload_max_filesize','-1');
            ini_set('post_max_size','-1');
            $driver_id = auth()->user()->id;

            $exist_driver_doc = DriverDocument::where('driver_id', $driver_id)->first();
            $driver_doc['vehicle_number'] = $request->vehicle_number ? $request->vehicle_number : null;
            $driver_doc['vehicle_model'] = $request->vehicle_model ? $request->vehicle_model : null;
            $driver_doc['vehicle_colour'] = $request->vehicle_colour ? $request->vehicle_colour : null;
            $driver_doc['pan_number'] = $request->pan_number ? $request->pan_number : null;
            $driver_photo = null;

            if ($request->file('driver_photo')) {
                if (isset($exist_driver_doc->driver_photo) && $exist_driver_doc->driver_photo && file_exists(public_path($exist_driver_doc->driver_photo))) {
                    unlink(public_path($exist_driver_doc->driver_photo));
                }
                $file = $request->file('driver_photo');
                $driver_photo = $request->file('driver_photo')
                ->move(
                    'driver',
                    strtotime('now') . '_driver_photo_' . $driver_id . '.' . $file->extension()
                );

                $driver_doc['driver_photo'] = $driver_photo->getPathName();
                $driver_doc['driver_photo_status'] = "pending";
                $driver_doc['driver_status'] = "pending";
            }

            if ($request->file('addhar_front')) {
                if (isset($exist_driver_doc->addhar_front) && $exist_driver_doc->addhar_front && file_exists(public_path($exist_driver_doc->addhar_front))) {
                    unlink(public_path($exist_driver_doc->addhar_front));
                }
                $file = $request->file('addhar_front');
                $addhar_front = $request->file('addhar_front')
                ->move(
                    'driver',
                    strtotime('now') . '_addhar_front_' . $driver_id . '.' . $file->extension()
                );

                $driver_doc['addhar_front'] = $addhar_front->getPathName();
                $driver_doc['addhar_front_status'] = "pending";
                $driver_doc['driver_status'] = "pending";
            }
            if ($request->file('addhar_back')) {
                if (isset($exist_driver_doc->addhar_back) && $exist_driver_doc->addhar_back && file_exists(public_path($exist_driver_doc->addhar_back))) {
                    unlink(public_path($exist_driver_doc->addhar_back));
                }
                $file = $request->file('addhar_back');
                $addhar_back = $request->file('addhar_back')
                ->move(
                    'driver',
                    strtotime('now') . '_addhar_back_' . $driver_id . '.' . $file->extension()
                );

                $driver_doc['addhar_back'] = $addhar_back->getPathName();
                $driver_doc['addhar_back_status'] = "pending";
                $driver_doc['driver_status'] = "pending";
            }
            if ($request->file('pan_card')) {
                if (isset($exist_driver_doc->pan_card) && $exist_driver_doc->pan_card && file_exists(public_path($exist_driver_doc->pan_card))) {
                    unlink(public_path($exist_driver_doc->pan_card));
                }
                $file = $request->file('pan_card');
                $pan_card = $request->file('pan_card')
                    ->move(
                        'driver',
                        strtotime('now') . '_pan_card_' . $driver_id . '.' . $file->extension()
                    );

                $driver_doc['pan_card'] = $pan_card->getPathName();
                $driver_doc['pan_card_status'] = "pending";
                $driver_doc['driver_status'] = "pending";
            }
            if ($request->file('tds_certificate')) {
                if (isset($exist_driver_doc->tds_certificate) && $exist_driver_doc->tds_certificate && file_exists(public_path($exist_driver_doc->tds_certificate))) {
                    unlink(public_path($exist_driver_doc->tds_certificate));
                }
                $file = $request->file('tds_certificate');
                $tds_certificate = $request->file('tds_certificate')
                ->move(
                    'driver',
                    strtotime('now') . '_tds_certificate_' . $driver_id . '.' . $file->extension()
                );

                $driver_doc['tds_certificate'] = $tds_certificate->getPathName();
                $driver_doc['tds_certificate_status'] = "pending";
                $driver_doc['driver_status'] = "pending";
            }
            if ($request->file('rc_front')) {
                if (isset($exist_driver_doc->rc_front) && $exist_driver_doc->rc_front && file_exists(public_path($exist_driver_doc->rc_front))) {
                    unlink(public_path($exist_driver_doc->rc_front));
                }
                $file = $request->file('rc_front');
                $rc_front = $request->file('rc_front')
                    ->move(
                        'driver',
                        strtotime('now') . '_rc_front_' . $driver_id . '.' . $file->extension()
                    );

                $driver_doc['rc_front'] = $rc_front->getPathName();
                $driver_doc['rc_front_status'] = "pending";
                $driver_doc['driver_status'] = "pending";
            }
            if ($request->file('rc_back')) {
                if (isset($exist_driver_doc->rc_back) && $exist_driver_doc->rc_back && file_exists(public_path($exist_driver_doc->rc_back))) {
                    unlink(public_path($exist_driver_doc->rc_back));
                }
                $file = $request->file('rc_back');
                $rc_back = $request->file('rc_back')
                    ->move(
                        'driver',
                        strtotime('now') . '_rc_back_' . $driver_id . '.' . $file->extension()
                    );

                $driver_doc['rc_back'] = $rc_back->getPathName();
                $driver_doc['rc_back_status'] = "pending";
                $driver_doc['driver_status'] = "pending";
            }
            if ($request->file('insurance_certificate')) {
                if (isset($exist_driver_doc->insurance_certificate) && $exist_driver_doc->insurance_certificate && file_exists(public_path($exist_driver_doc->insurance_certificate))) {
                    unlink(public_path($exist_driver_doc->insurance_certificate));
                }
                $file = $request->file('insurance_certificate');
                $insurance_certificate = $request->file('insurance_certificate')
                ->move(
                    'driver',
                    strtotime('now') . '_insurance_certificate_' . $driver_id . '.' . $file->extension()
                );

                $driver_doc['insurance_certificate'] = $insurance_certificate->getPathName();
                $driver_doc['insurance_certificate_status'] = "pending";
                $driver_doc['driver_status'] = "pending";
            }

            if($exist_driver_doc){
                DriverDocument::where('driver_id', $driver_id)->update($driver_doc);
                $driver = User::with('driverdoc')->where('id', $driver_id)->first();
                return $this->SuccessResponse($driver, __('Deliveryboy documents updated'));
            }else{
                $driver_doc['driver_id'] = $driver_id;
                //$driver_doc['driver_status'] = "deactivate";
				$driver_doc['driver_status'] = "pending";
                DriverDocument::create($driver_doc)->id;
                $driver = User::with('driverdoc')->where('id', $driver_id)->first();
                return $this->SuccessResponse($driver, __('Deliveryboy documents created'));
            }
        } catch (\Throwable $th) {
            Log::info(print_r($th->getMessage(),1));
            return $this->ErrorResponse(__($th->getMessage()));
        }
    }

    public function getDeliveryAgentProfile(){
        $driver = User::with(['driverdoc'])->where(['id'=> auth()->user()->id,'type'=> 'Deliveryboy','is_delete'=>'false'])->first();
        if($driver){
            return $this->SuccessResponse($driver, __('Deliveryboy Profile'));
        }else{
            return $this->ErrorResponse(__("Delivery agent not found"));
        }
    }

    public function DeliveryAgentStatus(Request $request)
    {
        try {
            $request->validate([
                'status' => 'required'
            ]);
            $userCheck = DriverDocument::where(['driver_id' => auth()->user()->id])->first();
            if($userCheck){
                if($userCheck->status == 'suspended'){
                    $user = User::where(['id' => auth()->user()->id, 'type' => 'Deliveryboy', 'is_delete' => 'false'])->update(['delivery_agent_status' => '0']);
                    return $this->ErrorResponse(__('Your account has been suspended, Please contact your administrator to keep this application working.'));
                }
                if($userCheck->status == 'deactive'){
                    $user = User::where(['id' => auth()->user()->id, 'type' => 'Deliveryboy', 'is_delete' => 'false'])->update(['delivery_agent_status' => '0']);
                    return $this->ErrorResponse(__('Your account has been deactivted, Please contact your administrator to keep this application working.'));
                }
            }
            $current_status = ($request->status==0)?"offline":"online";
            $user = User::where(['id' => auth()->user()->id, 'type' => 'Deliveryboy', 'is_delete' => 'false'])->update(['delivery_agent_status' => $request->status]);
            if ($user) {
                $user = User::where(['id' => auth()->user()->id, 'type' => 'Deliveryboy', 'is_delete' => 'false'])->first();
                return $this->SuccessResponse($user, __('You are now '.$current_status));
            } else {
                return $this->ErrorResponse(__('Something went wrong'));
            }
        } catch (\Throwable $th) {
            return $this->ErrorResponse(__($th->getMessage()));
        }
    }

    public function SubscriptionPrice(Request $request){
        $diff = Carbon::parse($request->start_date)->diffInDays(Carbon::parse($request->end_date)) + 1;
            $distance = calculate_distance($request->pickaddress[0]['latiude'], $request->pickaddress[0]['longitude'], $request->dropaddress[0]['latiude'], $request->dropaddress[0]['longitude']);
            $distance = (string)$distance;
            $price =  \App\Models\Price::where('pickuptype', 'subscription')->where('min_km', '<=', $distance)->where('max_km', '>=', $distance)->first();
            if ($price) {
                $getprice = $price->price;
                if($price->status_per_km == "True"){
                    $getprice = (int)$getprice * (int)$distance;
                }
                $price =  (int)$getprice * (int) $diff;
                return response()->json(["subscription_price" => $price, "status"=>200]);
            } else {
            return response('You have no subscription for ' . number_format($distance,2) . ' Km of range',401);
            }
    }

    public function getCity(){
        $cities = City::select('id','city_name')->orderBy('city_name')->get();
        if($cities)
            return response()->json(['success'=>'true','data'=>$cities],200);
        return response()->json(['success'=>'false','error'=>'no cities found.'],302);
    }

}
