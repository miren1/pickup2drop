<?php

namespace App\Http\Controllers\api;

use App\Helpers\PushNotification;
use Illuminate\Http\Request;
use App\Http\Controllers\api\Base\BaseController;
use App\Models\Delivery_fcm_token;
use App\Models\Dispute;
use Illuminate\Support\Facades\Log;
use App\Models\Pickup;
use App\Models\User;
use App\Models\DeliveryboyWalletHistory;
use App\Models\WalletHistory;
use Alert;

class DisputeController extends BaseController
{
    public function addDispute(Request $request)
    {
        $data = [
            'user_id' => $request->user()->id,
            'dispute' => $request->dispute,
            'order_id' => $request->order_id,
            'status' => 'Pending',
        ];

        $its_save = Dispute::create($data);
        if($its_save)
        {
            $updateStatus = Pickup::where('id',$request->order_id)->first();
            $updateStatus->dispute_status = 'pending';
            $its_update = $updateStatus->save();
            if($its_update){
                $admins = User::where(['is_delete'=>'false', 'type'=> 'Admin'])->get();
                foreach($admins as $admin){
                    $orderdata['message'] = "The order number #" . $request->order_id ." has been dispute!";
                    $orderdata['order_id'] = $request->order_id;
                    $orderdata['dispute_id'] = $its_save->id;
                    $admin->notify(new \App\Notifications\DisputeNotification($orderdata));
                }

                $customer = User::where('id',$request->user()->id)->first();
                $addtion =  array(
                    'Type' => "dispute",
                    'user' => $customer->name,
                );
                $msg = array(
                    'body'  =>  "Your order number #" . $request->order_id ." has been dispute!",
                );
                PushNotification::Push($msg, $customer->fcm_token, $addtion);

                return $this->SuccessResponse($data, __('Dispute add Successfully!'));
            }
        }else{
            return $this->ErrorResponse(__('Please try again!dispute not added'));
        }
    }

    public function index(Request $request)
    {
        if (!auth()->user()->can('Dispute-List')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        return view('dispute/index');
    }

    public function show(Request $request)
    {
        if (!auth()->user()->can('Dispute-List')) {
            return response()->json("Unauthorized access");
        }
        if ($request->ajax()) {

            // $data = Dispute::all();
            $data = Dispute::with('user')->orderBy('created_at','desc')->get();
            return \DataTables::of($data)
                    ->addColumn('mobile_number',function($row){
                        return $row->user->mobile_number ? $row->user->mobile_number : ' - ';
                    })
                    ->editColumn('created_at', function($row) {
                           $date = date('d-m-Y',strtotime($row->created_at));
                           return $date;
                    })
                    ->make(true);
        }
    }

    public function disputeCheckold(Request $request)
    {
        $dispute = Dispute::where('id', $request->dispute_id)->first();
        if($request->type == 'Valid'){
            $data = Pickup::where('id', $dispute->order_id)->first();
            if($data->original_amount < $request->amount){
                $message['message'] = "Enter amount is more than order amount !";
                $message['title'] = "Error !";
                $message['icon'] = "error";
                $message['status'] = "error";
                return response()->json($message);
            }
            $data->dispute_status = 'accept';
            $its_save = $data->save();
            if($its_save){
                $dispute = Dispute::where('id',$request->dispute_id)->update(['status'=>'Valid']);
                $deliveryboyWallet = User::where('id',$data->deliveryboy_id)->where('type','Deliveryboy')->first();
                $customerWallet = User::where('id',$data->user_id)->where('type','Customer')->first();
                // $wallet = User::where('id',$data->user_id)->where('type','Deliveryboy')->first();
                if($deliveryboyWallet)
                {
                    if($data->status == 'Delivered') {
                        $deliveryboyWallet->wallet = ((float)$deliveryboyWallet->wallet - (float)$data->order_commission);
                        $its_save = $deliveryboyWallet->save();
                        $allData = ['user_id' => $data->deliveryboy_id, 'order_id' => $data->id, 'amount' => $data->order_commission, 'pickaddress_id' => $data->pickaddress_id, 'dropaddress_id' => $data->dropaddress_id, 'deliveryboy_id' => $data->deliveryboy_id, 'status' => 'debit'];
                        $wallet_history = DeliveryboyWalletHistory::create($allData);
                    }
                }

                if($customerWallet){
                    $customerWalletHistory = [
                        'title' => 'Valid Dispute',
                        'amount' => $request->amount,
                        'status' => 'credit',
                        'payment_id' => 'Payment Refund',
                        'user_id' => $data->user_id
                    ];
                    $Wallet_history = WalletHistory::create($customerWalletHistory);
                    if(isset($data->wallet_amount) && $data->wallet_amount != 0){
                        if($data->wallet_amount >= $request->amount){
                            $customerWallet->wallet = ((float)$customerWallet->wallet + (float)$request->amount);
                        }else{
                            $customerWallet->wallet = (float)$customerWallet->wallet + (float)$data->wallet_amount;
                            $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
                            $api->payment->fetch($data->payment_id)->refund(array("amount"=> ((float)$request->amount - (float)$data->wallet_amount) * 100,"speed"=>"normal","receipt"=>"Receipt No. ".time()));
                        }
                    $its_save = $customerWallet->save();
                    }else{
                        $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
                        $api->payment->fetch($data->payment_id)->refund(array("amount"=> $request->amount * 100,"speed"=>"normal","receipt"=>"Receipt No. ".time()));
                    }
                }


                $customertoken = $customerWallet['fcm_token'];
                $addtion = array(
                    'Type' => 'Dispute_accept',
                    'user' => $customerWallet['name'],
                );
                $msg = array(
                    'title'  => 'Dispute Accepted',
                    'body' => 'Your dispute request has been accepted. Your deducted amount will be credited to your Pickup To Drop wallet very soon.',
                );

                PushNotification::Push($msg, $customertoken, $addtion);

				if($deliveryboyWallet){
                    $usertoken = $deliveryboyWallet['fcm_token'];
                    $addtion = array(
                        'Type' => 'Dispute_accept',
                        'user' => $deliveryboyWallet['name'],
                    );
                    $msg = array(
                        'title'  => 'Dispute Accepted',
                        'body' => 'The amount '. $data->order_commission . ' is debited from your wallet due to the order dispute (order ID: ' . $data->id . '). Contact the support team for further discussion.',
                    );
                    PushNotification::Push($msg, $usertoken, $addtion);
				}
                $message['message'] = "Dispute Accepted!";
                $message['title'] = "Success !";
                $message['icon'] = "success";
                $message['status'] = "success";
            }else{
                $message['message'] = "Please try again!dispute not accept!";
                $message['title'] = "Error !";
                $message['icon'] = "error";
                $message['status'] = "error";
            }
            return response()->json($message);
        }else{
            $data = Pickup::where('id', $dispute->order_id)->first();
            $data->dispute_status = 'reject';
            $its_save = $data->save();
            if($its_save){
                 $dispute = Dispute::where('id',$request->dispute_id)->update(['status'=>'Invalid']);
                $message['message'] = "Dispute Rejected!";
                $message['title'] = "Success !";
                $message['icon'] = "success";
                $message['status'] = "success";
            }else{
                $message['message'] = "Please try again!dispute not reject!";
                $message['title'] = "Error !";
                $message['icon'] = "error";
                $message['status'] = "error";
            }
            $customer = User::where('id', $data->user_id)->where('type', 'Customer')->first();
            $customertoken = $customer['fcm_token'];
            $addtion = array(
                'Type' => 'Dispute_reject',
                'user' => $customer['name'],
            );
            $msg = array(
                'title'  => 'Dispute Rejected',
                'body' => 'The dispute request you submitted has been denied. If you would like to discuss your dispute further, please contact the support team.',
            );
            PushNotification::Push($msg, $customertoken, $addtion);

            return response()->json($message);
        }
    }

	public function disputeCheck(Request $request)
    {
        if (!auth()->user()->can('Dispute-Show')) {
            $message['message'] = "Unauthorized ccess !";
            $message['title'] = "Error !";
            $message['icon'] = "error";
            $message['status'] = "error";
            return response()->json($message);
        }
        $dispute = Dispute::where('id', $request->dispute_id)->first();
        if ($request->type == 'Valid') {
            $data = Pickup::where('id', $dispute->order_id)->first();
            if ($data->paid_amount < $request->amount) {
                $message['message'] = "Enter amount is more than order amount !";
                $message['title'] = "Error !";
                $message['icon'] = "error";
                $message['status'] = "error";
                return response()->json($message);
            }
            $data->dispute_status = 'accept';
            $its_save = $data->save();
            if ($its_save) {
                $dispute = Dispute::where('id', $request->dispute_id)->update(['status' => 'Valid']);
                $deliveryboyWallet = User::where('id', $data->deliveryboy_id)->where('type', 'Deliveryboy')->first();
                $customerWallet = User::where('id', $data->user_id)->where('type', 'Customer')->first();
                // $wallet = User::where('id',$data->user_id)->where('type','Deliveryboy')->first();
                if ($deliveryboyWallet) {
                    if ($data->status == 'Delivered') {
                        $deliveryboyWallet->wallet = ((float)$deliveryboyWallet->wallet - (float)$data->order_commission);
                        $its_save = $deliveryboyWallet->save();
                        $allData = ['user_id' => $data->deliveryboy_id, 'order_id' => $data->id, 'amount' => $data->order_commission, 'pickaddress_id' => $data->pickaddress_id, 'dropaddress_id' => $data->dropaddress_id, 'deliveryboy_id' => $data->deliveryboy_id, 'status' => 'debit'];
                        $wallet_history = DeliveryboyWalletHistory::create($allData);
                    }
                }

                if ($customerWallet) {
                    $customerWalletHistory = [
                        'title' => 'Valid Dispute',
                        'amount' => $request->amount,
                        'status' => 'credit',
                        'payment_id' => 'Payment Refund',
                        'user_id' => $data->user_id
                    ];
                    $Wallet_history = WalletHistory::create($customerWalletHistory);
                    if (isset($data->wallet_amount) && $data->wallet_amount != 0) {
                        if ($data->wallet_amount >= $request->amount) {
                            $customerWallet->wallet = ((float)$customerWallet->wallet + (float)$request->amount);
                        } else {
                            $customerWallet->wallet = (float)$customerWallet->wallet + (float)$data->wallet_amount;
                            $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
                            $api->payment->fetch($data->payment_id)->refund(array("amount" => ((float)$request->amount - (float)$data->wallet_amount) * 100, "speed" => "normal", "receipt" => "Receipt No. " . time()));
                        }
                        $its_save = $customerWallet->save();
                    } else {
                        $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
                        $api->payment->fetch($data->payment_id)->refund(array("amount" => $request->amount * 100, "speed" => "normal", "receipt" => "Receipt No. " . time()));
                    }
                }


                $customertoken = $customerWallet['fcm_token'];
                $addtion = array(
                    'Type' => 'Dispute_accept',
                    'user' => $customerWallet['name'],
                );
                $msg = array(
                    'title'  => 'Dispute Accepted',
                    'body' => 'Your dispute request has been accepted. Your deducted amount will be credited to your Pickup To Drop wallet very soon.',
                );

                PushNotification::Push($msg, $customertoken, $addtion);

                if ($deliveryboyWallet) {
                    $usertoken = $deliveryboyWallet['fcm_token'];
                    $addtion = array(
                        'Type' => 'Dispute_accept',
                        'user' => $deliveryboyWallet['name'],
                    );
                    $msg = array(
                        'title'  => 'Dispute Accepted',
                        'body' => 'The amount ' . $data->order_commission . ' is debited from your wallet due to the order dispute (order ID: ' . $data->id . '). Contact the support team for further discussion.',
                    );
                    PushNotification::Push($msg, $usertoken, $addtion);
                }
                $message['message'] = "Dispute Accepted!";
                $message['title'] = "Success !";
                $message['icon'] = "success";
                $message['status'] = "success";
            } else {
                $message['message'] = "Please try again!dispute not accept!";
                $message['title'] = "Error !";
                $message['icon'] = "error";
                $message['status'] = "error";
            }
            return response()->json($message);
        } else {
            $data = Pickup::where('id', $dispute->order_id)->first();
            $data->dispute_status = 'reject';
            $its_save = $data->save();
            if ($its_save) {
                $dispute = Dispute::where('id', $request->dispute_id)->update(['status' => 'Invalid']);
                $message['message'] = "Dispute Rejected!";
                $message['title'] = "Success !";
                $message['icon'] = "success";
                $message['status'] = "success";
            } else {
                $message['message'] = "Please try again!dispute not reject!";
                $message['title'] = "Error !";
                $message['icon'] = "error";
                $message['status'] = "error";
            }
            $customer = User::where('id', $data->user_id)->where('type', 'Customer')->first();
            $customertoken = $customer['fcm_token'];
            $addtion = array(
                'Type' => 'Dispute_reject',
                'user' => $customer['name'],
            );
            $msg = array(
                'title'  => 'Dispute Rejected',
                'body' => 'The dispute request you submitted has been denied. If you would like to discuss your dispute further, please contact the support team.',
            );
            PushNotification::Push($msg, $customertoken, $addtion);

            return response()->json($message);
        }
    }

    public function showDetail($id){
        if (!auth()->user()->can('Dispute-Show')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        $data['dispute_data'] = Dispute::with('user')->find(base64_decode($id));
        $data['pickup_data'] = Pickup::with(['boy', 'Pickupaddress', 'Dropaddress'])->find($data['dispute_data']->order_id);


        return view('dispute.show', compact('data'));
    }
}
