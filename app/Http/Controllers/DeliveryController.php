<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Http\Request;

use App\Models\User;
use Hash;
Use Alert;
use App\Helpers\DeliveryboyMail;
use App\Mail\NotificationMail;
use App\Mail\TestEmail;
use Illuminate\Support\Facades\Log;
use App\Models\Address;
use App\Models\DriverDocument;
use App\Models\PaymentContact;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Models\Delivery_fcm_token;
use App\Helpers\PushNotification;
use App\Models\TDSCertificate;
use Exception;
use Illuminate\Support\Facades\Storage;

class DeliveryController extends Controller
{
    public function index()
    {
        if (!auth()->user()->can('Deliveryboy-List')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }

        $data['cities'] = City::select('id','city_name')->orderBy('city_name')->get();
        return view('deliveryboy/index',$data);
    }

    public function create()
    {
        if (!auth()->user()->can('Deliveryboy-Add')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
    	return view('deliveryboy/add');
    }


    public function Getall(Request $request)
    {
        if (!auth()->user()->can('Deliveryboy-List')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }

        $driver = User::with('defaultHomeAddresses', 'driverdoc','city')->whereHas('driverdoc',function($q) use($request){
            if($request->status == 'active'){
                $q->where('driver_status','active');
            }else if($request->status == 'deactive'){
                $q->whereIn('driver_status', ['pending', 'suspended',Null]);
            }
        })->whereHas('city',function ($q) use($request){
            if($request->city_id && $request->city_id != 'all')
                $q->where('id',$request->city_id);
        })->where('type', 'deliveryboy')->where('is_delete', 'false');

      return datatables()->of($driver)->addColumn('driver_status', function ($row) {
            return $row->driverdoc && $row->driverdoc->driver_status ? ($row->driverdoc->driver_status == 'pending' ? 'deactive' : $row->driverdoc->driver_status) : 'deactive';
        })->editColumn('birthdate',function($row){
            if ($row && isset($row['birthdate'])) {
                if (count(explode('/', $row['birthdate'])) == 1) {
                  $row['birthdate'] =  Carbon::parse($row['birthdate'])->format('d/m/Y');
                  return  $row['birthdate'];
                }else{
                    return  $row['birthdate'];
                }
            }
        })->toJson();
    }

    public function store(Request $request)
    {
        try {
            if (!auth()->user()->canany(['Deliveryboy-Add', 'Deliveryboy-Edit'])) {
                Alert::error('Error !', 'Unauthorized access.');
                return redirect()->route('home');
            }
            $this->validate(
                $request,
                [
                    'email' => 'required|regex:/(.+)@(.+)\.(.+)/i',
                ]
            );
            $driver_id = null;
            $arrData = array(
                'name' => $request->name,
                'email' => $request->email,
                'type' => $request->type,
                'gender' => $request->gender,
                'mobile_number' => $request->mobile_number,
                'birthdate' => $request->birthdate,
            );

            if ($request->id) {
                $isExist = User::where(['mobile_number'=> $request->mobile_number,'type'=> 'Deliveryboy','is_delete'=>'false'])
                ->where('id','!=', $request->id)
                ->first();
                if($isExist){
                    Alert::error('Error !', 'Mobile number exist.');
                    return redirect('deliveryboy');
                }
                $driver_id = $request->id;
                if ($request->password) {
                    $arrData['password'] = Hash::make($request->password);
                    $where = array('id' => $request->id);
                    $isupdate = User::where($where)->update($arrData);
                    Alert::success('Success !', 'Delivery Agent Updated Successfully.');
                } else {
                    $where = array('id' => $request->id);
                    $isupdate = User::where($where)->update($arrData);
                    Alert::success('Success !', 'Delivery Agent Updated Successfully.');
                }
            } else {
                $isExist = User::where(['mobile_number' => $request->mobile_number, 'type' => 'Deliveryboy', 'is_delete' => 'false'])->first();
                if ($isExist) {
                    Alert::error('Error !', 'Mobile number exist.');
                    return redirect('deliveryboy');
                }
                $curl = curl_init();
                $login = env('RAZORPAY_X_KEY_ID');
                $password = env('RAZORPAY_X_SECRET_KEY');
                $headers = array(
                    'Content-Type:application/json',
                    'Authorization: Basic ' . base64_encode("$login:$password"), // <---
                );

                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://api.razorpay.com/v1/contacts',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => '{
                          "name": "' . $arrData['name'] . '",
                          "email": "' . $arrData['email'] . '",
                          "contact": ' . $arrData['mobile_number'] . ',
                          "type": "customer",
                          "reference_id": "Acme Contact ID 12345",
                          "notes":{
                            "random_key_1": "Make it so.",
                            "random_key_2": "Tea. Earl Grey. Hot."
                        }
                    }',
                    CURLOPT_HTTPHEADER => $headers,
                ));

                $response = curl_exec($curl);
                $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                curl_close($curl);
                $resData = json_decode($response);
                if ($statusCode == 200 || $statusCode == 201) {
                    $arrData['password'] = Hash::make($request->password);
                    $arrData['pass'] = $request->password;
                    $isInsert = User::create($arrData);
                    $driver_id = $isInsert->id;
		           User::where('id', $isInsert->id)->update(['payment_contact_id' => $resData->id]);
                   $mailsent = Mail::to($request->email)->send(new NotificationMail($arrData));
                    if(true || ($mailsent && $mailsent['status'] == 202)){
                        if ($isInsert) {
                            $contactData = new PaymentContact;
                            $contactData->user_id = $isInsert->id;
                            $contactData->contactId = $resData->id;
                            $contactData->name = $resData->name;
                            $contactData->email = $resData->email;
                            $contactData->contact = $resData->contact;
                            $its_update = $contactData->save();
                            if ($its_update) {
                                User::where('id', $contactData->user_id)->update(['payment_contact_id' => $resData->id]);
                            }
                        }
                        Alert::success('Success !', 'Delivery Agent Created Successfully And Credential Send On Mail.');
                    } else {
                        Alert::error('Error !', 'Delivery Agent not added.');
                    }
                } else {
                    Alert::error('Error !', 'Something went wrong.');
                }
            }

        if(is_null($driver_id)){
            $update = false;
            $driver_doc['driver_id'] = $driver_id;
        }else{
            $exist_driver_doc = DriverDocument::where('driver_id',$driver_id)->first();
            $driver_doc = [];
            if($exist_driver_doc){
                $update = true;
            }else{
                $update = false;
                $driver_doc['driver_id'] = $driver_id;
            }
        }

        if(is_null($driver_id)){
            return redirect('deliveryboy');
        }
            if ($request->address) {
                $Address['address'] = $request->address;
                $Address['type'] = 'home';
                $Address['user_id'] = $driver_id;

                $address_exist = User::find($driver_id);
                if($address_exist && is_null($address_exist->home_address)){
                    $address_id = Address::create($Address)->id;
                    User::where('id',$driver_id)->update(['home_address'=>$address_id]);
                }else{
                    $address_id = $address_exist->home_address;
                    Address::where('id', $address_id)->update($Address);

                }
            }

            $driver_doc['vehicle_number'] = $request->vehicle_number ? $request->vehicle_number : null;
            $driver_doc['vehicle_model'] = $request->vehicle_model ? $request->vehicle_model : null;
            $driver_doc['vehicle_colour'] = $request->vehicle_colour ? $request->vehicle_colour : null;
            $driver_doc['pan_number'] = $request->pan_number ? $request->pan_number : null;
            $driver_photo = null;
            if ($request->file('driver_photo')) {
                if (isset($exist_driver_doc->driver_photo) && $exist_driver_doc->driver_photo && file_exists(public_path($exist_driver_doc->driver_photo))) {
                    unlink(public_path($exist_driver_doc->driver_photo));
                }
                $file = $request->file('driver_photo');
                $driver_photo = $request->file('driver_photo')
                    ->move('driver',strtotime('now') . '_driver_photo_' . $driver_id . '.' . $file->extension()
                );

                $driver_doc['driver_photo'] = $driver_photo->getPathName();
                $driver_doc['driver_photo_status'] = "approve";
            }

            if ($request->file('addhar_front')) {
                if (isset($exist_driver_doc->addhar_front) && $exist_driver_doc->addhar_front && file_exists(public_path($exist_driver_doc->addhar_front))) {
                    unlink(public_path($exist_driver_doc->addhar_front));
                }
                $file = $request->file('addhar_front');
                $addhar_front = $request->file('addhar_front')
                    ->move('driver',strtotime('now') . '_addhar_front_' . $driver_id . '.' . $file->extension()
                );

                $driver_doc['addhar_front'] = $addhar_front->getPathName();
                $driver_doc['addhar_front_status'] = "approve";
            }
            if ($request->file('addhar_back')) {
                if (isset($exist_driver_doc->addhar_back) && $exist_driver_doc->addhar_back && file_exists(public_path($exist_driver_doc->addhar_back))) {
                    unlink(public_path($exist_driver_doc->addhar_back));
                }
                $file = $request->file('addhar_back');
                $addhar_back = $request->file('addhar_back')
                    ->move('driver',strtotime('now') . '_addhar_back_' . $driver_id . '.' . $file->extension()
                );

                $driver_doc['addhar_back'] = $addhar_back->getPathName();
                $driver_doc['addhar_back_status'] = "approve";
            }
            if ($request->file('pan_card')) {
                if (isset($exist_driver_doc->pan_card) && $exist_driver_doc->pan_card && file_exists(public_path($exist_driver_doc->pan_card))) {
                    unlink(public_path($exist_driver_doc->pan_card));
                }
                $file = $request->file('pan_card');
                $pan_card = $request->file('pan_card')
                    ->move('driver',strtotime('now') . '_pan_card_' . $driver_id . '.' . $file->extension()
                );

                $driver_doc['pan_card'] = $pan_card->getPathName();
                $driver_doc['pan_card_status'] = "approve";
            }
            if ($request->file('tds_certificate')) {
                if (isset($exist_driver_doc->tds_certificate) && $exist_driver_doc->tds_certificate && file_exists(public_path($exist_driver_doc->tds_certificate))) {
                    unlink(public_path($exist_driver_doc->tds_certificate));
                }
                $file = $request->file('tds_certificate');
                $tds_certificate = $request->file('tds_certificate')
                    ->move('driver',strtotime('now') . '_tds_certificate_' . $driver_id . '.' . $file->extension()
                );

                $driver_doc['tds_certificate'] = $tds_certificate->getPathName();
                $driver_doc['tds_certificate_status'] = "approve";
            }
            if ($request->file('rc_front')) {
                if (isset($exist_driver_doc->rc_front) && $exist_driver_doc->rc_front && file_exists(public_path($exist_driver_doc->rc_front))) {
                    unlink(public_path($exist_driver_doc->rc_front));
                }
                $file = $request->file('rc_front');
                $rc_front = $request->file('rc_front')
                    ->move('driver',strtotime('now') . '_rc_front_' . $driver_id . '.' . $file->extension()
                );

                $driver_doc['rc_front'] = $rc_front->getPathName();
                $driver_doc['rc_front_status'] = "approve";
            }
            if ($request->file('rc_back')) {
                if (isset($exist_driver_doc->rc_back) && $exist_driver_doc->rc_back && file_exists(public_path($exist_driver_doc->rc_back))) {
                    unlink(public_path($exist_driver_doc->rc_back));
                }
                $file = $request->file('rc_back');
                $rc_back = $request->file('rc_back')
                    ->move('driver',strtotime('now') . '_rc_back_' . $driver_id . '.' . $file->extension()
                );

                $driver_doc['rc_back'] = $rc_back->getPathName();
                $driver_doc['rc_back_status'] = "approve";
            }
            if ($request->file('insurance_certificate')) {
                if (isset($exist_driver_doc->insurance_certificate) && $exist_driver_doc->insurance_certificate && file_exists(public_path($exist_driver_doc->insurance_certificate))) {
                    unlink(public_path($exist_driver_doc->insurance_certificate));
                }
                $file = $request->file('insurance_certificate');
                $insurance_certificate = $request->file('insurance_certificate')
                    ->move('driver',strtotime('now') . '_insurance_certificate_' . $driver_id . '.' . $file->extension()
                );

                $driver_doc['insurance_certificate'] = $insurance_certificate->getPathName();
                $driver_doc['insurance_certificate_status'] = "approve";
            }
            if($update){
                if(!is_null($driver_doc)){
                    DriverDocument::where('driver_id', $driver_id)->update($driver_doc);
                }
            }else{
                $driver_doc['joining_date'] = now();
                $driver_doc['driver_status'] = "pending";
                $driver = DriverDocument::create($driver_doc);
            }
            return redirect('deliveryboy');
        }catch(\Exception $e){
            Alert::error('error', $e->getMessage());
            return redirect('deliveryboy');
        }
    }


    public function edit($id)
    {
        if (!auth()->user()->can('Deliveryboy-Edit')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        $data['deliveryboy'] = User::with(['driverdoc', 'defaultHomeAddresses'])->find(base64_decode($id));
        if ($data['deliveryboy'] && isset($data['deliveryboy']['birthdate'])) {
            if (count(explode('/', $data['deliveryboy']['birthdate'])) == 1) {
                $data['deliveryboy']['birthdate'] =  Carbon::parse($data['deliveryboy']['birthdate'])->format('d/m/Y');
            }
        }
        return view('deliveryboy/add',$data);
    }

    public function destroy($id)
    {
        if (!auth()->user()->can('Deliveryboy-Delete')) {
            return response()->json('unauthorized_access');
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        // $deleted = User::destroy($id);
        $updateStatus = User::find($id);
        $updateStatus->is_delete = 'true';
        $its_update = $updateStatus->save();
        if ($its_update) {
            $status = true;
        }else{
            $status = false;
        }
        return response()->json($status);
    }

    public function DocumentList($id){
        if (!auth()->user()->can('Deliveryboy-Document')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        $data['deliveryboy_docs'] = DriverDocument::where('driver_id', base64_decode($id))->first();
        if($data['deliveryboy_docs']){
            return view('deliveryboy/docs', $data);
        }
        else{
            Alert::error('Error !', 'Documents not uploaded yet.');
            return redirect('deliveryboy');
        }
    }
    public function ApproveDocs(Request $request,$id,$field){

        if (!auth()->user()->can('Deliveryboy-Document')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }

        // dd(str_contains($request->getRequestUri(), 'approve'), str_contains($request->getRequestUri(), 'reject'));
        if (str_contains($request->getRequestUri(), 'approve')) {
            $status = "approve";
        }else if(str_contains($request->getRequestUri(), 'reject')){
            $status = "reject";
        }

        $updateDocs = [];
        if($field == "driver_photo"){
            $updateDocs['driver_photo_status'] = $status;
        }
        if($field == "addhar_front"){
            $updateDocs['addhar_front_status'] = $status;
        }
        if($field == "addhar_back"){
            $updateDocs['addhar_back_status'] = $status;
        }
        if($field == "pan_card"){
            $updateDocs['pan_card_status'] = $status;
        }
        if($field == "tds_certificate"){
            $field = "driving_licence";
            $updateDocs['tds_certificate_status'] = $status;
        }
        if($field == "rc_front"){
            $updateDocs['rc_front_status'] = $status;
        }
        if($field == "rc_back"){
            $updateDocs['rc_back_status'] = $status;
        }
        if($field == "insurance_certificate"){
            $updateDocs['insurance_certificate_status'] = $status;
        }
        $docUpdate = DriverDocument::where('id',$id)->update($updateDocs);

        if($field){
            $field = ucwords(str_replace('_',' ',$field));
        }
        if($docUpdate){
            if($status == "approve"){
                $docUpdate = DriverDocument::where('id', $id)->update(['joining_date'=>now()]);
                Alert::success('Success !', $field . ' Image Verified.');
            }
            if($status == "reject"){
                Alert::success('Success !', $field . ' Image Rejected.');
            }
            return redirect()->back();
        }else{
            Alert::error('Error !', 'Documents not Verfied ! .');
        }

    }
    public function approveDeliveryAgent($id,$status){

        if (!auth()->user()->can('Deliveryboy-Document')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        if($status == 'deactive'){
            $docUpdate = DriverDocument::where("id", $id)->update(['driver_status' => 'deactive']);
            if ($docUpdate) {
                Alert::success('Success !', 'Delivery agent deactive successfully.');
                return redirect()->back();
            }

        }
        $docUpdate = DriverDocument::find($id);
		$usertoken = User::where('id', $docUpdate->driver_id)->first('fcm_token');

        $field = [];
        if($docUpdate->driver_photo_status != "approve")
            $field[]='Driver photo';
        if($docUpdate->addhar_front_status != "approve")
            $field[] = 'Addhar front photo';
        if($docUpdate->addhar_back_status != "approve")
            $field[] = 'Addhar back photo';
        if($docUpdate->pan_card_status != "approve")
            $field[] = 'Pancard photo';
        if($docUpdate->rc_front_status != "approve")
            $field[] = 'RC Front';
        if($docUpdate->rc_back_status != "approve")
            $field[] = 'RC Back';
        if($docUpdate->insurance_certificate_status != "approve")
            $field[] = 'Insurance Certificate';
        if($docUpdate->tds_certificate_status != "approve")
            $field[] = 'Driving Licence';

            if($field){
                Alert::error('Error !', implode(', ', $field).' Are not verified! Please approve mentioned document first .');
                return redirect()->back();
            }

        $docUpdate = DriverDocument::where("id",$id)->update(['driver_status'=>'active']);
        if($docUpdate){
			 if($usertoken){
				 $addtion = array(
					 'Type' => 'Find One Pickup Request',
					 'user' => "user1",
				 );
				 $msg = array(
					 'body' => "Your document for your profile is approved",
					 'title' => 'Document Approved',
				 );
				 // Notification::Push($msg,$usertoken,$addtion);
				 PushNotification::push($msg, $usertoken['fcm_token'], $addtion);
			 }
            Alert::success('Success !','Delivery agent active successfully.');
            return redirect()->back();
        }else{
            Alert::error('Error !', 'Something went wrong ! .');
            return redirect()->back();
        }

    }

    public function toggleDeliveryAgent($id, $status)
    {
        if (!auth()->user()->can('Deliveryboy-Document')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        $docUpdate = DriverDocument::where('driver_id', $id)->first();

        if (is_null($docUpdate))
            return response()->json(['status' => 'error', 'message' => 'Delivery agent documents not updated!', 'data' => ''], 200);

        if($docUpdate->driver_photo_status != 'approve' || $docUpdate->addhar_front_status != 'approve' || $docUpdate->addhar_back_status != 'approve' || $docUpdate->pan_card_status != 'approve' || $docUpdate->tds_certificate_status != 'approve' || $docUpdate->rc_front_status != 'approve' || $docUpdate->rc_back_status != 'approve' || $docUpdate->insurance_certificate_status != 'approve' )
            return response()->json(['status' => 'error', 'message' => 'Delivery agent documents not updated!', 'data' => ''], 200);

        if ($status == 'suspended') {
            $docUpdate = DriverDocument::where("driver_id", $id)->update(['driver_status' => 'suspended']);
            User::where(['id' => $id, 'is_delete' => 'false', 'delivery_agent_status' => '1'])->update(['delivery_agent_status'=>'0']);
            if ($docUpdate) {
                return response()->json(['status' => 'success', 'message' => 'Delivery agent deactivated successfully!', 'data' => ''], 200);
            }
        }
        if ($status == 'deactive') {
            $docUpdate = DriverDocument::where("driver_id", $id)->update(['driver_status' => 'deactive']);
            User::where(['id' => $id, 'is_delete' => 'false', 'delivery_agent_status' => '1'])->update(['delivery_agent_status'=>'0']);
            if ($docUpdate) {
                return response()->json(['status' => 'success', 'message' => 'Delivery agent deactivated successfully!', 'data' => ''], 200);
            }
        }
        if ($status == 'active') {
            $docUpdate = DriverDocument::where("driver_id", $id)->update(['driver_status' => 'active']);
			$usertoken = User::where('id', $id)->first('fcm_token');
            if ($docUpdate) {
				if($usertoken){
				 $addtion = array(
					 'Type' => 'Find One Pickup Request',
					 'user' => "user1",
				 );
				 $msg = array(
					 'body' => "Your document for your profile is approved",
					 'title' => 'Document Approved',
				 );
				 // Notification::Push($msg,$usertoken,$addtion);
				 PushNotification::push($msg, $usertoken['fcm_token'], $addtion);
			 }
                return response()->json(['status' => 'success', 'message' => 'Delivery agent activated successfully!', 'data' => ''], 200);
            }
        }


        $field = [];
        if ($docUpdate->driver_photo_status != "approve")
        $field[] = 'Driver photo';
        if ($docUpdate->addhar_front_status != "approve")
        $field[] = 'Addhar front photo';
        if ($docUpdate->addhar_back_status != "approve")
        $field[] = 'Addhar back photo';
        if ($docUpdate->pan_card_status != "approve")
        $field[] = 'Pancard photo';
        if ($docUpdate->rc_front_status != "approve")
        $field[] = 'RC Front';
        if ($docUpdate->rc_back_status != "approve")
        $field[] = 'RC Back';
        if ($docUpdate->insurance_certificate_status != "approve")
        $field[] = 'Insurance Certificate';

        if ($field) {
            Alert::error('Error !', implode(', ', $field) . ' Are not verified! Please approve mentioned document first .');
            return response()->json(['status' => 'error', 'message' => implode(', ', $field) . ' Are not verified! Please approve mentioned document first .', 'data' => ''], 200);
        }

        $docUpdate = DriverDocument::where("driver_id", $id)->update(['driver_status' => 'active']);
        if ($docUpdate) {
            return response()->json(['status' => 'success', 'message' => 'Delivery agent activated successfully!', 'data' => ''], 200);
        } else {
            return response()->json(['status' => 'error', 'message' => 'Something went wrong!', 'data' => ''], 200);
        }
    }

    public function TDSUploadofDeliveryAgent(){
        if (!auth()->user()->canany(['Tds-Show', 'TDS-Upload'])) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        return view('deliveryboy/tdsformupload');
    }

    public function GetallDeliveryAgent()
    {
        if (!auth()->user()->canany(['Tds-Show', 'TDS-Upload'])) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        $driver = User::with('defaultHomeAddresses', 'driverdoc')->whereHas('driverdoc',function($q){
            $q->where('pan_number','!=',null);
        })->where('type', 'deliveryboy', 'driverdoc')->where('is_delete', 'false')->orderBy('created_at', 'desc');
        return datatables()->of($driver)->addColumn('driver_status', function ($row) {
            return $row->driverdoc && $row->driverdoc->driver_status ? ($row->driverdoc->driver_status == 'pending' ? 'deactive' : $row->driverdoc->driver_status) : 'deactive';
        })->addColumn('pan_number', function ($row) {
            return $row->driverdoc && $row->driverdoc->pan_number ? $row->driverdoc->pan_number : null;
        })->toJson();
    }

    public function tdsuploaded($id){
        if (!auth()->user()->canany(['Tds-Show', 'TDS-Upload'])) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }

        $data['files'] = TDSCertificate::where('delivery_agent_id', base64_decode($id))->get()->groupBy('financial_year');
        $data['user_details'] = User::where('id',base64_decode($id))->first();
        if(count($data['files']) > 0){
            return view('deliveryboy/tdsuploded', $data);
        }else{
            Alert::error('Error !', 'Record not found.');
            return redirect()->back();
        }
    }
    public function tdsuploadedfile(){
        if (!auth()->user()->canany(['Tds-Show', 'TDS-Upload'])) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        return view('deliveryboy/tdsuplodedfile');
    }

    public function tdsformstoreold(Request $request){
        if ($request->hasFile('tds_image')) {
            $files = $request->file('tds_image');
            dd(count($files));
            foreach ($files as $file) {
                $pan = explode('_',$file->getClientOriginalName())[0];
                $driverID = DriverDocument::where('pan_number',$pan)->first()->driver_id;

                $tds_certificate = $file->move(
                        'tdscertificate',
                    $file->getClientOriginalName()
                    );

                $tds_data['tds_cert'] = $tds_certificate->getPathName();
                $tds_data['delivery_agent_id'] = $driverID;
                $tds_data['pan_number'] = $pan;
                $tds_data['image_name'] = $file->getClientOriginalName();
                TDSCertificate::create($tds_data);
            }
            Alert::success('Success !', 'Tds file uploded successfully.');
            return redirect()->back();
        }
    }
    public function tdsformstore(Request $request){
        try{
            if (!auth()->user()->can('TDS-Upload')) {
                Alert::error('Error !', 'Unauthorized access.');
                return redirect()->route('home');
            }
            $this->validate(
                $request,
                [
                    'financial_year' => 'required',
                    'tds_image' => 'required',
                    'actualFileList' => 'required'
                ]
            );

            if ($request->file('tds_image')) {
                $files = $request->file('tds_image');
                $uploded_file = [];
                $not_found = [];
                $exist_file = [];
                foreach ($files as $file) {
                    if($file->getMimeType() == "application/pdf"){
                        if(!in_array($file->getClientOriginalName(),explode(",",$request->actualFileList)))
                            continue;

                        $existFile = TDSCertificate::where('image_name', $file->getClientOriginalName())->first();
                        if ($existFile) {
                            $exist_file[] = $existFile->image_name;
                        } else {
                            $pan = explode('_', $file->getClientOriginalName())[0];
                            $driverID = User::with('driverdoc')->where('is_delete', 'false')->whereHas('driverdoc', function ($q) use ($pan) {
                                $q->where('pan_number', $pan);
                            })->first();
                            //                        $driverID = DriverDocument::where('pan_number', $pan)->first();
                            if ($driverID) {
                                //                            $driverID = $driverID->driver_id;
                                $driverID = $driverID->id;
                                $path = env("DIGITALOCEAN_SPACES_DIR") . '/uploads/tdscertificate';
                                $temp_path = Storage::disk('digitalocean')->putFile($path, $file, 'public');
                                //                            $tds_certificate = $file->move('tdscertificate', $file->getClientOriginalName());
                                //                            $tds_data['tds_cert'] = $tds_certificate->getPathName();
                                $tds_data['tds_cert'] = $temp_path;
                                $tds_data['delivery_agent_id'] = $driverID;
                                $tds_data['pan_number'] = $pan;
                                $tds_data['image_name'] = $file->getClientOriginalName();
                                $tds_data['financial_year'] = $request->financial_year;
                                TDSCertificate::create($tds_data);
                                $uploded_file[] = $file->getClientOriginalName();
                            } else {
                                $not_found[] = $file->getClientOriginalName();
                            }
                        }
                    }

                }
                if (count($uploded_file) == count($files)){
                    alert()->html('Success !', "Tds file uploded successfully", 'success');
                    return redirect()->back();
                }
                if (count($uploded_file) > 0 && count($not_found) > 0 && count($exist_file) > 0) {
                    $not_found_file = implode('<br>', $not_found);
                    $file = implode('<br>', $exist_file);
                    $uploded = implode('<br>', $uploded_file);
                    alert()->html('Success !', 'Successfullly Uploded File : ' . $uploded . '<br> Already Existing File :<br>' . $file . '</br> File with not proper format :<br>' . $not_found_file, 'success');
                    return redirect()->back();
                }else if (count($not_found) > 0 && count($exist_file) > 0) {
                    $not_found_file = implode('<br>', $not_found);
                    $file = implode('<br>', $exist_file);
                    if(count($not_found)+ count($exist_file) == count($files)){
                        alert()->html('Error !','<p>Files are not uploded <br>Already Existing File :<br>' . $file . '</br> File with not proper format :<br>' . $not_found_file.'</p>', 'error');
                        return redirect()->back();
                    }else{
                        alert()->html('Success !', '<p>Files uploded successfully except Following files<br>Already Existing File :<br>' . $file . '</br> File with not proper format :<br>' . $not_found_file . '</p>', 'success');
                        return redirect()->back();
                    }
                }else if(count($not_found) > 0){
                    $not_found_file = implode(', ', $not_found);
                    if (count($not_found) == count($files)) {
                        alert()->html('Error !', '<p>Please upload files with matching pan number</p>', 'error');
                        return redirect()->back();
                    }else{
                        alert()->html('Success !', '<p>Files uploded successfully except following, Please upload files with matching pan number <br>'.$not_found_file.'</p>', 'success');
                        return redirect()->back();
                    }
                }else if(count($exist_file) > 0){
                    $file = implode(', ', $exist_file);
                    if(count($files) == count($exist_file)){
                        alert()->html('Error !', '<p>Files already uploded</p>', 'error');
                        return redirect()->back();
                    }
                    else{
                        alert()->html('Success !', '<p>Files uploded successfully except following, following file which already exist in system <br>' . $file . '</p>', 'success');
                        return redirect()->back();
                    }
                }else{
                    alert()->html('Success !', '<p>Files uploded successfully</p>', 'success');
                    return redirect()->back();
                }

            }
        }catch(Exception $e){
            Alert::error('error', $e->getMessage());
            return redirect()->back();
            dd($e->getMessage());
        }
    }
    public function updateWallet(Request $request){
        try {
            $user = User::where('id',$request->da_id)->update(['wallet'=>$request->wallet_amount]);
            if($user){
                Alert::success('Success !', 'Delivery agent wallet updated successfully.');
                return redirect()->back();
            }
        } catch (Exception $e) {
            dd($e->getMessage());
        }

    }

    public function deleteTDSCetificate(Request $request){
        try {
            $certificate = TDSCertificate::find($request->id);
            $da_id = $certificate->delivery_agent_id;
            if(Storage::disk('digitalocean')->exists($certificate->tds_cert))
                Storage::disk('digitalocean')->delete($certificate->tds_cert);

            $certificate->delete();
            $tdsfile = TDSCertificate::where('delivery_agent_id', $da_id)->count();
            return response()->json(['success'=>'File deleted successfully.','tds_file'=> $tdsfile],200);
        }catch(\Exception $e){
            return response()->json(['error'=>"Error while deleting file.\nPlease contact to administrator."],302);
        }
    }
}
