<?php

namespace App\Http\Controllers;

use App\Helpers\PushNotification;
use Illuminate\Http\Request;
use App\Models\Pickup;
use App\Models\User;
use App\Models\Delivery_fcm_token;
Use Alert;
use App\Models\DeliveryboyWalletHistory;
use Notification;
class PickupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!auth()->user()->can('Order-List')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        $deliverys = User::where('type','Deliveryboy')->get();
        return view('pickup/index',compact('deliverys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        if (!auth()->user()->can('Order-Show')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        $data['pickup'] = Pickup::with('Pickupaddress','Dropaddress','boy','image','customer', 'dispute')->where('id',base64_decode($id))->first();
        $data['deliveryboy'] = User::with("driverdoc")->whereHas("driverdoc",function($q){
            $q->where('driver_status', 'active');
        })->where('type','deliveryboy')->where('is_delete','false')->get();
        return view('pickup/view',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Get_all($filter="all")
    {
        if (!auth()->user()->can('Order-List')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        // dd($filter);
        $pickup = Pickup::with(['customer', 'boy', 'dispute'])->whereNotIn('type', ['tiffin', 'subscription'])->orderBy('id', 'DESC');
        if (strpos($filter, 'status') !== false || strpos($filter, 'id') !== false) {
            $filters = explode('&', $filter);
            foreach($filters as $filter){
                if (strpos($filter, 'status') !== false) {
                    $status = explode('=', $filter);
                        if(isset($status[1])){
                            $filter = $status[1];
                            if ($filter == 'pending') {
                                $pickup->where('status', 'Pickup Requested')->where('dispute_status','Not Add');
                            } else if ($filter == 'process') {
                                $pickup->whereIn('status', ['Out Of Pickup', 'Picked Up', 'Out Of Delivery', 'Request Accept'])->where('dispute_status', 'Not Add');
                            } else if ($filter == 'complete') {
                                $pickup->where('status', 'Delivered')->where('dispute_status', 'Not Add');
                            } else if ($filter == 'cancel') {
                                $pickup->where('status', 'Cancel Request')->where('dispute_status', 'Not Add');
                            } else if ($filter == 'dispute') {
                                $pickup->whereHas('dispute', function ($q) {
                                    $q->whereNotNull('status');
                                });
                            }
                        }
                }
                if (strpos($filter, 'id') !== false) {
                    $id = explode('-', $filter);
                    if (isset($id[1])) {
                        $id = base64_decode($id[1]);
                        $pickup->where('user_id', $id);
                    }
                }
            }
           
        }

        return datatables()->of($pickup)
        ->addColumn('mobile_number',function($data){
            return $data->customer && $data->customer->mobile_number ? $data->customer->mobile_number : '-';
        })->editColumn('status',function($data){
            if ($data->status == "Request Accept")
                return "Pickup Requested";
            else if ($data->status == "Out Of Pickup")
                return 'Out For Pickup';
            else if ($data->status == "Out Of Delivery")
                return 'Out For Delivery';
            else if ($data->status == "Cancel Request")
                return 'Request Cancelled';
            else
                return $data->status;
        })->rawColumns(['status'])->toJson();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function DeliveryBoyChange(Request $request)
    {
        // dd($request->all());
        $pickup = Pickup::where('id',$request->id)->update(['deliveryboy_id'=>$request->deliveryboy_id]);
        if($pickup)
        {
            $dboys = Delivery_fcm_token::where('deliveryboy_id',$request->deliveryboy_id)->get();
            // dd($dboy);
            foreach ($dboys as $key=>$delivery) {
                // $dd[$key] = $delivery['deliveryboy_id']."_".$delivery['fcm_token'];
                $usertoken = $delivery['fcm_token'];
                $addtion=  array
                (
                    'Type'=>'Delivery Was Assigned',
                    'user'=>$delivery['name'],
                );
                $msg = array
                (
                    'body'  => 'Delivery Assigned',
                    'title' => 'Now You Have One Delivery',
                );
                PushNotification::Push($msg,$usertoken,$addtion);
            }
            $data['message'] = "Delivery Agent Changed";
            $data['title'] = "Success !";
            $data['icon'] = "success";
        }
        else
        {
            $data['message'] = "Delivery Agent Not Change";
            $data['title'] = "Error !";
            $data['icon'] = "error";
        }

        return response()->json($data);
    }

    public function assignDeliveryBoy(Request $request)
    {
        $originalpickup = Pickup::where('id', $request->order_id)->first();
        $pickup = Pickup::where('id', $request->order_id)->update(['deliveryboy_id' => $request->da_id]);
        if ($pickup) {
            $daHistory = DeliveryboyWalletHistory::where('order_id',$request->order_id)->first();
            $userdata = User::where('id', $request->da_id)->first();
            $fcmToken = $userdata->fcm_token;
            if($daHistory){
                $daHistory->update(['user_id' => $request->da_id]);

                if($originalpickup->status == 'Delivered'){
                    $new_da = User::where('id', $request->da_id)->first();
                    $new_da->wallet += $originalpickup->order_commission;
                    $new_da->save();

                    $old_da = User::where('id', $originalpickup->deliveryboy_id)->first();
                    $old_da->wallet -= $originalpickup->order_commission;
                    $old_da->save();
                }

                $data['message'] = "Delivery Agent Reassigned Successfully";
                $data['status'] = "success";
                $data['icon'] = "success";

            }else{
                $daHistoryData = [
                    'user_id' => $request->da_id,
                    'amount' => $originalpickup->order_commission,
                    'order_id' => $request->order_id,
                ];
                DeliveryboyWalletHistory::create($daHistoryData);
                $data['message'] = "Delivery Agent Assigned Successfully";
                $data['status'] = "success";
                $data['icon'] = "success";
            }
            $addtion =  array(
                'Type' => 'Delivery Was Assigned',
                'user' => $userdata->name,
            );
            $msg = array(
                'body'  => 'Delivery Assigned',
                'title' => 'Now You Have One Delivery',
            );
            PushNotification::Push($msg, $fcmToken, $addtion);
        } else {
            $data['message'] = "Delivery Agent Not Change";
            $data['status'] = "error";
            $data['icon'] = "error";
        }

        return response()->json($data);
    }

}
