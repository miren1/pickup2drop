<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\DeliveryboyWalletHistory;
use App\Models\AutoTransactionHistory;
use DataTables;
use Alert;

class TransactionHistoryController extends Controller
{
    public function index(Request $request)
    {
        if (!auth()->user()->can('Transaction-History-List')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
       return view('transaction.index');
    }

    public function show(Request $request)
    {
        if (!auth()->user()->can('Transaction-History-List')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        if($request->ajax())
        {
            $data = User::where('type','Deliveryboy')->where('is_delete','false')->get();
            return \DataTables::of($data)
                    ->editColumn('created_at', function($row) {
                           $date = date('d-m-Y',strtotime($row->created_at));
                           return $date;
                    })
                    ->make(true);
        }
    }
    public function view(Request $request,$id)
    {
        if (!auth()->user()->can('Transaction-History-Show')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        $Id = base64_decode($id);
        $walletbalance = User::find($Id);
        $balance = $walletbalance->wallet;
        return view('transaction.view',compact('Id','balance'));
    }

    public function transactionData(Request $request)
    {
        if (!auth()->user()->can('Transaction-History-Show')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        if($request->ajax())
        {
            $data = DeliveryboyWalletHistory::with('Orders.customers')->where('user_id',$request->id)->orderBy('order_id', 'DESC')->get();
            return \DataTables::of($data)
                    ->addColumn('mobile_number',function($row){
                        return $row->orders && $row->orders->customer && $row->orders->customer->mobile_number ? $row->orders->customer->mobile_number : '-';
                    })
                    ->editColumn('created_at', function($row) {
                        // dd($row->created_at);
                        $date = date('d-m-Y',strtotime($row->created_at));
                        return $date;
                    })
                    ->make(true);
        }
    }

    public function bankTransaction(Request $request)
    {
        if (!auth()->user()->can('Transaction-History-Show')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        if($request->ajax())
        {
            $data = AutoTransactionHistory::with('customers')->where('user_id',$request->id)->get();
            return \DataTables::of($data)
                    ->editColumn('created_at', function($row) {
                        $date = date('d-m-Y',strtotime($row->created_at));
                        return $date;
                    })
                    ->make(true);
        }
    }
}
