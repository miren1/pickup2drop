<?php

namespace App\Http\Controllers;

use App\Models\Dispute;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Pickup;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use RealRashid\SweetAlert\Facades\Alert;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['customer'] = User::where('type', 'Customer')->where('is_delete', 'false')->count();
        $data['deliveryboy'] = User::with('defaultHomeAddresses', 'driverdoc','city')->whereHas('driverdoc',function($q) {
            if(false){
                $q->where('driver_status','active');
            }else if(false){
                $q->whereIn('driver_status', ['pending', 'suspended',Null]);
            }
        })->whereHas('city',function ($q){
            if(false)
                $q->where('id',4);
        })->where('type', 'Deliveryboy')->where('is_delete', 'false')->count();
        $data['pending'] = Pickup::where('status', 'Pickup Requested')->where('dispute_status', 'Not Add')->whereNotIn('type', ['tiffin', 'subscription'])->count();
        $data['process'] = Pickup::whereIn('status', ['Out Of Pickup', 'Picked Up', 'Out Of Delivery', 'Request Accept'])->whereNotIn('type', ['tiffin', 'subscription'])->where('dispute_status', 'Not Add')->count();
        $data['complete'] = Pickup::where('status', 'Delivered')->whereNotIn('type', ['tiffin', 'subscription'])->where('dispute_status', 'Not Add')->count();
        $data['cancel'] = Pickup::where('status', 'Cancel Request')->whereNotIn('type', ['tiffin', 'subscription'])->where('dispute_status', 'Not Add')->count();
        $data['dispute'] = Dispute::count();
        $data['tiffin'] = Pickup::whereIn('type', ['tiffin', 'subscription'])->where('dispute_status', 'Not Add')->count();
        // $data['tiffin_subscription'] = Pickup::where('type','tiffin')->whereNotIn('status',['Delivered','Cancel Request','Pickup Requested'])->count();
        $data['tiffin_subscription'] = Pickup::whereIn('type', ['tiffin', 'subscription'])->where('is_status', 'active')->count();
        return view('home', $data);
    }


    public function FnGetSubscription()
    {
        $date = date('Y-m-d');
        //dd($date);
        $obj = Pickup::where('type','tiffin')->where('is_status','active')->where('end_date','<=',$date)->get();
        //dd($obj);
        if (count($obj) != 0) {

            foreach ($obj as $key => $value) {
               $endDate = $value->end_date;
               //dd($endDate);

                $objSend = Pickup::find($value->id);
                $objSend->is_status = "deactive";
                $objSend->save();
            }

        }


    }

    public function profile(){
        return view('profilepage');
    }
    public function store(Request $request)
    {
        try{
            $this->validate($request,
            [
                'profile_photo_path' => 'image|mimes:jpeg,png,jpg|max:2048',
                'email' => 'required|regex:/(.+)@(.+)\.(.+)/i',
                'name' => 'required',
            ]);
            $ArrData = array(
                'name' => $request->name,
                'email' => $request->email,
            );

            $file = $request->file('profile_photo_path');
            if ($file) {
                $photo = User::find(Auth::user()->id);
                if ($photo->profile_photo_path) {
                    unlink(public_path($photo->profile_photo_path));
                }
                // $ArrData['profile_photo_path'] = $request->file('profile_photo_path')->move('avatars',strtotime('now') . '_' . Auth::user()->id . '.' . $file->extension());
                // $ArrData['profile_photo_path'] = $ArrData['profile_photo_path']->getPathName();
                $imageName = strtotime('now') . '_' . Auth::user()->id . '.' . $request->file('profile_photo_path')->getClientOriginalName();

                $img = Image::make(request()->file('profile_photo_path'));

                $img->resize(500, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });


                $img->save('avatars' . '/' . $imageName);


                $imgPath = 'avatars' . '/' . $imageName;
                $ArrData['profile_photo_path'] = $imgPath;
            }

            $where = array('id' => Auth::user()->id);
            $is_update = User::where($where)->update($ArrData);
            if ($is_update) {
                Alert::success('success', "Profile Updated Successfully");
                // return redirect()->back()->with(['success' => 'Profile Updated Successfully.']);
                return redirect('/admin/profile');
            } else {
                Alert::error('error', "Something went wrong. Please try after sometimes");
                return redirect()->back()->withErrors(['Something went wrong. Please try after sometimes']);
                return redirect('/admin/profile');
            }
        }catch(\Exception $e){
            Alert::error('error', $e->getMessage());
            return redirect('/admin/profile');
        }

    }

    public function updatePassword(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'old_password' => ['required'],
                'password' => 'required|confirmed',
            ]);

            if ($validator->fails())
            {
                return back()->withInput()->withErrors($validator);
            }else{
                if (Hash::check($request->old_password, auth()->user()->password)) {
                    auth()->user()->password = Hash::make($request->password);
                    auth()->user()->save();
                    Alert::success('success', "Password Updated Successfully");
                    return redirect()->back()->with(['success' => 'Password updated successfully.']);
                }
                Alert::error('error' , "Old password doesn't match");
                return redirect()->back()->withErrors(['error' => "Old password doesn't match"]);
            }
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }
}
