<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use DataTables;
use Alert;

class CustomerController extends Controller
{
    public function index()
    {
        if(!auth()->user()->can('Customer-list')){
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
            
        }
        return view('customer/index');        
    }

    public function show()
    {
        if (!auth()->user()->can('Customer-list')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        
        $data = User::where('type','Customer')->where('is_delete','false')->orderBy('created_at','DESC')->get();
        return Datatables::of($data)->make(true);
        
    }

    public function privacyPolicy(Request $request)
    {
        return view('newpages.privacy-policy');
    }
    public function driverPrivacyPolicy(Request $request)
    {
        return view('newpages.driver-privacy-policy');
    }

    public function termsCondition(Request $request)
    {
        return view('newpages.terms-condition');
    }

}
