<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Commission;
use Alert;

class CommissionController extends Controller
{
    public function index()
    {
        if (!auth()->user()->can('Update-Order-Commission')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }

        return view('commission/index');
    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('Update-Order-Commission')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        $data = Commission::first();
        if($data){
           $data->commission = $request->commission;
           $its_save = $data->save();
           if($its_save)
           {
            Alert::success('Success !', 'Delivery Agent Commission Updated Successfully.');
            return redirect()->back();
           }
        }else{
            // dd('dd');
            $create = new Commission;
            $create->commission = $request->commission;
            $its_save = $create->save();
            if($its_save)
            {
                Alert::success('Success !', 'Delivery Agent Commission Created Successfully.');
                return redirect()->back();
            }
        }
    }


}
