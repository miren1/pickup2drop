<?php

namespace App\Http\Controllers;

use App\Helpers\PushNotification;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Area;
use App\Models\NotificationHistory;
use Notification;
use Alert;

class NotificationCotroller extends Controller
{
    public function index()
    {
        if (!auth()->user()->can('Send-Notification')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        $users = User::where('type','Customer')->get();
        $areas = Area::where('delete_status',false)->get();
    	return view('notification.index',compact('users','areas'));
    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('Send-Notification')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        if($request->status == "now"){
            if($request->type == "delivery_agent"){
                $users = User::where('type', 'Deliveryboy')->where('fcm_token','!=','test')->whereNotNull('fcm_token')->where('is_delete','false')->pluck('fcm_token');
            }
            else if ($request->type == "customer") {
                $users = User::where('type', 'Customer')->where('fcm_token','!=','test')->whereNotNull('fcm_token')->where('is_delete', 'false')->pluck('fcm_token');
            } else{
                $users = User::whereIn('type',['Customer','Deliveryboy'])->where('fcm_token','!=','test')->whereNotNull('fcm_token')->where('is_delete', 'false')->pluck('fcm_token');
            }
			
			$FcmToken = $users->unique()->chunk(20)->toArray();

             foreach($FcmToken as $usertoken){
                $addtion=  array
                (
                    'Type'=>'All notify',
                    'user'=>'send all',
                );
                $msg = array
                (
                    'body'  => ucfirst($request->description),
                    'title' => ucfirst($request->title),
                );
                PushNotification::Push($msg,$usertoken,$addtion);
             }
		
            Alert::success('Success !', 'Notification Sent successfully.');
        }else{
            $request['time'] = date('Y-m-d h:i A',strtotime($request->date.' '.$request->time));
            $request['active'] = "true";
            $notification = NotificationHistory::create($request->all());
            if($notification){
                Alert::success('Success !', 'Notification Schedule successfully.');
            }else{
                Alert::error('Success !', 'Something wrong try again..');
            }
        }
    	return redirect(route('notification1.index'));
    }
}
