<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Areacode;
use App\Models\User;
use App\Models\DeliveryArea;
Use Alert;
use App\Imports\AreaImport;
use App\Models\City;
use Excel;
use PHPUnit\Exception;

class AreacodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!auth()->user()->can('Area-List')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }

        $cities = City::all();
        return view('areacode/index',compact('cities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!auth()->user()->can('Area-Add')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }

        $data['deliveryboy'] = User::where('type','Deliveryboy')->get();
        return view('areacode/add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {
        try {
            if (!auth()->user()->can('Area-Add')) {
                Alert::error('Error !', 'Unauthorized access.');
                return redirect()->route('home');
            }

            $this->validate(
                $request,
                [
                    'pincode' => 'required|min:6|max:6',
                ]
            );

            if($request->id){
                $check = Areacode::where('pincode', $request->pincode)->where('id','!=', $request->id)->exists();
            }else{
                $check = Areacode::where('pincode', $request->pincode)->exists();
            }
            
            if (!$check) {
                $arrData = array(
                    'name' => $request->name,
                    'pincode' => $request->pincode,
//             'deliveryboy_id'=>$request->deliveryboy_id,
                );
                if ($request->id) {
                    $where = array('id' => $request->id);
                    $isupdate = Areacode::where($where)->update($arrData);
                    Alert::success('Success !', 'Area code updated successfully !');
                    return redirect('areacode');
                } else {
                    $isInsert = Areacode::create($arrData);
                    if ($isInsert) {
                        //     $areaId = $isInsert->id;
                        //     $DeliveryArea = array(
                        //                    'deliveryboy_id'=>$request->deliveryboy_id,
                        //         'area_id' => $areaId,
                        //     );
                        //     $areacode = DeliveryArea::create($DeliveryArea);
                        Alert::success('Success !', 'Areacode Created Successfully.');
                        return redirect('areacode');
                    }
              
                }
            }
            Alert::error('Error !', 'Areacode already exists...');
            return redirect('areacode');
        }catch (\Exception $e){
            Alert::error('Error !', $e->getMessage());
            return redirect('areacode');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Getall()
    {
        if (!auth()->user()->can('Area-List')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }

      return datatables()->of(Areacode::orderBy('created_at','desc')->get())->toJson();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!auth()->user()->can('Area-Edit')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }

       $data['areacode'] = Areacode::find(base64_decode($id));
       $data['deliveryboy'] = User::where('type','Deliveryboy')->where('is_delete','false')->get();

        return view('areacode/add',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!auth()->user()->can('Area-Delete')) {
            return response()->json('unauthorized_access');
        }

         $deleted = Areacode::destroy($id);
         $DeliveryArea = DeliveryArea::where('area_id',$id)->delete();
        if ($deleted) {
            $status = true;
        }else{
            $status = false;
        }
        return response()->json($status);
    }
    public function importAreaCode(Request $request){
        try{
            if (!auth()->user()->can('Area-List')) {
                Alert::error('Error !', 'Unauthorized access.');
                return redirect()->route('home');
            }
            $city = $request->city;
            Excel::import(new AreaImport($city), request()->file('file'));
            Alert::success('Success !', 'Areacode Imported Successfully.');
            return redirect('areacode');
        }catch(\Exception $th){
            Alert::error('Error !', 'File type not supported...');
            return redirect('areacode');
        }

    }
}
