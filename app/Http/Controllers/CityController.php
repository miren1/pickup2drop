<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!auth()->user()->can('City-List')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }

        return view('city.index');
    }
    public function Getall()
    {
        if (!auth()->user()->can('City-List')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        return datatables()->of(City::all())->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('city.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!auth()->user()->canany(['City-Add','City-Edit'])) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        $check = City::Where('city_name', $request->city_name)->where('state',$request->state)->exists();
        if ($request->id) {
            $check = City::Where('city_name', $request->city_name)->where('state',$request->state)->where('id','!=', $request->id)->exists();
            if ($check) {
                Alert::error('Error !', 'City already exist.');
                return redirect('city');
            }
        }
            $arrData = array(
                'city_name' => $request->city_name,
                'state' => $request->state,
            );
            if ($request->id) {
                $where = array('id' => $request->id);
                $isupdate = City::where($where)->update($arrData);
                Alert::success('Success !', 'City Updated Successfully.');
                return redirect('city');
            } else {
                if($check){
                    Alert::error('Error !', 'City already exist.');
                    return redirect('city');
                }
                $isInsert = City::create($arrData);
                Alert::success('Success !', 'City Created Successfully.');
                return redirect('city');
            }
        
        return redirect('city');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show(City $city)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        if (!auth()->user()->can('City-Edit')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
      
        return view('city.create', compact('city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!auth()->user()->can('City-Delete')) {
            return response()->json("unauthorized_access");
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }

        $city = City::find($id);
        if($city->delete()){
            $status = true;
        }else{
            $status = false;
        }
        return response()->json($status);

    }
}
