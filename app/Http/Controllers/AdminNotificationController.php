<?php

namespace App\Http\Controllers;

use App\Models\Dispute;
use App\Models\Pickup;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Alert;

class AdminNotificationController extends Controller
{
    public function index()
    {
        if (!auth()->user()->can('Notification-List')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        return view('admin-notification.index');
    }
    public function Getall()
    {
        if (!auth()->user()->can('Notification-List')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        return datatables()->of(auth()->user()->notifications)->toJson();
    }
    public function show($id)
    {
        if (!auth()->user()->can('Notification-Show')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        $notification = auth()->user()->notifications->find(base64_decode($id));

        if(!isset($notification->data['dispute_id'])){
            $notification->markAsRead();
            return redirect('deliveryboy');
        }

        $data['notification_data'] = $notification;
        $data['dispute_data'] = Dispute::with('user')->find($notification->data['dispute_id']);
        $data['pickup_data'] = Pickup::with(['boy', 'Pickupaddress', 'Dropaddress'])->find($notification->data['order_id']);

        $notification->markAsRead();

        return view('admin-notification.show',compact('data'));
    }
    public function markAllRead()
    {
        if (!auth()->user()->can('Notification-Show')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        $notification = auth()->user()->notifications;

        if($notification){
            foreach($notification as $n){
                $n->markAsRead();
            }
        }
        return redirect()->back();
    }
}
