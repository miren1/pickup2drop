<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Wallet;
use App\Models\WalletHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class WalletController extends Controller
{
    public function index()
    {
        if (!auth()->user()->can('Wallet-List')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        return view('wallet.index');
    }

    public function Create()
    {
        if (!auth()->user()->can('Wallet-Add')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        $users = User::where(['type'=>'Customer','is_delete'=>'false'])->get();
        $default = Wallet::where(['type'=>'default','is_delete'=>0])->first();
        $referral = Wallet::where(['type'=>'referral','is_delete'=>0])->first();
        $by_referral = Wallet::where(['type'=>'by_referral','is_delete'=>0])->first();
        return view('wallet.add',compact('default','users','referral','by_referral'));
    }

    public function Store(Request $request)
    {
        // dd($request->all());
        if (!auth()->user()->can('Wallet-Add')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        if($request->id)
        {
            return $this->Update($request->except('_token'));
        }
        // dd($request->all());
        if($request->type=="add_to_all")
        {
            $request['user_id'] = 0;
            $add = Wallet::create($request->all());
            $wallet = User::where('type','Customer')->increment('wallet',$request->amount);
            if($wallet)
            {
                Alert::success('Success !', "Balance added to all user's account.");
            }
            else
            {
                Alert::error('Error !', 'Something Went Wrong Try Again.');
            }
        }
        else if($request->type=="add_to_one")
        {
            $add = Wallet::create($request->all());
            $wallet = User::where('id',$request->user_id)->increment('wallet',$request->amount);
            if($wallet)
            {
                $Wallet_history_params= [
                    'title' => 'Add Balance to One User',
                    'amount' => $request->amount,
                    'status' => 'credit',
                    'payment_id' => 'One User',
                    'user_id' => $request->user_id
                ];
                $Wallet_history = WalletHistory::forceCreate($Wallet_history_params);
                Alert::success('Success !', "Balance added to selected user's account.");
               // return redirect('wallet');
                return redirect('wallet');
            }
            else
            {
                Alert::error('Error !', 'Something Went Wrong Try Again..');
            }
        }
        else if($request->type=="add_to_all")
        {
            $wallet = Wallet::create($request->all());
            if($wallet)
            {
                Alert::success('Success !', 'Default Plan Added For new Users.');
                return redirect('wallet');
            }
            else
            {
                Alert::error('Error !', 'Something Went Wrong Try Again..');
                return redirect('wallet');
            }
        }
        else
        {
            $wallet = Wallet::create($request->all());
            if($wallet)
            {
                Alert::success('Success !', 'Plan Added successfully.');
                return redirect('wallet');
            }
            else
            {
                Alert::error('Error !', 'Something Went Wrong Try Again..');
            }
        }
        return redirect('wallet');
    }

    public function show(Request $request)
    {
        if (!auth()->user()->can('Wallet-List')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        if ($request->ajax()) {
            $data = Wallet::with('user')->where(['is_delete'=>0])->orderBy('created_at','desc')
            ->get();
            // return $data;
            return \DataTables::of($data)
                    ->addColumn('mobile_number',function($row){
                        return $row->user && $row->user->mobile_number ? $row->user->mobile_number : '  ';
                    })
                    ->editColumn('created_at', function($row) {
                           $date = date('d-m-Y',strtotime($row->created_at));
                           return $date;
                    })
                    ->make(true);
        }
    }

    public function Edit($id)
    {
        if (!auth()->user()->can('Wallet-Edit')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        // // dd(base64_decode($id));
        // $users = User::where('type','Customer')->get();
        $wallet = Wallet::where(['type'=>'default','is_delete'=>0])->first();
        return view('wallet.add',compact('wallet'));
    }

    public function Update($request)
    {
        // dd($request['id']);
        if (!auth()->user()->can('Wallet-Edit')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        $wallet = Wallet::where('id',$request['id'])->update(['amount'=>$request['amount']]);
        if($wallet)
        {
            Alert::success('Success !', 'Default Plan Updated Successfully.');
        }
        else
        {
            Alert::error('Error !', 'Something Went Wrong Try Again..');
        }
        return redirect(route('wallet.index'));
    }

    public function Destroy(Request $request)
    {
        // dd(base64_decode($request->id));
        if (!auth()->user()->can('Wallet-Delete')) {
            return response()->json('unauthorized_access');
        }

        $wallet = Wallet::find(base64_decode($request->id));
        if($wallet->type == 'add_to_one'){
            $user = User::where('id', $wallet->user_id)->update(['wallet'=> DB::raw('wallet -'.$wallet->amount)]);
        }else if($wallet->type == 'add_to_all'){
            $user = User::where('type', 'Customer')->update(['wallet' => DB::raw('wallet -'. $wallet->amount)]);
        }
        $wallet = $wallet->update(['is_delete'=>1]);
        if($wallet)
        {
            $data = true;
        }
        else
        {
            $data = false;
        }
        return response()->json($data);
    }


}
