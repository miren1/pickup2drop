<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\Facades\DataTables;
use Alert;

class RolePermissionController extends Controller
{
    public function index(Request $request)
    {
        if (auth()->user()->type != 'Admin') {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        if ($request->ajax()) {
            $role = Role::where('name','!=','admin')->get();
            return DataTables::of($role)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn = '<div class="menu-item px-3">
                            <a type="button" data-id="' . $row->id . '"  onclick="deleteRole(' . $row->id . ')"  id="getDeleteId">
                                <i class="fas text-danger fa-solid fa-trash" tooltip="Delete" style="font-size: 1.5rem;"></i>
                            </a>
                        </div>';
                return $btn;
            })->rawColumns(['action'])
            ->make(true);
        }
        return view('RolesPermission.index');
    }
    public function assign(Request $request)
    {
        if (auth()->user()->type != 'Admin') {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        $roles = Role::where('name', '!=', 'admin')->get();
        $permissions = Permission::get()->groupBy('module_name');
        return view('RolesPermission.assign', compact(['roles', 'permissions']));
    }

    public function storerole(Request $request)
    {   
        try {
            if (auth()->user()->type != 'Admin') {
                Alert::error('Error !', 'Unauthorized access.');
                return redirect()->route('home');
            }
            if(strtolower($request->role_name) == 'admin'){
                Alert::error('Error !', 'This is Predefined role, Please use another role name');
                return redirect()->back();
            }
            
            $checkRole = Role::where('name',$request->role_name)->first();
            if($checkRole != null){
                Alert::error('Error !', 'Role Already Exists');
                return redirect()->back();
            }
            $request->validate([
                'role_name' => 'required|unique:roles,name'
            ]);


            Role::create(['name' => $request->role_name]);

            Alert::success('Success !', 'Role Created Successfully.');
            return redirect()->back();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors('error', $e->getMessage());
        }
    }

    public function getPermission($id)
    {
        try {
            if (auth()->user()->type != 'Admin') {
                Alert::error('Error !', 'Unauthorized access.');
                return redirect()->route('home');
            }
            $role_permission = Role::with('permissions')->find($id);
            if ($role_permission)
                return response()->json(['data' => $role_permission], 200);

            return response()->json(['code' => 302, 'message' => 'Role not found', 302]);
        } catch (\Exception $e) {
            return response()->json(['code' => 302, 'message' => $e->getMessage()], 302);
        }
    }

    public function storePermission(Request $request)
    {
        try {
            if (auth()->user()->type != 'Admin') {
                Alert::error('Error !', 'Unauthorized access.');
                return redirect()->route('home');
            }
            $request->validate([
                'role_id' => 'required',
            ]);

            $role = Role::findById($request->role_id);
            if($request->permissions){
                $permissions = Permission::whereIn('id', $request->permissions)->get();
                $role->permissions()->sync($permissions);
            }else{
                $role->permissions()->sync([]);
            }

            Artisan::call('permission:cache-reset');
            return response()->json(['message' => 'Permission updated successfully.'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 302);
        }
    }
    public function checkAssign($id)
    {
        if (auth()->user()->type != 'Admin') {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        $model = DB::table('model_has_roles')->where('role_id', $id)->get();
        if (count($model) > 0) {
            return response()->json(['assign' => true], 200);
        } else {
            return response()->json(['assign' => false], 200);
        }
    }
    public function destoryRole($id)
    {
        if (auth()->user()->type != 'Admin') {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }

        $model = DB::table('model_has_roles')->where('role_id', $id)->get();
        if (count($model) > 0) {
            $data['assign'] = true;
            $data['status'] = false;
            return response()->json($data);
        } else {
            $role = Role::findById($id);
            if ($role->delete()) {
                $data['assign'] = false;
                $data['status'] = true;
                return response()->json($data);
            }else{
                $data['assign'] = false;
                $data['status'] = false;
                return response()->json(false);
            }
        }
        
        
    }
}
