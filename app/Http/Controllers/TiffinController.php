<?php

namespace App\Http\Controllers;

use App\Helpers\PushNotification;
use Auth;
use Notification;
Use Alert;
use App\Models\User;
use App\Models\Pickup;
use Illuminate\Http\Request;
use App\Models\Delivery_fcm_token;

class TiffinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!auth()->user()->can('Subscription-List')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        return view('tiffin/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['deliveryboy'] = User::where('type','deliveryboy')->get();
        return view('tiffin/add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $ArrStore = array(
            'item_name'=>$request->item_name,
            'description'=>$request->description,
            'amount'=>$request->amount,
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
        );

        if ($request->id) {
            //dd($request->all());
           $where = array('id'=>$request->id);
           $ArrStore['is_status'] = $request->is_status;
           $ArrStore['deliveryboy_id'] = $request->deliveryboy_id;

           $isupdate = Pickup::where($where)->update($ArrStore);
           $deliveryfcm = Delivery_fcm_token::where('deliveryboy_id',$request->deliveryboy_id)->get();
           foreach ($deliveryfcm as $delivery) {
            // $dd[] = $delivery['fcm_token'];
            $usertoken = $delivery['fcm_token'];
            $addtion=  array
                    (
                        'Type'=>'One Subscription Assigned',
                        'user'=>$delivery['name'],
                    );
            $msg = array
                    (
                        'body'  => 'Subscription Assigned',
                        'title' => 'New Subscription Assigned',
                    );
            PushNotification::Push($msg,$usertoken,$addtion);
        }
           Alert::success('Success !', 'Subscription Update Successfully.');
        }else{

            $isInsert = Pickup::create($ArrStore);

            Alert::success('Success !', 'Subscription Created Successfully.');

           return redirect('subscription');

        }
         return redirect('subscription');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Get_all($filter = 'All')
    {
        if (!auth()->user()->can('Subscription-List')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        $pickup = Pickup::with(['customers', 'boy', 'Pickupaddress', 'Dropaddress'])->whereIn('type', ['tiffin', 'subscription']);
        if($filter == 'active'){
            $pickup = $pickup->where('is_status','active');
        }
        return datatables()->of($pickup->orderby('id','DESC'))->toJson();
    }


    public function view($id)
    {
        if (!auth()->user()->can('Subscription-Show')) {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }

        $data['pickup'] = Pickup::with('Pickupaddress','Dropaddress','boy','image','customer')->where('id',base64_decode($id))->first();
       //dd($data);
        return view('tiffin/view',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['tiffin'] = Pickup::find(base64_decode($id));
        $data['deliveryboy'] = User::where('type','deliveryboy')->where('is_delete','false')->get();
        return view('tiffin/add',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = Pickup::destroy($id);
        if($deleted) {
            $status = true;
        }else{
            $status = false;
        }
        return response()->json($status);
    }
}
