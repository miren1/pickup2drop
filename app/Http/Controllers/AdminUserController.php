<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Contracts\Role;
use Spatie\Permission\Models\Role as ModelsRole;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;
use Alert;

class AdminUserController extends Controller
{
    public function index(Request $request)
    {
        if (auth()->user()->type != 'Admin') {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        if ($request->ajax()) {
            $enduser_data = User::where('type', 'User')->orderBy('created_at', 'desc')->get();
            return DataTables::of($enduser_data)
                ->addIndexColumn()
                ->addColumn('role', function ($row) {
                    return $row->getRoleNames() && isset($row->getRoleNames()[0]) ? $row->getRoleNames()[0] : ' - ';
                })
                ->make(true);
        }
        return view('adminuser.index');
    }
    public function create()
    {
        if (auth()->user()->type != 'Admin') {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        $roles = ModelsRole::where('name','!=','admin')->get();
        return view('adminuser.create', compact('roles'));
    }
    public function store(Request $request)
    {
        if (auth()->user()->type != 'Admin') {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
//        $existEmail = User::where('email', $request->email)->where('type', 'User')->where('is_delete','false')->get();
        $existEmail = User::where('email', $request->email)->where('is_delete','false')->get();
        if (count($existEmail) > 0) {
            Alert::error('Error !', 'The Email has already been taken.');
            return Redirect::back()->withInput();

            return Redirect::back()
                ->withErrors("The Email has already been taken")
                ->withInput();
        }


        $validator = Validator::make(
            $request->all(),
            [
                'name' => ['required'],
                'password' => [
                    'required',
                    'regex:/[a-z]/',      // must contain at least one lowercase
                    'regex:/[A-Z]/',      // must contain at least one uppercase
                    'regex:/[0-9]/',      // must contain at least one digit
                    'regex:/[@$!%*#?&]/', // must contain a special character
                ],
            ],
            ['password.regex' => "Your password must contain atleast one uppercase, one lowercase, one number and one special character with minimum length of 8 character"]
        );
        if ($validator->fails()) {
            Alert::error('Error !', 'Your password must contain atleast one uppercase, one lowercase, one number and one special character with minimum length of 8 character.');
            return Redirect::back()->withInput();;
        }

        $user = User::create(
            [
                'name' => $request->name,
                'email' => $request->email,
                'mobile_number' => $request->mobile_number,
                'email_verified_at' => date('Y-m-d H:i:s'),
                'password' => Hash::make($request->password),
                'type' => 'User'
            ]
        );
        if ($request->role) {
            $role = ModelsRole::where('id', $request->role)->get();
            $user->assignRole($role);
        }
        // if(count($request->role) > 0){
        //     $role = ModelsRole::whereIn('id', $request->role)->get();
        //     $user->assignRole($role);
        // }
        Alert::success('Success !', 'User added successfully.');
        return redirect()->route('user.index');
    }
    public function show($id)
    {
        if (auth()->user()->type != 'Admin') {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        $user = User::find(base64_decode($id));
        return view('adminuser.show', compact('user'));
    }
    public function edit($id)
    {
        if (auth()->user()->type != 'Admin') {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        $user = User::with('Roles')->find(base64_decode($id));
        $roles = ModelsRole::where('name', '!=', 'admin')->get();
        return view('adminuser.edit', compact('user', 'roles'));
    }
    public function update(Request $request, $id)
    {
        if (auth()->user()->type != 'Admin') {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        $existEmail = User::where('email', $request->email)->where('type', 'User')->where('is_delete','false')->where('id', '!=', $id)->get();
        if (count($existEmail) > 0) {

            Alert::error('Error !', 'The Email has already been taken.');
            return Redirect::back()->withInput();

            return Redirect::back()
                ->withErrors("The Email has already been taken")
                ->withInput();
        }
        $userData = [
            'name' => $request->name,
            'email' => $request->email,
            'mobile_number' => $request->mobile_number,
            'email_verified_at' => date('Y-m-d H:i:s'),
            // 'password' => Hash::make($request->password),
            'userable_type' => 'User'
        ];
        $user = User::find($id);
        if ($request->role) {
            $user->roles()->detach();
            $role = ModelsRole::where('id', $request->role)->get();
            $user->assignRole($role);
        }

        $user->update($userData);
        if ($user) {
            Alert::success('Success !', 'User Updated successfully.');
            return redirect()->route('user.index');
        } else {
            Alert::error('Error !', 'Something went wrong.');
            return redirect()->route('user.index');
        }
    }
    public function destory($id)
    {
        if (auth()->user()->type != 'Admin') {
            Alert::error('Error !', 'Unauthorized access.');
            return redirect()->route('home');
        }
        $user = User::find($id);
        // $user = User::find(base64_decode($id));
        $user->roles()->detach();
        $its_delete = $user->delete();
        if ($its_delete) {
            $status = true;
        } else {
            $status = false;
        }
        return response()->json($status);
    }
}
