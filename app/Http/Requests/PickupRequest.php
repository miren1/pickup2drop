<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PickupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'user_id' =>'required|int',
           'item_name' =>'required|string',
           'item_weight' =>'required|numeric',
           //'pickaddress' =>'required_with:address.*.office_shop_no',
           //'dropaddress' =>'required|string'
        ];
    }
}
