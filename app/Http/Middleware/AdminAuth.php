<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if( Auth::check() )
        {
            // allow admin to proceed with request
            if ( Auth::user()->isAdmin() || Auth::user()->type == "User") {
                 return $next($request);
            }else{
                Auth::logout();
                return redirect('/');
            }
        }

        abort(404);  // for other user throw 404 error
    }
}
