<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class PushNotification
{
    public static function Push($msg, $usertoken, $data)
    {
        $msg['sound'] = "bell_2";
        $msg['android_channel_id'] = "myapp_notification";

        $fields = [
            'notification'  => $msg,
            'data'          => $data,
            "priority"      => "high",
        ];

        if(is_array($usertoken)){
            //$fields['registration_ids'] = $usertoken;
			$fields['registration_ids'] = Arr::flatten($usertoken);
        }else{
            $fields['to'] = $usertoken;
        }
        $headers = [
            'Authorization: key=' . env('API_ACCESS_KEY'),
            'Content-Type: application/json'
        ];
//Log::info($fields);
        #Send Reponse To FireBase Server
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS,  json_encode($fields));
        $result = curl_exec($ch );
        curl_close( $ch );
		Log::info(print_r(json_encode($result),1));
    }
}
