<?php
namespace App\Helpers;
use Auth;
// use SendGrid\Mail\Mail;
 class DeliveryboyMail
 {
       public static function sendEmailTo($from, $body, $subject, $send_to){
        $key = env('MAIL_API_KEY');

        // dd($key);
        // $key='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';
        //$from = "mailto:fulaji@xsquaretec.com";
        /*$subject = "Simple";*/
        $email = new \SendGrid\Mail\Mail(); 
        $email->setFrom($from);
        $email->setSubject($subject);
        $email->addTo($send_to);
        $email->addContent("text/html", $body);
        $sendgrid = new \SendGrid($key);
        try {
            $response = $sendgrid->send($email);
            return array('status' =>  $response->statusCode());
        } catch (Exception $e) {
            return array('status' => 'error' , 'error' => $e->getMessage());
        }
    }


    public static function curl_post($url,$data)
    {
        $curl = curl_init();
        $login = env('RAZORPAY_X_KEY_ID');
        $password = env('RAZORPAY_X_SECRET_KEY');
        $headers = array(
                        'Content-Type:application/json',
                        'Authorization: Basic '. base64_encode("$login:$password"), // <---
                    );
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>$data,
          CURLOPT_HTTPHEADER => $headers,
        ));
        $response = curl_exec($curl);
        $resData = json_decode($response);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        if($statusCode == 200){
           return array('response' =>$resData);
        }else{
		   return array('response' =>$resData);
        }
    }

 }



