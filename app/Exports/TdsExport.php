<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class TdsExport implements FromCollection, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $transaction,$count;


    public function __construct($transaction)
    {
        $this->transaction = $transaction;
        $this->count = 0;
    }

    public function map($transaction): array
    {
        $this->count++;
        return [
            $this->count,
            $transaction->customers && $transaction->customers->name ? $transaction->customers->name : '',
            //'BTW2524K', // $transaction->customers ? $transaction->customers->driverdoc->pan_number : '', pancard number is needs to added in driver document for that
            $transaction->customers && $transaction->customers->driverdoc && $transaction->customers->driverdoc->pan_number ? $transaction->customers->driverdoc->pan_number : '',
            $transaction->customers && $transaction->customers->defaultHomeAddresses && $transaction->customers->defaultHomeAddresses->address ? $transaction->customers->defaultHomeAddresses->address : '',
            $transaction->tds ? $transaction->tds : 0,
            $transaction->amount ? $transaction->amount : 0,
            $transaction->transaction_id ? $transaction->transaction_id : 0,
            $transaction->created_at ?  date('d-m-Y', strtotime($transaction->created_at)) : '',            
        ];
    }

    public function headings(): array
    {
        return [
            '#',
            'Driver name',
            'PAN Number',
            'Address',
            'TDS',
            'Payout',
            'Transaction ID',
            'Date'
        ];
    }

    public function collection()
    {
        return $this->transaction;
    }
}
