<?php

namespace App\Console\Commands;

use App\Exports\TdsExport;
use Illuminate\Console\Command;
use App\Models\AutoTransactionHistory;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Excel as BaseExcel;
use Maatwebsite\Excel\Facades\Excel;

class GenerateTDSStatement extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Generate:TdsStatement';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate TDS Statement on monthly basis';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info('Generate TDS Statement on '.now());

        $start = Carbon::now()->startOfMonth();
        $end = Carbon::now()->endOfMonth();

        $auto = AutoTransactionHistory::with('customers.driverdoc', 'customers.defaultHomeAddresses')->whereBetween('created_at', [$start, $end])->get();

        $attachment = Excel::raw(new TdsExport($auto), BaseExcel::XLSX);

        Mail::raw('TDS statement', function ($msg) use ($attachment) {
            $msg->to(['sumit@xsquaretec.com','nehalkumar@xsquaretec.com'])->subject('Test TDS Deduction statement from command');
            $msg->attachData($attachment, now()->format('d_m_y') . '_TDS.xlsx');
        });
    }
}
