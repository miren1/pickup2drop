<?php

namespace App\Console\Commands;

use App\Helpers\PushNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Models\NotificationHistory;
use App\Models\User;
use Notification;

class NotificationCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::Info('notification cron');
        $time = date('Y-m-d h:i A',strtotime('now'));
        // dd($time);
        $notification = NotificationHistory::where('time',$time)->where('active','true')->first();

        if($notification)
        {
            if($notification->type == "delivery_agent"){
                $users = User::where('type', 'Deliveryboy')->whereNotNull('fcm_token')->where('is_delete','false')->get();
            }
            else if ($notification->type == "customer") {
                $users = User::where('type', 'Customer')->whereNotNull('fcm_token')->where('is_delete', 'false')->get();
            } else{
                $users = User::whereIn('type',['Customer','Deliveryboy'])->whereNotNull('fcm_token')->where('is_delete', 'false')->get();
            }
            // dd($users);
            foreach($users as $key=>$delivery)
            {
                $usertoken = $delivery['fcm_token'];
                $addtion=  array
                (
                    'Type'=>'All notify',
                    'user'=>'send all',
                );
                $msg = array
                (
                    'body'  => ucfirst($notification->description),
                    'title' => ucfirst($notification->title),
                );
                PushNotification::Push($msg,$usertoken,$addtion);
            }

            NotificationHistory::where('id',$notification->id)->update(['active'=>'false']);
        }
    }
}
