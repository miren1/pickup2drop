<?php

namespace App\Console\Commands;

use App\Helpers\PushNotification;
use App\Models\Pickup;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SubscriptionNotificationtoDriver extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:subscription_notification_driver';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info("cron running send subscription notification");
        $time = Carbon::now()->addHour()->format('h:i a');

        $subscriptionOrder = Pickup::with('boy')->where('type','subscription')->where(function ($q){
            $q->whereDate('start_date','<=',date("y-m-d"));
            $q->orwhereDate('start_date','<=',date('d-M-y'));
        })->where(function($q){
            $q->whereDate('end_date','>=',date("y-m-d"));
            $q->orwhereDate('end_date','>=',date('d-M-y'));
        })->whereNotNull('deliveryboy_id')
            ->where('start_time',$time)
            ->get();

        foreach($subscriptionOrder as $singleOrder){
            if($singleOrder->boy->delivery_agent_status == 0)
                continue;

            $addtion=  [
                'Type'=>'Pickup Subscription',
                'user'=>$singleOrder->boy->name,
            ];
            $msg = [
                'body'  => 'You have to pickup subscription in one hour',
                'title' => 'Pickup Subscription',
            ];
            PushNotification::Push($msg,$singleOrder->boy->fcm_token,$addtion);
        }
        return 0;
    }
}
