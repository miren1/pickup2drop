<?php

namespace App\Console\Commands;

use App\Helpers\DeliveryboyMail;
use App\Models\AutoTransactionHistory;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class WeeklyTransaction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'weekly:transaction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Weekly payout to the delivery agent.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info("start weekly transaction".now());
        $data = User::where('type', 'Deliveryboy')
        ->where('is_delete', 'false')
        ->where('bank_details_status', 'Verified')->get();

        $transaction = [];
        foreach ($data as $value) {
            if ($value->wallet > 0) {
                $tds = number_format((float)($value->wallet * 0.05), 2);
                $amount = number_format((float)($value->wallet - $tds), 2);
                $url =  'https://api.razorpay.com/v1/payouts';
                $data = '{
                          "account_number": "' . env('RAZORPAY_X_ACCOUNT_NUMBER') . '",
                          "fund_account_id": "' . $value->fund_account_id . '",
                          "amount": ' . ($amount * 100) . ',
                          "currency": "INR",
                          "mode": "IMPS",
                          "purpose": "salary",
                          "reference_id": "' . time() . '",
                            "narration": "Smart Merchant Service Ltd",
                            "notes": {
                                "notes_key_1":"Tea, Earl Grey, Hot",
                                "notes_key_2":"Tea, Earl Grey… decaf."
                            }
                        }';

                $curlData_2 = DeliveryboyMail::curl_post($url, $data);
                if ($curlData_2 && $curlData_2['response'] && isset($curlData_2['response']->id)) {
                    $arrayData = ['user_id' => $value->id, 'transaction_id' => $curlData_2['response']->id, 'fund_account_id' => $curlData_2['response']->fund_account_id, 'amount' => ($curlData_2['response']->amount / 100), 'status' => 'Pending', 'tds' => $tds];
                    $transaction[] = AutoTransactionHistory::create($arrayData);
                    $dataUpdate = User::where('fund_account_id', $value->fund_account_id)->update(['wallet' => 0]);
                }else{
                    Log::channel('weeklytransaction')->info('Delivery Agent ID : ' . $value->id . 'Delivery Agent Name : ' . $value->name .'response'. print_r($curlData_2,1));
                }
            }
        }

        Log::info("End weekly transaction".now());
    }
}
