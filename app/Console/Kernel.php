<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\NotificationCron::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('notification:cron')->everyMinute();
        $schedule->command('cron:subscription_notification_driver')->everyMinute();
        // $schedule->command('Generate:TdsStatement')->dailyAt('17:00');
        // $schedule->command('Generate:TdsStatement')->lastDayOfMonth('23:00');
        //$schedule->command('weekly:transaction')->weeklyOn(1, '00:01');
		$schedule->command('weekly:transaction')->dailyAt('00:01');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
